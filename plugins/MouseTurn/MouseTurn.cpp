#include "MouseTurn.h"

#include <Core.h>
#include <OpenGLManager.h>
#include <Renderable.h>
#include <LogManager.h>

MouseTurn* MouseTurn::g_Instance = nullptr;

void MouseTurnSetup() {
  vrwm_logdeb("MouseTurn: Setup called");
  if (!MouseTurn::g_Instance) {
    MouseTurn::g_Instance = new MouseTurn();
  }
}
void MouseTurnTeardown() {
  if (MouseTurn::g_Instance) {
    delete MouseTurn::g_Instance;
  }
}

MouseTurn::MouseTurn()
    : core_(vrwm::Core::Instance())
    , modkey_down_(false)
    , modkey_double_press_time_(0.0f)
    , orientation_last_frame_(glm_enh::quat::Identity)
    , forward_movement_per_wheel_spin_(0.5f) {
  vrwm_log(vrwm::Info, "MouseTurn plugin constructor");
}
MouseTurn::~MouseTurn() {
  vrwm_log(vrwm::Info, "MouseTurn plugin destructor");
}

void MouseTurn::MouseMoved(float x, float y) {
  glm::vec2 r(x, y);
  /*std::cout << x << " " << y << std::endl;*/
  r *= -0.1f;

  if (core_.oculus_manager().mode() == vrwm::OculusManager::UsageMode::Full) {
    MouseMovedFullOculus(r);
  } else {
    MouseMovedNoOculus(r);
  }

}

void MouseTurn::MouseMovedFullOculus(const glm::vec2& r) {
  if (!modkey_down_) {
    glm::vec3 head_up = core_.openGL_manager().GetUp();
    core_.cursor().Rotate(r.x, head_up);
    core_.cursor().Rotate(r.y, core_.cursor().GetRight());
  }
  //if (vrwm::Renderable::HasGrabbedRenderable()) {
    //vrwm::Renderable::PeekUngrab();
  //}
}

void MouseTurn::MouseMovedNoOculus(const glm::vec2& r) {
  if (modkey_down_) {
    vrwm::OpenGLManager& renderer = core_.openGL_manager();
    renderer.Rotate(r.x, glm_enh::vec3::Up);
    renderer.Rotate(r.y, renderer.GetRight());
  } else {
    glm::vec3 head_up = core_.openGL_manager().GetUp();
    core_.cursor().Rotate(r.x, head_up);
    core_.cursor().Rotate(r.y, core_.cursor().GetRight());
  }

  //if (vrwm::Renderable::HasGrabbedRenderable()) {
    //vrwm::Renderable::PeekUngrab();
  //}
}

void MouseTurn::PreRender() {
  if (!modkey_down_) {
    modkey_double_press_time_ += core_.time_since_last_frame();
  }

  vrwm::OpenGLManager& gl = core_.openGL_manager();
  glm::quat diff = glm_enh::FromToRotation(
      orientation_last_frame_, gl.orientation());

  /* Rotate the cursor with the head if modkey up. */
  if (modkey_down_) {
    core_.cursor().set_direction(diff * core_.cursor().direction());
  }

  orientation_last_frame_ = gl.orientation();
}

bool MouseTurn::ModKeyPressed(KeyCode code,
                              const vrwm::ActiveModifiers& modifiers,
                              KeySym keysym) {
  modkey_down_ = true;
  return true;
}
bool MouseTurn::ModKeyReleased(KeyCode code,
                               const vrwm::ActiveModifiers& modifiers,
                               KeySym keysym) {
  modkey_down_ = false;
  /* TODO: make this configurable */
  if (modkey_double_press_time_ < 0.25f) {
    core_.cursor().set_direction(core_.openGL_manager().orientation());
  }
  modkey_double_press_time_ = 0.0f;
  return true;
}
