#ifndef MOUSE_TURN_H
#define MOUSE_TURN_H

#include <glm/vec2.hpp>
#include <glm/gtc/quaternion.hpp>

#include <plugin/EventReceiver.h>

extern void MouseTurnSetup();
extern void MouseTurnTeardown();

VRWM_PLUGIN_INFO("MouseTurn",
    "This plugin is used to make the mouse control the head orientation if no "
      "Oculus Rift is present. It also makes the virtual cursor move around.",
    0, 1,
    MouseTurnSetup, MouseTurnTeardown);

class MouseTurn
  : public vrwm::plugin::MouseCursorMovedReceiver
  , public vrwm::plugin::RenderEventReceiver
  , public vrwm::plugin::KeyReceiver {
 public:
  static MouseTurn* g_Instance;

  MouseTurn();
  ~MouseTurn();
  void MouseMoved(float x, float y) override;
  void MouseMovedFullOculus(const glm::vec2& r);
  void MouseMovedNoOculus(const glm::vec2& r);

  bool ModKeyPressed(KeyCode code,
                     const vrwm::ActiveModifiers& modifiers,
                     KeySym keysym) override;
  bool ModKeyReleased(KeyCode code,
                      const vrwm::ActiveModifiers& modifiers,
                      KeySym keysym) override;
  bool KeyPressed(KeyCode code,
                  const vrwm::ActiveModifiers& modifiers,
                  KeySym keysym) override {
    return true;
  }
  bool KeyReleased(KeyCode code,
                   const vrwm::ActiveModifiers& modifiers,
                   KeySym keysym) override {
    return true;
  }

  void PreRender();
  void PreRenderStage(vrwm::plugin::RenderEventReceiver::RenderStage) { }
  void PostRenderStage(vrwm::plugin::RenderEventReceiver::RenderStage) { }
  void PostRender() { }

 private:
  vrwm::Core& core_;
  bool modkey_down_;
  float modkey_double_press_time_;
  glm::quat orientation_last_frame_;

  // TODO: read from config
  float forward_movement_per_wheel_spin_;
};

#endif
