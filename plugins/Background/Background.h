#ifndef MOUSE_TURN_H
#define MOUSE_TURN_H

#include <plugin/EventReceiver.h>
#include <SceneNode.h>

extern void BackgroundSceneSetup();
extern void BackgroundSceneTeardown();

VRWM_PLUGIN_INFO("BackgroundScene",
    "This plugin loads a background scene from a file when VRWM is started. "
      "The background scene file can be specified in the configuration file "
      "via the background variable. Example:\n"
      "vrwm = {\n"
      "  ...\n"
      "}\n"
      "background = \"foo/bar.obj\";\n"
      "\n"
      "If your background scene requires it, you can also set a scale to make "
      "the background larger. In this case, the syntax differs slightly:\n"
      "background = {\n"
      "  file = \"foo/bar.obj\";\n"
      "  scale = 10.0; // if not present, defaults to 1.0\n"
      "}\n"
      "\n"
      "The file specified will be searched in ~/.vrwm/assets and "
      "/usr/share/vrwm/assets (in that order). Supported file types are "
      "listed here: http://assimp.sourceforge.net/main_features_formats.html",
    0, 1,
    BackgroundSceneSetup, BackgroundSceneTeardown);

class BackgroundScene : vrwm::plugin::ProgrammStatusReceiver {
 public:
  static BackgroundScene* g_Instance;

  BackgroundScene();
  ~BackgroundScene();

  void PreInit() final override { }
  void PreWindowSearch() final override { }
  void PostInit() final override;
  // The scene node will be destroyed automatically by VRWM.
  void Teardown() final override { }

 private:
  void LoadScene(const std::string& filename, float scale);

  vrwm::SceneNode* background_scene_root_;
};

#endif
