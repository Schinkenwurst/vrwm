#include "Background.h"

#include <glm/vec3.hpp>

#include <Core.h>
#include <OpenGLManager.h>
#include <LogManager.h>
#include <SceneNode.h>

BackgroundScene* BackgroundScene::g_Instance = nullptr;

void BackgroundSceneSetup() {
  vrwm_logdeb("MouseTurn: Setup called");
  if (!BackgroundScene::g_Instance) {
    BackgroundScene::g_Instance = new BackgroundScene();
  }
}
void BackgroundSceneTeardown() {
  if (BackgroundScene::g_Instance) {
    delete BackgroundScene::g_Instance;
  }
}

BackgroundScene::BackgroundScene() {
  background_scene_root_= nullptr;
  vrwm_log(vrwm::Info, "BackgroundScene plugin constructor");
}
BackgroundScene::~BackgroundScene() {
  vrwm_log(vrwm::Info, "BackgroundScene plugin destructor");
}

void BackgroundScene::PostInit() {
  assert(vrwm::Core::Instance().openGL_manager().root_scene_node());

  auto& config = vrwm::Core::Instance().config();

  std::string filename;
  if (config.LookupValue("background", filename)) {
    LoadScene(filename, 1.0f);
  } else if (config.LookupValue("background.file", filename)) {
    float scale = 1.0f;
    config.LookupValue("background.scale", scale);
    LoadScene(filename, scale);
  }
}

void BackgroundScene::LoadScene(const std::string& filename, float scale) {
  vrwm_clogdeb("Loading background scene '%s' with scale %f",
    filename.c_str(), scale);
  vrwm::SceneNode* root =
    vrwm::Core::Instance().openGL_manager().root_scene_node();
  background_scene_root_ =
    root->CopyAndAttach(vrwm::Assets::LoadAssetFile(filename));
  background_scene_root_->SetWorldScale(glm::vec3(scale));
}



