#include "CursorMesh.h"

#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>

#include <Core.h>
#include <OpenGLManager.h>
#include <LogManager.h>
#include <SceneNode.h>
#include <FileTools.h>
#include <GLprogram.h>
#include <GLshader.h>

CursorMesh* CursorMesh::g_Instance = nullptr;

void CursorMeshSetup() {
  if (!CursorMesh::g_Instance) {
    CursorMesh::g_Instance = new CursorMesh();
  }
}
void CursorMeshTeardown() {
  if (CursorMesh::g_Instance) {
    delete CursorMesh::g_Instance;
  }
}

// We give the renderer a higher execution order to make it execute later. It
// will execute after the MouseTurn-Plugin has updated the cursor position.
CursorMesh::CursorMesh() : RenderEventReceiver(100) {
  cursor_node_ = nullptr;
  rim_material_ = nullptr;
}
CursorMesh::~CursorMesh() {
}

void CursorMesh::PreRender() {
  assert(cursor_node_);
  auto cursor = vrwm::Core::Instance().cursor();
  if (cursor.has_cached_raycast_hit()) {
    auto h = cursor.cached_raycast_hit();
    cursor_node_->SetWorldPosition(h.GetPoint());
    glm::quat dir = h.renderable->scene_node().world_orientation();
    dir = glm_enh::rotate(dir, glm::radians(180.0f),
      h.renderable->scene_node().GetUp());
    cursor_node_->SetWorldOrientation(dir);
  } else {
    cursor_node_->SetWorldPosition(cursor.GetRay().GetPoint(distance_));
    glm::quat dir = cursor.direction();
    cursor_node_->SetWorldOrientation(dir);
  }
}

void CursorMesh::PostInit() {
  CreateRimMaterial();

  auto root = vrwm::Core::Instance().openGL_manager().root_scene_node();
  assert(root);
  cursor_node_ = root->CreateChild("cursor");
  assert(cursor_node_);

  auto& config = vrwm::Core::Instance().config();

  glm::vec4 rim_color;
  if (config.LookupColor("cursor.rim", rim_color)) {
    SetRimColor(rim_color);
  } else if (config.LookupColorRGB("cursor.rim", rim_color)) {
    rim_color.a = 1.0f;
    SetRimColor(rim_color);
  } else {
    vrwm_log(vrwm::Info, "No rim color for cursors.");
    rim_color.a = 0.0f;
    SetRimColor(rim_color);
  }

  distance_ = 5.0f;
  config.LookupValue("cursor.distance", distance_);

  float scale = 1.0;
  config.LookupValue("cursor.scale", scale);
  cursor_node_->SetWorldScale(glm::vec3(scale));

  std::string filename;
  if (config.LookupValue("cursor.normal", filename)) {
    config.LookupValue("cursor.normal_scale", scale);
    LoadNormalCursor(filename, scale);
  }
}

void CursorMesh::LoadNormalCursor(const std::string& filename, float scale) {
  vrwm_clogdeb("Loading cursor '%s' with scale %f",
    filename.c_str(), scale);
  normal_cursor_ =
    cursor_node_->CopyAndAttach(vrwm::Assets::LoadAssetFile(filename));
  normal_cursor_->SetWorldScale(glm::vec3(scale));
  SetRimMaterial(*normal_cursor_);
}

void CursorMesh::CreateRimMaterial() {
  auto found_vshader = vrwm::FileTools::Find("shader/default.vert.glsl");
  auto found_fshader = vrwm::FileTools::Find(
    "shader/rimlight_diffuse.frag.glsl");

  if (!found_vshader.first || !found_fshader.first) {
    vrwm_log(vrwm::Error, "Cannot find rim light shaders.");
    return;
  }

  vrwm::ShaderList shaders;
  std::set<std::string> defines;
  // We need to have normals in the shaders.
  defines.insert("NORMALS");
  shaders.push_back(vrwm::GLshader::FromFile(
      found_vshader.second, GL_VERTEX_SHADER, defines));
  shaders.push_back(vrwm::GLshader::FromFile(
      found_fshader.second, GL_FRAGMENT_SHADER, defines));
  vrwm::GLprogramPtr program(new vrwm::GLprogram(std::move(shaders)));
  auto& gl_manager = vrwm::Core::Instance().openGL_manager();
  program->Bind("MatrixBlock", gl_manager.matrix_block().binding());
  program->Bind("SceneBlock", gl_manager.scene_block().binding());

  bool transparent = false;
  rim_material_ = vrwm::draw::MaterialPtr(
    new vrwm::draw::Material(program, transparent));
  SetRimColor(glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
}

void CursorMesh::SetRimColor(const glm::vec4& color) {
  rim_material_->SetUniform<glm::vec4>("color_rim", color);
}

void CursorMesh::SetRimMaterial(vrwm::SceneNode& s) {
  for (auto it = s.begin_renderables(); it != s.end_renderables(); ++it) {
    SetRimMaterial(**it);
  }
  for (auto it = s.begin_children(); it != s.end_children(); ++it) {
    SetRimMaterial(**it);
  }
}

void CursorMesh::SetRimMaterial(vrwm::Renderable& r) {
  r.set_material(rim_material_);
}



