#ifndef MOUSE_TURN_H
#define MOUSE_TURN_H

#include <X11/Xlib.h>

#include <plugin/EventReceiver.h>
#include <SceneNode.h>
#include <Cursor.h>
#include <draw/Material.h>

extern void CursorMeshSetup();
extern void CursorMeshTeardown();

// For the sake of readability I will not make line breaks at 80 characters
// here. This enhances visibility of "real" new lines in the string via \n.
VRWM_PLUGIN_INFO("CursorMesh",
    "This plugin displays meshes for VRWM's cursor. It can be configured in the config file:\n"
      "vrwm = {\n"
      "  ...\n"
      "}\n"
      "cursor = { // Those are the default values\n"
      "  // Rim color. If not present, no rim. The elements are the RGBA\n"
      "  // values. Alpha is the strength of the rim and may be omitted in\n"
      "  // which case 1.0 is used. Alternatively, you can use integers in\n"
      "  // the range of [0, 256) or a single hex color code string\n"
      "  // (e.g. \"#FFFFFF\")\n"
      "  rim = [1.0, 1.0, 1.0, 1.0]; \n"
      "  scale = 1.0; // Makes all cursor larger / smaller\n"
      "  distance = 5.0; // Distance (in meters) if not on a window\n"
      "  normal = \"cursors/default/normal.obj\";\n"
      "  normal_scale = 1.0; // The normal cursor's scale is scale * normal_scale\n"
      "  // This list will be expanded in the future\n"
      "}\n"
      "\n"
      "The file specified will be searched in ~/.vrwm/assets and /usr/share/vrwm/assets (in that order). Supported file types are listed here: http://assimp.sourceforge.net/main_features_formats.html",
    0, 1,
    CursorMeshSetup, CursorMeshTeardown);

class CursorMesh : public vrwm::plugin::ProgrammStatusReceiver,
    public vrwm::plugin::RenderEventReceiver {
 public:
  static CursorMesh* g_Instance;

  CursorMesh();
  ~CursorMesh();

  void PreRender() final override;
  void PreRenderStage(vrwm::plugin::RenderEventReceiver::RenderStage)
    final override { }
  void PostRenderStage(vrwm::plugin::RenderEventReceiver::RenderStage)
    final override { }
  void PostRender() final override { }

  void PreInit() final override { }
  void PreWindowSearch() final override { }
  void PostInit() final override;
  // The scene node will be destroyed automatically by VRWM.
  void Teardown() final override { }

 private:
  void LoadNormalCursor(const std::string& filename, float scale);

  void CreateRimMaterial();
  void SetRimColor(const glm::vec4& color);

  // \brief Recursive
  void SetRimMaterial(vrwm::SceneNode& s);
  void SetRimMaterial(vrwm::Renderable& r);

  float distance_;
  vrwm::SceneNode* cursor_node_;
  vrwm::SceneNode* normal_cursor_;
  std::map<Atom, vrwm::SceneNode*> additional_cursors_;
  vrwm::draw::MaterialPtr rim_material_;
};

#endif
