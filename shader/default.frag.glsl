#version 330

uniform vec4 color;

out vec4 out_color;

void main(void) {
    out_color = color;
}

