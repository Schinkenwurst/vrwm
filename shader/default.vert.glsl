// When implementing your own shader, observe these locations.
layout(location = 0) in vec3 vertex_pos;
layout(location = 1) in vec2 texture_coord;
layout(location = 2) in vec3 vertex_normal;
layout(location = 3) in vec3 vertex_tangent;
layout(location = 4) in vec3 vertex_bitangent;
layout(location = 5) in vec4 vertex_color;

// All uniform blocks defined in this shader are automatically managed by VRWM.
// Therefore, when implementing your own shader, they must have the same name
// and data types.
layout(std140) uniform MatrixBlock {
  // member            previous | alignment | start | size
  mat4 projection;  //      0            16       0     64
  mat4 view;        //     64            16      64     64
  mat4 model;       //    128            16     128     64
  mat4 mvp;         //    192            16     192     64
  mat3 normal;      //    256            16     256     12  - column 0
                    //    268            16     272     12  - column 1
                    //    284            16     288     12  - column 2
} matrix;

layout(std140) uniform SceneBlock {
  // member                 offset | alignment | start | size
  vec3 camera_position;  //      0          16       0     12
  vec3 camera_direction; //     12          16      16     12
  float time;            //     28           4      28      4
  // TODO: lights
} scene;

out Fragment {
  vec3 world_pos;
#ifdef TEXTURES
  vec2 texture_coord;
#endif
#ifdef NORMALS
  vec3 normal;
#endif
#ifdef TANGENTS
  vec3 tangent;
  vec3 bitangent;
#endif
} out_fragment;

void main(void) {
    out_fragment.world_pos = vec3(matrix.model * vec4(vertex_pos, 1.0));
    gl_Position = matrix.mvp * vec4(vertex_pos, 1.0);
#ifdef TEXTURES
    out_fragment.texture_coord = texture_coord;
#endif
#ifdef NORMALS
    out_fragment.normal = normalize(matrix.normal * vertex_normal);
#endif
#ifdef TANGENTS
    out_fragment.tangent = normalize(matrix.normal * vertex_tangent);
#endif
#ifdef TANGENTS
    out_fragment.bitangent = normalize(matrix.normal * vertex_bitangent);
#endif
}
