uniform vec4 color_diffuse = vec4(1.0, 1.0, 1.0, 1.0);
uniform vec3 color_specular = vec3(0.0, 0.0, 0.0);
uniform vec3 color_ambient = vec3(0.05, 0.05, 0.05);
uniform float specular_shininess = 0;
uniform sampler2D texture_diffuse;
uniform sampler2D texture_specular;
uniform sampler2D texture_normal;
uniform sampler2D texture_shiny;

layout(std140) uniform SceneBlock {
  vec3 camera_position;
  vec3 camera_direction;
  float time;
} scene;

in Fragment {
  vec3 world_pos;
  vec2 texture_coord;
  vec3 normal;
  vec3 tangent;
  vec3 bitangent;
} in_fragment;

out vec4 out_color;

// constant lights
vec3 light_pos = vec3(10.0f, 2.0f, 10.0f);


// These variables are written by the shader
vec3 light_dir;
vec3 cam_dir;
vec3 bumped_normal; // With the normal map applied

vec4 calcDiffuse() {
  vec4 diffuse = color_diffuse;
#ifdef TEX_DIFFUSE
  diffuse *= texture2D(texture_diffuse, in_fragment.texture_coord);
#endif

  float LdotN = max(dot(light_dir, bumped_normal), 0.0);
  return vec4(diffuse.rgb * LdotN, diffuse.a);
}

vec4 calcSpecular() {
  vec3 halfway = normalize(cam_dir + light_dir);
  float cos_theta = max(dot(bumped_normal, halfway), 0.0);

  // TODO: replace 1.0 with light strength
  float shiny = specular_shininess;
#ifdef TEX_SHINY
  shiny *= texture2D(texture_shiny, in_fragment.texture_coord);
#endif
  vec4 color = 1.0 * pow(cos_theta, shiny) * vec4(color_specular, 0.0);
#ifdef TEX_SPECULAR
  color *= texture2D(texture_specular, in_fragment.texture_coord);
#endif
  return color;
}

void main(void) {
  float t = scene.time * 0.5;
  light_pos = vec3(
    mat4(cos(t), 0, -sin(t), 0,
         0,      1,       0, 0,
         sin(t), 0,  cos(t), 0,
         0,      0,       0, 1) *
    vec4(10 + sin(t*0.2) * 10.0,
          5 + sin(t * 0.7) * 5,
         10 + sin(t*0.4) * 10.0, 1.0));

  light_dir = normalize(light_pos - in_fragment.world_pos);
  cam_dir = normalize(scene.camera_position - in_fragment.world_pos);

#ifdef BUMP_MAP
  vec3 normalmap = normalize(
    2.0 * texture2D(texture_normal, in_fragment.texture_coord).rgb - vec3(1.0));
  bumped_normal = normalize(in_fragment.tangent * normalmap.r +
    in_fragment.bitangent * normalmap.g + in_fragment.normal * normalmap.b);
#else
  bumped_normal = in_fragment.normal;
#endif

  out_color = vec4(color_ambient, 0.0) + calcDiffuse();
  //out_color = vec4(color_ambient, 0.0);
#ifdef SPECULAR
  out_color += calcSpecular();
#endif
}
