uniform vec4 color_diffuse = vec4(1.0, 1.0, 1.0, 1.0);
uniform vec3 color_ambient = vec3(0.05, 0.05, 0.05);
uniform sampler2D texture_diffuse;

uniform vec4 color_rim = vec4(1.0, 1.0, 1.0, 1.0);

layout(std140) uniform SceneBlock {
  vec3 camera_position;
  vec3 camera_direction;
  float time;
} scene;

in Fragment {
  vec3 world_pos;
  vec2 texture_coord;
  vec3 normal;
} in_fragment;

out vec4 out_color;

// constant lights
vec3 light_pos = vec3(10.0f, 2.0f, 10.0f);


// These variables are written by the shader
vec3 light_dir;
vec3 cam_dir;
vec3 normal;

vec4 calcDiffuseWithRim() {
  vec4 diffuse = color_diffuse;
#ifdef TEX_DIFFUSE
  diffuse *= texture2D(texture_diffuse, in_fragment.texture_coord);
#endif

  float LdotN = max(dot(light_dir, normal), 0.0);
  // rim factor
  float f = 1.0 - dot(cam_dir, normal);
  f = smoothstep(0.0, 1.0, f);
  float rim = pow(f, 4.0);
  return mix(
    vec4(diffuse.rgb * LdotN, diffuse.a),
    vec4(color_rim.rgb, diffuse.a),
    rim * color_rim.a);
}

void main(void) {
  float t = scene.time * 0.5;
  light_pos = vec3(
    mat4(cos(t), 0, -sin(t), 0,
         0,      1,       0, 0,
         sin(t), 0,  cos(t), 0,
         0,      0,       0, 1) *
    vec4(10 + sin(t*0.2) * 10.0,
          5 + sin(t * 0.7) * 5,
         10 + sin(t*0.4) * 10.0, 1.0));
  normal = normalize(in_fragment.normal);

  light_dir = normalize(light_pos - in_fragment.world_pos);
  cam_dir = normalize(scene.camera_position - in_fragment.world_pos);

  out_color = vec4(color_ambient, 0.0) + calcDiffuseWithRim();
  //out_color = vec4(1.0, 0.0, 0.0, 1.0);
}
