uniform sampler2D texture;
uniform vec4 color = vec4(1.0, 1.0, 1.0, 1.0);

in Fragment {
  vec2 texture_coord;
} in_fragment;

out vec4 out_color;

void main(void) {
  out_color = color * texture2D(texture, in_fragment.texture_coord);
}

