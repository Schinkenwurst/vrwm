#ifndef LOGMANAGER_H
#define LOGMANAGER_H

#include <chrono>
#include <cstdarg>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <stdexcept>
#include <string>
#include <sstream>

#include <assimp/Logger.hpp>

namespace vrwm {

/// \brief The importance of the message being logged
enum LogLevel {
  Debug,
  Info,
  Warning,
  Error,
};

/// \brief Manages printing and logging of messages.
/// Logs to std::cout as well as the file specified as kLogFileName.
/// You can also attach an error callback function which is called if a message
/// with a LogLevel of SYSERR has been logged. In case of C++ style logging,
/// the error callback will be called when the SYSERR level has been changed or
/// std::endl is written.
///
/// Because C++ style prints are a major pain to implement, the LogManager can
/// only do C style logs.
///
/// Useful macros are included at the end of the file to automatically capture
/// and log the file and line from where the log was called.
/// These are vrwm_log (which needs you to supply a log level), vrwm_logdeb
/// (uses loglevel Debug), vrwm_clog (printf-like variant, allows e.g. %i) and
/// vrwm_clogdeb (printf-like with level debug).
///
/// In release versions (i.e. VRWM_DEBUG is not defined), makros will NOT log
/// the file and line (for better performance) and debug messages will not be
/// logged.
///
/// \code
/// vrwm_clog(vrwm::Info, "Hello %s", "World");
/// vrwm_clog(vrwm::Warning, "Foo should be equal to %i.", 42);
/// vrwm_logdeb("This will not be visible in release version.");
/// vrwm_log(vrwm::Error, "Critical error!"); // automatically calls the
///                                           // error callback function.
/// \endcode
/// Will produce something similar to this if VRWM_DEBUG is not defined.
///  13:37:00 |   Info  | Hello World
///  13:37:00 | Warning | Foo should be equal to 42.
///  13:37:00 |  Error  | Critical error!
///
/// ...and something like this if it is defined:
///  13:37:00 |   Info  | Hello World   (/dev/vrwm/example.cpp:1)
///  13:37:00 | Warning | Foo should be equal to 42.   (/dev/vrwm/example.cpp:2)
///  13:37:00 |  Debug  | This will not be visible in release mode.   (/dev/vrwm/example.cpp:3)
///  13:37:00 |  Error  | Critical error!   (/dev/vrwm/example.cpp:4)
///
/// This class also serves as a logger for messages logged by the assimp
/// library.
class LogManager : public Assimp::Logger {
 public:
  friend LogManager& operator<<(LogManager&, LogLevel);
  template <class T> friend LogManager& operator<<(LogManager&, const T&);

  virtual ~LogManager();

  /// Gets the singleton and creates it if it did not exist
  static LogManager& Instance();
  /// Destroys the singleton. Call this at the end of your program to
  /// ensure that no memory leaks occur!
  static void DestroySingleton();

  /// \brief Sets the error callback function which is called when an error
  /// with level Error is logged.
  ///
  /// Setting the error callback function is optional and will not modify
  /// the regular behavior of logging.
  /// The error callback function is mainly used in GUI programs.
  /// It enables you to display a pop-up should something go severely wrong.
  ///
  /// Since the error callback function does not depend on the creation of
  /// the singleton, you can call this function at any time.
  ///
  /// \param f The function. e.g. a pointer to
  ///          void yourFunctionName(const std::string &str)
  static void SetErrorCallback(void (*f)(const std::string &));

  /// \brief Logs a message.
  ///
  /// Logs a message with a timestamp to a file and without a timestamp to
  /// cout.
  ///
  /// Additionally, if a error callback function has been set and the
  /// log level is Error, the callback function will be called.
  ///
  /// If the project has not been compiled as Debug target, a message
  /// with the level Debug will not be logged or printed.
  ///
  /// \param msg The message to be logged.
  /// \param level The importance of the message.
  void Log(const std::string &msg, LogLevel level);

  /// \brief printf-like variant of log
  ///
  /// \see std::printf
  /// \see log
  void Log(const char* format, LogLevel level, ...);
  /// \brief vprintf-like variant of log
  ///
  /// \see std::printf
  /// \see log
  void vLog(const char* format, LogLevel level, va_list args);

  ///////////////////////////////
  /// Assimp Logger functions ///
  ///////////////////////////////

  /// \brief Always returns false.
  /// For simplicity's sake, we do not allow stream attaching.
  bool attachStream(Assimp::LogStream*, unsigned int) final override {
    return false;
  }
  /// \brief Always returns false.
  /// For simplicity's sake, we do not allow stream attaching.
  bool detatchStream(Assimp::LogStream*, unsigned int) final override {
    return false;
  }

  /// \brief Declared by assimp's Logger class.
  void OnDebug(const char* message) final override {
    Log("Assimp: " + std::string(message), LogLevel::Debug);
  }
  /// \brief Declared by assimp's Logger class.
  void OnInfo(const char* message) final override {
    Log("Assimp: " + std::string(message), LogLevel::Info);
  }
  /// \brief Declared by assimp's Logger class.
  void OnWarn(const char* message) final override {
    Log("Assimp: " + std::string(message), LogLevel::Warning);
  }
  /// \brief Declared by assimp's Logger class.
  void OnError(const char* message) final override {
    Log("Assimp: " + std::string(message), LogLevel::Error);
  }

private:
  /// \brief Constructor
  /// Cannot be called!
  LogManager();
  /// \brief Copy-Constructor
  /// Cannot be called!
  LogManager(const LogManager &cc);
  /// \brief Converts an enum value to its correspondent string.
  std::string Stringify(LogLevel level);

  /// \brief Writes the current date and time into the log file.
  void InitialLog();

 private:
  /// \brief The file name where the LogManager logs to.
  /// Will be resolved into the ~/.vrwm/log
  const std::string kLogFileName = "log";

  /// \brief The singleton instance
  static LogManager *g_singleton_;

  /// \brief The output file. Messages are written there.
  std::ofstream file_;
  /// \brief The previous line if current level is Error.
  /// Used for calling the callback function.
  /// Empty if the level is not Error.
  std::ostringstream last_line_;

  /// If an error occurred while opening the output file this flag is false
  /// and log will not write to the output file.
  bool log_to_file_;

  /// \brief Error callback function.
  ///
  /// This function gets called if something with LogLevel Error gets
  /// logged.
  static void (*error_callback_)(const std::string &);
};

//////////////////////////////////////////////
// Implementation of quick access functions //
//////////////////////////////////////////////
// These are defined as macros to           //
// additionally log the file and line when  //
// compiled as debug version.               //
//////////////////////////////////////////////

/// \brief Calls singleton and then log a debug message.
/// \param msg The message to be logged (std::string).
#ifdef VRWM_DEBUG
  #define vrwm_logdeb(msg) \
    vrwm::LogManager::Instance().Log( \
      msg + std::string("  (") + __FILE__ + ":" + \
      std::to_string(__LINE__) + ")", \
      vrwm::Debug);
#else
  #define vrwm_logdeb(msg) //no debug messages in release
#endif

/// \brief Calls singleton and then log a debug message.
/// \param msg The message to be logged.
#ifdef VRWM_DEBUG
  #define vrwm_clogdeb(msg, args...) \
    vrwm::LogManager::Instance().Log(msg "  (%s:%i)", vrwm::Debug, \
      args, __FILE__, __LINE__);
#else
  #define vrwm_clogdeb(msg, args...) //no debug messages in release
#endif

/// \brief Calls singleton and then log.
/// \see LogManager::log
/// \param msg The message to be logged.
/// \param level The importance of the message.
#ifdef VRWM_DEBUG
  #define vrwm_log(level, msg) \
    vrwm::LogManager::Instance().Log( \
      msg + std::string("  (") + __FILE__ + ":" + \
      std::to_string(__LINE__) + ")", \
      level);
#else
  #define vrwm_log(level, msg) \
    vrwm::LogManager::Instance().Log(msg, level);
#endif

/// \brief Calls singleton and then the printf-like log.
/// \see LogManager::log
#ifdef VRWM_DEBUG
  #define vrwm_clog(level, msg, args...) \
    vrwm::LogManager::Instance().Log(msg "  (%s:%i)", level, \
      args, __FILE__, __LINE__);
#else
  #define vrwm_clog(level, msg, args...) \
    vrwm::LogManager::Instance().Log(msg, level, args);
#endif

} //vrwm

#endif // LOGMANAGER_H
