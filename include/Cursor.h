#ifndef POINTER_H
#define POINTER_H

#include <glm/gtc/quaternion.hpp>

#include "GLglobal.h"
#include "Ray.h"
#include "EventHandler.h"
#include "SceneNodeGrab.h"

namespace vrwm {

class Core;

// \brief A virtual mouse cursor.
//
// The cursor is just a quaternion that specifies a direction. The "origin"
// of the cursor is at the user's head (OpenGLManager's offset from ground).
// Due to the crappy typedefs of Xlib, this class is not called Cursor.
class VirtualCursor {
 public:
  VirtualCursor(Core& c)
    : core_(c)
    , origin_(glm_enh::vec3::Zero)
    , direction_(glm_enh::quat::Identity)
    , has_cached_raycast_hit_(false) {
  }

  Ray GetRay() const {
    return Ray(origin_, direction_ * glm_enh::vec3::Forward);
  }

  // \brief Called at the end of the frame to execute and cache a ray cast.
  // Also calls the hover_exit callback of the previous vertex group if it
  // changed.
  void CacheRaycast();
  // \brief True if the cached ray cast has hit anything.
  bool has_cached_raycast_hit() const { return has_cached_raycast_hit_; }
  // \brief The cached raycasts. Check has_cached_raycast_hit() beforehand!
  RaycastHit& cached_raycast_hit() { return cached_raycast_hit_; }
  // \brief The cached raycasts. Check has_cached_raycast_hit() beforehand!
  const RaycastHit& cached_raycast_hit() const { return cached_raycast_hit_; }
  Renderable* GetRenderableUnderPointer() {
    return has_cached_raycast_hit_ ? cached_raycast_hit_.renderable : nullptr;
  }

  void set_origin(const glm::vec3& o) { origin_ = o; }
  const glm::vec3& origin() const { return origin_; }

  // \brief Returns the current direction of the cursor.
  const glm::quat& direction() const { return direction_; }
  // \brief Sets the direction of the cursor.
  void set_direction(const glm::quat& d) { direction_ = d; }

  // \brief Rotates by angle_rad radians around axis.
  void Rotate(float angle_rad, const glm::vec3& axis) {
    direction_ = glm_enh::rotate(direction_, angle_rad, axis);
  }
  // \brief yaw (in world space) by radians.x, pitch (local space) by radians.y
  void Rotate(const glm::vec2& radians) {
   RotateYaw(radians.x);
   RotatePitch(radians.y);
  }
  // \brief Yaws the cursor (in world space) by the given angle.
  void RotateYaw(float radian) {
    direction_ = glm_enh::rotate(direction_, radian, glm::vec3(0.0f, 1.0f, 0.0f));
  }
  // \brief Pitches the cursor (in local space) by the given angle.
  void RotatePitch(float radian) {
    direction_ = glm_enh::rotate(direction_, radian, GetRight());
  }

  // \brief Returns the forward vector of the cursor.
  glm::vec3 GetForward() const { return direction_ * glm_enh::vec3::Forward; }
  // \brief Returns a vector that points to the right.
  glm::vec3 GetRight() const { return direction_ * glm_enh::vec3::Right; }
  // \brief Returns a vector that points upwards.
  glm::vec3 GetUp() const { return direction_ * glm_enh::vec3::Up; }

  // \brief Generates a ray based on an origin and the cursor direction.
  Ray GetRay(const glm::vec3& head_position) const {
   return Ray(head_position, GetForward());
  }

  // \brief Executes Client::InjectPointerMotion on the Client under the Cursor.
  // Automatically called each frame.
  void Hover();
  // \brief Executes a mouse button down.
  // This means that the OpenGLManager executes a ray cast with the cursor
  // and the vertex group that has been hit by that raycast gets its
  // click_callback called.
  void Click(MouseButton button, const ActiveModifiers& modifiers);
  // \brief Executes a mouse button up.
  // Basically the same as Click but with the release_callback.
  // Also automatically ungrabs a grabbed renderable if mouse1 has been
  // released.
  void Release(MouseButton button, const ActiveModifiers& modifiers);

  /// \brief Grabs a node to prepare it for resize or move.
  /// \param grab_point The start point of the grab relative to the scene node.
  void GrabSceneNode(SceneNode& n, const glm::vec3& grab,
      SceneNodeGrab::GrabType type) {
    scene_node_grab_ = SceneNodeGrab(n, grab, type);
  }
  void set_scene_node_grab(const SceneNodeGrab& sng) { scene_node_grab_ = sng; }
  SceneNodeGrab& scene_node_grab() { return scene_node_grab_; }

 private:
  // \brief The core instance.
  Core& core_;
  // \brief The origin point of the cursor.
  glm::vec3 origin_;
  // \brief The direction of the cursor.
  glm::quat direction_;

  // \brief The hit that has been executed at the beginning of the frame.
  // It is more performant to execute the cursor raycast once at the beginning
  // of the frame and re-use it rather than doing multiple ray casts each frame
  // with no result changes.
  RaycastHit cached_raycast_hit_;
  // \brief True if the cached raycast has hit anything. False if it didn't.
  bool has_cached_raycast_hit_;

  SceneNodeGrab scene_node_grab_;
};

}

#endif // POINTER_H
