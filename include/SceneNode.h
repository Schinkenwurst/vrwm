#ifndef VRWM_SCENE_NODE_H
#define VRWM_SCENE_NODE_H

#include <string>
#include <set>

#include <glm/vec3.hpp>
#include <glm/matrix.hpp>
#include <glm/gtx/quaternion.hpp>

#include "GLglobal.h"

namespace vrwm {

class Core;
class Renderable;
class OpenGLManager;
class SceneNode;

class SceneNodeObserver {
 public:
  friend class SceneNode;
  SceneNodeObserver();
  SceneNodeObserver(const SceneNodeObserver& copy);
  SceneNodeObserver(SceneNodeObserver&& move);
  SceneNodeObserver& operator=(const SceneNodeObserver& copy);
  SceneNodeObserver& operator=(SceneNodeObserver&& move);
  virtual ~SceneNodeObserver();
  void Observe(SceneNode* n);
  // Sets the SceneNode without telling him about it.
  void ForceObserve(SceneNode* n) { observing_ = n; }
  SceneNode* observing() { return observing_; }
  // Callbacks
  virtual void PositionChanged(const glm::vec3& new_world_pos,
                               const glm::vec3& old_world_pos);
  virtual void OrientationChanged(const glm::quat& new_world_orientation,
                                  const glm::quat& old_world_orientation);
  virtual void ScaleChanged(const glm::vec3& new_world_scale,
                            const glm::vec3& old_world_scale);
  virtual bool ScaleChangeRequest(const glm::vec3& future_world_scale,
                                  const glm::vec3& old_world_scale);
  virtual void SceneNodeDestroyed();
  virtual void ChildAdded(SceneNode& child);
  virtual void ChildRemoved(SceneNode& child);
 private:
  SceneNode* observing_;
};

// TODO: this class has horrible memory management
// SceneNodeObserver to track the billboard target
class SceneNode : SceneNodeObserver {
 public:
  friend class OpenGLManager;
  friend class SceneNodeObserver;

  /// \brief Prefer CreateChild(). Ideally only use this for parentless nodes.
  SceneNode(Core& core, const std::string& name, SceneNode* parent = nullptr);
  SceneNode(const SceneNode& copy);
  SceneNode(SceneNode&& move) = delete;
  SceneNode& operator=(const SceneNode& copy);
  SceneNode& operator=(SceneNode&& move) = delete;
  ~SceneNode();

  SceneNode* CreateChild(const std::string& name);
  /// \brief Copies the supplied node and adds it to this node.
  /// Does not do anything if the node is null.
  /// \returns the copy of to_clone.
  SceneNode* CopyAndAttach(const SceneNode* to_clone);
  void Destroy();

  /// \brief Returns the local position of the node.
  const glm::vec3& local_position() const { return local_position_; }
  /// \brief Sets the local position of the node.
  /// If the Node is a billboard, it will face the target afterwards.
  /// Setting the local position updates the model matrix as well as the model
  /// matrices of all child nodes.
  void SetLocalPosition(const glm::vec3& p);

  /// \brief Returns the world position of the node.
  glm::vec3 world_position() const { return glm::vec3(model_matrix_[3]); }
  /// \brief Sets the world position of the node.
  /// If the Node is a billboard, it will face the target afterwards.
  /// Setting the local position updates the model matrix as well as the model
  /// matrices of all child nodes.
  void SetWorldPosition(const glm::vec3& p);
  void SetWorldPosition(float x, float y, float z) {
    SetWorldPosition(glm::vec3(x, y, z));
  }

  /// \brief Returns the local orientation of the node.
  const glm::quat& local_orientation() const { return local_orientation_; }
  /// \brief Sets the local orientation of the node.
  /// If the Node is a billboard, it will still be rotated but its orientation
  /// will reset the next time it is moved. Setting the local orientation
  /// updates the model matrix as well as the model matrices of all child
  /// nodes.
  void SetLocalOrientation(const glm::quat& o);
  /// \brief Returns the world orientation of the node.
  const glm::quat& world_orientation() const { return world_orientation_; }
  /// \brief Sets the world orientation of the node.
  void SetWorldOrientation(const glm::quat& o);


  /// \brief Is the node a billboard? (Always faces a target)
  bool IsBillboard() const { return billboard_target_ != nullptr; }
  SceneNode* billboard_target() { return billboard_target_; }
  const SceneNode* billboard_target() const { return billboard_target_; }
  /// \brief Should the node be a billboard? (Always faces a target)
  /// Target may be nullptr to make this node not face anything automatically.
  void SetBillboardTarget(SceneNode* target);
  /// \brief True if the renderables attached to this node are drawn.
  bool is_visible() const { return is_visible_; }
  bool is_visible_in_hierarchy() const { return is_visible_in_hierarchy_; }
  /// \brief Should the renderables attached to this node be drawn?
  /// If a node is not visible, its children won't be visible either.
  void SetVisible(bool v);
  /// \brief Returns the first parent that has no invisible ancestors.
  /// Returns NULL if the root scene node is not visible.
  SceneNode* FirstVisibleParentInHierarchy() {
    return parent_ ?
      (parent_->is_visible_in_hierarchy_ ? parent_ :
        parent_->FirstVisibleParentInHierarchy()) :
      nullptr;
  }

  /// \brief Gets the plane of this node.
  /// The plane is exactly like the mathematical plane of infinite size.  The
  /// origin of the node will be on that plane and the normal of the plane is
  /// the node's orientation.
  ///
  /// \returns a pair of vec3 and float. vec3 is the normal and float is the
  /// offset from {0, 0, 0} along the normal to reach a point that is on the
  /// plane.
  std::pair<glm::vec3, float> GetPlane() const;

  /// \brief Rotates the node.
  /// Rotates x radians around x-axis, y rads around y axis and z rads around z
  /// (in that order).
  /// Even rotates if the node is a billboard, the orientation will reset
  /// when it is moved the next time.
  void Rotate(const glm::vec3& rads, bool local_space);
  /// \brief Rotates the Renderable.
  /// Rotates by the specified angle around the axis.
  /// Even rotates if the node is a billboard, the orientation will reset
  /// when it is moved the next time.
  void Rotate(float rads, const glm::vec3& axis);
  void Rotate(const glm::quat& q);

  /// \brief Gets the world space forward of the node
  glm::vec3 GetForward() const {
    return world_orientation_ * glm_enh::vec3::Forward;
  }
  /// \brief Gets the world space right of the node
  glm::vec3 GetRight() const {
    return world_orientation_ * glm_enh::vec3::Right;
  }
  /// \brief Gets the world space up of the node
  glm::vec3 GetUp() const {
    return world_orientation_ * glm_enh::vec3::Up;
  }

  /// \brief Returns the node's local scale.
  const glm::vec3& local_scale() const { return local_scale_; }
  /// \brief Sets the node's local scale.
  void SetLocalScale(const glm::vec3& s, bool force = false);
  /// \brief Returns the node's world scale.
  const glm::vec3& world_scale() const { return world_scale_; }
  /// \brief Sets the node's world scale.
  void SetWorldScale(const glm::vec3& s, bool force = false);
  void SetWorldScale(float x, float y, float z, bool force = false) {
    SetWorldScale(glm::vec3(x, y, z), force);
  }

  /// \brief Converts a world space point to local space.
  glm::vec3 ToLocalSpace(const glm::vec3& v) const {
    return glm::vec3(inverse_model_matrix_ * glm::vec4(v, 1.0f));
  }
  /// \brief Converts a local space point to world space.
  glm::vec3 ToWorldSpace(const glm::vec3& v) const {
    return glm::vec3(model_matrix_ * glm::vec4(v, 1.0f));
  }

  /// \brief Makes the node face the specified direction (world space).
  void LookAt(const glm::vec3& direction,
      const glm::vec3& desiredUp = glm_enh::vec3::Up);

  /// \brief The model matrix of this node.
  /// i.e. parent's model matrix * position * orientation * scale
  const glm::mat4& model_matrix() const { return model_matrix_; }
  /// \brief the inverse model matrix of this node.
  /// i.e. 1/scale * transposed orientation * -position * inverse parent's
  const glm::mat4& inverse_model_matrix() const {
    return inverse_model_matrix_;
  }

  bool inherit_position() const { return inherit_position_; }
  bool inherit_orientation() const { return inherit_orientation_; }
  bool inherit_scale() const { return inherit_scale_; }
  void SetInheritPosition(bool inherit);
  void SetInheritOrientation(bool inherit);
  void SetInheritScale(bool inherit);

  /// \brief Finds node recursive in all children by depth first search.
  SceneNode* FindByName(const std::string& name);
  const SceneNode* FindByName(const std::string& name) const;
  void FindAllByName(const std::string& name, std::set<SceneNode*>& out);
  void FindAllByName(const std::string& name,
    std::set<const SceneNode*>& out) const;

  std::set<SceneNode*>::iterator begin_children() { return children_.begin(); }
  std::set<SceneNode*>::const_iterator begin_children() const {
    return children_.begin();
  }
  std::set<SceneNode*>::iterator end_children() { return children_.end(); }
  std::set<SceneNode*>::const_iterator end_children() const {
    return children_.end();
  }

  std::set<Renderable*>::iterator begin_renderables() {
    return renderables_.begin();
  }
  std::set<Renderable*>::const_iterator begin_renderables() const {
    return renderables_.begin();
  }
  std::set<Renderable*>::iterator end_renderables() {
    return renderables_.end();
  }
  std::set<Renderable*>::const_iterator end_renderables() const {
    return renderables_.end();
  }

  SceneNode* parent() { return parent_; }
  const SceneNode* parent() const { return parent_; }
  void Reparent(SceneNode* new_parent);

  std::string& name() { return name_; }
  const std::string& name() const { return name_; }
  void set_name(const std::string& name) { name_ = name; }

  void AttachRenderable(Renderable& r);
  /// \brief Faster than detach followed by attach. Don't call directly.
  /// Don't call this function directly. Call Renderable::SetSceneNode()
  /// instead.
  void RelocateRenderable(Renderable& r, SceneNode& target);
  void DetachRenderable(Renderable& r);

  ///////////////////////////////////
  /// SceneNodeObserver functions ///
  ///////////////////////////////////

  void PositionChanged(const glm::vec3& new_world_pos,
                       const glm::vec3& old_world_pos);
  void OrientationChanged(const glm::quat& new_world_orientation,
                          const glm::quat& old_world_orientation);
  void ScaleChanged(const glm::vec3& new_world_scale,
                    const glm::vec3& old_world_scale);
  void SceneNodeDestroyed();

 private:
  void AddObserver(SceneNodeObserver& o);
  void RemoveObserver(SceneNodeObserver& o);

  /// \brief Updates the orientation so the node faces the target.
  void CalculateBillboardOrientation();
  /// \brief Checks if the billboard target is one of the children.
  /// \throws a std::runtime_error if it is.
  void RecursiveBillboardTargetCheck(SceneNode* target);

  /// \brief Updates matrices based on local and parent information
  /// Model and inverse model matrix of this node and its children (recursive)
  /// will get updated.
  void UpdateMatrices();

  void RecursiveRemoveAllFromDraw();
  void RemoveAllFromDraw();
  void RemoveFromDraw(Renderable& r);
  void RecursiveAddAllToDraw();
  void AddAllToDraw();
  void AddToDraw(Renderable& r);

  void RecursiveSetVisibleInHierarchy(bool v);

  void RecursiveGatherRenderables(std::set<Renderable*>& out);

  Core& core_;

  /// \brief Position without parent.
  glm::vec3 local_position_;
  /// \brief Orientation without parent.
  glm::quat local_orientation_;
  /// \brief Scale without parent.
  glm::vec3 local_scale_;

  // store world orientation and scale because they can't be calculated from
  // the model matrix.
  glm::quat world_orientation_;
  glm::vec3 world_scale_;

  bool inherit_position_;
  bool inherit_orientation_;
  bool inherit_scale_;

  glm::mat4 model_matrix_, inverse_model_matrix_;

  /// \brief The position that this node will face.
  /// May be set to nullptr to indicate that this node is not a billboard.
  SceneNode* billboard_target_;

  /// \brief
  bool is_visible_;
  bool is_visible_in_hierarchy_;

  std::string name_;

  std::set<SceneNode*> children_;
  SceneNode* parent_;

  // TODO: Bounding box for culling

  std::set<Renderable*> renderables_;

  std::set<SceneNodeObserver*> observers_;

};

}

#endif
