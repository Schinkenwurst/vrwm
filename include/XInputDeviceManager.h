#ifndef VRWM_XINPUTDEVICEMANAGER_H
#define VRWM_XINPUTDEVICEMANAGER_H

#include <X11/Xlib.h>
#include <X11/extensions/XInput2.h>
#include <glm/vec2.hpp>

#include "LogManager.h"

namespace vrwm {

class Core;

class XInputDeviceManager {
  friend class Core;

 public:
  XInputDeviceManager(Core& c);

  int get_mouse_device_id() const { return mouse_device_id_; }
  int get_x_axis_valuator_id() const { return x_axis_valuator_id_; }
  int get_y_axis_valuator_id() const { return y_axis_valuator_id_; }

  // \brief Wrapper around XIWarpPointer
  // This function omits all irrelevant parameters.
  // TODO: elaborate more on what it does.
  void WarpPointer(double dest_x, double dest_y);
  void WarpPointer(const glm::vec2& v)  { WarpPointer(v.x, v.y); }
  void WarpPointer(const glm::ivec2& v) { WarpPointer(v.x, v.y); }
  void WarpPointer(const glm::uvec2& v) { WarpPointer(v.x, v.y); }

 private:
  void Init();
  static void PrintDeviceInfo(const XIDeviceInfo* device, LogLevel level);

  Core& core_;
  // \brief The used mouse device of XInput for direct access to movement.
  int mouse_device_id_;
  int x_axis_valuator_id_, y_axis_valuator_id_;

};

}

#endif

