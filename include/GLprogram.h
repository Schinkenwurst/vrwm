#ifndef GLPROGRAMM_H
#define GLPROGRAMM_H

#include <map>
#include <memory>
#include <vector>

#include <GL/glew.h>

#include "GLshader.h"
#include "draw/VertexAttribute.h"
#include "UniformBlocks.h"

namespace vrwm {

typedef std::vector<GLshader> ShaderList;
class GLproram;
typedef std::shared_ptr<GLprogram> GLprogramPtr;

// \brief a compiled GLSL program.
class GLprogram {
 public:
  // \brief Compiles the program using the passed shaders.
  GLprogram(ShaderList&& shaders);
  ~GLprogram();

  uint16_t id() {
    // TODO: use custom ids to ensure 16 bit!
    return static_cast<uint16_t>(program_);
  }

  // \brief Recompiles all shaders and links them again.
  // When a shader was read from a file, that file will be read again.
  void Recompile();
  void PrintCompileLog();

  // \brief Wrapper around glUseProgram.
  void Use() {
    if (!printed_linklog_) {
      PrintCompileLog();
    }
    glUseProgram(program_);
  }

  // \brief Returns the id of an attribute.
  // Errors will be logged.
  GLint Attrib(const char* name) const;
  GLint Attrib(const std::string& name) const { return Attrib(name.c_str()); }
  // \brief Returns the id of a uniform.
  // Errors will be logged if suppressWarning is false.
  GLint Uniform(const char* name, bool suppressWarning = false) const;
  GLint Uniform(const std::string& name, bool suppressWarning = false) const {
    return Uniform(name.c_str(), suppressWarning);
  }

  GLuint UniformBlockIndex(const char *name);
  GLuint UniformBlockIndex(const std::string& name) {
    return UniformBlockIndex(name.c_str());
  }

  void Bind(GLuint block_index, GLuint binding_point);
  void Bind(GLuint block_index, UniformBlock& block) {
    return Bind(block_index, block.binding());
  }
  void Bind(const char* name, UniformBlock& block) {
    return Bind(UniformBlockIndex(name), block.binding());
  }
  void Bind(const std::string& name, UniformBlock& block) {
    return Bind(UniformBlockIndex(name), block.binding());
  }
  void Bind(const char* name, GLuint binding_point) {
    return Bind(UniformBlockIndex(name), binding_point);
  }
  void Bind(const std::string& name, GLuint binding_point) {
    return Bind(UniformBlockIndex(name), binding_point);
  }


  draw::ResolvedVertexAttribute Resolve(const draw::NamedVertexAttribute& a)
      const {
    return draw::ResolvedVertexAttribute(Attrib(a.name().c_str()), a);
  }

 private:
  void Link();

  // \brief OpenGL handle of the program.
  GLuint program_;

  ShaderList shaders_;
  bool printed_linklog_;
  std::map<GLuint, GLuint> active_block_bindings_;
};

}

#endif

