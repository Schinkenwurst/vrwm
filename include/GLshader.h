#ifndef GLSHADER_H
#define GLSHADER_H

#include <memory>
#include <string>
#include <map>
#include <set>

#include <GL/glew.h>

namespace vrwm {

class GLprogram;
class GLshader;

// \brief A loaded vertex, geometry, fragment, etc. shader
class GLshader {
  friend class GLprogram;

 public:
  // Allow use of move ctor.
  GLshader(GLshader &&move);
  ~GLshader();

  void PrintCompileLog();

  // \brief Loads a shader from file and compiles it.
  //
  // The GLSL version (#version 333) string will be prepended automatically.
  //
  // \param defines Each string of the set will be prepended as #define.
  //
  // \throws std::runtime_error if the file cannot be loaded or shader
  //    compilation fails.
  static GLshader FromFile(const std::string &filename, GLenum type,
    const std::set<std::string>& defines = std::set<std::string>());

  // \brief Loads a shader from file.
  //
  // The GLSL version string will NOT be prepended.
  //
  // \throws std::runtime_error if shader creation fails.
  static GLshader FromCode(const std::string &code, GLenum type);

  // \brief Recompiles this shader if it was read from a file.
  //
  // If it was not read from a file, you have to manually call the Compile()
  // function with the new code.
  // Should not be called manually, is called when GLprogram recompiles.
  void Recompile();

  // \brief Sets the shader source code and compiles it.
  // Don't forget that you have to relink the program.
  void Compile(const char* code);

 private:
  // \throws If shader creation fails.
  GLshader(const char* code, GLenum type);
  // \brief Prohibit use of copy-ctor (prevents double free)
  GLshader(const GLshader& copy) = delete;
  // \brief prohibit use of assignment (prevents double free)
  GLshader& operator=(GLshader& other) = delete;

  static std::string LoadFile(const std::string& filename,
    const std::set<std::string>& defines);

 private:
  static const char* kGLSLVersionStr;

  // \brief The OpenGL handle of the shader.
  GLuint handle_;
  // \brief The type of the shader.
  GLenum type_;

  std::string filename_;
  std::set<std::string> defines_;

};

}

#endif

