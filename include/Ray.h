#ifndef RAY_H
#define RAY_H

#include <glm/vec3.hpp>

#include "GLglobal.h"
#include "draw/VertexGroup.h"
#include "Renderable.h"

namespace vrwm {

// \brief A ray has an origin and a direction and is used in ray casts.
struct Ray {
  glm::vec3 origin, direction;

  Ray(const glm::vec3& origin, const glm::vec3& direction) :
    origin(origin), direction(direction) {
  }
  Ray(const Ray& cc) : origin(cc.origin), direction(cc.direction) {
  }
  Ray(float ox, float oy, float oz, float dx, float dy, float dz) :
    origin(ox, oy, oz), direction(dx, dy, dz) {
  }

  // \brief Returns a point along the direction of the ray.
  glm::vec3 GetPoint(float t) const {
    return origin + t * direction;
  }

};

class Renderable;
// \brief The result of a ray cast.
struct RaycastHit {
  RaycastHit() :
    ray(glm_enh::vec3::Zero, glm_enh::vec3::Forward),
    t(std::numeric_limits<float>::infinity()) {
  }
  RaycastHit(const RaycastHit& cc) :
    ray(cc.ray), t(cc.t), renderable(cc.renderable),
    triangle_ID(cc.triangle_ID),
    triangle(cc.triangle), v1_v2_factor(cc.v1_v2_factor),
    v1_v3_factor(cc.v1_v3_factor) {
  }
  // \brief The ray that was used in the raycast that produced this hit
  Ray ray;
  // \brief The parameter of the ray at which the hit occured.
  float t;
  // \brief The renderable that has been hit.
  Renderable* renderable;
  // \brief The ID of the triangle that has been hit.
  int triangle_ID;
  // \brief The model-space values of the hit triangle. (NOT world space)
  //
  // Please note that when the triangle was drawn as GL_TRIANGLE_STRIP v1 and
  // v2 are swapped if the triangle ID is not divideable by 2.
  // This is done by the RaycastableTriangleIterator to ensure consistent
  // normals.
  draw::VertexGroup::RaycastableTriangle triangle;
  // \brief Where exactly in triangle-space did the ray cast hit?
  // This means that GetPoint() in model-space is equal to
  // v1 + (v2 - v1) * v1_v2_factor + (v3 - v1) * v1_v3_factor
  // Where v1, v2 and v3 are the members of the stored
  // VertexGroup::RaycastableTriangle
  float v1_v2_factor, v1_v3_factor;

  // \brief Returns the point in world space where the ray hit the renderable.
  glm::vec3 GetPoint() const { return ray.GetPoint(t); }
  // \brief The same as GetPoint() * inverse model matrix;
  glm::vec3 GetModelSpacePoint() const {
    const glm::vec3& v1 = triangle.v1, v2 = triangle.v2, v3 = triangle.v3;
    return v1 + (v2 - v1) * v1_v2_factor + (v3 - v1) * v1_v3_factor;
  }
};


}

#endif // RAY_H
