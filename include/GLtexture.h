#ifndef GLTEXTURE_H
#define GLTEXTURE_H

#include <memory>
#include <string>
#include <map>
#include <memory>
#include <vector>

#include <GL/glew.h>

namespace vrwm {

class GLtexture;
typedef std::shared_ptr<GLtexture> GLtexturePtr;

/// \brief a 2D texture
/// Binds to GL_TEXTURE_2D
class GLtexture {
 public:
  friend class GLtextureSwapper;

  static GLtexturePtr Load(const std::string& filename);
  // \brief Loads a grayscale image and converts it to a normal map.
  // If the image was already loaded it will NOT be converted again.
  static GLtexturePtr LoadHeightmap(const std::string& filename);

  GLtexture();
  GLtexture(GLtexture&& move);
  ~GLtexture();

  /// \brief wrapper around glActiveTexture and glBindTexture.
  /// Binds to GL_TEXTURE_2D. See GLtextureSwapper for further details.
  /// \returns The texture unit this texture is bound to.
  GLint Use();

  /// \brief Wrapper around glTexParameter
  /// This texture will be bound afterwards.
  void SetParameter(GLenum pname, GLfloat param);
  void SetParameter(GLenum pname, GLint param);

  // \brief Wrapper around glTexImage2D()
  void SetData(GLint level, GLsizei w, GLsizei h, void* data,
    GLenum type = GL_FLOAT, GLenum internal_format = GL_RGBA,
    GLenum format = GL_RGBA);

  void GenerateMipmap();

  GLuint handle() { return handle_; }
  int texture_unit() const { return texture_unit_; }

 private:
  GLtexture(const GLtexture& cc); //prohibit copy-ctor

  void LoadImage(const std::string &filename);
  // \brief Loads a height map and converts it to a normal map.
  void LoadHeightmapImage(const std::string& filename);
  // \brief Converts a height map to an RGB32 normal map.
  // Assumes the heightmap parameter is a height map (i.e. R = G = B = A when
  // channel = 4) but this is neither checked nor enforced.
  // Output format RGBA32 so one byte (0, 255) for each red, green, blue value.
  std::vector<uint8_t> ConvertToNormalMap(const unsigned char* heightmap,
    int w, int h, int channels);

  void set_texture_unit(int u) { texture_unit_ = u; }

  GLuint handle_;
  int texture_unit_;
};

}
#endif
