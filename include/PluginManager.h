#ifndef PLUGIN_MANAGER_H
#define PLUGIN_MANAGER_H

#include <functional>
#include <map>
#include <string>
#include <typeinfo>
#include <vector>

#include <dlfcn.h>

#include "plugin/EventReceiver.h"
#include "FileTools.h"
#include "LogManager.h"

namespace vrwm {
namespace plugin {

// \brief Loads plugins and manages them.
//
// If a plugin has already been loaded it will not be loaded again.
class PluginManager {
 public:
  struct PluginInformation {
    void* handle;
    std::string name, description;
    int required_version_major, required_version_minor;
    int plugin_version_major, plugin_version_minor;
    std::function<void()> setup_function, teardown_function;
    std::string file_path;
  };

  PluginManager();
  ~PluginManager();

  // \brief Loads all plugins.
  //
  // This means all plugins that are located in the the
  // kPluginSubpath of all directories of FileTools::kSearchedFolders with the
  // extensions of kPluginExtensions.
  void LoadAllPlugins() noexcept;
  // \brief Loads all plugins specified in the vector.
  // The strings are complete paths to the file.
  void LoadPluginsByPath(const std::vector<std::string>& paths) noexcept {
    LoadPluginsByPath(paths.data(), paths.size());
  }
  // \brief Loads all plugins specified in the array.
  // The strings are complete paths to the file.
  void LoadPluginsByPath(const std::string* paths, size_t count) noexcept;

  // \brief Loads all plugins specified in the vector.
  // The strings are file names without full path. They will be searched in the
  // subdirectories of the searched paths with the usual extensions.
  void LoadPluginsByFileName(const std::vector<std::string>& file_names)
      noexcept {
    LoadPluginsByFileName(file_names.data(), file_names.size());
  }
  // \brief Loads all plugins specified in the array.
  // The strings are file names without full path. They will be searched in the
  // subdirectories of the searched paths with the usual extensions.
  void LoadPluginsByFileName(const std::string* names, size_t count) noexcept;

  // \brief Loads a plugin by name.
  //
  // This means that all available plugins files are searched until one is
  // found whose internal name (i.e. the name specified using the
  // VRWM_PLUGIN_INFO macro) maches the given name.
  //
  // \returns Returns the internal plugin name.
  // \throws std::runtime_error if no such plugin.
  const std::string& LoadPluginByName(const std::string& name);
  // \brief Loads a plugin by its file name.
  //
  // This means that all appropriate folders are searched for a file with the
  // given name and one of the extensions specified by PluginManager.
  //
  // \returns Returns the internal plugin name of the loaded plugin.
  // \throws std::runtime_error if no plugin with the name.
  const std::string& LoadPluginByFileName(const std::string& file_name);
  // \brief Loads a plugin by its full path.
  //
  // \returns Returns the internal name of the loaded plugin (specified
  //    using the VRWM_PLUGIN_INFO macro)
  // \throws std::runtime_error if the plugin fails to load.
  const std::string& LoadPluginByPath(const std::string& path);
  // \brief Unloads the loaded plugin with the appropriate internal name.
  void UnloadPlugin(const std::string& name) noexcept;

  // \brief Adds an event receiver to the list of observers.
  // An event receiver is one of the classes in plugin/EventReceiver.h.
  // This function is called automatically in their ctors.
  template <class T>
  void Add(T* event_receiver) noexcept {
    receivers_[static_cast<size_t>(T::kID)].insert(
        std::make_pair(event_receiver->priority, event_receiver));
  }

  // \brief Removes an event receiver from the list of observers.
  // Automatically called in the dtor of the event receiver.
  template <class T>
  void Remove(T* event_receiver) noexcept {
    auto range = receivers_[static_cast<size_t>(T::kID)].equal_range(
        event_receiver->priority);
    for (auto it = range.first; it != range.second; ++it) {
      if (it->second == event_receiver) {
        receivers_[static_cast<size_t>(T::kID)].erase(it);
        break;
      }
    }
  }

  // \brief Executes a lambda function for each receiver of the specified class.
  // Prefer the variadic template flavor if possible. It's probably more
  // performant and easier to write.
  template <class T>
  void ForEach(std::function<void(T*)> f);
  // \brief Executes a lambda function for each receiver of the specified class.
  // Returns false and aborts if a single receiver returns false.
  // Returns true if all receivers returned true.
  // Prefer the variadic template flavor if possible. It's probably more
  // performant and easier to write.
  template <class T>
  bool ForEach(std::function<bool(T*)> f);

  // \brief Executes the passed function-pointer for all receivers of that type.
  // Prefer these variant because it looks nicer and is probably faster.
  // Example usage:
  // \code
  //    PluginManager& m = ...
  //    MouseButton button = ...
  //    ActiveModifiers modifiers = ...
  //    m.ForEach(&MouseClickReceiver::MouseReleased, button, modifiers);
  // \endcode
  template <class T, class... Params, class... Args>
  void ForEach(void(T::*f)(Params...), Args&... params);

  // \brief Executes the passed function-pointer for all receivers of that type.
  // Prefer this variant because it looks nicer and is probably faster.
  // Returns false and aborts if a single receiver returns false.
  // Returns true if all receivers returned true.
  template <class T, class... Params, class... Args>
  bool ForEach(bool(T::*f)(Params...), Args&... params);

 private:
  // \brief Returns true if a plugin's is compatible.
  // The plugin version will be checked against the current vrwm version.
  static bool IsCompatible(int plugin_major, int plugin_minor);

  // \brief The plugin sub directory name.
  // This means the sub directory of the FileTools::kSearchPaths which will be
  // searched for plugins.
  static const std::string kPluginSubPath;
  // \brief The possible extensions of a plugin.
  static const std::array<std::string, 2> kPluginExtensions;

  // \brief Contains the currently loaded libraries.
  std::map<std::string, PluginInformation> loaded_libraries_;
  // \brief Contains all existing event receivers.
  // The array indices are the event receiver's id.
  // The multimap uses an integer to note the priority of the event receiver.
  // Lower priority receivers are called first.
  std::array<std::multimap<int, void*>, (size_t) plugin::ReceiverID::Size>
    receivers_;
};

///////////////////////////////////////////
// Implementation of templated functions //
///////////////////////////////////////////

template <class T>
void PluginManager::ForEach(std::function<void(T*)> f) {
  auto& map = receivers_[static_cast<size_t>(T::kID)];
  for (auto& pair : map) {
    try {
      T* t = static_cast<T*>(pair.second);
      f(t);
    } catch (std::exception& e) {
      vrwm_clog(Warning,
          "Exception while calling lambda for event receiver of type '%s': %s",
          Warning, typeid(T).name(), e.what());
    }
  }
}

template <class T>
bool PluginManager::ForEach(std::function<bool(T*)> f) {
  bool success = true;
  auto& map = receivers_[static_cast<size_t>(T::kID)];
  for (auto& pair : map) {
    try {
      T* t = static_cast<T*>(pair.second);
      if (!(success = f(t))) break;
    } catch (std::exception& e) {
      vrwm_clog(Warning,
          "Exception while calling lambda for event receiver of type '%s': %s",
          typeid(T).name(), e.what());
    }
  }
  return success;
}

template <class T, class... Params, class... Args>
void PluginManager::ForEach(void(T::*f)(Params...), Args&... params) {
  auto& map = receivers_[static_cast<size_t>(T::kID)];
  for (auto& pair : map) {
    try {
      T* t = static_cast<T*>(pair.second);
      (t->*f)(params...);
    } catch(std::exception& e) {
      vrwm_clog(Warning,
          "Exception while calling function of event receiver '%s': %s",
          typeid(T).name(), e.what());
    }
  }
}

template <class T, class... Params, class... Args>
bool PluginManager::ForEach(bool(T::*f)(Params...), Args&... params) {
  bool success = true;
  auto& map = receivers_[static_cast<size_t>(T::kID)];
  for (auto& pair : map) {
    try {
      T* t = static_cast<T*>(pair.second);
      if (!(success = (t->*f)(params...))) break;
    } catch(std::exception& e) {
      vrwm_clog(Warning,
          "Exception while calling function of event receiver '%s': %s",
          typeid(T).name(), e.what());
    }
  }
  return success;
}

}
}

#endif // PLUGIN_MANAGER_H
