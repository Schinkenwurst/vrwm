#ifndef CLIENT_H
#define CLIENT_H

#include <set>

#include <X11/Xlib.h>
#include <X11/extensions/Xdamage.h>

#include "Atoms.h"
#include "Rect.h"
#include "GLglobal.h"
#include "GLtexture.h"
#include "EventHandler.h"
#include "WindowProperties.h"

namespace vrwm {

class Core;
struct Ray;
class Renderable;

// \brief A client of X.
// You can get various interessting information such as the size, the OpenGL
// texture with the window contents, etc. from it.
//
// Because the Client is only an abstract model for the window contents, it
// does not provide any functionality for rendering. Rendering of a client
// always has to be done by a Renderable which defines the position, size and
// shape of the Client.
//
// The on-screen size is in meters and defined by the virtual DPI (which will
// be converted to pixel per centimeter) and the actual x11_size in pixels.
//
// To render a client it must be bound to one or more Renderables which take
// care of the representation. To do so, they may use the opengl texture that
// is automatically kept in sync with the window contents.
//
// When a client is destroyed, all renderables it is attached to will be
// notified.
//
// Currently, a client can not become larger than the screen size as this would
// cause issues with input not being received by GTK applications.
//
// Colormap policy: A client gets colormap focus when it is activated
// (Activate()). It also gets colormap focus if it changed its colormap
// (EventHandler::EventColormap() and UpdateInternalData()). Suspending
// colormap installation by the client is NOT implemented. I simply think that
// this is not worth the effort.
// TODO: add support for windows larger than the screen size.
// TODO: set Client as window property
// TODO: Caching of properties such as WM_STATE
class Client {
 public:
  friend class Core;
  friend class EventHandler;

  // \brief Constructor. Sets up name and moves window to top-left corner.
  Client(Core& core, Window id, const XWindowAttributes& attr,
    bool renderable = true);
  virtual ~Client();

  // \brief Returns the window attributes.
  // Should always succeed except in race conditions where the window has been
  // already been destroyed on the server but is still managed. In these cases,
  // all fields will be 0.
  XWindowAttributes GetAttributes();

  // \brief Closes the window.
  // The client will remain valid, as will its OpenGL texture.
  void Close();

  // \brief Sets up initial values and prepares this window for rendering.
  //
  // Redirects the window contents to pixmap rendering and sets up the OpenGL
  // texture. Also checks if this client is already in a mapped state (which
  // can happen when VRWM is restarted or replaced a previously active window
  // manager) and calls GotMapped() in that case.
  //
  // Is automatically called by VRWM.
  void Initialize();

  // \brief True if the window is an override redirect window
  bool is_override_redirect() const { return is_override_redirect_; }
  // \brief Returns the group this window belongs to (0 if in no group.)
  // The group leader as defined by the ICCCM is a window which may or may not
  // be mapped. The rendering system automatically assigns windows of the same
  // group to the same scene node (or a same parent scene node) so they move
  // together.
  // TODO: When scene nodes exist: Set to scene node based on group.
  XID GetGroup() const { return GetWindowManagerHints()->window_group; }
  // \brief Returns the ID of the window which has been assigned by X.
  Window ID() const { return id_; }
  // \brief Returns the position and size of this window in X.
  // This means that values are in pixel and position is top-left corner.
  const WindowPos& x11_position() const { return x11_position_; }
  // \brief Returns the name of the window.
  // This Name is an UTF-8 encoded string. If the _NET_WM_NAME defined by the
  // EWMH is not set, VRWM falls back to WM_NAME defined by the ICCCM.
  std::string GetName() const;
  // \brief Returns the icon name of the window. (ICCCM)
  std::string GetIconName() const;
  // \brief Returns the applications name. (ICCCM)
  std::string GetApplicationName() const;
  // \brief Returns the application class. (ICCCM)
  std::string GetClassName() const;
  // \brief Returns the window that this window is transient for. (ICCCM)
  Client* GetTransientFor() const;
  // \brief Returns the available protocols. (ICCCM)
  std::set<Atom> GetProtocols() const;
  // \brief Returns the color maps of this window. (ICCCM)
  std::vector<Window> GetColormapWindows() const;

  std::pair<bool, WindowProperties::WmState> GetState() const;
  WindowProperties::EWMHWindowStates GetEWMHStates() const;
  WindowProperties::AllowedActions GetAllowedActions() const;
  // \brief Wrapper around XGetWMHints(). Automatically frees storage.
  // If no WM_HINTS property is set, returns a valid XWMHints with everything
  // set to 0.
  std::shared_ptr<XWMHints> GetWindowManagerHints() const;
  // \brief Wrapper around XGetNormalHints(). Automatically frees storage.
  // If no WM_SIZE_HINTS property is set, returns a valid XSizeHints with
  // everything set to 0.
  std::shared_ptr<XSizeHints> GetSizeHints() const;
  // \brief Returns the state of _NET_WM_WINDOW_TYPE.
  // Only returns the most preferred type (i.e. the first Window Type specified
  // in _NET_WM_WINDOW_TYPE that is recognized by VRWM). Also takes care of the
  // special confitions in the EWMH that denote which window type must be used
  // for a window that does not have the property.
  WindowType GetWindowType() const;

  bool IsInNormalState() const {
    auto exists_state = GetState();
    return exists_state.first &&
      exists_state.second.state == WindowState::WS_NormalState;
  }

  // \brief Assumes name is UTF-8
  void SetName(const std::string& name);
  // \brief Assumes name is UTF-8
  void SetIconName(const std::string& name);
  void SetState(const WindowProperties::WmState& s);
  // \brief Updates the property and executes the respective functionality.
  // After calling this function, the corresponding Atom will be part of the
  // _NET_WM_STATE property if value was true and not be part of it if value
  // was false.
  // There could be a conflict between Maximize{V,H} and Fullscreen. When a
  // making a window maximized or fullscreen, it will be restored first (i.e.
  // it loses the respective other flag(s)).
  void SetEWMHState(EWMHWindowState s, bool value);
  // \brief Allows for update of two states simultaneous.
  void SetEWMHState(EWMHWindowState s1, EWMHWindowState s2, bool v1, bool v2);
  // \brief Updates the _NET_WM_STATE property to reflect the passed states.
  // This means the property will be completely overwritten with the new
  // values.
  // As with the single state version, the respective functionality of all
  // changed properties will be executed.
  void SetEWMHState(const WindowProperties::EWMHWindowStates& states);

  // \brief SetEWMHState with modal
  void SetModal(bool v = true) { SetEWMHState(EWMHWindowState::Modal, v); }
  // \brief SetEWMHState with sticky
  void SetSticky(bool v = true) { SetEWMHState(EWMHWindowState::Sticky, v); }
  // \brief SetEWMHState with maximized v
  void SetMaximizedV(bool v = true) {
    SetEWMHState(EWMHWindowState::MaximizedVert, v);
  }
  // \brief SetEWMHState with maximized h
  void SetMaximizedH(bool v = true) {
    SetEWMHState(EWMHWindowState::MaximizedHorz, v);
  }
  // \brief SetEWMHState with maximized h and v (same value)
  void SetMaximized(bool v = true) {
    SetEWMHState(EWMHWindowState::MaximizedHorz,
      EWMHWindowState::MaximizedVert, v, v);
  }
  // \brief SetEWMHState with shaded
  void SetShaded(bool v = true) { SetEWMHState(EWMHWindowState::Shaded, v); }
  // \brief SetEWMHState with skip taskbar
  void SetSkipTaskbar(bool v = true) {
    SetEWMHState(EWMHWindowState::SkipTaskbar, v);
  }
  // \brief SetEWMHState with skip pager
  void SetSkipPager(bool v = true) {
    SetEWMHState(EWMHWindowState::SkipPager, v);
  }
  // \brief SetEWMHState with hidden
  void SetHidden(bool v = true) { SetEWMHState(EWMHWindowState::Hidden, v); }
  // \brief SetEWMHState with fullscreen
  void SetFullscreen(bool v = true) {
    SetEWMHState(EWMHWindowState::Fullscreen, v);
  }
  // \brief SetEWMHState with above
  void SetAbove(bool v = true) { SetEWMHState(EWMHWindowState::WS_Above, v); }
  // \brief SetEWMHState with below
  void SetBelow(bool v = true) { SetEWMHState(EWMHWindowState::WS_Below, v); }
  // \brief SetEWMHState with demands attention
  void SetDemandsAttention(bool v = true) {
    SetEWMHState(EWMHWindowState::DemandsAttention, v);
  }
  // \brief SetEWMHState with focused
  void SetFocused(bool v = true) { SetEWMHState(EWMHWindowState::Focused, v); }

  // \brief Updates the property.
  // After calling this function, the corresponding Atom will be part of the
  // _NET_WM_ALLOWED_ACTIONS property if value was true and not be part of it
  // if value was false.
  void SetAllowedAction(AllowedAction a, bool value);
  // \brief Updates the _NET_WM_ALLOWED_ACTIONS property.
  // This means the property will be completely overwritten with the new
  // values.
  void SetAllowedActions(const WindowProperties::AllowedActions& states);


  void DeleteState() { DeleteProperty(Atoms::WM_STATE); }
  void DeleteProperty(Atom property);

  // \brief Makes this window an inferior of the passed window.
  // Wrapper around XReparentWindow().
  void Reparent(const Client& newParent);

  // \brief Returns the pixel per centimeter resolution of this client.
  float pixel_per_centimeter() const { return pixel_per_centimeter_; }
  // \brief Sets the pixel per centimeter resolution of this client.
  void set_pixel_per_centimeter(float pxcm) { pixel_per_centimeter_ = pxcm; }
  // \brief Returns pixel per centimeter as dots per inch.
  float dpi() const { return pixel_per_centimeter_ * 2.54f; }
  // \brief Sets pixel per centimeter as dots per inch.
  void set_dpi(float dpi) { pixel_per_centimeter_ = dpi / 2.54f; }

  // \brief Returns the half width of the window in meters.
  float GetHalfWidth() const {
    return x11_position_.size.x * 0.5f / pixel_per_centimeter_ * 0.01f;
  }
  // \brief Returns the half height of the window in meters.
  float GetHalfHeight() const {
    return x11_position_.size.y * 0.5f / pixel_per_centimeter_ * 0.01f;
  }

  // \brief Makes the Renderable observe this Client.
  void associate_with_renderable(Renderable* r);
  // \brief Removes the Renderable from the observer list.
  void separate_from_renderable(Renderable* r) {
    renderables_.erase(r);
  }

  // \brief const begin iterator of all Renderables that observe this Client.
  std::set<Renderable*>::const_iterator begin_renderables() const {
    return renderables_.begin();
  }
  // \brief const end iterator of all Renderables that observe this Client.
  std::set<Renderable*>::const_iterator end_renderables() const {
    return renderables_.end();
  }
  // \brief begin iterator of all Renderables that observe this Client.
  std::set<Renderable*>::iterator begin_renderables() {
    return renderables_.begin();
  }
  // \brief end iterator of all Renderables that observe this Client.
  std::set<Renderable*>::iterator end_renderables() {
    return renderables_.end();
  }

  // \brief Returns the texture.
  // The texture's content is what the window looks like.
  // Remains valid until no longer referenced anywhere.
  // If the window or Client no longer exist, the texture will contain the last
  // known state of the window.
  GLtexturePtr texture() { return texture_; }
  const GLtexturePtr texture() const { return texture_; }
  // \brief Wrapper around texture()->use()
  void UseTexture() const { assert(texture_); texture_->Use(); }
  void GenerateMipmap() const { assert(texture_); texture_->GenerateMipmap(); }
  GLtexturePtr icon() { return icon_; }
  const GLtexturePtr icon() const { return icon_; }
  void UseIcon() const { assert(icon_); icon_->Use(); }

  // \brief Injects a keyboard event into the client.
  void InjectKeyEvent(const XKeyEvent& e);
  // \brief Injects a mouse button event into the client.
  // \param pressed True if the button has been pressed, False on release.
  // \param normalized_coordinates The position the virtual mouse has hit the
  //    client. Both x and y must be in the range of [0, 1]. This value gets
  //    multiplied by the X11 size to calculate the pixel position.
  // \param button The button that has been pressed or released.
  // \param modifiers Currently active keyboard modifiers. This means stuff
  //    like "Ctrl is currently pressed".
  void InjectButtonEvent(bool pressed, const glm::vec2& normalized_coordinates,
      MouseButton button, const ActiveModifiers& modifiers);
  // \brief Injects a window enter / exit event.
  // \param enter Has the window been entered or exitted?
  // \param normalized_pointer_pos Where is the cursor in normalized window
  //    coordinates? When the window has been exitted, this value is ignored
  //    and the last known position is used instead.
  //    x and y must be in the range of [0, 1]. This value
  //    gets multiplied by the X11 size to calculate the pixel position.
  void InjectCrossingEvent(bool enter,
      const glm::vec2& normalized_pointer_pos = glm::vec2(0.0f, 0.0f));
  // \brief Tells the Window that the mouse has moved.
  //
  // The Event will not be sent if the old and new position don't vary by
  // at least one pixel.
  void InjectPointerMotion(const glm::vec2& normalized_coordinates,
      const ActiveModifiers& modifiers);
  // \brief Gives this Client input focus.
  // That means that it receives all keyboard input.
  // This function conforms to the ICCCM section 4.1.7 and only assigns input
  // focus to a client which has specifies input field in the WM_HINTS to true.
  // If the field is specified and false, focus will not be assigned. If that
  // field is not specified, input focus will be assigned.
  void Focus();
  // \brief True if the window currently has input focus.
  bool has_focus() const { return has_focus_; }

  // \brief Maps the window.
  // Should not be called directly. Use Activate() instead.
  void Map();
  // \brief Maps the window on top of the stack.
  void MapRaised();
  // \brief Unmaps the window.
  // Should not be called directly. Usually, when a window is unmapped, it is
  // to be set into iconic state. Call Hide() instead!
  void Unmap();
  // \brief Puts the window into normal state.
  // If this window is not the top-level window, its top-level window will be
  // activated instead.
  // Putting the window into normal state maps it and unmaps the icon window.
  // Tries to make the window get input focus.
  void Activate();
  // \brief Puts the window into iconic state.
  // If this window is not the top-level window, its top-level window will be
  // put into iconic state instead.
  // Iconic state unmaps the top-level window and displays the icon window
  // instead.
  // No effect if window already in iconic state.
  // Pixmap and OpenGL texture remain valid.
  void Hide();

  // \brief Called when the window got mapped (became visible)
  // Updates the pixmap, notifies the renderable and offers input focus.
  void GotMapped();
  // \brief Called when the window got minimized.
  // This function is called when the window got unmapped (i.e. became
  // invisible) but that unmap was not the result of withdrawing the window. In
  // case the window is unmapped with the intent of not using it anymore
  // GotWithdrawn() is called instead.
  // After calling this function, the pixmap will stay valid but won't update
  // anymore because painting on unmapped windows is not possible. The pixmap
  // will update again when GotMapped() is called the next time (i.e. the
  // window is un-minimized)
  void GotMinimized();
  // \brief Called when the window is withdrawn by the client.
  // Afterwards, the pixmap and texture will not update (but stay valid).
  void GotWithdrawn();

  // \brief Sets a new window size.
  // A clients size will always be at least 1x1 pixel and respect its size
  // hints as per ICCCM. However, at the current time, the size can not grow
  // larger than the screen size for technical reasons.
  // \returns true if the size will be changed. false if there is a pending
  // resize or this resize request would not change the client's size (e.g.
  // because the requested size is smaller than the minimum size).
  bool Resize(unsigned int width, unsigned int height);
  bool Resize(const glm::uvec2& size) { return Resize(size.x, size.y); }
  // \brief Calculates an appropriate size based on size hints and returns it.
  // A clients size will always be at least 1x1 pixel and respect its size
  // hints as per ICCCM. However, at the current time, the size can not grow
  // larger than the screen size for technical reasons.
  glm::uvec2 CalculateNearestSize(unsigned int width,
                                  unsigned int height) const;

  // \brief Brings this Client to the top of the stack (XRaiseWindow())
  void Raise();

  std::string ToString() const;

  // \brief Logs several details about this window with log level Info.
  // Name, id, pos, size, class, size hints, application name, wm hints, ...
  void PrintDetails() const;

  // \brief Called by the EventHandler when a property changes.
  // This function then updates the appropriate attribute(s) to reflect the
  // change.
  // \param update: True if updated, false if deleted
  void UpdateInternalData(Atom property, bool update);

  // \brief Wraps XGetWindowProperty and returns the string.
  // \throws std::runtime_error if the property does not exist or its type is
  // not a string (or UTF8_STRING if the utf8 flag is true).
  std::string QueryStringProperty(Atom property, bool utf8 = false) const;
  // \brief Wraps around XGetWindowProperty to return a Window as Client.
  std::vector<Client*> QueryWindowProperty(Atom property) const;
  // \brief Wraps around XGetWindowProperty
  // Careful: Large unsigned longs become signed.
  std::vector<long> QueryCardinalProperty(Atom property, bool is_signed) const;
  // \brief Wraps around XGetWindowProperty to return Atoms.
  std::vector<Atom> QueryAtomProperty(Atom property) const;

  // \brief Installs all colormaps required by this client.
  // When this function is called on a subwindow, the call will be redirected
  // to the top-level window.
  // As specified in ICCCM 4.1.8
  void InstallColormaps();

  // \brief Sets the _NET_WM_DESKTOP
  void ChangeDesktop(int new_desktop);

  // \brief Pings to client as per EWMH ping protocol.
  void Ping();
  // \brief Called when the ping has been answered by client.
  void PingAnswered();
  Time GetTimeSincePing() const;

  bool is_transparent() const { return is_transparent_; }

 private:
  // \brief Sets the input focus. Called by Core.
  void set_has_focus(bool v) { has_focus_= v; }
  // \brief Send ClientMessage to ask Window if it wants input focus.
  // Only sends the message when WM_TAKE_FOCUS is a supported protocol.
  // See ICCCM 4.1.7
  void OfferFocus();

  // \brief Sends a ClientMessageEvent to the client.
  // The message will be as specified in ICCCM 4.2.8.
  void SendClientMessage(Atom message_type) {
    long data[3] = {0L, 0L, 0L};
    SendClientMessage(message_type, data);
  }
  // \brief Sends a ClientMessageEvent to the client.
  // The message will be as specified in ICCCM 4.2.8.
  void SendClientMessage(Atom message_type, long additional_data[3]);

  // \brief Called when the size of the window has changed.
  void SizeChanged(unsigned int new_width, unsigned int new_height);
  // \brief Destroys the old pixmap and creates a new one.
  // Automatically called by vrwm when necessary.
  void UpdatePixmap();

  // \brief Destroyed the pixmap if it exists.
  void FreePixmap();

  // \brief Maximizes this window vertically.
  void MaximizeV(const WindowProperties::EWMHWindowStates& old_states);
  // \brief Maximizes this window horizontally.
  void MaximizeH(const WindowProperties::EWMHWindowStates& old_states);
  void MaximizeHV(const WindowProperties::EWMHWindowStates& old_states);
  // \brief Makes this window full screen.
  void MakeFullscreen(const WindowProperties::EWMHWindowStates& old_states);
  // \brief Un-maximizes or fullscreens this window.
  void Restore(bool w, bool h);

  // \brief Updates the property and executes functionality.
  void UpdateState(const WindowProperties::EWMHWindowStates& new_states,
                   const WindowProperties::EWMHWindowStates& old_states);
  void UpdateFrameExtents();

  // \brief Updates the icon texture based on _NET_WM_ICON
  // Afterwards, the icon_ texture is set.
  // The smaller versions of the icon will be incorporated in the mipmaps of
  // the texture.
  void UpdateIcon();

  // \brief The vrwm core.
  Core& core_;
  // \brief The list of all Renderables which observe this Client.
  // TODO: possiblity to set primary renderable
  std::set<Renderable*> renderables_;

  // \brief is this window an override redirect window
  bool is_override_redirect_;
  // \brief The window ID of this client in X11
  Window id_;
  // \brief The position and size of this client in X11
  WindowPos x11_position_;
  // \brief The position and size before the window went into fullscreen.
  // This is also set before maximization.
  WindowPos x11_position_before_fullscreen_;
  // \brief The last known pointer position inside this window.
  // Used during InjectKeyEvent.
  // x and y are in the range of [0, 1].
  glm::vec2 last_normalized_pointer_position_;

  // \brief This clients virtual pixels per centimeter.
  // Used to calculate the size of this client in the 3D space by
  // pixel_per_centimeter * 0.01 * x11_position_.size
  float pixel_per_centimeter_;

  // \brief True if this client currently has input focus.
  bool has_focus_;
  // \brief True if this client should be rendered.
  // Clients such as the composite overlay window and the OpenGL window (see
  // Core) must not be rendered and thus don't need a texture, pixmap, etc.
  bool rendering_enabled_;
  // \brief The pixmap which X11 automatically keeps in sync with the contents.
  Pixmap pixmap_;
  // \brief The same pixmap for GLX, which is bound to the texture.
  GLXPixmap glxpixmap_;
  // \brief May contain data for a next resize if a resize is currently pending.
  // This means that when a window has just been resized and is resized again
  // before the pixmap has been updated that resize is written into this
  // variable.
  // This is neccessary because resizing a window too quickly would cause the
  // pixmap to be wrong and cause crashes.
  glm::uvec2 pending_resize_;

  // \brief The start time of the previous ping. 0 if no pending ping.
  Time ping_start_time_;

  // \brief The OpenGL texture which looks just like the window does.
  // Remains valid until no longer referenced, even after the window has been
  // closed and the Client has been destroyed.
  GLtexturePtr texture_;

  GLXFBConfig* framebuffer_config_;
  bool is_transparent_;

  // \brief The OpenGL texture which holds the icon (i.e. logo) of the window.
  // Updated from _NET_WM_ICON.
  // Remains valid until no longer referenced, even after the window has been
  // closed and the Client has been destroyed.
  GLtexturePtr icon_;
};


} //vrwm

#endif // CLIENT_H

