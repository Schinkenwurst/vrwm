#ifndef VRWM_ATOM_H
#define VRWM_ATOM_H

#include <X11/Xlib.h>
#include <X11/Xatom.h>

#include <string>

// \file
// \brief In this file various atoms for usage with Xlib are defined.
// It also contains various wrappers around Xlib functions concerning atoms.

namespace vrwm {
class Atoms {
 public:
  /// \brief Wrapper around XGetAtomName() which handles memory.
  static std::string GetName(Atom a);

  static void Initialize(Display* dpy);

  // \brief Mouse axis.
  // The atom names are not defined by XInput but have always held true.
  static Atom REL_X, REL_Y, REL_HORIZ_WHEEL, REL_VERT_WHEEL;

  static Atom UTF8_STRING;

  // \brief Defined by xlib
  static Atom WM_TRANSIENT_FOR;

  /////////
  // ICCCM
  /////////

  static Atom WM_PROTOCOLS;
  static Atom WM_STATE;
  static Atom WM_TAKE_FOCUS;
  static Atom WM_CHANGE_STATE;
  static Atom WM_DELETE_WINDOW;
  static Atom WM_COLORMAP_WINDOWS;

  ////////
  // EWMH
  ////////

  // EWM Root Window Properties (and Related Messages)
  static Atom _NET_SUPPORTED, _NET_CLIENT_LIST, _NET_CLIENT_LIST_STACKING,
    _NET_NUMBER_OF_DESKTOPS, _NET_DESKTOP_GEOMETRY, _NET_DESKTOP_VIEWPORT,
    _NET_CURRENT_DESKTOP, _NET_DESKTOP_NAMES, _NET_ACTIVE_WINDOW,
    _NET_WORKAREA, _NET_SUPPORTING_WM_CHECK, _NET_VIRTUAL_ROOTS,
    _NET_DESKTOP_LAYOUT, _NET_SHOWING_DESKTOP;
  // EWMH Other Root Window Messages
  static Atom _NET_CLOSE_WINDOW, _NET_MOVERESIZE_WINDOW, _NET_WM_MOVERESIZE,
    _NET_RESTACK_WINDOW, _NET_REQUEST_FRAME_EXTENTS;
  // EWMH Application Window Properties
  static Atom _NET_WM_NAME, _NET_WM_VISIBLE_NAME, _NET_WM_ICON_NAME,
    _NET_WM_VISIBLE_ICON_NAME, _NET_WM_DESKTOP, _NET_WM_WINDOW_TYPE,
    _NET_WM_STATE, _NET_WM_ALLOWED_ACTIONS, _NET_WM_STRUT,
    _NET_WM_STRUT_PARTIAL, _NET_WM_ICON_GEOMETRY, _NET_WM_ICON, _NET_WM_PID,
    _NET_WM_HANDLED_ICONS, _NET_WM_USER_TIME, _NET_WM_USER_TIME_WINDOW,
    _NET_FRAME_EXTENTS, _NET_WM_OPAQUE_REGION, _NET_WM_BYPASS_COMPOSITOR;
  // EWMH Window Manager Protocols
  static Atom _NET_WM_PING, _NET_WM_SYNC_REQUEST, _NET_WM_FULLSCREEN_MONITORS;
  // EWMH Other Properties
  static Atom _NET_WM_FULL_PLACEMENT;
  //EWMH Compositing Managers

  // EWMH Window Types (_NET_WM_WINDOW_TYPE)
  static Atom _NET_WM_WINDOW_TYPE_DESKTOP, _NET_WM_WINDOW_TYPE_DOCK,
    _NET_WM_WINDOW_TYPE_TOOLBAR, _NET_WM_WINDOW_TYPE_MENU,
    _NET_WM_WINDOW_TYPE_UTILITY, _NET_WM_WINDOW_TYPE_SPLASH,
    _NET_WM_WINDOW_TYPE_DIALOG, _NET_WM_WINDOW_TYPE_DROPDOWN_MENU,
    _NET_WM_WINDOW_TYPE_POPUP_MENU, _NET_WM_WINDOW_TYPE_TOOLTIP,
    _NET_WM_WINDOW_TYPE_NOTIFICATION, _NET_WM_WINDOW_TYPE_COMBO,
    _NET_WM_WINDOW_TYPE_DND, _NET_WM_WINDOW_TYPE_NORMAL;

  // EWMH Window States (_NET_WM_STATE)
  static Atom _NET_WM_STATE_MODAL, _NET_WM_STATE_STICKY,
    _NET_WM_STATE_MAXIMIZED_VERT, _NET_WM_STATE_MAXIMIZED_HORZ,
    _NET_WM_STATE_SHADED, _NET_WM_STATE_SKIP_TASKBAR, _NET_WM_STATE_SKIP_PAGER,
    _NET_WM_STATE_HIDDEN, _NET_WM_STATE_FULLSCREEN, _NET_WM_STATE_ABOVE,
    _NET_WM_STATE_BELOW, _NET_WM_STATE_DEMANDS_ATTENTION,
    _NET_WM_STATE_FOCUSED;
  static Atom _NET_WM_STATE_REMOVE, _NET_WM_STATE_ADD, _NET_WM_STATE_TOGGLE;

  // EWMH Allowed Actions (_NET_WM_ALLOWED_ACTIONS)
  static Atom _NET_WM_ACTION_MOVE, _NET_WM_ACTION_RESIZE,
    _NET_WM_ACTION_MINIMIZE, _NET_WM_ACTION_SHADE, _NET_WM_ACTION_STICK,
    _NET_WM_ACTION_MAXIMIZE_HORZ, _NET_WM_ACTION_MAXIMIZE_VERT,
    _NET_WM_ACTION_FULLSCREEN, _NET_WM_ACTION_CHANGE_DESKTOP,
    _NET_WM_ACTION_CLOSE, _NET_WM_ACTION_ABOVE, _NET_WM_ACTION_BELOW;

  // Compositing Manager Selection
  // VRWM only supports one screen.
  static Atom _NET_WM_CM_S0;

  ////////
  // VRWM
  ////////

  // VRWM_CLIENT_DPI
  // VRWM_CLIENT_LIST_STACKING
  // VRWM_CREATE_RENDERABLE with data
  // VRWM_RENDERABLE_POS
  // VRWM_MOVERESIZE like EWMH but in VR
  // VRWM_WM_STATE_VR (i.e. the client uses VR itself)
  // VRWM_WM_ACTION_MOVE
};
} //vrwm

#endif // ATOM_H

