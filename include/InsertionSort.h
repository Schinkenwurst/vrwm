#ifndef VRWM_INSERTION_SORT_H
#define VRWM_INSERTION_SORT_H

#include <algorithm>

namespace vrwm {
namespace sort {

  template <class RandomAccessIterator, class Compare>
  void InsertionSort(RandomAccessIterator first, RandomAccessIterator last,
      Compare comp) {
    if (first == last) return;
    for (auto i = first + 1; i != last; ++i) {
      auto x = std::move(*i);
      auto j = i;
      while (j > first && comp(x, *(j - 1))) {
        *j = std::move(*(--j));
      }
      *j = std::move(x);
    }
  }

  template <class T, class Compare>
  void InsertionSort(std::vector<T>& v, Compare comp) {
    InsertionSort(v.begin(), v.end(), comp);
  }

}
}

#endif


