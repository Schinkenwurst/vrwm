#ifndef VRWM_ASSETS_H
#define VRWM_ASSETS_H

#include <deque>
#include <map>
#include <string>
#include <memory>

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

#include <assimp/material.h>

// Forward declarations of assimp classes.
struct aiMesh;
struct aiTexture;
struct aiNode;
struct aiScene;

namespace vrwm {

class GLtexture;
class SceneNode;

typedef std::shared_ptr<GLtexture> GLtexturePtr;

namespace draw {
  class VertexGroup;
  typedef std::shared_ptr<VertexGroup> VertexGroupPtr;
  class Material;
  typedef std::shared_ptr<Material> MaterialPtr;
}

// \brief Stores currently loaded assets.
class Assets {
 public:
  // \brief Contains all currently loaded textures.
  // File name => texture
  // This map is used to reduce memory consumption
  // A texture that has already been loaded will not be loaded again.
  static std::map<std::string, GLtexturePtr> g_textures;

  static std::map<std::string, draw::VertexGroupPtr> g_meshes;

  enum class MaterialType {
    Simple = 0,
    Diffuse, DiffuseTextured,
    Specular, SpecularTextured,
    SpecularTexturedShinyTextured, Bumped, BumpedSpecular,
    BumpedSpecularTextured, BumpedSpecularTexturedShinyTextured,

    TransparentDiffuse,
    TransparentDiffuseTextured, TransparentSpecular, TransparentSpecularTextured,
    TransparentSpecularTexturedShinyTextured,
    TransparentBumped, TransparentBumpedSpecular,
    TransparentBumpedSpecularTextured,
    TransparentBumpedSpecularTexturedShinyTextured,

    SIZE,
  };
  static std::array<draw::MaterialPtr,
    static_cast<size_t>(MaterialType::SIZE)> g_materials;
  static std::map<std::string, draw::MaterialPtr> g_custom_materials;

  // \brief Asset file name => Root scene node of that file
  static std::map<std::string, SceneNode*> g_asset_files;

 public:
  // \brief Returns a currently loaded texture or nullptr.
  static GLtexturePtr texture(const std::string& name);
  static draw::VertexGroupPtr mesh(const std::string& name);
  static draw::MaterialPtr material(const std::string& name);
  static draw::MaterialPtr material(MaterialType type);
  // \brief Stores a GLtexture.
  // See GLtexture::Load();
  static GLtexturePtr add(std::string&& name, GLtexture* texture);
  // \brief Stores a GLtexture.
  // See GLtexture::Load();
  static GLtexturePtr add(const std::string& name, GLtexture* texture);
  static draw::VertexGroupPtr add(const std::string& name,
    draw::VertexGroup* mesh);
  // \brief Internal use only! Stores default materials.
  static draw::MaterialPtr add(MaterialType type, draw::Material* mat);
  static draw::MaterialPtr add(const std::string& name, draw::Material* mat);

  // \brief Loads a 3D asset file using Assimp.
  //
  // The file is searched in the following directories (in that order):
  //      ~/.vrwm/assets
  //      /usr/share/vrwm/assets
  // Most major 3D file formats are supported. For an extensive list, see the
  // assimp manual: http://assimp.sourceforge.net/main_features_formats.html
  //
  // After calling this function, the loaded file's root scene node is stored
  // in g_asset_files under the asset files map. Additionally, each of the
  // clients meshes, materials as well as textures are stored in their
  // respective maps under the name "<assetfile>/<resource>".
  //
  // TODO: adjust
  // For Instance: If your file "test.obj" contains a mesh called "foobar" that
  // uses a material "foobar material" which in turn uses the texture
  // "barfoo.png", the following entries will be added to the Assets class:
  // g_asset_files (see Assets::asset_file())
  //    test.obj
  // g_textures (see Assets::texture())
  //    test.obj/barfoo.png
  // g_meshes (see Asset::mesh())
  //    test.obj/foobar
  // g_custom_materials (see Assets::material())
  //    test.obj/foobar material
  //
  // You should not use that scene node directly. Always copy it beforehand
  // (e.g. by attaching it via my_node->CopyAndAttach(LoadAssetFile(...));
  // which also performs a nullptr check)
  // TODO: make shared_ptr
  static const SceneNode* LoadAssetFile(const std::string& pathless_filename);

  // \brief Removes the textures from the map.
  // Thus destroying them when they are no longer used by an object.
  static void TearDownTextures();
  static void TearDownMeshes();
  static void TearDownMaterials();
  static void TearDownAssetFiles();
  static void TearDown();

 private:
  struct MaterialInfo {
    glm::vec4 color_diffuse;
    glm::vec3 color_specular, color_ambient;
    float shininess;
    GLtexturePtr tex_diffuse, tex_specular, tex_ambient, tex_normal, tex_shiny;
  };

  static std::deque<draw::VertexGroupPtr> Import(const aiMesh* const* meshes,
    unsigned int n);
  static std::deque<draw::MaterialPtr> Import(const aiMaterial* const* mats,
    unsigned int n, const std::string& asset_dir);
  static SceneNode* Import(const aiScene* scene, const aiNode* node,
    const std::deque<draw::VertexGroupPtr>& meshes,
    const std::deque<draw::MaterialPtr>& materials,
    SceneNode* parent);

  static draw::VertexGroup* Import(const aiMesh* mesh);
  static size_t CalculateFloatsPerVertex(const aiMesh* mesh);
  static void FillVertexBuffer(const aiMesh* mesh, void* buffer,
    size_t buffer_size);
  static void* WritePositions(const aiMesh* mesh, void* buffer);
  static void* WriteNormals(const aiMesh* mesh, void* buffer);
  static void* WriteTangentsAndBitangents(const aiMesh* mesh, void* buffer);
  static void* WriteTextureCoordinates(const aiMesh* mesh, void* buffer);
  static void* WriteColorData(const aiMesh* mesh, void* buffer);
  static draw::VertexGroup* CreateMesh(const aiMesh* mesh, const void* buffer,
    size_t buffer_size);
  static void SetRenderingOrder(draw::VertexGroup* vrwm_mesh,
    const aiMesh* assimp_mesh);
  static void SetVertexAttributes(draw::VertexGroup* vrwm_mesh,
    const aiMesh* assimp_mesh);

  static draw::Material* Import(const aiMaterial* mat,
    const std::string& asset_directory);
  static MaterialInfo GetMaterialProperties(const aiMaterial* mat,
    const std::string& asset_directory);
  static MaterialType SelectAppropriateMaterialType(const MaterialInfo& info);
  static draw::Material* CreateMaterial(MaterialType t,
    const MaterialInfo& info);

  // \brief Loads the texture of the type from the material.
  //
  // A texture is specified as a path name which will be loaded into a
  // GLtexture object and returned. If the file does not exist, an error will
  // be logged and nullptr is returned. If the material does not have
  // any textures of that type, nullptr is returned but nothing will be logged.
  //
  // While Assimp has the capability of multiple textures per type (e.g. 2
  // textures of type DIFFUSE and 1 of type SPECULAR) only the first one is
  // loaded in any case.
  //
  // The texture will be searched by the rules specified in the comment of
  // kAssetTextureFallbackSubPath.
  static GLtexturePtr LoadTexture(const std::string& asset_directory,
    const aiMaterial* mat, aiTextureType type, bool as_heightmap = false);
  static std::string FindTexture(const std::string& asset_directory,
    const std::string& texture_name);

  static void LogMeshInfo(const aiMesh* mesh);
  static void LogTextureInfo(const aiString& path,
    aiTextureMapMode map_mode_u, aiTextureMapMode map_mode_v);
  static void LogMaterialInfo(const MaterialInfo& info);
  static int ToGLWrapParameter(aiTextureMapMode m);

  // \brief The asset file sub directory name: "assets/"
  //
  // This means the sub directory of the FileTools::kSearchPaths which will be
  // searched for asset files which are to be loaded by LoadAssetFile().
  // Textures associated with that file will be searched differently. See
  // kAssetTextureFallbackSubPath;
  static const std::string kAssetFileSubPath;

  // \brief Specifies the sub directory for textures. "assets/textures/"
  //
  // Textures associated with an asset file will first be searched in the
  // directory of the asset file and, if not found there, will be searched in
  // this sub directory of FileTools::kSearchPaths.
  //
  // If a texture specifies a subpath (e.g. "foo/bar/barfoo.png") it will first
  // be searched with the full path and if it still cannot be found, only the
  // texture name (without its path) is searched.
  //
  // Example:
  //  LoadAssetFile("mystuff/foobar.obj");
  //  Let's say that file is located in
  //  /usr/share/vrwm/assets/mystuff/foobar.obj
  //  and let's say the .obj file (its .mtl file actually) requires a texture
  //  called "examplematerial/barfoo.png". Therefore, the following files are
  //  searched (in that order):
  //  /usr/share/vrwm/assets/mystuff/examplematerial/barfoo.png
  //  ~/vrwm/assets/textures/examplematerial/barfoo.png
  //  /usr/share/vrwm/assets/textures/examplematerial/barfoo.png
  //  ~/vrwm/assets/textures/barfoo.png
  //  /usr/share/vrwm/assets/textures/barfoo.png
  //
  // This means that you can safely put all your textures into a single
  // directory and if they are used by different model files, they will not
  // take up additional space. Of course, its up to you to take care that
  // different textures have different names to avoid overwriting files.
  static const std::string kAssetTextureFallbackSubPath;
};

} // vrwm

#endif
