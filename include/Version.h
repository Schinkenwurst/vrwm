#ifndef VRWM_VERSION_H
#define VRWM_VERSION_H

// \file
// \brief This file contains defines for the current VRWM version.
//
// Also contains defines for the plugin system version. Each plugin compiles
// the plugin system version into itself to allow VRWM to check if the VRWM
// version the plugin was compiled with is compatible to the version of this
// instance of VRWM.

#define VRWM_VERSION_MAJOR 0
#define VRWM_VERSION_MINOR 2

#define VRWM_PLUGIN_VERSION_MAJOR 0
#define VRWM_PLUGIN_VERSION_MINOR 2

#endif // VRWM_VERSION_H
