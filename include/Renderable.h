#ifndef RENDERABLE_H
#define RENDERABLE_H

#include <cstring>
#include <string>
#include <vector>
#include <memory>

#include <X11/Xlib.h>
#include <glm/gtx/quaternion.hpp>

#include "draw/Uniform.h"
#include "XlibWrappers.h"

namespace vrwm {

class Client;
class Core;
struct Ray;
struct RaycastHit;
class SceneNode;

namespace draw {
  class Material;
  typedef std::shared_ptr<Material> MaterialPtr;
  class VertexGroup;
  typedef std::shared_ptr<VertexGroup> VertexGroupPtr;
}

// \brief Used for rendering and thus making something visible.
//
// A Renderable is rendered by the OpenGLManager. It can be used to render
// arbitrary 3D models.
// A Renderable may also use a Client's OpenGL texture as you would normally
// render a texture on a surface. This makes an X11 window visible to the user.
//
// Usually, when rendering a Client's texture, you also want to enable the user
// to click on it. This is done by making a vertex group raycastable and
// providing the appropriate callback functions. See the implementation of
// CreateDefaultRenderable() for details of how to do it.
//
// A renderable may be a billboard. In that case its orientation is
// automatically updated to face the user's head when the Renderable is moved.
//
// To allow moving and resizing of a Renderable, it can be "grabbed". A grab
// entails a type (e.g. ScaleVertical or MovePlane) and the grab origin. When
// a grab is released, the grab origin point and grab release point are
// reported to the Renderable which then executes the appropriate actions.
// Only one Renderable may be grabbed at the same time.
// TODO: update doc
// TODO: update doc on the callback functions
// TODO: Use a Client-stub which automatically holds the textures.
class Renderable {
 public:
  typedef std::function<void(Renderable& r, Client& c)> ClientCallback;
  typedef std::function<void(Renderable& r, const RaycastHit&, MouseButton,
    bool, const ActiveModifiers&)> MouseButtonCallback;
  typedef std::function<void(Renderable& r, const RaycastHit&,
    const ActiveModifiers&)> HoverCallback;
  typedef std::function<bool(Renderable& r,
    const glm::vec3& future_world_scale)> SceneNodeScaleRequest;

  Renderable(Core& c, SceneNode& n, draw::VertexGroupPtr mesh,
    draw::MaterialPtr mat);
  Renderable(const Renderable& copy);
  Renderable& operator=(const Renderable& rhs);

  static void ConstructDefaultClientRenderableMesh();
  static void ConstructDefaultClientDecoratorMeshes();
  // \brief Factory for a default Renderable.
  //
  // This factory method produces a rectangular Renderable to which the client
  // window is mapped. It also provides a crude Button for closing, a crude
  // title bar for moving and a window frame for resizing.
  //
  // This function should serve as a starting point for implementing your own
  // CreateRenderable() function which produces a really fancy Renderable.
  // It shows you all the tasks that a renderable has to perform in order to
  // be usable.
  static void CreateDefaultClientRenderable(Core& core, Client* c);
  static void CreateDefaultClientDecorators(Core& core, SceneNode& n,
                                            Client* c);
  virtual ~Renderable();

  // \brief Renders this renderable.
  void Render();

  // \brief Makes this renderable observe the Client.
  void set_client(Client* c);
  // \brief Gets the observed Client. nullptr if no observed Client.
  Client* client() { return client_; }
  // \brief Gets the observed Client. nullptr if no observed Client.
  const Client* client() const { return client_; }

  const draw::VertexGroupPtr vertex_group() const { return vertex_group_; }
  draw::VertexGroupPtr vertex_group() { return vertex_group_; }
  void set_vertex_group(draw::VertexGroupPtr m) {
    assert(m); vertex_group_ = m;
  }

  bool Transparent() const;

  // \brief Casts a ray against this renderable and returns the intersection.
  //
  // Raycast() respects the existing information in out_hit which means you can
  // safely use the same RaycastHit object to execute raycasts against multiple
  // Renderables and only the nearest hit will be returned.
  //
  // \param r the ray which is in question.
  // \param [out] out_hit Will be filled with information about the hit.
  //
  // \returns If this renderable has not been hit or if the hit occured after
  //    the previous hit that was written to the same out_hit, false is
  //    returned.  Otherwise, true is returned and the RaycastHit object has
  //    been filled with the correct information.
  bool Raycast(const Ray& r, RaycastHit& out_hit);

  // \brief Sets the arbitrary user data to a specific value.
  // Renderable does not take ownership of the user data.
  // The caller is responsible for freeing the memory.
  void set_user_data(void* data) { user_data_ = data; }
  // \brief Returns the user data.
  void* user_data() const { return user_data_; }

  bool on_scene_node_resize(const glm::vec3& future_world_scale) {
    if (on_scene_node_resize_) {
      return on_scene_node_resize_(*this, future_world_scale);
    } else {
      return true;
    }
  }

  // \brief Calls the on client map callback function.
  void on_client_map(Client& c) {
    if (on_client_map_) on_client_map_(*this, c);
  }
  // \brief Calls the on client iconic callback function.
  void on_client_iconic(Client& c) {
    if (on_client_iconic_) on_client_iconic_(*this, c);
  }
  // \brief Calls the on client close callback function.
  void on_client_close(Client& c) {
    if (on_client_close_) on_client_close_(*this, c);
  }
  // \brief Calls the on client resize callback function.
  void on_client_resize(Client& c) {
    if (on_client_resize_) on_client_resize_(*this, c);
  }
  void on_button(const RaycastHit& h, MouseButton b, bool down,
      const ActiveModifiers& mods) {
    if (on_button_) on_button_(*this, h, b, down, mods);
  }
  void on_hover(const RaycastHit& h, const ActiveModifiers& mods) {
    if (on_hover_) on_hover_(*this, h, mods);
  }
  void on_hover_exit(const RaycastHit& h, const ActiveModifiers& mods) {
    if (on_hover_exit_) on_hover_exit_(*this, h, mods);
  }

  void set_on_scene_node_resize(SceneNodeScaleRequest f) {
    on_scene_node_resize_ = f;
  }
  // \brief Callback function that is executed when a client is mapped.
  void set_on_client_map(ClientCallback f) { on_client_map_ = f; }
  // \brief Callback function that is executed when a client is minimized.
  void set_on_client_iconic(ClientCallback f) { on_client_iconic_ = f; }
  // \brief callback function that is executed when a client is closed.
  void set_on_client_close(ClientCallback f) { on_client_close_ = f; }
  // \brief callback function that is executed when a client is closed.
  void set_on_client_resize(ClientCallback f) { on_client_resize_ = f; }

  void set_on_button(MouseButtonCallback f) { on_button_ = f; }
  void set_on_hover(HoverCallback f) { on_hover_ = f; }
  void set_on_hover_exit(HoverCallback f) { on_hover_exit_ = f; }

  SceneNode& scene_node() { assert(scene_node_); return *scene_node_; }
  const SceneNode& scene_node() const {
    assert(scene_node_); return *scene_node_;
  }
  void SetSceneNode(SceneNode& n);
  // \brief Don't call this! Use SetSceneNode().
  // Sets the scene node without telling them about it.
  void ForceSetSceneNode(SceneNode& n) { scene_node_ = &n; }
  SceneNode* grab_scene_node() { return grab_scene_node_; }
  const SceneNode* grab_scene_node() const { return grab_scene_node_; }
  void set_grab_scene_node(SceneNode* s) { grab_scene_node_ = s; }

  size_t index_in_sorted() const { return index_in_sorted_; }
  /// \brief Internal use only! Do not touch!
  void set_index_in_sorted(size_t i) { index_in_sorted_ = i; }

  draw::MaterialPtr material() { return material_; }
  const draw::MaterialPtr material() const { return material_; }
  void set_material(draw::MaterialPtr m) { material_ = m; }

  Core& core() { return core_; }

 private:
  Renderable() = delete;

  // \brief The core instance.
  Core& core_;
  // \brief The observed Client. May be nullptr.
  Client* client_;

  // \brief The scene node that this renderable is attached to. Never null!
  SceneNode* scene_node_;
  // \brief The scene node to use for SceneNodeGrab regarding this renderable.
  // May be null in which case this renderable cannot be grabbed.
  SceneNode* grab_scene_node_;

  // \brief The vertex group which make up this renderable.
  draw::VertexGroupPtr vertex_group_;
  // \brief The index in the sorted array of the OpenGLManager.
  // This index is used to quickly update the model matrix used for rendering.
  size_t index_in_sorted_;

  // \brief Arbitrary user data. Renderable is NOT responsible for the memory.
  void* user_data_;

  // \brief The GLSL program and textures used when drawing this renderable.
  draw::MaterialPtr material_;

  SceneNodeScaleRequest on_scene_node_resize_;

  MouseButtonCallback on_button_;
  HoverCallback on_hover_;
  HoverCallback on_hover_exit_;

  // \brief This callback function is called when the client is un-minimized.
  // (i.e. becomes visible from minimized state)
  // Should most likely set the renderable to visible in some way.
  // This function will also be called when the renderable is first attached to
  // a client (during creation of the renderable).
  ClientCallback on_client_map_;

  // \brief This callback function is called when the client is minimized.
  // Should most likely set the renderable to invisible.
  ClientCallback on_client_iconic_;

  // \brief This callback function is called when the client is withdrawn.
  // Pixmap and x window do not exist. OpenGL texture will still be valid if it
  // is also referenced somewhere else.
  ClientCallback on_client_close_;

  // \brief This callback function is called when a client is resized.
  ClientCallback on_client_resize_;
};

} //vrwm

#endif // RENDERABLE_H
