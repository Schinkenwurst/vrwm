#ifndef VRWM_GLTEXTURE_SWAPPER_H
#define VRWM_GLTEXTURE_SWAPPER_H

#include "GLtexture.h"

#include <vector>
#include <stack>

namespace vrwm {

/// \brief Keeps track of all texture units and which textures are bound.
/// When a texture needs to be used but is currently not bound, it will be bound
/// to a unused texture unit. This also activates the texture unit. If no
/// texture unit is free, it will be bound on the texture unit determined by a
/// least recently used (LRU) algorithm.
///
/// LRU: Each texture use during the current frame will be counted.  When a
/// texture needs to be used but is not currently bound and no unused texture
/// unit exists, it will be bound to the texture unit that was least frequently
/// used in the last frame and this frame.
///
/// Because the OpenGL state is global, this class is a singleton.
class GLtextureSwapper {
 public:
  static GLtextureSwapper& Instance() {
    if (!g_instance_) g_instance_ = new GLtextureSwapper();
    return *g_instance_;
  }
  static void DestroyInstance() {
    if (g_instance_) delete g_instance_;
  }
  ~GLtextureSwapper();

  /// \brief Binds the texture.
  /// If the texture is bound to a texture unit, the count of that unit will be
  /// increased and it will be active (glActiveTexture()). If the texture is
  /// not bound, it will be bound to the least frequently used texture unit
  /// (during the last frame) or an unused unit. That texture unit is active
  /// after this call.
  void Bind(GLtexture& t);
  /// \brief Unbinds the texture.
  /// The unit it is bound to will be pushed to the unused stack.
  /// The previously active texture unit remains active.
  void Unbind(GLtexture& t);

  /// \brief Prepares for a new frame. Must be called after each frame.
  /// The previously active texture unit remains active.
  void BeginFrame();

  void SetActiveUnit(int u);
  int active_unit() const { return active_unit_; }

  /// \brief Temporarily prevents referenced textures from being swapped.
  /// This is used during materials / shaders that require multiple textures
  /// to be active at the same time. When this material binds all of its
  /// texture it could occur that one of its textures replaces another one of
  /// its textures which would cause wrong rendering output.
  /// Calling this function before using textures guarantees these textures
  /// to be active at the same time until Thaw() is called.
  /// Call Thaw() to resume normal scheduling.
  void Freeze();

  // \brief Forever freezes the specified unit.
  //
  // This means the texture swapper will NEVER use that texture unit. Even
  // Thaw() will NOT freeze that unit. This function is used to prevent using a
  // texture that is used by another low-level API, namely the Oculus SDK.
  void FreezeUnitForever(int unit);

  /// \brief Undoes the FreezeNew() operation
  /// After calling Thaw(), each texture may be subject to unbinding again.
  void Thaw();

 private:
  GLtextureSwapper();
  GLtextureSwapper(const GLtextureSwapper& copy) = delete;
  GLtextureSwapper(GLtextureSwapper&& move) = delete;
  GLtextureSwapper& operator=(const GLtextureSwapper& copy) = delete;
  GLtextureSwapper& operator=(GLtextureSwapper&& move) = delete;
  static GLtextureSwapper* g_instance_;

  /// \brief Binds the texture to the LRU texture unit.
  void Fault(GLtexture& t);

  void TryFreezeUnit(int unit);

  int num_texture_units_;

  std::vector<size_t> usage_count_[2];
  int current_index_;
  std::vector<GLtexture*> bound_textures_;
  std::vector<int> unused_units_;

  int active_unit_;

  std::vector<std::pair<int, size_t>> frozen_units_;
  bool frozen_;

  const size_t kFrozen = static_cast<size_t>(-1);
};

}

#endif

