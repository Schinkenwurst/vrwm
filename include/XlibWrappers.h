#ifndef VRWM_MODIFIER_H
#define VRWM_MODIFIER_H

#include <bitset>
#include <string>
#include <map>
#include <set>

#include <X11/Xlib.h>

// \file
// \brief Everything in this file are simply wrappers.
// For everything else, especially Keysym and key codes, the usual Xlib stuff
// is used.
namespace vrwm {

// \brief Bitset indices for modifiers.
// Those are the indices of the bits in ActiveModifiers (see below)
// The bitset is easier to write and comprehend imo.
enum ModifierIndex {
  ModifierInvalid =  -1,
  KeyboardShift =     0,
  KeyboardLock =      1,
  KeyboardControl =   2,
  KeyboardMod1 =      3,
  KeyboardMod2 =      4,
  KeyboardMod3 =      5,
  KeyboardMod4 =      6,
  KeyboardMod5 =      7,
  ModMouseButton1 =   8,
  ModMouseButton2 =   9,
  ModMouseButton3 =   10,
  ModMouseButton4 =   11,
  ModMouseButton5 =   12,

  ModifierSize = 13,
};

// \brief Get the name, keysym and keycode of all the modifier indices.
struct ModifierNames {
  // \brief The names of the indices.
  // e.g. "Shift" -> 0, "Lock" -> 1, etc.
  static const std::map<std::string, int> g_names;
  static std::set<KeyCode> g_modifiers;

  // \brief Gets a modifier index by name.
  static ModifierIndex GetModifier(const std::string& name);
  // \brief Gets the name of an index.
  static std::string GetName(ModifierIndex m);

  // \brief Returns the KeyCode of a modifier.
  // Note that currently modifiers that result from multiple simultaneos key
  // presses are not supported.
  static KeyCode GetKeycodeOfModifier(ModifierIndex m);

  // \brief Returns the modifier index of the key code.
  // If the KeyCode is not a valid modifier, ModifierInvalid will be returned.
  static ModifierIndex GetModifier(KeyCode c);

 private:
  // \brief Private ctor - Nothing to instantiate here.
  ModifierNames();
};

// \brief Encapsulation of mouse button into an enum.
// Because a function
//      void Pressed(MouseButton button);
// looks so much nicer than
//      void Pressed(int button);
enum MouseButton {
  MouseButton1 = Button1,
  MouseButton2 = Button2,
  MouseButton3 = Button3,
  MouseButton4 = Button4,
  MouseButton5 = Button5,
};

// \brief Typedef of a bitset with the active modifier keys.
// Typedeffed bitset to the modifiers so they can be accessed like this:
// if (modifier[KeyboardShift])
// Which is way nicer to read than
// if ((modifiers & ShiftMask) == ShiftMask)
//
// Use the values from ModifierIndex to access the bitset.
typedef std::bitset<ModifierSize> ActiveModifiers;

} //vrwm

#endif

