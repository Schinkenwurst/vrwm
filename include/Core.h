#ifndef CORE_H
#define CORE_H

#include <map>
#include <vector>

#include <glm/glm.hpp>
#include <X11/Xlib.h>

#include "Client.h"
#include "Configuration.h"
#include "Cursor.h"

//TODO: move display_ to somewhere global. I need it far to often

namespace vrwm {

class EventHandler;
class OculusManager;
class OpenGLManager;
class XInputDeviceManager;

namespace plugin {
class PluginManager;
}

// \brief The core of vrwm.
// This is where most of the window managing stuff happens.
// This class is a singleton.
class Core {
 public:
  // \brief Singleton initialization, happens in main().
  static int InitializeInstance();
  // \brief Gets the singleton instance.
  static Core& Instance();
  // \brief Destroys the singleton instance.
  static void DestroyInstance();
  // \brief Destructor.
  ~Core();

  // \brief Stops VRWM after the current frame.
  void Shutdown() { shutdown_ = true; }
  // \brief Restarts vrwm.
  // This means that the singleton instance is destroyed and the process is
  // restarted.
  static void RestartVRWM();
  // \brief Launches an arbitrary program.
  // May also contain parameters, they are passed correctly.
  static void Launch(const std::string& command);
  // \brief This function is called when errors in xlib occur.
  static int ErrorHandler(Display* dpy, XErrorEvent* err);
  // \brief This function is called when an error is logged to file.
  // It is used to tell GL to set the clear color to red temporarily
  static void LogErrorCallback(const std::string& err);

  // \brief Runs the main loop. Don't call this! Called in main().
  // \return 0 on success, error code on failure.
  int MainLoop();

  // \brief Returns the root window.
  Window GetRootWindow() const { return DefaultRootWindow(display_); }

  // \brief Returns true if the passed window is managed by vrwm.
  bool HasClient(Window id) { return managed_windows_.count(id); }
  // \brief Returns the Client with the passed window ID.
  // Core keeps ownership of the Client.
  // Returns nullptr if the client is not managed by vrwm.
  Client* GetClient(Window id);
  // \brief Same as above, but const.
  const Client& GetClient(Window id) const;

  // \brief Makes the passed client have input focus.
  // Called by EventHandler in response to FocusIn.
  void set_focus_client(Client& c);
  // \brief Takes input focus from the currently focused client.
  void unset_focus_client();
  // \brief Returns the currently focused client.
  // nullptr if no client has input focus.
  Client* focus_client() { return focus_client_; }
  // \brief Returns the currently focused client.
  // nullptr if no client has input focus.
  const Client* focus_client() const { return focus_client_; }

  // \brief Tells the Core that the cursor is currently not on any windows.
  // Generates an appropriate window exit event.
  void unset_cursor_client();
  // \brief Sets the client under the cursor.
  // Generates appropriate window enter / exit events.
  // \param normalized_coordinates Where the cursor is in the window.
  void set_cursor_client(Client& c, const glm::vec2& normalized_coordinates);
  // \brief Returns the client that is currently under the cursor.
  // nullptr if no client under cursor.
  Client* cursor_client() { return cursor_client_; }
  // \brief Returns the client that is currently under the cursor.
  // nullptr if no client under cursor.
  const Client* cursor_client() const { return cursor_client_; }

  // \brief Returns the Configuration.
  Configuration& config() { return config_; }
  // \brief Returns the Configuration.
  const Configuration& config() const { return config_; }
  // \brief Returns the plugin manager.
  plugin::PluginManager& plugin_manager() { return *plugin_manager_; }
  // \brief Returns the plugin manager.
  const plugin::PluginManager& plugin_manager() const {
    return *plugin_manager_;
  }
  // \brief Returns the oculus manager.
  OculusManager& oculus_manager() { return *oculus_manager_; }
  // \brief Returns the oculus manager.
  const OculusManager& oculus_manager() const { return *oculus_manager_; }
  // \brief Returns the OpenGL manager.
  OpenGLManager& openGL_manager() { return *openGL_manager_; }
  // \brief Returns the OpenGL manager.
  const OpenGLManager& openGL_manager() const { return *openGL_manager_; }
  // \brief Returns the virtual mouse cursor.
  VirtualCursor& cursor() { return cursor_; }
  // \brief Returns the virtual mouse cursor.
  const VirtualCursor& cursor() const { return cursor_; }
  // \brief Returns the event handler.
  EventHandler& event_handler() { return *event_handler_; }
  // \brief Returns the event handler.
  const EventHandler& event_handler() const { return *event_handler_; }
  // \brief Returns the XInput device manager.
  XInputDeviceManager& xinput_manager() { return *xinput_manager_; }
  // \brief Returns the XInput device manager.
  const XInputDeviceManager& xinput_manager() const { return *xinput_manager_; }

  // \brief Manages the window and returns the newly created client.
  // If the window is already managed, returns the appropriate client.
  Client* Manage(Window id);
  // \brief Makes the window not be managed anymore.
  // Does nothing if the window is not managed anyway.
  void Unmanage(Window id);

  // \brief Returns the X11 display.
  Display* display() const { return display_; }
  // \brief Returns the window that is designated for OpenGL rendering.
  Window GetOpenGLWindowID() const { return openGL_window_->ID(); }

  // \brief Returns the aspect ratio of the screen.
  float AspectRatio() const;

  float time() const { return time_since_start_; }
  // Returns the time in seconds that has passed since the last event has been
  // handled.
  float time_since_last_event() const { return time_since_last_event_; }
  // Returns the time in seconds that has passed since the last frame has been
  // rendered.
  float time_since_last_frame() const { return time_since_last_frame_; }

  // \brief Returns the size of the screen in pixel.
  glm::uvec2 GetScreenSize() const {
    Screen* s = ScreenOfDisplay(display_, screen_);
    return glm::uvec2(s->width, s->height);
  }

  // \brief Installs the passed colormap.
  // Wrapper around XInstallColormap().
  void InstallColormap(Colormap colormap) {
    XInstallColormap(display_, colormap);
  }

  // \brief Removes window from _NET_CLIENT_LIST and _NET_CLIENT_LIST_STACKING
  //
  // This is called when a window is unmapped.
  void RemoveFrom_NET_CLIENT_LISTs(Window window);
  // \brief Adds a window to _NET_CLIENT_LIST and _NET_CLIENT_LIST_STACKING.
  // The window will be prepended to net_client_list_stacking_ (i.e. it is
  // considered to be topmost) and net_client_list
  void AddTo_NET_CLIENT_LISTs(Window window);
  // \brief Updates the stacking order stored in _NET_CLIENT_LIST_STACKING.
  void Update_NET_CLIENT_LIST_STACKING(Window window, Window above);

 private:
  // \brief Private ctor to prevent manual instantiation of the singleton.
  Core();
  // \brief Private ctor to prevent manual instantiation of the singleton.
  Core(const Core& cc) = delete;
  // \brief Private operator= to prevent manual instantiation of the singleton.
  Core& operator=(const Core& other) = delete;

  // \brief Called during creation of the singleton. Sets up vrwm.
  int Init();

  // \brief Parses the config and the command line arguments.
  // Initializes the Configuration-Object.
  void ParseConfig();
  // \brief Initializes the plugin manager.
  void InitPlugins();
  // \brief Initializes the connection to the X server.
  void InitConnection();
  // \brief Makes the atoms of the Atom class available.
  void InitAtoms();
  // \brief Initializes the XComposite extension and creates the overlay window.
  void InitComposite();
  // \brief Initializes the XInput extension.
  void InitInput();
  // \brief Creates the OpenGL window which everything is rendered to.
  void InitOpenGLWindow();
  // \brief Initializes the core functionality of vrwm.
  void InitWindowManager();
  // \brief Executed after startup but before the main loop.
  void PostStartup();

  void Update_NET_ACTIVE_WINDOW(Client* c);

  // \brief Sets the _NET_CLIENT_LIST_STACKING properties of root.
  // The net_client_list_stacking_ member of the Core is used to set it.
  void Set_NET_CLIENT_LIST_STACKING();
  // Sets the _NET_CLIENT_LIST and _NET_CLIENT_LIST_STACKING properties of root.
  // The net_client_list_ member of the Core is used to set it.
  void Set_NET_CLIENT_LIST();

  // \brief The singleton instance;
  static Core* g_core_instance;
  // \brief Function pointer to the default xlib error handler.
  static XErrorHandler g_default_xlib_error_handler;


  Configuration config_;
  EventHandler* event_handler_;
  OculusManager* oculus_manager_;
  OpenGLManager* openGL_manager_;
  plugin::PluginManager* plugin_manager_;
  XInputDeviceManager* xinput_manager_;
  VirtualCursor cursor_;

  // \brief The X11 display.
  Display* display_;
  // \brief The screen which we are on.
  int screen_;
  // \brief The composite overlay window which is created by XComposite
  // Guaranteed to be above all other windows but below the screensaver.
  Client* composite_overlay_window_;
  // \brief The window which everything is rendered to.
  // Is a child of the composite_overlay_window.
  Client* openGL_window_;

  // \brief The list of the currently managed windows.
  // It's a map to simplify lookup of window ID -> client.
  std::map<Window, Client*> managed_windows_;
  // \brief The one client which currently has input focus.
  // Every keyboard input will be sent to this client and thus inserted
  // in e.g. a text field.
  Client* focus_client_;
  // \brief The one client which the cursor is currently over.
  // Used to generate the appropriate window entry/exit events.
  Client* cursor_client_;

  // \brief in seconds.
  float time_since_start_;
  // \brief in seconds.
  float time_since_last_event_;
  // \brief in seconds.
  float time_since_last_frame_;

  // \brief Is true if shutdown should occur after this frame.
  bool shutdown_;

  // \brief Used in the EWMH property _NET_CLIENT_LIST_STACKING.
  std::vector<Window> net_client_list_stacking_;
  // \brief Used in the EWMH property _NET_CLIENT_LIST.
  std::vector<Window> net_client_list_;
};

} //vrwm

#endif // CORE_H
