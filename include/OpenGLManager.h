#ifndef OPENGLMANAGER_H
#define OPENGLMANAGER_H

#include <math.h>
#include <set>
#include <string>
#include <vector>

#include <X11/Xlib.h>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include "Assets.h"
#include "draw/VertexGroup.h"
#include "draw/Material.h"

namespace vrwm {

class Core;
class Renderable;
struct Ray;
struct RaycastHit;
class SceneNode;

// \brief Main class used for rendering.
//
// Measurements of distances are in meters.
// Measurements of rotations are in radians.
class OpenGLManager {
 public:
  OpenGLManager(Core& core);
  ~OpenGLManager();

  // \brief Returns the position in view direction at the specified distance.
  glm::vec3 GetLookAt(float distance) const;

  // \brief Rotates the view.
  void Rotate(float angle_radians, const glm::vec3& axis);
  // \brief Sets the orientation.
  void set_orientation(const glm::quat& q) { orientation_ = q; }

  // \brief Adds a renderable to the rendering system.
  // The added renderable will used for rendering, raycasting, etc.
  void Add(Renderable* c) { renderables_.insert(c); }
  // \brief Removes a renderable from the rendering system.
  void Remove(Renderable* c) { renderables_.erase(c); }
  // \brief Checks if a renderable is registered here.
  bool HasRenderable(Renderable* c) {
    return renderables_.count(c);
  }

  // \brief Calls SetupViewMatrix() and SetupProjectionMatrix()
  void SetupMatrices();
  // \brief Calculates the view matrix.
  // Uses the offset from ground and current orientation to calculate a view
  // matrix that can be used for rendering.
  // The view matrix is located at GLglobal::g_view_matrix.
  void SetupViewMatrix();
  // \brief Calculates a projection matrix with a fixed fov.
  // The matrix is located at GLglobal::g_projection_matrix.
  void SetupProjectionMatrix();
  // \brief Sets a projection matrix.
  // The matrix is located at GLglobal::g_projection_matrix.
  void SetProjectionMatrix(const glm::mat4& m);
  // \brief Call before rendering to sort the renderables.
  void SortRenderables();
  // \brief Clears the screen with a nice and cozy blue.
  void Clear();
  // \brief Renders all Renderables in unspecified order.
  void Render();

  // \brief Recompiles GLprograms and relinks them.
  void RecompileAllPrograms();

  // \brief Draws a line between the two points.
  // The points are in world space.
  void DrawLine(const glm::vec3& start, const glm::vec3& end,
    const glm::vec4& color);
  void DrawLine(float sx, float sy, float sz, float ex, float ey, float ez,
    const glm::vec4& color) {
    DrawLine(glm::vec3(sx, sy, sz), glm::vec3(ex, ey, ez), color);
  }
  void DrawLine(const glm::vec3& start, const glm::vec3& end,
    float r, float g, float b, float a) {
    DrawLine(start, end, glm::vec4(r, g, b, a));
  }

  // \brief Returns a vector that points forwards.
  glm::vec3 GetForward() const {
    return orientation_ * glm::vec3(0.0f, 0.0f, -1.0f);
  }
  // \brief Returns a vector that points right.
  glm::vec3 GetRight() const {
    return orientation_ * glm::vec3(1.0f, 0.0f, 0.0f);
  }
  // \brief Returns a vector that points up.
  glm::vec3 GetUp() const {
    return orientation_ * glm::vec3(0.0f, 1.0f, 0.0f);
  }

  // \brief Returns the offset from {0, 0, 0}
  const glm::vec3 offset_from_ground() const {
    return offset_from_ground_;
  }

  // \brief Returns the orientation quaternion.
  const glm::quat& orientation() const {
    return orientation_;
  }
  // \brief Returns the orientation as a mat4.
  glm::mat4 GetOrientationAsMatrix() const {
    return glm::mat4_cast(orientation_);
  }

  // Calls Raycast() on all Renderables.
  bool RaycastAll(const Ray& r, RaycastHit& out_hit);

  // \brief Makes the screen flash red
  void ErrorOccured() {
    error_occurance_countdown_ = error_occurance_countdown_max_;
  }

#ifdef VRWM_DEBUG
  // \brief Returns a human readable string of an OpenGL error.
  static std::string FormatDebugOutputARB(GLenum source, GLenum type, GLuint id,
                                          GLenum severity, const char *message);
  // \brief OpenGL debug context error callback function.
  static void DebugCallbackARB(GLenum source, GLenum type, GLuint id,
                               GLenum severity, GLsizei length,
                               const GLchar* message, const void* user_param);
#endif

  void AddToRendering(Renderable& r);
  void AddToRendering(const std::set<Renderable*>& r);
  void RemoveFromRendering(Renderable& r);
  void RemoveFromRendering(const std::set<Renderable*>& r);

  SceneNode* root_scene_node() { return root_scene_node_; }
  const SceneNode* root_scene_node() const { return root_scene_node_; }
  SceneNode* billboard_target() { return billboard_target_; }
  const SceneNode* billboard_target() const { return billboard_target_; }

  uniblock::MatrixBlock& matrix_block() { return matrix_block_; }
  uniblock::SceneBlock& scene_block() { return scene_block_; }

 private:
  // \brief Initializes the OpenGLManager.
  void Init();
  void InitMaterials();

  draw::MaterialPtr AddBlinnPhongMaterial(Assets::MaterialType type,
      bool transparent, const std::set<std::string>& defines) {
    return AddMaterial(type, transparent, "/default.vert.glsl",
      "/blinnphong.frag.glsl", defines);
  }
  draw::MaterialPtr AddMaterial(Assets::MaterialType type, bool transparent,
    std::string vshader, std::string fshader,
    const std::set<std::string>& defines);

  void ConstructRay();

  void SetupMatrixBlock(const Renderable& r);
  void SetupSceneBlock();

  // \brief The Core instance.
  Core& core_;

  // \brief The view and projection matrix.
  static glm::mat4 g_view_matrix, g_projection_matrix;

  struct OpaqueRenderableComparator {
    bool operator()(const Renderable* lhs, const Renderable* rhs);
  };
  struct TransparentRenderableComparator {
    bool operator()(const Renderable* lhs, const Renderable* rhs);
  };

  std::vector<Renderable*> sorted_opaque_renderables_;
  std::vector<Renderable*> sorted_transparent_renderables_;

  SceneNode* root_scene_node_, *billboard_target_;

  std::set<Renderable*> renderables_;

  draw::VertexGroup ray_mesh_;
  draw::Material ray_material_;

  // \brief The view direction.
  glm::quat orientation_;
  // \brief The offset from {0, 0, 0}
  glm::vec3 offset_from_ground_;


  const glm::vec4 clear_color_normal_ = glm::vec4(0.2f, 0.4f, 0.9f, 1.0f);
  const glm::vec4 clear_color_error_  = glm::vec4(0.7f, 0.4f, 0.4f, 1.0f);
  const float error_occurance_countdown_max_ = 1.5f;
  // \brief is set to the above constant when an error occurs.
  float error_occurance_countdown_;

  uniblock::MatrixBlock matrix_block_;
  uniblock::SceneBlock scene_block_;
};

}

#endif // OPENGLMANAGER_H
