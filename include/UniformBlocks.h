#ifndef VRWM_UNIFORM_BLOCKS_H
#define VRWM_UNIFORM_BLOCKS_H

#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>

#include "GLbuffer.h"

namespace vrwm {

class UniformBlock : public GLbuffer<GL_UNIFORM_BUFFER> {
 public:
  UniformBlock(GLuint binding) : GLbuffer(), binding_(binding) { }
  GLuint binding() const { return binding_; }
  void BindBlock() {
    CALL_GL(glBindBufferBase(GL_UNIFORM_BUFFER, binding_, buffer()));
  }
 private:
  GLuint binding_;
};

namespace uniblock {

class MatrixBlock : public UniformBlock {
 public:
  static_assert(sizeof(glm::mat4) == 64, "4x4 matrix must be 64 bytes long.");
  MatrixBlock() : UniformBlock(0) {
    SetData(normal2.end(), nullptr, GL_DYNAMIC_DRAW);
  }
  // See the default vertex shader for why the offsets are at that position.
  BufferMember<glm::mat4,   0> projection;
  BufferMember<glm::mat4,  64> view;
  BufferMember<glm::mat4, 128> model;
  BufferMember<glm::mat4, 192> mvp;
  // mat3 has gaps in block.
  BufferMember<glm::vec3, 256> normal0;
  BufferMember<glm::vec3, 272> normal1;
  BufferMember<glm::vec3, 288> normal2;
};

class SceneBlock : public UniformBlock {
 public:
  static_assert(sizeof(glm::vec3) == 12, "vec3 must be 12 bytes long.");
  SceneBlock() : UniformBlock(1) {
    SetData(time.end(), nullptr, GL_DYNAMIC_DRAW);
  }
  // See the default vertex shader for why the offsets are at that position.
  BufferMember<glm::vec3,  0> camera_position;
  BufferMember<glm::vec3, 16> camera_direction;
  BufferMember<float,     28> time;
};

}
}

#endif

