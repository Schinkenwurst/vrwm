#ifndef VRWM_EVENTHANDLER_H
#define VRWM_EVENTHANDLER_H

#include <X11/extensions/XI2.h>

#include "XlibWrappers.h"

namespace vrwm {


class Core;
// \brief Handles incoming events from the X server.
//
// By default, the EventHandler automatically executes all actions that the
// mouse and keyboard usually exceutes. Such as: Notify windows of mouse
// movement, notify windows that the pointer has entered / left the window,
// clicks of buttons 1 through 5, keyboard input, etc.
// This behaviour can be disabled by plugins by implementing the appropriate
// receiver and returning false.
// You then need to take of these actions yourself.
//
// For processing, each event type is bound to zero or one function of the
// EventHandler. These functions are specified in an array which allows instant
// lookups (at the price of only one function per event).
class EventHandler {
 public:
  friend class Core;
  // Typedef of a function pointer of functions   void foo(const XEvent& evt)
  typedef void (EventHandler::*EventHandlerFunc)(const XEvent& evt);
  typedef void (EventHandler::*GenericEventHandlerFunc)(
      const XGenericEventCookie& evt);
  typedef void (EventHandler::*ClientMessageHandlerFunc)(
      const XClientMessageEvent& evt);

  EventHandler();
  ~EventHandler();

  // \brief Calls the appropriate handler functions for all queued events.
  void HandleQueuedEvents();

  void EventConfigureRequest(const XEvent& event);
  void EventMapRequest(const XEvent& event);
  void EventMapNotify(const XEvent& event);
  //void resizeEvent(const XEvent& event);
  void EventKeyPressOrReleased(const XEvent& event);
  void EventDestroyNotify(const XEvent& event);
  void EventConfigureNotify(const XEvent& event);
  void EventExpose(const XEvent& event);
  void EventUnmapNotify(const XEvent& event);
  void EventFocusChange(const XEvent& event);
  void EventClientMessage(const XEvent& event);
  void EventPropertyChange(const XEvent& event);
  void EventWindowCrossing(const XEvent& event);
  void EventColormap(const XEvent& event);

  void XIEventRawMotion(const XGenericEventCookie& event);
  void XIEventButton(const XGenericEventCookie& event);

  // Client message event handler declarations
  // CMsg = Client Message
  void CMsgWM_CHANGE_STATE(const XClientMessageEvent& e);
  void CMsg_NET_ACTIVE_WINDOW(const XClientMessageEvent& e);
  void CMsg_NET_CLOSE_WINDOW(const XClientMessageEvent& e);
  void CMsg_NET_MOVERESIZE_WINDOW(const XClientMessageEvent& e);
  void CMsg_NET_REQUEST_FRAME_EXTENTS(const XClientMessageEvent& e);
  void CMsg_NET_WM_DESKTOP(const XClientMessageEvent& e);
  void CMsg_NET_WM_STATE(const XClientMessageEvent& e);
  void CMsg_NET_WM_PING(const XClientMessageEvent& e);

  static const char* GetReadableEventType(int type);
  // \brief same for XInputExtension events
  static const char* GetReadableXIEventType(int xi2_type);

  const ActiveModifiers& active_modifiers() {
    return active_modifiers_;
  }

  Time current_time() { return current_time_; }
  bool mouse_grabbed() const { return mouse_grabbed_; }

 private:
  void SetXInputExtensionData(int opcode, int eventbase, int errorbase);
  void IncrementTime(Time t) { current_time_ += t; }

  // \brief Contains all function pointers to the event handlers.
  // Indices are the event types. Thus the lookup is in O(1).
  EventHandlerFunc event_handlers_[LASTEvent];
  // \brief Contains all function pointers to event handlers of XInput events.
  // They take a XGenericEventCookie as parameter which they use to get the
  // data and cast it to the correct type.
  GenericEventHandlerFunc xi_event_handlers_[XI_LASTEVENT];

  // \brief Contains all function pointer to handlers for XClientMessageEvents.
  // Because the number of possible Atoms is not constant, we have to implement
  // it as map.
  std::map<Atom, ClientMessageHandlerFunc> client_message_handlers_;

  // \brief True if the mouse is currently grabbed by any client.
  // See the implementation of EventWindowCrossing for details on why this must
  // be detected.
  bool mouse_grabbed_;
  // \brief Number of button 1 releases since the mouse has been grabbed.
  // See XIEVentButton() for details on why this is required.
  int releases_since_mouse_grab_;
  // \brief OpCode, event base and error base of XInput2 events.
  // For XInput2 stuff see XInputDeviceManager.
  int xi2_opcode_ = 0, xi2_event_ = 0, xi2_error_ = 0;

  ActiveModifiers active_modifiers_;

  // \brief The current x server time.
  // Updated on each event as well as by the Core's main loop.
  Time current_time_ = 0;
};

} // vrwm

#endif // EVENTHANDLER
