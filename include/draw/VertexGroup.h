#ifndef VRWM_VERTEX_GROUP_H
#define VRWM_VERTEX_GROUP_H

#include <string>
#include <vector>

#include "../GLglobal.h"
#include "../GLprogram.h"
#include "../EventHandler.h"
#include "Material.h"

namespace vrwm {

struct RaycastHit;

namespace draw {

class VertexGroup;
typedef std::shared_ptr<VertexGroup> VertexGroupPtr;

class VertexAttribute;

// \brief A VertexGroup is used for rendering.
// It defines the vertices and the material with which those vertices should be
// drawn.
//
// Vertex groups can also be marked as raycastable which allows the
// renderable to execute ray casts against those triangles.
// When raycasts should be performed, the draw mode must either be triangle,
// triangle strip or triangle fan. Trying to ray cast agains any other
// primitive will cause an exception.
class VertexGroup {
 public:
  // \brief A triangle that is used during ray casting. Local positions.
  struct RaycastableTriangle {
    glm::vec3 v1, v2, v3;
  };

  // \brief An iterator that iterates over all raycastable triangles.
  // You should usually not need to use this.
  // Usage is just like usual:
  // \code
  //    RaycastableTriangleIterator current = group.begin_raycastable_triangles();
  //    RaycastableTriangleIterator end = group.end_raycastable_traingles();
  //    for (; current != end; ++current) {
  //      RaycastableTriangle& triangle = *current;
  //      ...
  //    }
  // \endcode
  // \throws std::invalid_operation If mode is not triangle, triangle strip or
  //   triangle fan.
  class RaycastableTriangleIterator :
    std::iterator<std::input_iterator_tag, RaycastableTriangle> {
   public:
    friend class VertexGroup;
    typedef const RaycastableTriangle& const_reference;
    typedef const RaycastableTriangle* const_pointer;

    RaycastableTriangleIterator(const VertexGroup* v);
    RaycastableTriangleIterator(const RaycastableTriangleIterator& cc);
    RaycastableTriangleIterator& operator=(
       const RaycastableTriangleIterator& other);
    bool operator==(const RaycastableTriangleIterator& other) const;
    bool operator!=(const RaycastableTriangleIterator& other) const;
    const_reference operator*() const;
    const_pointer operator->() const;
    RaycastableTriangleIterator& operator++();
    const RaycastableTriangleIterator operator++(int);
   private:
    void Cache();
    // \brief Returns the address of the current vertex.
    // The current vertex is in the data of the vertex group.
    const float* CalculateVertexPointer(size_t i) const;

    // \brief Cached value of for easy dereference.
    RaycastableTriangle cached_value_;
    // \brief The vertex group this iterator iterates over.
    const VertexGroup* vertex_group_;
    // \brief The current position in the vertex_group.
    size_t position_;
  };

  VertexGroup();
  VertexGroup(const VertexGroup& cc);
  VertexGroup(VertexGroup&& c) noexcept;
  ~VertexGroup();

  VertexGroup& operator=(const VertexGroup& right);
  VertexGroup& operator=(VertexGroup&& move) noexcept;

  // \brief Returns the OpenGL handle of the vertex buffer object.
  GLuint vertex_buffer() const { return vertex_buffer_; }
  // \brief Returns the OpenGL handle of the vertex array object.
  GLuint vertex_array() const { return vertex_array_; }

  // \brief Completely replaces the current data in the buffer.
  // Wrapper arround glBufferData().
  void SetData(const void* new_data, GLsizeiptr byte_size);

  // \brief Modifies a portion of the buffer.
  // Wrapper around glBufferSubData(). Thus, it also reports an error if offset
  // + size defines a range beyond the bounds of the buffer's data store (the
  // size specified in SetData()).
  // \throws std::out_of_range if data is too large or out of range.
  void ModifyData(void* replacement_data, GLintptr offset,
      GLsizeiptr data_size);
  // \brief Returns true if the VertexGroup has some data.
  bool HasData() const { return vertex_data_ != nullptr; }

  // \brief Sets the parameters used for glDrawElements().
  void SetElements(GLenum mode, const std::vector<GLuint>& elements);

  // \brief Returns true if this vertex group is raycastable.
  bool is_raycastable() const { return is_raycastable_; }
  // \brief To enable raycast, call AddRaycastableVertexAttribute()
  void DisableRaycast() { is_raycastable_ = false; }

  // \brief Returns an iterator to the start of the raycastable triangles.
  RaycastableTriangleIterator begin_raycastable_triangles() const;
  // \brief Returns a past-the-end iterator of the raycastable triangles.
  RaycastableTriangleIterator end_raycastable_triangles() const;

  // \brief Adds a VertexAttribute that should be hit by ray casts.
  //
  // VertexGroup must have a valid GLprogram set. Also sets the specified
  // vertices as raycastable.
  // Assumes the following: Each vertex are 3 GL_FLOATs (positions).
  // Only one vertex attribute can be set as raycastable at the same time.
  //
  // You probably want to use this function to set the vertex positions
  // (i.e. the attribute named "vertexPos" when using the default shaders).
  // The only exception to this rule is if you want to create a Renderable that
  // is not supposed to be raycastable.
  void AddRaycastableVertexAttribute(const ResolvedVertexAttribute& a);
  // \brief Adds a vertex attribute.
  void AddVertexAttribute(const ResolvedVertexAttribute& a);

  void Render();

 private:
  // \brief Local copy of the data sent to the vertex buffer.
  // Used in raycasts to prevent fetching the exact same data from the graphics
  // hardware which would be slow.
  // TODO: Low priority: ALL the vertex data are stored in here, not just the
  //  positions that are used for ray casting. This consumes a bit of RAM.
  void* vertex_data_;
  GLsizeiptr vertex_data_size_;

  // \brief byte stride between the raycastable vertices.
  // Iterator assumes 3 GL_FLOATs per vertex.
  GLsizei raycastable_stride_;
  // \brief byte offset at the start of vertex data until raycastable data.
  const GLvoid* raycastable_offset_;
  // \brief Does this VertexGroup have a raycastable vertex attribute?
  bool is_raycastable_;

  // \brief The vertex buffer object.
  GLuint vertex_buffer_;
  // \brief The vertex array object.
  GLuint vertex_array_;

  GLuint index_buffer_;

  // \brief The mode in glDrawElements().
  // i.e. one of: GL_POINTS, GL_LINE_STRIP, GL_LINE_LOOP, GL_LINES,
  // GL_LINE_STRIP_ADJACENCY, GL_LINES_ADJACENCY, GL_TRIANGLE_STRIP,
  // GL_TRIANGLE_FAN, GL_TRIANGLES, GL_TRIANGLE_STRIP_ADJACENCY and
  // GL_TRIANGLES_ADJACENCY
  //
  // Default: triangle strip
  GLenum rendering_mode_;
  GLenum elements_type_;
  // \brief Amount of vertices that are used for rendering.
  // For now, we'll simply enforce integers. Might be a bit wasteful.
  size_t num_elements_;
};

} // draw
} // vrwm

#endif // VRWM_VERTEX_GROUP_H

