#ifndef VRWM_DRAW_UNIFORM_UPLOADER_H
#define VRWM_DRAW_UNIFORM_UPLOADER_H

#include <glm/glm.hpp>
#include <GL/glew.h>

#include "../GLtexture.h"

namespace vrwm {
namespace draw {

template<class T, size_t N>
struct UniformUploader {
  //void operator()(GLint location, size_t count, T* t);
  void operator()(GLint location, T** t) {
    operator()(location, *t);
  }
};

template<size_t N> struct UniformUploader<GLfloat, N> {
  void operator()(GLint location, GLfloat* t) {
    glUniform1fv(location, N, t);
  }
};
template<size_t N> struct UniformUploader<glm::vec2, N> {
  void operator()(GLint location, glm::vec2* t) {
    glUniform2fv(location, N, &(*t)[0]);
  }
};
template<size_t N> struct UniformUploader<glm::vec3, N> {
  void operator()(GLint location, glm::vec3* t) {
    glUniform3fv(location, N, &(*t)[0]);
  }
};
template<size_t N> struct UniformUploader<glm::vec4, N> {
  void operator()(GLint location, glm::vec4* t) {
    glUniform4fv(location, N, &(*t)[0]);
  }
};
template<size_t N> struct UniformUploader<GLint, N> {
  void operator()(GLint location, GLint* t) {
    glUniform1iv(location, N, t);
  }
};
template<size_t N> struct UniformUploader<glm::ivec2, N> {
  void operator()(GLint location, glm::ivec2* t) {
    glUniform2iv(location, N, &(*t)[0]);
  }
};
template<size_t N> struct UniformUploader<glm::ivec3, N> {
  void operator()(GLint location, glm::ivec3* t) {
    glUniform3iv(location, N, &(*t)[0]);
  }
};
template<size_t N> struct UniformUploader<glm::ivec4, N> {
  void operator()(GLint location, glm::ivec4* t) {
    glUniform4iv(location, N, &(*t)[0]);
  }
};
template<size_t N> struct UniformUploader<unsigned int, N> {
  void operator()(GLint location, unsigned int* t) {
    glUniform1uiv(location, N, t);
  }
};
template<size_t N> struct UniformUploader<glm::uvec2, N> {
  void operator()(GLint location, glm::uvec2* t) {
    glUniform2uiv(location, N, &(*t)[0]);
  }
};
template<size_t N> struct UniformUploader<glm::uvec3, N> {
  void operator()(GLint location, glm::uvec3* t) {
    glUniform3uiv(location, N, &(*t)[0]);
  }
};
template<size_t N> struct UniformUploader<glm::uvec4, N> {
  void operator()(GLint location, glm::uvec4* t) {
    glUniform4uiv(location, N, &(*t)[0]);
  }
};
template<size_t N> struct UniformUploader<GLtexturePtr, N> {
  void operator()(GLint location, GLtexturePtr* t) {
    GLint units[N];
    for (size_t i = 0; i < N; ++i) {
      units[i] = t[i]->Use();
    }
    glUniform1iv(location, N, units);
  }
};
template<size_t N> struct UniformUploader<glm::mat2, N> {
  void operator()(GLint location, glm::mat2* t) {
    glUniformMatrix2fv(location, N, false, &(*t)[0][0]);
  }
};
template<size_t N> struct UniformUploader<glm::mat3, N> {
  void operator()(GLint location, glm::mat3* t) {
    glUniformMatrix3fv(location, N, false, &(*t)[0][0]);
  }
};
template<size_t N> struct UniformUploader<glm::mat4, N> {
  void operator()(GLint location, glm::mat4* t) {
    glUniformMatrix4fv(location, N, false, &(*t)[0][0]);
  }
};
template<size_t N> struct UniformUploader<glm::mat2x3, N> {
  void operator()(GLint location, glm::mat2x3* t) {
    glUniformMatrix2x3fv(location, N, false, &(*t)[0][0]);
  }
};
template<size_t N> struct UniformUploader<glm::mat3x2, N> {
  void operator()(GLint location, glm::mat3x2* t) {
    glUniformMatrix3x2fv(location, N, false, &(*t)[0][0]);
  }
};
template<size_t N> struct UniformUploader<glm::mat2x4, N> {
  void operator()(GLint location, glm::mat2x4* t) {
    glUniformMatrix2x4fv(location, N, false, &(*t)[0][0]);
  }
};
template<size_t N> struct UniformUploader<glm::mat4x2, N> {
  void operator()(GLint location, glm::mat4x2* t) {
    glUniformMatrix4x2fv(location, N, false, &(*t)[0][0]);
  }
};
template<size_t N> struct UniformUploader<glm::mat3x4, N> {
  void operator()(GLint location, glm::mat3x4* t) {
    glUniformMatrix3x4fv(location, N, false, &(*t)[0][0]);
  }
};
template<size_t N> struct UniformUploader<glm::mat4x3, N> {
  void operator()(GLint location, glm::mat4x3* t) {
    glUniformMatrix4x3fv(location, N, false, &(*t)[0][0]);
  }
};

}
}

#endif

