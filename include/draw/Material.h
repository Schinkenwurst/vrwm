#ifndef VRWM_MESH_H
#define VRWM_MESH_H

#include <cassert>
#include <cstdlib>
#include <functional>
#include <memory>

#include <GL/glew.h>
#include <GL/gl.h>

#include "../GLprogram.h"
#include "../GLtexture.h"
#include "../LogManager.h"
#include "Uniform.h"

namespace vrwm {

namespace draw {

class Material;
typedef std::shared_ptr<Material> MaterialPtr;

// A material holds a specific program as well as uniforms for this program.
// When a material is copied (i.e. by using a ctor or by using foo = bar) the
// uniforms will be deep copied while the program remains a shallow copy.
// Rationale: Programs don't change while Uniforms do very often.
class Material {
 public:
  // Dummy constructor for later assigning.
  Material();
  Material(GLprogramPtr program, bool transparent = false);
  Material(const Material& copy);
  ~Material();

  void Use();
  uint32_t hash() const {
    return (program_->id() << 16) | id_;
  }
  bool transparent() const { return transparent_; }
  GLprogramPtr program() { return program_; }
  const GLprogramPtr program() const { return program_; }

  void RecompileProgram();

  //TODO: Allow changing of program (auto update uniform locations)

  bool HasUniform(const std::string& name) {
    return program_->Uniform(name) != -1;
  }

  template<class T, size_t N = 1, class Uploader = UniformUploader<T, N> >
  Uniform<T, N, Uploader>* GetUniform(const std::string& name,
    bool verbose = true);
  template<class T, size_t N = 1, class Uploader = UniformUploader<T, N> >
  Uniform<T, N, Uploader>* GetUniform(GLint location);

  template<class T, size_t N = 1, class Uploader = UniformUploader<T, N> >
  void SetUniform(const std::string& name, const T& value, bool verbose = true);
  template<class T, size_t N = 1, class Uploader = UniformUploader<T, N> >
  void SetUniform(GLint location, const T& value);

  void DestroyUniform(GLint location);
  void DestroyAllUniforms();
  //TODO uniform blocks

  Material& operator=(const Material& other);

 private:
  Material(Material&& move);
  Material& operator=(Material&& move);

  // \brief Checks if other matches the template parameters.
  // Returns false if type T differs or if N is larger than other's N (because
  // this could lead to out-of-bounds). If the types are equal and if N is
  // smaller than or equal to other's N true is returned. The Uniform can
  // safely be used and no out-of-bounds access can occur.
  template <class T, size_t N>
  bool IsSameUniformType(AbstractUniform* other, const std::string& name);

#ifndef VRWM_DISCARD_INACTIVE_UNIFORMS
  template <class T, size_t N, class Uploader>
  Uniform<T, N, Uploader>* GetInactiveUniform(const std::string& name);
#endif

  //EnableCallbackFunc on_enable_;

  uint16_t id_;
  static uint16_t g_next_regular_ID_;
  static std::vector<uint16_t> g_unused_IDs_;
  static uint16_t GetNextID();

  bool transparent_;

  // \brief The GLSL program which is used during rendering.
  GLprogramPtr program_;

  std::map<GLint, AbstractUniform*> uniforms_;
#ifndef VRWM_DISCARD_INACTIVE_UNIFORMS
  // \brief Uniforms with location -1. Identified by their name.
  std::map<std::string, AbstractUniform*> inactive_uniforms_;
#endif
};

template<class T, size_t N, class Uploader>
Uniform<T, N, Uploader>* Material::GetUniform(const std::string& name,
    bool verbose) {
  GLint location = program_->Uniform(name, !verbose);
  if (location == -1) {
#ifdef VRWM_DISCARD_INACTIVE_UNIFORMS
    if (verbose) {
      vrwm_clog(Warning, "No uniform with name '%s'.", name.c_str());
    }
    return nullptr;
#else
    return GetInactiveUniform<T, N, Uploader>(name);
#endif
  }
  auto it = uniforms_.find(location);
  if (it != uniforms_.end()) {
    if (!IsSameUniformType<T, N>(it->second, name)) {
      return nullptr;
    } else {
      return static_cast<Uniform<T, N, Uploader>*>(it->second);
    }
  } else {
    auto uniform = new Uniform<T, N, Uploader>(name);
    uniform->set_location(location);
    uniforms_[location] = uniform;
    return uniform;
  }
}

#ifndef VRWM_DISCARD_INACTIVE_UNIFORMS
template <class T, size_t N, class Uploader>
Uniform<T, N, Uploader>* Material::GetInactiveUniform(const std::string& name) {
  auto it = inactive_uniforms_.find(name);
  if (it != inactive_uniforms_.end()) {
    if (!IsSameUniformType<T, N>(it->second, name)) {
      return nullptr;
    } else {
      return static_cast<Uniform<T, N, Uploader>*>(it->second);
    }
  } else {
    vrwm_clog(Debug, "Uniform with name '%s' not found. I will store it "
      "anyway in case you want to recompile the shaders on the fly.",
      name.c_str());
    auto uniform = new Uniform<T, N, Uploader>(name, -1);
    inactive_uniforms_[name] = uniform;
    return uniform;
  }
}
#endif

template<class T, size_t N, class Uploader>
Uniform<T, N, Uploader>* Material::GetUniform(GLint location) {
  assert(location != -1);
  auto it = uniforms_.find(location);
  if (it != uniforms_.end()) {
    std::string name = "location: " + std::to_string(location);
    if (!IsSameUniformType<T, N>(it->second, name)) {
      return nullptr;
    } else {
      return static_cast<Uniform<T, N, Uploader>*>(it->second);
    }
  } else {
    auto uniform = new Uniform<T, N, Uploader>(location);
    uniforms_[location] = uniform;
    return uniform;
  }
}

template <class T, size_t N>
bool Material::IsSameUniformType(AbstractUniform* other,
    const std::string& name) {
  if (!other->EqualTypes<T>()) {
    vrwm_clog(Error, "Uniform (%s) is not of type '%s'. "
      "It is of type '%s'.", name.c_str(),
      typeid(T).name(), other->type.name());
    return false;
  }
  if (!other->EqualSizes<N>()) {
    if (other->size() < N) {
      vrwm_clog(Error, "Uniform (%s) has less array indices than "
        "expected. It has %u but the caller assumes %u.", name.c_str(),
        other->size(), N);
      return false;
    } else {
      vrwm_clog(Warning, "Uniform (%s) has more indices than "
        "expected. It has %u but the caller assumes %u. It will be used.",
        name.c_str(), other->size(), N);
      // normal return value accepted. The caller cannot go out-of bounds.
    }
  }
  return true;
}

template<class T, size_t N, class Uploader>
void Material::SetUniform(const std::string& name, const T& value,
    bool verbose) {
  auto uniform = GetUniform<T, N, Uploader>(name, verbose);
  if (uniform) {
    uniform->set(value);
  }
}

template<class T, size_t N, class Uploader>
void Material::SetUniform(GLint location, const T& value) {
  auto uniform = GetUniform<T, N, Uploader>(location);
  if (uniform) {
    uniform->set(value);
  }
}

}
}

#endif

