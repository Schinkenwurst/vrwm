#ifndef VRWM_DRAW_UNIFORM_H
#define VRWM_DRAW_UNIFORM_H

#include <cassert>
#include <functional>
#include <string>
#include <typeindex>

#include <GL/glew.h>

#include "UniformUploader.h"

namespace vrwm {
namespace draw {

class AbstractUniform {
 public:
  AbstractUniform(const std::type_index& type, std::string name,
      GLint location) : type(type), name_(std::forward<std::string>(name)),
      location_(location) {
  }
  virtual ~AbstractUniform() { }
  virtual void Use() = 0;
  virtual AbstractUniform* Clone() const = 0;
  virtual size_t size() const = 0;
  template <class T>
  bool EqualTypes() { return std::type_index(typeid(T)) == type; }
  template <size_t N>
  bool EqualSizes() { return N == size(); }
  const std::type_index type;

  // \brief Material calls this to tell the Uniform of its location.
  void set_location(GLint l) { location_ = l; }

  // \brief Returns the name of the Uniform variable.
  const std::string& name() const { return name_; }
  // \brief Returns the location.
  GLint location() const { return location_; }

 protected:
  // \brief The Uniform variable's name.
  const std::string name_;
  // \brief The Uniform variable's location in the GLSL program.
  // The location of the uniform is only available after the Uniform has been
  // added to a VertexGroup with a valid GLprogram.
  GLint location_;
};

// \brief Represents a uniform variable in the shader program code.
template<class T, size_t N = 1, class Uploader = UniformUploader<T, N> >
class Uniform : public AbstractUniform {
 public:

  // \brief Constructs the Uniform.
  // \param name The name of the uniform variable.
  // \param f The callback function which is called on enable.
  Uniform(std::string name) : AbstractUniform(typeid(T),
      std::forward<std::string>(name), -1) {
  }
  Uniform(GLint location) : AbstractUniform(typeid(T), "", location) {
  }
  Uniform(std::string name, GLint location) : AbstractUniform(typeid(T),
      std::forward<std::string>(name), location) {
  }
  Uniform(const Uniform& cc) : AbstractUniform(typeid(T), cc.name_,
      cc.location_) {
    for (size_t i = 0; i < N; ++i) {
      value_[i] = cc.value_[i];
    }
  }

  AbstractUniform* Clone() const {
    return new Uniform<T, N, Uploader>(*this);
  }

  Uniform& operator=(const Uniform& copy) {
    name_ = copy.name_;
    location_ = copy.location_;
    value_ = copy.value_;
    return *this;
  }

  size_t size() const { return N; }

  void set(const T& value, size_t i = 0) { assert(i < N); value_[i] = value; }
  T& value(size_t i = 0) { assert(i < N); return value_[i]; }
  const T& value(size_t i = 0) const { assert(i < N); return value_[i]; }

  virtual void Use() final {
    if (location_ == -1) {
      return;
    }
    Uploader()(location_, value_);
  }

 private:
  T value_[N];
};

}
}

#endif // VRWM_DRAW_UNIFORM_H
