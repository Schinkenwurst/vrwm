#ifndef VRWM_OCULUS_MANAGER_H
#define VRWM_OCULUS_MANAGER_H

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

// Inclusion of OVR_CAPI_GL MUST happen after GLglobal.h because we need to
// include glxew.h before libOVR includes glx.h
#include "GLglobal.h"
#include "../OculusSDK/LibOVR/Include/OVR.h"
#include "../OculusSDK/LibOVR/Src/OVR_CAPI_GL.h"

namespace vrwm {

class Core;

// \brief Handles setup for the oculus rift.
class OculusManager {
 public:
  // NoOculus: Don't use Oculus
  // Simulated: Use stereo rendering, does not need a physical device
  // Full: Use the Oculus Rift. Also use its input.
  //
  // Full also falls back to a simulated device if no real device is present.
  enum class UsageMode {
    NoOculus, Simulated, Full
  };

  OculusManager(Core& core, UsageMode mode);
  ~OculusManager();

  // \brief Renders a frame for the Oculus Rift.
  // Uses several of OpenGLManager's function.
  void Render();

  // \brief Converts a quaternion from ovr to glm
  static glm::quat OvrToGlm(const ovrQuatf& q) {
    return glm::quat(q.w, q.x, q.y, q.z);
  }
  // \brief Converts a vec3 from ovr to glm
  static glm::vec3 OvrToGlm(const ovrVector3f& v) {
    return glm::vec3(v.x, v.y, v.z);
  }
  // \brief Converts a mat4 from ovr to glm
  static glm::mat4 OvrToGlm(const ovrMatrix4f& m) {
    glm::mat4 r;
    for (int row = 0; row < 4; ++row)
      for (int col = 0; col < 4; ++col)
        r[col][row] = m.M[col][row];
    return r;
  }

  // \brief Returns the usage mode.
  UsageMode mode() const { return mode_; }

 private:
  // \brief Initializes the OculusManager
  // \throws std::runtime_error on error
  void Init();
  // \brief Sets up frame buffers for rendering.
  // \throws std::runtime_error on error.
  void InitRendering();

  // \brief The instance of the core.
  Core& core_;

  UsageMode mode_;
  ovrHmd head_mounted_display_;
  ovrHmdDesc description_;
  bool rendering_only_;
  //ovrSizei texture_size_;

  ovrEyeRenderDesc eye_render_desc_[2];
  ovrSizei eye_render_size_;
  ovrGLTexture eye_texture_[2];

  GLuint color_texture_;
  GLuint depth_renderbuffer_;
  GLuint framebuffer_;
};

}

#endif
