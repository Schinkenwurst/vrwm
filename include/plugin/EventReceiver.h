#ifndef EVENTRECEIVER_H
#define EVENTRECEIVER_H

#include <vector>

#include <X11/Xlib.h>
#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>

#include "../EventHandler.h"
#include "../Version.h"

// \file
// \brief This file declares everything you need to implement your own plugin.
//
// A brief tutorial on how to write a plugin:
//
// In one of your header files use the VRWM_PLUGIN_INFO macro.
// A Setup() and Teardown() should create and destroy an object of your class
// with new and delete.
// That class should inherit from one or more of the event receivers specified
// below. You can use the priority in the constructor to control the execution
// order of the event receiver. Lower priorities are called first.
//
// That's all. You will automatically receive the corresponding events.
//
// Code example which prints "Hello World!" after each frame
// (irrelevant stuff ommitted):
// \code
//    //.h
//    extern void Setup();
//    extern void Teardown();
//    VRWM_PLUGIN_INFO("Hello World", "A simple hello world plugin",
//      0, 1, Setup, Teardown);
//    class HelloWorldPlugin : public RenderEventReceiver {
//      static HelloWorldPlugin* g_Instance; // Plugins are singletons.
//      void PostRender(); // Called after each frame
//    };
//
//    //.cpp
//    void Setup() { HelloWorldPlugin::g_Instance = new HelloWorldPlugin(); }
//    void Teardown() { delete HelloWorldPlugin::g_Instance; }
//    //lout is a macro which rolls out to
//    //  vrwm::LogManager::Instance() << vrwm::INFO
//    void HelloWorld::PostRender() { vrwm::lout << "Hello World!" << endl; }
// \endcode
//
// For any receiver function that returns a bool the following rule applies:
// If your event receiver returns false, no other event receiver with a priority
// that is higher than yours will receive that event. Additionally, VRWM will
// also not handle that event.

#define EXPORT __attribute__ ((visibility ("default")))

// \brief Defines all neccessary data to load your plugin.
// Use this macro in a .h file to specify all neccessary data of your plugin.
// If you don't use it, VRWM will most likely not be able to load your plugin.
#define VRWM_PLUGIN_INFO(name, description, plugin_version_major, \
    plugin_version_minor, setup_function, teardown_function) \
  EXPORT extern const char* const _vrwm_plugin_name = name; \
  EXPORT extern const char* const _vrwm_plugin_description = description; \
  EXPORT extern int const _vrwm_required_version_major = VRWM_PLUGIN_VERSION_MAJOR; \
  EXPORT extern int const _vrwm_required_version_minor = VRWM_PLUGIN_VERSION_MINOR; \
  EXPORT extern int const _vrwm_plugin_version_major = plugin_version_major; \
  EXPORT extern int const _vrwm_plugin_version_minor = plugin_version_minor; \
  EXPORT extern void(* const _vrwm_plugin_setup_function)() = setup_function; \
  EXPORT extern void(* const _vrwm_plugin_teardown_function)() = teardown_function;

namespace vrwm {

// Forward declarations
class Client;
class Renderable;

namespace plugin {

// \brief The ID of the plugin class type.
// Every type of receiver must have such an ID.
// It is used by the plugin manager to efficiently register all different
// kinds of receivers in an array.
enum class ReceiverID : size_t {
  RenderEventReceiver      = 0,
  XlibEventReceiver        = 1,
  MouseCursorMovedReceiver = 2,
  ProgrammStatusReceiver   = 3,
  MouseClickReceiver       = 4,
  KeyReceiver              = 5,
  RenderableMovedReceiver  = 6,
  ClientEventReceiver      = 7,

  Size // Must always be last!
};

// \brief Receives events at various stages in the render pipeline.
class RenderEventReceiver {
 public:
  static const ReceiverID kID;
  const int priority;

  // \brief The render stages. Order:
  // Oculus begin (optional)
  // Opaque begin, end
  // Transparent begin, end (currently not used)
  // Oculus end (optional)
  //
  // Oculus render stage will not occur if currently not rendering for the
  // oculus (i.e. the whole monitor is used instead of those "eye" renders)
  enum class RenderStage {
    Opaque, Transparent, Oculus
  };

  RenderEventReceiver(int priority = 0);
  virtual ~RenderEventReceiver() = 0;

  // \brief Called before a frame is rendered.
  virtual void PreRender() = 0;
  // \brief Called before a render stage starts.
  virtual void PreRenderStage(RenderStage) = 0;
  // \brief Called after a render stage.
  virtual void PostRenderStage(RenderStage) = 0;
  // \brief Called after the frame has rendered and the buffers are swapped.
  virtual void PostRender() = 0;
};

// \brief Receives all XEvents send by xlib. Completely unprocessed.
// Usage: Simply call SetFunc() to register your own handler function. You will
// automatically get all events you registered for.
// You will receive all events before VRWM handles them. If your handler
// returns false, the event will not be handled by any other XlibEventReceiver
// and VRWM will also not handle the event.
//
// You should not need an XlibEventReceiver most of the time. Try to prefer
// other types of receivers as they are easier to understand. For instance, if
// you need to handle mouse movement, consider using a MouseCursorMovedReceiver.
class XlibEventReceiver {
 public:
  static const ReceiverID kID;
  const int priority;

  typedef bool (*EventHandlerFunc)(const XEvent& evt);

  XlibEventReceiver(int priority = 0);
  virtual ~XlibEventReceiver() = 0;

  // \brief Use this to register your own event handler function.
  void SetFunc(int event_type, EventHandlerFunc f);
  // \brief Calls your handlers before VRWM handles the event.
  // Return false to prevent VRWM and any other plugin (with higher priority)
  // to handle it. You should do so with caution.
  bool Handle(const XEvent& e);

 protected:
  // \brief All registered event handlers of your event receiver.
  EventHandlerFunc event_handlers_[LASTEvent];

};

//TODO: rename mouse moved
// \brief Gets notified whenever the mouse is moved.
class MouseCursorMovedReceiver {
 public:
  static const ReceiverID kID;
  const int priority;

  MouseCursorMovedReceiver(int priority = 0);
  virtual ~MouseCursorMovedReceiver() = 0;

  // \brief Called when the mouse is moved.
  // x and y are in pixel per second.
  virtual void MouseMoved(float x, float y) = 0;
};

// \brief Gets notified on various stages of VRWM's run time.
class ProgrammStatusReceiver {
 public:
  static const ReceiverID kID;
  const int priority;

  ProgrammStatusReceiver(int priority = 0);
  virtual ~ProgrammStatusReceiver() = 0;

  // \brief Called before most of VRWM's init functionality.
  // The configuration file has already been read. All plugins have been
  // loaded. Nothing else has been initialized yet.
  virtual void PreInit() = 0;
  // \brief Called before PostInit, shortly before windows are found.
  // Startup of everything else is already done.
  virtual void PreWindowSearch() = 0;
  // \brief Called after VRWM's init and before the main loop is run.
  virtual void PostInit() = 0;
  // \brief Called just before the Core instance is destroyed.
  virtual void Teardown() = 0;
};

// \brief Receiver for when the user clicks or releases a mouse button.
class MouseClickReceiver {
 public:
   static const ReceiverID kID;
   const int priority;

   MouseClickReceiver(int priority = 0);
   virtual ~MouseClickReceiver() = 0;

   // \brief Called when the user presses a mouse button.
   // If you return false, the click is supressed and not handled by VRWM.
   virtual bool MousePressed(MouseButton button,
                             const ActiveModifiers& modifiers) = 0;
   // \brief Called when the user releases a mouse button.
   // If you return false, the release is not handled by VRWM.
   virtual bool MouseReleased(MouseButton button,
                              const ActiveModifiers& modifiers) = 0;
};

// \brief Receiver for keyboard key events.
class KeyReceiver {
 public:
  static const ReceiverID kID;
  const int priority;

  KeyReceiver(int priority = 0);
  virtual ~KeyReceiver() = 0;

  // \brief Called when the user presses VRWM's modifier key.
  virtual bool ModKeyPressed(KeyCode keyCode,
                             const ActiveModifiers& modifiers,
                             KeySym keysym) = 0;
  // \brief Called when the user releases VRWM's modifier key.
  virtual bool ModKeyReleased(KeyCode keyCode,
                              const ActiveModifiers& modifiers,
                              KeySym keysym) = 0;

  // \brief Called when the user presses a key on the keyboard.
  //
  // If the key is the modifier key, ModKeyPressed is called instead.
  // Plugins should check if the modifier key is pressed to execute special
  // commands.
  virtual bool KeyPressed(KeyCode keycode,
                          const ActiveModifiers& modifiers,
                          KeySym keysym) = 0;
  // \brief Called when the user releases a key on the keyboard.
  virtual bool KeyReleased(KeyCode keycode,
                           const ActiveModifiers& modifiers,
                           KeySym keysym) = 0;
};

// \brief Allows you to react to certain events of a window.
class ClientEventReceiver {
 public:
  static const ReceiverID kID;
  const int priority;

  ClientEventReceiver(int priority = 0);
  virtual ~ClientEventReceiver() = 0;

  // \brief Called when a Client object has been newly made visible.
  //
  // This function is usually not called on previously minimized clients.
  // However, it will be called if the client was minimized but does not have
  // any renderables attached (as this counts as completely invisible).
  // Therefore, when setting up your renderable, you need to take care that you
  // never delete it or otherwise unattach it from the client for as long as
  // that client lives (unless you make sure that another of your renderable is
  // still attached).
  //
  // In this function you can create a Renderable that should render this
  // Client. If you do, you SHOULD set the "created" parameter to true to tell
  // other plugins that this client already has had a renderable created. This
  // also prevents VRWM from creating a default client renderable. Plugins MAY
  // ignore the value of this parameter if they want to create a renderable for
  // this client regardless whether another plugin has already created one.
  // Your plugin MUST NOT set the variable to false if it has been true.
  //
  // Example for creating your own Renderable:
  // \code
  //    void MyPluginClass::ClientCreated(Client& c, bool& created) {
  //      auto r = new Renderable(Core::Instance(), scene_node, material);
  //      // Setup the Renderable...
  //      r->set_client(c);
  //      created = true;
  //    }
  // \endcode
  //
  // A renderable's on_client_map callback function will be called when it is
  // attached to a client.
  virtual void ClientCreated(Client& c, bool& created) = 0;

  // \brief Called after a client has been mapped (i.e. un-minimized).
  //
  // If you have a renderable attached to that client (i.e. you created one in
  // the ClientCreated() function), it is usually not neccessary to react to
  // this function. Use the on_client_map callback function of the renderable
  // to react to client maps.
  virtual void ClientMapped(Client& c) = 0;

  // \brief Called after a client has been minimized.
  //
  // If you have a renderable attached to that client (i.e. you created one in
  // the ClientCreated() function), it is usually not neccessary to react to
  // this function. Use the on_client_iconic callback function of the
  // renderable to react to minimizing the client.
  virtual void ClientMinimized(Client& c) = 0;

  // \brief Called when a client is withdrawn or destroyed.
  //
  // The Client's pixmap may be invalid and MUST NOT be used.
  // All xlib functions that use the window's ID MUST also NOT be used as they
  // may fail because that window may not exist anymore.
  //
  // However, you are still able to access the OpenGL texture as it is separate
  // from the X window and is only destroyed when it is no longer referenced.
  // If you need access to the OpenGL texture after this function ended, you
  // need to store the shared pointer somewhere.
  virtual void ClientClosed(Client& c) = 0;

  // \brief Called when a client has been resized.
  //
  // w and h are the new client width and height respectively.
  virtual void ClientResized(Client& c, unsigned int w, unsigned int h) = 0;
};

}
}

#endif // EVENTRECEIVER_H
