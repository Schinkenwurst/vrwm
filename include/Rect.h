#ifndef RECT_H
#define RECT_H

#include <cstdint>

#include <glm/glm.hpp>

namespace vrwm {

// \brief The position and size of a window.
struct WindowPos {
 public:
  WindowPos(int x, int y, unsigned int w, unsigned int h)
    : pos(x, y), size(w, h) {
  }
  WindowPos(const glm::ivec2& pos, const glm::uvec2& size)
    : pos(pos), size(size) {
  }

  // \brief Returns the center position of the window. (pos + 0.5 * width)
  glm::vec2 GetCenter() const {
    return glm::vec2(pos) + glm::vec2(size) * 0.5f;
  }

  glm::ivec2 pos;
  glm::uvec2 size;
};

} // vrwm

#endif // RECT_H
