#ifndef VRWM_WINDOW_PROPERTIES_H
#define VRWM_WINDOW_PROPERTIES_H

// \file
// \brief Defines structs and constants to easily get window properties.
//
// These structs are defined in either the ICCCM or the EWMH.
//
// In many cases, constants require some sort of namespace to differentiate
// between the same constants defined in the Xlib. Constants defined in this
// file have the exact same value but are encapsulated in enums for enhanced
// readability of data types (e.g.  int  versus  WindowState).
#include <bitset>
#include <cstdint>
#include <vector>

#include "Atoms.h"

namespace vrwm {

class Client;

// \brief used in WM_STATE property and WM_HINTS
enum class WindowState : uint32_t {
  WS_WithdrawnState = 0,
  WS_NormalState = 1,
  WS_IconicState = 3,
};

// \brief used in _NET_WM_WINDOW_TYPE
enum class WindowType {
  Desktop, Dock, Toolbar, Menu, Utility, Splash, Dialog, Dropdown, Popup,
  Tooltip, Notification, Combo, Dnd, Normal,
  Invalid = -1,
};

// \brief used in _NET_WM_STATE
enum class EWMHWindowState : int {
  Modal = 0, Sticky, MaximizedVert, MaximizedHorz, Shaded, SkipTaskbar,
  SkipPager, Hidden, Fullscreen, WS_Above, WS_Below, DemandsAttention, Focused,

  Size, // Size of this enum, always keep before Invalid!
  Invalid = -1
};

// \brief used in _NET_WM_ALLOWED_ACTIONS
enum class AllowedAction : int {
  Move = 0, Resize, Minimize, Shade, Stick, MaximizeHorz, MaximizeVert,
  Fullscreen, ChangeDesktop, Close, AA_Above, AA_Below,

  Size, // Size of this enum, always keep before Invalid!
  Invalid = -1
};

class WindowProperties {
 public:
  // Because each of the states is a flag that determines wether this state is
  // set, the representation as std::bitset seems appropriate. Access the
  // individual bits using the values of the enum EWMHWindowState.
  typedef std::bitset<static_cast<size_t>(EWMHWindowState::Size)>
    EWMHWindowStates;
  // Because each of the states is a flag that determines wether this state is
  // set, the representation as std::bitset seems appropriate. Access the
  // individual bits using the values of the enum EWMHWindowState.
  typedef std::bitset<static_cast<size_t>(AllowedAction::Size)>
    AllowedActions;

  // \brief used in the WM_STATE property.
  struct WmState {
    WindowState state;
    Client* icon_window;
  };

  // \brief Used in the _NET_WM_ICON property.
  struct Icon {
    uint32_t width, height;
    // Careful! data_ptr could potentially go out of scope!
    struct Color {
      uint8_t b, g, r, a;
    }* data_ptr;
  };

  static WindowType ToWindowType(Atom a);
  static Atom ToAtom(WindowType t);

  static EWMHWindowState ToWindowState(Atom a);
  static Atom ToAtom(EWMHWindowState t);
  static std::vector<Atom> ToAtoms(const EWMHWindowStates& states);
  static EWMHWindowStates ToWindowStates(const std::vector<Atom>& atoms);

  static AllowedAction ToAllowedAction(Atom a);
  static Atom ToAtom(AllowedAction t);
  static std::vector<Atom> ToAtoms(const AllowedActions& states);
  static AllowedActions ToAllowedActions(const std::vector<Atom>& atoms);

 private:
  WindowProperties() = delete;
};
} //vrwm

#endif

