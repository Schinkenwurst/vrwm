#ifndef VRWM_CONFIGURATION_H
#define VRWM_CONFIGURATION_H

#include <array>
#include <fstream>
#include <string>
#include <vector>

#include <libconfig.h++>

#include "OculusManager.h"
#include "XlibWrappers.h"

namespace vrwm {

struct ArgumentHandler;

// \brief Configuration file parser.
// Looks for ~/.vrwm/vrwm.conf followed by /usr/share/vrwm.conf
// (see kConfigFileName and FileTools)
// Uses libconfig for parsing of the config file.
class Configuration {
 public:
  Configuration();
  ~Configuration();

  // \brief Searches for a configuration file and loads it.
  // Search paths are as specified in FileTools::Find().
  // After this call, the config member is filled appropriatly. Also, the
  // config_file_name member is set.
  // This happens automatically at the start of vrwm, before all plugins are
  // loaded.
  // an also be used to re-parse the config.
  //
  // Exceptions of libconfig are propagated upwards.
  void ParseConfig();

  // \brief Constructs the core configuration.
  // This happens automatically at the start of vrwm, before all plugins are
  // loaded.
  //
  // Exceptions thrown by libconfig will be propagated to the caller.
  void InitCoreConfiguration();

  // \brief Wrapper around libconfig::Config::lookupValue().
  template <class T>
  bool LookupValue(const char* path, T& out_value) {
    return config.lookupValue(path, out_value);
  }

  // \brief Does a lookup and interprets the value.
  //
  // The value that is looked up is a color. The data specified at the path can
  // have one of five different formats:
  //
  // 1) An array of 4 floats in the range of [0.0, 1.0], each of the floats is
  // one of the color channels. RGBA. If a value is 1.0, you MUST specify it as
  // 1.0 and not simply 1 to make libconfig aware that it is a floating point
  // value and not an integer.
  // Example: [1.0, 1.0, 1.0, 1.0]
  // 2) 4 ints in the range of [0, 255]. RGBA.
  // Example: [255, 255, 255, 255]
  // 3) A single string specifying 4 hexadecimal values in the range of [0,
  // 255] each. I.e. the first 2 characters of the string are the red channel,
  // the second 2 characters green, etc. As always: RGBA. A prepended # is
  // optional.
  // Example 1: "#FFFFFFFF"
  // Example 2: "FFFFFFFF"
  // 4) The name of a color in english. The following values are currently
  // accepted (each has an alpha value of 1.0):
  // "red", "green", "blue", "black", "white", "cyan", "magenta", "yellow",
  // "gray"
  // 5) A hexadecimal integer (may also be a non-hex int, though I don't really
  // recommend it.) Please note that this is NOT supported in vec3.
  // Example: 0xFFFFFFFF
  bool LookupColor(const char* path, glm::vec4& out_color) {
    return LookupColor(path, &out_color[0], 4);
  }
  // \brief RGB version of LookupColor().
  bool LookupColor(const char* path, glm::vec3& out_color) {
    return LookupColor(path, &out_color[0], 3);
  }
  // \brief RGB version LookupColor(). out_color.a will remain the same.
  bool LookupColorRGB(const char* path, glm::vec4& out_color) {
    return LookupColor(path, &out_color[0], 3);
  }

  // \brief Wrapper around libconfig::Config::lookupValue().
  template <class T>
  bool LookupValue(const std::string& path, T& out_value) {
    return config.lookupValue(path, out_value);
  }

  // \brief The full path and name of the config file.
  // Empty if no config file has been found.
  std::string config_file_name;

  // \brief Use this member to access the values in the config file.
  // See libconfig's documentation if you want to know the details.
  libconfig::Config config;


  // \brief The virtual dots per inch.
  // Each client will get those dpi.
  // Thus, reducing this value makes the clients larger.
  float virtual_dpi;
  // \brief Should oculus be required or not.
  OculusManager::UsageMode oculus_mode;

  // \brief True if the user may resize / scale a window by modkey + click.
  bool direct_grab_enabled;

  // \brief The modifier key of vrwm.
  // If this modifier is pressed, special vrwm commands are executed,
  // such as the program-launcher, reset pointer to center, etc.
  // Plugins should adhere to this to execute special functionality.
  // VRWM will not redirect the input to the currently active window if it
  // is pressed.
  ModifierIndex modkey;

  // \brief Which terminal should be used?
  std::string terminal;

  // \brief Should clients be automatically focused?
  // Can be set to automatically focus by looking at them, pointing at them
  // or don't focus automatically.
  // In any case, clicking on a client will always cause it to be focused.
  enum class AutomaticClientSelectionMode {
    LookAt = 0, PointAt = 1, AutoSelectNone = 2,
  } automatic_selection_mode;
  // \brief The time it takes for auto focus to occur.
  float automatic_selection_time;

 private:
  // \brief Generates ~/.vrwm/vrwm.conf with a sane default configuration.
  // An existing config file will not be overwritten.
  // Exceptions are thrown in case of errors.
  void CreateExampleConfigFile();

  bool LookupColor(const char* path, float* out, int n);
  // \brief Retrieves n values from the array s and converts them to float.
  template <class T>
  static void GetRawValues(const libconfig::Setting& s, float* out, int n);
  static bool HexStringToColor(const std::string& color, float* out, int n);
  static bool InterpretColorString(const std::string& color, float* out, int n);
  static void UIntToColor(unsigned int col, float* out, int n);
  static unsigned int HexStringToUInt(const std::string& hex_str);

  // \brief The name of the configuration file that is searched. vrwm.conf
  // Searched in the folders specified by FileTools.
  static const std::string kConfigFileName;

  // \brief Will be used to create ~/.vrwm/vrwm.conf if no config file found.
  static const char* kExampleConfigFile;

};

template <class T>
void Configuration::GetRawValues(const libconfig::Setting& s, float* out,
    int n) {
  for (int i = 0; i < n; ++i) {
    T value = s[i];
    out[i] = value;
  }
}


}

#endif // VRWM_CONFIGURATION_H
