#ifndef GLGLOBAL_H
#define GLGLOBAL_H

#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glx.h>

#include "LogManager.h"

// \file
// \brief Contains several global OpenGL-related variables and functions.

// \brief Checks for and logs any OpenGL errors.
#define CHECK_GL_ERRORS() {                                             \
  unsigned int err = GL_NO_ERROR;                                       \
  do{                                                                   \
    err = glGetError();                                                 \
    if (err != GL_NO_ERROR){                                            \
      vrwm::LogManager::Instance().Log(                                 \
        "%s:%i: OpenGL error: %s", vrwm::Warning, __FILE__, __LINE__,   \
        GLErrToStr(err).c_str());                                       \
    }                                                                   \
  } while (err != GL_NO_ERROR);                                         \
}

// \brief Executes the specififed expression and checks for OpenGL errors.
#define CALL_GL(exp) exp;                                               \
{                                                                       \
  unsigned int err = GL_NO_ERROR;                                       \
  do{                                                                   \
    err = glGetError();                                                 \
    if (err != GL_NO_ERROR){                                            \
      vrwm::LogManager::Instance().Log(                                 \
        "%s:%i: OpenGL error in expr ("#exp"): %s", vrwm::Warning,      \
        __FILE__, __LINE__, GLErrToStr(err).c_str());                   \
    }                                                                   \
  } while (err != GL_NO_ERROR);                                         \
}

/// \brief a GLX function that will be bound on runtime
extern void (*glXBindTexImageEXT_func)(Display*, GLXDrawable, int, const int*);
/// \brief a GLX function that will be bound on runtime
extern void (*glXReleaseTexImageEXT_func)(Display*, GLXDrawable, int);

// \brief Converts an OpenGL error code to a human readable string.
extern std::string GLErrToStr(int err);


// \brief Enhancements to glm.
// Everything in this namespace is written in a slightly other code style to
// be similar to glm
namespace glm_enh {
  namespace vec3 {
    const glm::vec3 Forward(0.0f, 0.0f, -1.0f),
                    Right(1.0f, 0.0f, 0.0f),
                    Up(0.0f, 1.0f, 0.0f),
                    Zero(0.0f, 0.0f, 0.0f);
  }
  namespace quat {
    const glm::quat Identity(1.0f, 0.0f, 0.0f, 0.0f);
  }
  // \brief Alternate version of glm::rotate
  // Because glm::rotate returns an unexpected result (it seems to rotate in
  // another order).
  // Axis will be normalized if it isn't.
  glm::quat rotate(const glm::quat& q, float radians, const glm::vec3& axis);
  // In this flavor, axis will not be normalized. Its magnitude is the angle
  // (as always, in radian)
  glm::quat rotate(const glm::quat& q, const glm::vec3& axis);

  // \brief Returns the quaternion that rotates "from" to "to".
  // Assumes they are normalized.
  glm::quat FromToRotation(const glm::vec3& from, const glm::vec3& to);
  // \brief Returns the quaternion that rotates "from" to "to".
  // Assumes they are normalized.
  glm::quat FromToRotation(const glm::quat& from, const glm::quat& to);

  // \brief Returns a quaternion that makes forward be the same as direction.
  // glm has no LookAt function for quaternions. So I've just written one
  // myself that does something similar to glm::LookAt and a quat_cast of
  // the result.
  // It's a bit more performant because we do a bit less unneccessary stuff and
  // it is definitely easier to use.
  glm::quat LookAt(const glm::vec3& direction,
      const glm::vec3& desiredUp = glm_enh::vec3::Up);

  // \brief Returns the squared length of a vector.
  inline float lengthSq(const glm::vec3& v) { return glm::dot(v, v); }

  inline bool IsNormalized(const glm::vec3& v) {
    return glm::abs(1.0f - lengthSq(v)) < 1e-6f;
  }
}

// \brief Used to print glm::vec2 with ostreams.
std::ostream& operator<<(std::ostream& o, const glm::vec2& v);
// \brief Used to print glm::vec3 with ostreams.
std::ostream& operator<<(std::ostream& o, const glm::vec3& v);
// \brief Used to print glm::vec4 with ostreams.
std::ostream& operator<<(std::ostream& o, const glm::vec4& v);
// \brief Used to print glm::mat4 with ostreams.
std::ostream& operator<<(std::ostream& o, const glm::mat4& m);
// \brief Used to print glm::quat with ostreams.
std::ostream& operator<<(std::ostream& o, const glm::quat& q);

/// \brief used for to_string for custom types, such as glm::vec3, etc.
template<class T>
inline std::string vrwm_to_string(const T& v) {
  std::ostringstream tmp;
  tmp << v;
  return tmp.str();
}


namespace vrwm {
struct Ray;
namespace math {
  inline uint32_t LowerPowerOfTwo(uint32_t x) {
    x = x | (x >> 1);
    x = x | (x >> 2);
    x = x | (x >> 4);
    x = x | (x >> 8);
    x = x | (x >> 16);
    return x - (x >> 1);
  }

  // \brief Checks at what point a ray intersects a plane with infinite size.
  // \param [out] out_t the parameter of the ray where the plane has been hit.
  //    Will have a negative or unchanged value if the plane has not been hit.
  // \returns true if the plane has been hit by the ray.
  bool RaycastPlane(const Ray& r, const glm::vec3& plane_normal,
      float plane_offset, float& out_t);
  // \brief Checks at what point a ray intersects a plane with infinite size.
  inline bool RaycastPlane(const Ray& r,
      const std::pair<glm::vec3, float>& plane, float& out_t) {
    return RaycastPlane(r, plane.first, plane.second, out_t);
  }
}
}

#endif
