#ifndef FILETOOLS_H
#define FILETOOLS_H

#include <array>
#include <string>
#include <vector>
#include <cassert>

namespace vrwm {

// \brief Provides various functions for file system access.
// Unix only!
class FileTools {
 public:
  // \brief Returns the home path, i.e. /home/yourname/
  // \throws std::runtime_error if the home name cannot be resolved for
  //    whatever reason.
  static std::string GetHomePath() throw(std::runtime_error);

  // \brief Merges the path name with the file name.
  //
  // The path name does not need to end with a trailing / and may have the
  // ~ as first character to specifiy the user's home directory.
  //
  // \throws std::runtime_error if the home path cannot be resolved or some
  //    other grave error occurs.
  static std::string Resolve(const std::string& path,
                             const std::string& filename = "")
                               throw(std::runtime_error);
  // \brief Returns true if the specified file exists.
  static bool FileExists(const std::string& file) noexcept;
  // \brief Returns true if the specified file is a directory.
  static bool IsDirectory(const std::string& file) noexcept;
  // \brief Returns true if the file matches the extension.
  static bool MatchesExtension(const std::string& file,
                               const std::string& extension) noexcept;
  // \brief Removes the path and extension from the passed string.
  // e.g.  "/foo/bar.ext" => "bar"
  static std::string Filename(const std::string& full_path) noexcept;
  // \brief Removes the path from the passed string.
  // e.g.  "/foo/bar.ext" => "bar.ext"
  static std::string FilenameExt(const std::string& full_path) noexcept;

  // \brief Removes the file name to return the directory.
  // e.g. "/foo/bar.ext" => "/foo/"
  // Input directories MUST end with a trailing '/' (i.e. if bar is a
  // directory, input MUST look like "/foo/bar/", NOT "/foo/bar")
  // Does NOT resolve a tilde (~).
  static std::string Directory(const std::string& full_path) noexcept;

  // \brief Searches for a file and returns the first occurence.
  //
  // The file is searched in the directories specified in kSearchedFolders.
  // The filename may also contain sub directories, e.g.
  // foo/bar.ext
  //
  // \returns true and the absolute file path on success, false and "" on fail.
  static std::pair<bool, std::string> Find(const std::string& file) noexcept;

  // \brief Returns all files in a given subdirectory.
  //
  // All files in the subdirectories of the directories specified in
  // kSearchedFolders.
  //
  // If two files have the same name but are in different directories, only the
  // first occurence will be returned.
  //
  // \param subdir The subdirectory of kSearchedFolders which is searched.
  //    Does not need to have a trailing '/'.
  // /param extension Can be used to filter the files by extension. Wildcards
  //    are NOT accepted. This means you MUST pass ".so" if you want all shared
  //    libraries, "*.so" will NOT return the correct result.
  static std::vector<std::string> AllFilesIn(
      const std::string& subdir,
      const std::string& extension = "")
      noexcept;

  // \brief Creates the given directory if it doesn't exist.
  //
  // The directory will be created with owner read write acess. If the
  // directory does exist, the function does nothing. If creation fails for
  // whatever reason, an exception will be thrown with a descriptive failure
  // message.
  static void Mkdir(const std::string& path);

  // \brief Creates the directory that is kSearchedFolders[0]
  static void MkdirFirstSearchPath() {
    assert(FileTools::kSearchedFolders.size() > 0);
    Mkdir(Resolve(kSearchedFolders[0]));
  }

  // \brief These are the folders in which the files are searched.
  // May contain a tilde at the beginning which is resolved as expected.
  // The order of the array specifies the priority of the path.
  // i.e. ~/.vrwm/  is searched before  /usr/share/vrwm/  because it is at a
  // lower position in the array.
  static const std::array<std::string, 2> kSearchedFolders;
};

}

#endif // FILETOOLS_H
