#ifndef VRWM_GLBUFFER_H
#define VRWM_GLBUFFER_H

#include <GL/glew.h>

#include "GLglobal.h"

namespace vrwm {

//enum class GLbufferTarget {
  //Array, CopyRead, CopyWrite, ElementArray, PixelPack, PixelUnpack, Texture,
  //TransformFeedback, Uniform,
//};

enum class BufferAccess {
  ReadOnly, WriteOnly, ReadWrite,
};

enum class RangeAccessFlags {
  RangeAccessFlagsNone = 0,
  InvalidateRange = GL_MAP_INVALIDATE_RANGE_BIT,
  InvalidateBuffer = GL_MAP_INVALIDATE_BUFFER_BIT,
  FlushExplicit = GL_MAP_FLUSH_EXPLICIT_BIT,
  Unsychronized = GL_MAP_UNSYNCHRONIZED_BIT,
};

inline GLenum ToMapBufferAccess(BufferAccess access) {
  switch (access) {
    case BufferAccess::ReadOnly:  return GL_READ_ONLY;
    case BufferAccess::WriteOnly: return GL_WRITE_ONLY;
    case BufferAccess::ReadWrite: return GL_READ_WRITE;
    default: throw std::runtime_error("Invalid enum value of BufferAccess");
  }
}

inline GLbitfield ToMapBufferRangeAccess(BufferAccess access) {
  switch (access) {
    case BufferAccess::ReadOnly:  return GL_MAP_READ_BIT;
    case BufferAccess::WriteOnly: return GL_MAP_WRITE_BIT;
    case BufferAccess::ReadWrite: return GL_MAP_READ_BIT | GL_MAP_WRITE_BIT;
    default: throw std::runtime_error("Invalid enum value of BufferAccess");
  }
}

/// \brief Allows access into mapped buffer via operator[].
/// Specifically useful in uniform buffer blocks.
/// Allows access like this:
/// GLmappedBuffer mapped = //...
/// mapped[model_matrix] = foo;
template <class T, GLsizeiptr start>
class BufferMember {
 public:
  constexpr size_t offset() const { return start; }
  constexpr size_t end() const { return offset() + size(); }
  constexpr size_t size() const { return sizeof(T); }
};

template <GLenum target>
class GLbuffer;

class GLmappedBuffer {
 public:
  GLmappedBuffer(GLenum target, void* data, GLintptr offset, GLsizeiptr length);
  GLmappedBuffer(const GLmappedBuffer&) = delete;
  GLmappedBuffer(GLmappedBuffer&& rvalue);
  GLmappedBuffer& operator=(const GLmappedBuffer&) = delete;
  GLmappedBuffer& operator=(GLmappedBuffer&& rvalue) = delete;
  ~GLmappedBuffer();

  void* data;

  template <class T>
  T& at(GLsizeiptr byte_offset) {
    assert(byte_offset < length_);
    return *(reinterpret_cast<T*>(
      reinterpret_cast<uint8_t*>(data) + byte_offset));
  }
  template <class T>
  const T& at(GLsizeiptr byte_offset) const {
    assert(byte_offset < length_);
    return *(reinterpret_cast<T*>(
      reinterpret_cast<uint8_t*>(data) + byte_offset));
  }

  /// Allows access like this:
  /// GLmappedBuffer mapped = //...
  /// mapped[model_matrix] = foo;
  template <class T, GLsizeiptr start>
  T& operator[](BufferMember<T, start>) {
    assert(offset_ <= start);
    return at<T>(start - offset_);
  }
  template <class T, GLsizeiptr start>
  const T& operator[](BufferMember<T, start>) const {
    assert(offset_ <= start);
    return at<T>(start - offset_);
  }

 private:
  GLenum target_;
  GLintptr offset_;
  GLsizeiptr length_;
};

template <GLenum target>
class GLbuffer {
 public:
  GLbuffer() : size_(0) { CALL_GL(glGenBuffers(1, &buffer_)); }
  virtual ~GLbuffer() { CALL_GL(glDeleteBuffers(1, &buffer_)); }

  void Bind() {
    if (GLbuffer<target>::g_currently_bound_ != buffer_) {
      CALL_GL(glBindBuffer(target, buffer_));
      GLbuffer<target>::g_currently_bound_ =  buffer_;
    }
  }

  static void Unbind() {
    if (GLbuffer<target>::g_currently_bound_ != 0) {
      CALL_GL(glBindBuffer(target, 0));
      GLbuffer<target>::g_currently_bound_ =  0;
    }
  }

  GLmappedBuffer Map(BufferAccess access);
  GLmappedBuffer MapRange(GLintptr offset, GLsizeiptr length,
    BufferAccess access,
    RangeAccessFlags optional_flags = RangeAccessFlags::RangeAccessFlagsNone);
  template <class M, GLsizeiptr start>
  GLmappedBuffer MapMember(BufferMember<M, start>, BufferAccess access,
      RangeAccessFlags optional_flags = RangeAccessFlags::RangeAccessFlagsNone)
  {
    return MapRange(start, sizeof(M), access, optional_flags);
  }

  void SetData(GLsizeiptr size, const GLvoid* data, GLenum usage) {
    size_ = size;
    Bind();
    CALL_GL(glBufferData(target, size, data, usage));
  }
  void SetSubData(GLintptr offset, GLsizeiptr size, const GLvoid* data) {
    assert(offset + size <= size_);
    Bind();
    CALL_GL(glBufferSubData(target, offset, size, data));
  }
  void GetSubData(GLintptr offset, GLsizeiptr size, GLvoid* out_data) {
    assert(offset + size <= size_);
    Bind();
    CALL_GL(glGetBufferSubData(target, offset, size, out_data));
  }

  GLuint buffer() const { return buffer_; }
  GLsizeiptr size() const { return size_; }

 private:

  static GLuint g_currently_bound_;

  GLuint buffer_;
  GLsizeiptr size_;
};

template <GLenum target>
GLuint GLbuffer<target>::g_currently_bound_ = 0;

template <GLenum target>
GLmappedBuffer GLbuffer<target>::Map(BufferAccess access) {
  Bind();
  void* data = CALL_GL(glMapBuffer(target, ToMapBufferAccess(access)));
  if (data == nullptr) {
    throw std::runtime_error("Unable to map OpenGL buffer.");
  }
  return GLmappedBuffer(target, data, 0, size_);
}

template <GLenum target>
GLmappedBuffer GLbuffer<target>::MapRange(GLintptr offset, GLsizeiptr length,
    BufferAccess access, RangeAccessFlags optional_flags) {
  assert(static_cast<GLsizeiptr>(offset) + length <= size_);
  Bind();
  void* data = CALL_GL(glMapBufferRange(target, offset, length,
      ToMapBufferRangeAccess(access) |
        static_cast<GLbitfield>(optional_flags)));
  if (data == nullptr) {
    throw std::runtime_error("Unable to map OpenGL buffer.");
  }
  return GLmappedBuffer(target, data, offset, length);
}

};

#endif

