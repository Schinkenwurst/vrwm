#include "../include/Configuration.h"

#include <stdlib.h>
#include <stdexcept>

#include "../include/FileTools.h"
#include "../include/LogManager.h"

using namespace vrwm;

const std::string Configuration::kConfigFileName = "vrwm.conf";
const char* Configuration::kExampleConfigFile =
  "vrwm = {\n"
  "  oculus_mode = 2\n"
  "  dpi = 4.0\n"
  "  terminal = \"xterm\"\n"
  "}\n"
  "\n"
  "bending_windows = {\n"
  "  bend = false;\n"
  "}\n"
  "\n"
  "background = {\n"
  "  file = \"backgrounds/simple_hex/simple_hex.obj\";\n"
  "  scale = 1.0;\n"
  "}\n"
  "\n"
  "cursor = {\n"
  "  // Rim color. If not present, no rim. The elements are the RGBA\n"
  "  // values. Alpha is the strength of the rim and may be omitted in\n"
  "  // which case 1.0 is used. Alternatively, you can use integers in\n"
  "  // the range of [0, 256) or a single hex color code string\n"
  "  // (e.g. \"#FFFFFF\")\n"
  "  rim = [1.0, 1.0, 1.0, 1.0]; \n"
  "  normal = \"cursors/default/normal.obj\"\n"
  "}\n";


Configuration::Configuration() :
    virtual_dpi(72),
    oculus_mode(OculusManager::UsageMode::Full),
    direct_grab_enabled(true),
    modkey(KeyboardMod1), terminal("xterm"),
    automatic_selection_mode(AutomaticClientSelectionMode::AutoSelectNone),
    automatic_selection_time(0.25f) {
}
Configuration::~Configuration() {
}

void Configuration::ParseConfig() {
  auto find_result = FileTools::Find(kConfigFileName);
  if (find_result.first) {
    vrwm_log(Info, "Using config file: " + find_result.second);
    config_file_name = find_result.second;
    try {
      config.readFile(config_file_name.c_str());
    } catch(libconfig::ParseException& e) {
      vrwm_log(Error, std::string("Error on config file parse: ") +
        e.getFile() + ":" + std::to_string(e.getLine()) + ": " + e.getError());
      throw e;
    }
  } else {
    vrwm_log(Warning, "No config file found.");
    vrwm_log(Info, "A default config file will be created.");
    try {
      CreateExampleConfigFile();
      vrwm_log(Info, "A default config file has been created.");
      ParseConfig();
    } catch (std::exception& e) {
      vrwm_clog(Error, "Unable to create default config file: %s", e.what());
    } catch (...) {
      vrwm_log(Error, "Unable to create default config file.");
    }
  }
}

void Configuration::CreateExampleConfigFile() {
  FileTools::MkdirFirstSearchPath();
  std::string path = FileTools::Resolve(FileTools::kSearchedFolders[0],
    Configuration::kConfigFileName);
  assert(!FileTools::FileExists(path));
  std::ofstream file(path);
  if (!file) {
    vrwm_clog(Error, "Opening file '%s' failed.", path.c_str());
    throw std::runtime_error("Opening the file failed.");
  }

  file << kExampleConfigFile;

  if (!file) {
    vrwm_clog(Error, "Writing to file '%s' failed.", path.c_str());
    throw std::runtime_error("Writing to file failed.");
  }
}

void Configuration::InitCoreConfiguration() {
  if (config_file_name.empty()) return;

  // virtual dpi
  LookupValue("vrwm.dpi", virtual_dpi);
  // oculus rift mode
  int oculus_mode_int = 0;
  LookupValue("vrwm.oculus_mode", oculus_mode_int);
  switch (oculus_mode_int) {
    case 0: oculus_mode = OculusManager::UsageMode::NoOculus;  break;
    case 1: oculus_mode = OculusManager::UsageMode::Simulated; break;
    case 2: oculus_mode = OculusManager::UsageMode::Full;      break;
    default: oculus_mode = OculusManager::UsageMode::NoOculus; break;
  }

  // auto selection
  int auto_selection_int = 0;
  if (LookupValue("vrwm.automatic_selection_mode", auto_selection_int)
      && auto_selection_int > 0 && auto_selection_int <= 2) {
    automatic_selection_mode = static_cast<AutomaticClientSelectionMode>(
        auto_selection_int);
    LookupValue("vrwm.automatic_selection_time",
        automatic_selection_time);
  }

  // modifer key
  std::string modkey_str;
  if (LookupValue("vrwm.modkey", modkey_str)) {
    modkey = ModifierNames::GetModifier(modkey_str);
  }
  vrwm_log(Info, "Modifier key is " + ModifierNames::GetName(modkey) +
    " and has index " + std::to_string(modkey));

  LookupValue("vrwm.direct_grab_enabled", direct_grab_enabled);

  // terminal
  LookupValue("vrwm.terminal", terminal);
  vrwm_log(Info, "Using terminal " + terminal);
}

bool Configuration::LookupColor(const char* path, float* out, int n) {
  if (!config.exists(path)) {
    return false;
  }

  bool success = true;
  assert(n <= 4);
  float f[4];
  try {
    libconfig::Setting& setting = config.lookup(path);
    if (setting.isArray()) {
      libconfig::Setting& first_value = setting[0];
      if (first_value.getType() == libconfig::Setting::TypeInt) {
        Configuration::GetRawValues<int>(setting, f, n);
        for (int i = 0; i < n; ++i) {
          f[i] /= 255.0f;
        }
      } else if (first_value.getType() == libconfig::Setting::TypeFloat) {
        Configuration::GetRawValues<float>(setting, f, n);
      } else {
        throw std::runtime_error("Array values are neither int nor float.");
      }
    } else if (setting.getType() == libconfig::Setting::TypeString) {
      std::string color = setting;
      success = InterpretColorString(color, f, n) ||
        HexStringToColor(color, f, n);
    } else if (setting.getType() == libconfig::Setting::TypeInt) {
      if (n == 4) {
        UIntToColor(setting, f, n);
      }
    } else {
      success = false;
    }
  } catch(std::exception& e) {
    vrwm_clog(Warning, "Unable to interpret '%s' as color: %s", path,
      e.what());
    success = false;
  }

  if (success) {
    std::memcpy(out, f, n * sizeof(float));
  }

  return success;
}

bool Configuration::HexStringToColor(const std::string& color, float* out,
    int n) {
  if (color.size() == 0) {
    return false;
  }

  size_t start = 0;
  if (color[0] == '#') {
    ++start;
  }

  if (color.size() - start < static_cast<size_t>(n) * 2) {
    // String is not long enough.
    return false;
  }

  unsigned int hex = Configuration::HexStringToUInt(color.substr(start));
  Configuration::UIntToColor(hex, out, n);
  return true;
}

void Configuration::UIntToColor(unsigned int col, float* out, int n) {
  for (int i = n - 1; i >= 0; --i) {
    out[n - 1 - i] = (float) (0xFF & (col >> (i * 8))) / (float) 0xFF;
  }
}

unsigned int Configuration::HexStringToUInt(const std::string& hex_str) {
  unsigned int to_return = 0;
  unsigned int exp = 1;
  for (size_t i = hex_str.size() - 1; i != static_cast<size_t>(-1); --i) {
    if (hex_str[i] >= '0' && hex_str[i] <= '9') {
      to_return += (hex_str[i] - '0') * exp;
    } else if (hex_str[i] >= 'A' && hex_str[i] <= 'F') {
      to_return += (hex_str[i] - 'A' + 10) * exp;
    } else if (hex_str[i] >= 'a' && hex_str[i] <= 'f') {
      to_return += (hex_str[i] - 'a' + 10) * exp;
    } else {
      throw std::runtime_error("Invalid hexadecimal character.");
    }
    exp *= 16;
  }
  return to_return;
}

bool Configuration::InterpretColorString(const std::string& color, float* out,
    int n) {
  assert(n == 3 || n == 4);
  if (n == 4) {
    out[3] = 1.0f;
  }

  bool success = true;
  if (color == "red") {
    out[0] = 1.0f; out[1] = 0.0f; out[2] = 0.0f;
  } else if (color == "green") {
    out[0] = 0.0f; out[1] = 1.0f; out[2] = 0.0f;
  } else if (color == "blue") {
    out[0] = 0.0f; out[1] = 0.0f; out[2] = 1.0f;
  } else if (color == "black") {
    out[0] = 0.0f; out[1] = 0.0f; out[2] = 0.0f;
  } else if (color == "white") {
    out[0] = 1.0f; out[1] = 1.0f; out[2] = 1.0f;
  } else if (color == "cyan") {
    out[0] = 0.0f; out[1] = 1.0f; out[2] = 1.0f;
  } else if (color == "magenta") {
    out[0] = 1.0f; out[1] = 0.0f; out[2] = 1.0f;
  } else if (color == "yellow") {
    out[0] = 1.0f; out[1] = 1.0f; out[2] = 0.0f;
  } else if (color == "gray") {
    out[0] = 0.5f; out[1] = 0.5f; out[2] = 0.5f;
  } else {
    success = false;
  }
  return success;
}

