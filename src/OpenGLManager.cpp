#include "../include/OpenGLManager.h"

#include <sstream>

#include <glm/glm.hpp>
#include <glm/gtc/epsilon.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>

#include "../include/Assets.h"
#include "../include/Core.h"
#include "../include/GLglobal.h"
#include "../include/LogManager.h"
#include "../include/Ray.h"
#include "../include/PluginManager.h"
#include "../include/plugin/EventReceiver.h"
#include "../include/Renderable.h"
#include "../include/SceneNode.h"
#include "../include/draw/VertexAttribute.h"
#include "../include/InsertionSort.h"

using namespace vrwm;

glm::mat4 OpenGLManager::g_view_matrix(1.0f);
glm::mat4 OpenGLManager::g_projection_matrix(1.0f);

//TODO: height via parameter / oculus
OpenGLManager::OpenGLManager(Core& core)
    : core_(core)
    , orientation_(1.0f, 0.0f, 0.0f, 0.0f)
    , offset_from_ground_(0.0f, 1.75f, 0.0f) {
  Init();
}
OpenGLManager::~OpenGLManager() {
  vrwm_log(Info, "Destruction of all Renderables");
  delete root_scene_node_;
  while(!renderables_.empty()) {
    delete *(renderables_.begin());
  }
  vrwm_log(Info, "Teardown of assets");
  Assets::TearDown();
}

glm::vec3 OpenGLManager::GetLookAt(float distance) const {
  return offset_from_ground_ + GetForward() * distance;
}

void OpenGLManager::Rotate(float angle_radians,
                           const glm::vec3& rotation_vector) {
  orientation_ = glm_enh::rotate(orientation_, angle_radians, rotation_vector);
}

void OpenGLManager::SetupMatrices() {
  SetupViewMatrix();
  SetupProjectionMatrix();
}
void OpenGLManager::SetupViewMatrix() {
  glm::mat3 inv_rot = glm::mat3_cast(orientation_);
  inv_rot = glm::transpose(inv_rot);
  OpenGLManager::g_view_matrix = glm::mat4(inv_rot);
  OpenGLManager::g_view_matrix[3][3] = 1.0f;
  OpenGLManager::g_view_matrix = glm::translate(OpenGLManager::g_view_matrix,
      -offset_from_ground_);
  auto mapped = matrix_block_.MapMember(matrix_block_.view,
    BufferAccess::WriteOnly, RangeAccessFlags::InvalidateRange);
  mapped[matrix_block_.view] = OpenGLManager::g_view_matrix;
}

void OpenGLManager::SetupProjectionMatrix() {
  SetProjectionMatrix(glm::perspective(
      glm::radians(80.0f),
      core_.AspectRatio(),
      0.01f,
      10000.0f));
}

void OpenGLManager::SetProjectionMatrix(const glm::mat4& m) {
  OpenGLManager::g_projection_matrix = m;
  auto mapped = matrix_block_.MapMember(matrix_block_.projection,
    BufferAccess::WriteOnly, RangeAccessFlags::InvalidateRange);
  mapped[matrix_block_.projection] = m;
}

void OpenGLManager::Clear() {
  error_occurance_countdown_ = std::max(0.0f,
    error_occurance_countdown_ - Core::Instance().time_since_last_frame());
  float lerp_error =
    error_occurance_countdown_ / error_occurance_countdown_max_;
  glm::vec4 clear_color =
    lerp_error * clear_color_error_ + (1.0f - lerp_error) * clear_color_normal_;
  CALL_GL(glClearColor(clear_color.r, clear_color.g, clear_color.b,
      clear_color.a));
  CALL_GL(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
  CALL_GL(glClearDepth(1));
}

void OpenGLManager::SortRenderables() {
  // TODO: Periodically check if another alogrithm would be faster
  //
  // Insertion sort was chosen because it operates well on small sets of
  // almost-sorted objects.
  sort::InsertionSort(sorted_opaque_renderables_, OpaqueRenderableComparator());
  sort::InsertionSort(sorted_transparent_renderables_,
    TransparentRenderableComparator());
}

void OpenGLManager::Render() {
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);
  glEnable(GL_CULL_FACE);
  glFrontFace(GL_CCW);

  SetupSceneBlock();

  glDisable(GL_BLEND);
  auto render_stage = plugin::RenderEventReceiver::RenderStage::Opaque;
  core_.plugin_manager().ForEach(&plugin::RenderEventReceiver::PreRenderStage,
      render_stage);
  for (auto r : sorted_opaque_renderables_) {
    SetupMatrixBlock(*r);
    r->Render();
  }
  core_.plugin_manager().ForEach(&plugin::RenderEventReceiver::PostRenderStage,
      render_stage);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  render_stage = plugin::RenderEventReceiver::RenderStage::Transparent;
  core_.plugin_manager().ForEach(&plugin::RenderEventReceiver::PreRenderStage,
      render_stage);
  for (auto r : sorted_transparent_renderables_) {
    SetupMatrixBlock(*r);
    r->Render();
  }
  core_.plugin_manager().ForEach(&plugin::RenderEventReceiver::PostRenderStage,
      render_stage);
}

void OpenGLManager::DrawLine(const glm::vec3& start, const glm::vec3& end,
    const glm::vec4& color) {
  glm::vec3 direction = end - start;
  float length = glm::length(direction);
  glm::mat4 model = glm::translate(glm::mat4(1.0f), start);
  model *= glm::mat4_cast(glm_enh::LookAt(direction / length));
  model = glm::scale(model, glm::vec3(length, length, length));

  {
    size_t block_start = matrix_block_.model.offset();
    size_t block_end = matrix_block_.mvp.end();
    auto mapped = matrix_block_.MapRange(block_start, block_end - block_start,
      BufferAccess::WriteOnly);
    mapped[matrix_block_.model] = model;
    mapped[matrix_block_.mvp] = g_projection_matrix * g_view_matrix * model;
  }

  ray_material_.SetUniform<glm::vec4>("color", color);
  ray_material_.Use();
  ray_mesh_.Render();
}

void OpenGLManager::SetupMatrixBlock(const Renderable& r) {
  size_t start = matrix_block_.model.offset();
  size_t end = matrix_block_.normal2.end();
  auto mapped =
    matrix_block_.MapRange(start, end - start, BufferAccess::WriteOnly);
  mapped[matrix_block_.model] = r.scene_node().model_matrix();
  mapped[matrix_block_.mvp] = g_projection_matrix * g_view_matrix *
    r.scene_node().model_matrix();
  glm::mat3 normal(r.scene_node().inverse_model_matrix());
  normal = glm::transpose(normal);
  mapped[matrix_block_.normal0] = normal[0];
  mapped[matrix_block_.normal1] = normal[1];
  mapped[matrix_block_.normal2] = normal[2];
}

void OpenGLManager::SetupSceneBlock() {
  auto mapped = scene_block_.Map(BufferAccess::WriteOnly);
  mapped[scene_block_.camera_position] = offset_from_ground_;
  mapped[scene_block_.camera_direction] = GetForward();
  mapped[scene_block_.time] = core_.time();
}

void OpenGLManager::Init() {
  root_scene_node_ = new SceneNode(core_, "root");
  root_scene_node_->SetVisible(true);
  assert(root_scene_node_->is_visible_in_hierarchy());
  billboard_target_ = root_scene_node_->CreateChild("billboard target");
  billboard_target_->SetLocalPosition(offset_from_ground_);

  error_occurance_countdown_ = 0.0f;
  core_.cursor().set_origin(
    offset_from_ground() - glm::vec3(0.0f , 0.05f, 0.0f));

  matrix_block_.BindBlock();
  scene_block_.BindBlock();

  InitMaterials();

  Renderable::ConstructDefaultClientRenderableMesh();
  Renderable::ConstructDefaultClientDecoratorMeshes();
  ConstructRay();
}

void OpenGLManager::InitMaterials() {
  std::set<std::string> uses;
  uses.insert("TEXTURES");
  AddMaterial(Assets::MaterialType::Simple, false,
    "/default.vert.glsl", "/textured.frag.glsl", uses);

  uses.clear();
  uses.insert("NORMALS");
  AddBlinnPhongMaterial(Assets::MaterialType::Diffuse, false, uses);
  AddBlinnPhongMaterial(Assets::MaterialType::TransparentDiffuse, true, uses);
  uses.insert("TEXTURES");
  uses.insert("TEX_DIFFUSE");
  AddBlinnPhongMaterial(Assets::MaterialType::DiffuseTextured, false, uses);
  AddBlinnPhongMaterial(Assets::MaterialType::TransparentDiffuseTextured,
    true, uses);
  uses.insert("SPECULAR");
  AddBlinnPhongMaterial(Assets::MaterialType::Specular, false, uses);
  AddBlinnPhongMaterial(Assets::MaterialType::TransparentSpecular, true, uses);
  uses.insert("TEX_SPECULAR");
  AddBlinnPhongMaterial(Assets::MaterialType::SpecularTextured, false, uses);
  AddBlinnPhongMaterial(Assets::MaterialType::TransparentSpecularTextured,
    true, uses);
  uses.insert("TEX_SHINY");
  AddBlinnPhongMaterial(Assets::MaterialType::SpecularTexturedShinyTextured,
    false, uses);
  AddBlinnPhongMaterial(
    Assets::MaterialType::TransparentSpecularTexturedShinyTextured, true,
    uses);

  uses.erase("TEX_SHINY");
  uses.erase("TEX_SPECULAR");
  uses.erase("SPECULAR");
  uses.insert("TANGENTS");
  uses.insert("BUMP_MAP");
  AddBlinnPhongMaterial(Assets::MaterialType::Bumped, false, uses);
  AddBlinnPhongMaterial(Assets::MaterialType::TransparentBumped, true, uses);

  uses.insert("SPECULAR");
  AddBlinnPhongMaterial(Assets::MaterialType::BumpedSpecular, false,
    uses);
  AddBlinnPhongMaterial(Assets::MaterialType::TransparentBumpedSpecular,
    true, uses);
  uses.insert("TEX_SPECULAR");
  AddBlinnPhongMaterial(Assets::MaterialType::BumpedSpecularTextured, false,
    uses);
  AddBlinnPhongMaterial(Assets::MaterialType::TransparentBumpedSpecularTextured,
    true, uses);
  uses.insert("TEX_SHINY");
  AddBlinnPhongMaterial(
    Assets::MaterialType::BumpedSpecularTexturedShinyTextured, false, uses);
  AddBlinnPhongMaterial(
    Assets::MaterialType::TransparentBumpedSpecularTexturedShinyTextured, true,
    uses);
}

draw::MaterialPtr OpenGLManager::AddMaterial(Assets::MaterialType traits,
    bool transparent, std::string vshader, std::string fshader,
    const std::set<std::string>& defines) {
  ShaderList shaders;
  shaders.push_back(GLshader::FromFile(
        VRWM_SHADERDIR + vshader, GL_VERTEX_SHADER, defines));
  shaders.push_back(GLshader::FromFile(
        VRWM_SHADERDIR + fshader, GL_FRAGMENT_SHADER, defines));
  auto prog = GLprogramPtr(new GLprogram(std::move(shaders)));
  prog->Bind("MatrixBlock", matrix_block_.binding());
  prog->Bind("SceneBlock", scene_block_.binding());
  return Assets::add(traits, new draw::Material(prog, transparent));
}

void OpenGLManager::ConstructRay() {
  float main_vertex_data[] = {
     0.0f,  0.0f,  0.0f,
     0.0f,  0.0f, -1.0f,
  };
  ray_mesh_.SetData(main_vertex_data, sizeof(main_vertex_data));
  std::vector<GLuint> rendering_order = { 0, 1 };
  ray_mesh_.SetElements(GL_LINE_STRIP, rendering_order);
  ray_mesh_.AddVertexAttribute(draw::ResolvedVertexAttribute(0, 3));
  ray_material_ = *Assets::material(Assets::MaterialType::Simple);
  GLtexturePtr p = GLtexturePtr(new GLtexture());
  float white_data[] = {1.0, 1.0, 1.0, 1.0};
  p->SetData(0, 1, 1, white_data);
  ray_material_.SetUniform<GLtexturePtr>("texture", p);
}

bool OpenGLManager::RaycastAll(const Ray& r, RaycastHit& out_hit) {
  bool result = false;
  for (Renderable* renderable : renderables_) {
    if (renderable->Raycast(r, out_hit)) {
      result = true;
    }
  }
  return result;
}

#ifdef VRWM_DEBUG
std::string OpenGLManager::FormatDebugOutputARB(GLenum source, GLenum type,
                                                GLuint id, GLenum severity,
                                                const char *message) {
  const char* sourceFmt = "UNDEFINED(0x%04X)";
  // I'll ignore the 80 character per line limit here because it already looks
  // aweful enough.
  switch (source) {
    case GL_DEBUG_SOURCE_API_ARB:             sourceFmt = "API";            break;
    case GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB:   sourceFmt = "WINDOW_SYSTEM";  break;
    case GL_DEBUG_SOURCE_SHADER_COMPILER_ARB: sourceFmt = "SHADER_COMPILER";break;
    case GL_DEBUG_SOURCE_THIRD_PARTY_ARB:     sourceFmt = "THIRD_PARTY";    break;
    case GL_DEBUG_SOURCE_APPLICATION_ARB:     sourceFmt = "APPLICATION";    break;
    case GL_DEBUG_SOURCE_OTHER_ARB:           sourceFmt = "OTHER";          break;
  }

  const char* typeFmt = "UNDEFINED(0x%04X)";
  switch (type) {
    case GL_DEBUG_TYPE_ERROR_ARB:               typeFmt = "ERROR";               break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_ARB: typeFmt = "DEPRECATED_BEHAVIOR"; break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB:  typeFmt = "UNDEFINED_BEHABIOR";  break;
    case GL_DEBUG_TYPE_PORTABILITY_ARB:         typeFmt = "PORTABILITY";         break;
    case GL_DEBUG_TYPE_PERFORMANCE_ARB:         typeFmt = "PERFORMANCE";         break;
    case GL_DEBUG_TYPE_OTHER_ARB:               typeFmt = "OTHER";               break;
  }

  const char* severityFmt = "UNDEFINED";
  switch (severity) {
    case GL_DEBUG_SEVERITY_HIGH_ARB:   severityFmt = "HIGH";   break;
    case GL_DEBUG_SEVERITY_MEDIUM_ARB: severityFmt = "MEDIUM"; break;
    case GL_DEBUG_SEVERITY_LOW_ARB:    severityFmt = "LOW";    break;
  }

  std::ostringstream oss;
  oss << "OpenGL: " << message << " [source=" << sourceFmt << " type=" << typeFmt
    << " severity=" << severityFmt << " id=" << id << "]: " << std::endl;
  return oss.str();
}

void OpenGLManager::DebugCallbackARB(GLenum source, GLenum type, GLuint id,
                                     GLenum severity, GLsizei length,
                                     const GLchar* message, const void* user_param) {
  if (severity == GL_DEBUG_SEVERITY_HIGH_ARB) {
    vrwm_log(Warning, FormatDebugOutputARB(source, type, id, severity, message));
  } else if (severity == GL_DEBUG_SEVERITY_MEDIUM_ARB) {
    vrwm_log(Info, FormatDebugOutputARB(source, type, id, severity, message));
  } else if (severity == GL_DEBUG_SEVERITY_LOW_ARB) {
    vrwm_log(Debug, FormatDebugOutputARB(source, type, id, severity, message));
  } else {
#ifdef VRWM_DEBUG_GL_VERBOSE
    // This can get reeeeeaaally verbose. Each buffer map produces one on my
    // machine.
    vrwm_log(Debug, FormatDebugOutputARB(source, type, id, severity, message));
#endif
  }
}
#endif

void OpenGLManager::AddToRendering(Renderable& r) {
  if(r.Transparent()) {
    sorted_transparent_renderables_.push_back(&r);
    r.set_index_in_sorted(sorted_transparent_renderables_.size() - 1);
  } else {
    sorted_opaque_renderables_.push_back(&r);
    r.set_index_in_sorted(sorted_opaque_renderables_.size() - 1);
  }
}

void OpenGLManager::AddToRendering(const std::set<Renderable*>& r) {
  for (auto current : r) {
    AddToRendering(*current);
  }
}

void OpenGLManager::RemoveFromRendering(Renderable& r) {
  bool update = false;
  auto& vector = r.Transparent() ?
    sorted_transparent_renderables_ :
    sorted_opaque_renderables_;
  for (size_t i = 0; i < vector.size(); ++i) {
    if (vector[i] == &r) {
      vector.erase(vector.begin() + i);
      update = true;
      if (i == vector.size()) {
        break;
      }
    }
    if (update) {
      vector[i]->set_index_in_sorted(i);
    }
  }
}

void OpenGLManager::RemoveFromRendering(const std::set<Renderable*>& r) {
  if (r.empty()) {
    return;
  }

  // Erase first
  for (size_t i = sorted_transparent_renderables_.size() - 1;
      i != static_cast<size_t>(-1);
      --i) {
    if (r.count(sorted_transparent_renderables_[i])) {
      sorted_transparent_renderables_.erase(
        sorted_transparent_renderables_.begin() + i);
    }
  }
  // Then update the indices
  for (size_t i = 0; i < sorted_transparent_renderables_.size(); ++i) {
    sorted_transparent_renderables_[i]->set_index_in_sorted(i);
  }

  // Erase first
  for (size_t i = sorted_opaque_renderables_.size() - 1;
      i != static_cast<size_t>(-1);
      --i) {
    if (r.count(sorted_opaque_renderables_[i])) {
      sorted_opaque_renderables_.erase(
        sorted_opaque_renderables_.begin() + i);
    }
  }
  // Then update the indices
  for (size_t i = 0; i < sorted_opaque_renderables_.size(); ++i) {
    sorted_opaque_renderables_[i]->set_index_in_sorted(i);
  }
}

bool OpenGLManager::OpaqueRenderableComparator::operator()(
    const Renderable* lhs, const Renderable* rhs) {
  uint32_t hash_lhs = lhs->material()->hash();
  uint32_t hash_rhs = rhs->material()->hash();
  // Try material based sorting first to reduce the need for state changes.
  // If the hashes are equal, use distance based sorting
  if (hash_lhs == hash_rhs) {
    auto p = Core::Instance().openGL_manager().offset_from_ground();
    // Distance based sorting: Front to back for opaque objects to increase the
    // number of early depth test fails and thus decrease the amount of
    // fragment shader invocations.
    float d_lhs = glm_enh::lengthSq(p - lhs->scene_node().world_position());
    float d_rhs = glm_enh::lengthSq(p - rhs->scene_node().world_position());
    return d_lhs < d_rhs;
  } else {
    return hash_lhs < hash_rhs;
  }
}

bool OpenGLManager::TransparentRenderableComparator::operator()(
    const Renderable* lhs, const Renderable* rhs) {
  // Strict back to front ordering for transparent objects to ensure that the
  // transparency effects are ok.
  auto p = Core::Instance().openGL_manager().offset_from_ground();
  float d_lhs = glm_enh::lengthSq(p - lhs->scene_node().world_position());
  float d_rhs = glm_enh::lengthSq(p - rhs->scene_node().world_position());
  return d_lhs > d_rhs;
}

void OpenGLManager::RecompileAllPrograms() {
  // We don't actually iterate through all programs, but through all
  // renderables. We currently have no access to all programs.
  for (auto r : renderables_) {
    assert(r->material());
    r->material()->RecompileProgram();
  }

}

