#include "../include/FileTools.h"

#include <stdlib.h>
#include <stdexcept>
#include <set>

// Not cross plattform
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <pwd.h>
#include <errno.h>
#include <string.h>

#include "../include/LogManager.h"

using namespace vrwm;

const std::array<std::string, 2> FileTools::kSearchedFolders { {
  "~/.vrwm/",
  "/usr/share/vrwm/"
} };

std::string FileTools::GetHomePath() throw(std::runtime_error) {
  const char* home;
  home = getenv("HOME");
  if (!home) {
    home = getpwuid(getuid())->pw_dir;
  }
  if (!home) {
    throw std::runtime_error("Unable to retrieve home path.");
  }
  return home;
}

std::string FileTools::Resolve(const std::string& path,
                               const std::string& filename)
                                 throw(std::runtime_error) {
  // Only resolves ~ if it is the first character in the path.
  // This is done to reduce complexity of this function. ~ should not occur
  // anywhere else in the path.
  std::string resolved = path;
  if (resolved.size() > 0 && resolved[0] == '~') {
    resolved.replace(0, 1, GetHomePath());
  }

  // Set the last slash if it doesn't exist.
  if (resolved.size() > 0 && !filename.empty() &&
      resolved.back() != '/' && resolved.back() != '\\') {
    resolved.push_back('/');
  }

  // Append to file name (with possible sub directories) to form the full
  // global file name.
  resolved.append(filename);

  // Replace backslashes '\' with forward slashes '/'.
  size_t backslash_pos = resolved.find('\\');
  while (backslash_pos != std::string::npos) {
    resolved[backslash_pos] = '/';
    backslash_pos = resolved.find('\\', ++backslash_pos);
  }

  return resolved;
}

bool FileTools::FileExists(const std::string& file) noexcept {
  return access(file.c_str(), F_OK) != -1;
}
bool FileTools::IsDirectory(const std::string& file) noexcept {
  struct stat st;
  lstat(file.c_str(), &st);
  return S_ISDIR(st.st_mode);
}
bool FileTools::MatchesExtension(const std::string& file,
                                 const std::string& extension) noexcept {
  if (extension.empty()) {
    return file.rfind('.') == std::string::npos;
  }
  size_t rfind = file.rfind(extension);
  return rfind != std::string::npos &&
    (rfind + extension.size()) == file.size();
}

std::string FileTools::Filename(const std::string& full_path) noexcept {
  std::string::size_type begin, end;
  begin = full_path.rfind('/');
  end = full_path.rfind('.');
  if (begin == std::string::npos) {
    return full_path.substr(0, end);
  } else {
    ++begin;
    if (end > begin) {
      return full_path.substr(begin, end - begin);
    } else {
      return full_path.substr(begin);
    }
  }
}

std::string FileTools::FilenameExt(const std::string& full_path) noexcept {
  std::string::size_type begin;
  begin = full_path.rfind('/');
  if (begin == std::string::npos) {
    return full_path;
  } else {
    ++begin;
    return full_path.substr(begin);
  }
}

std::string FileTools::Directory(const std::string& full_path) noexcept {
  std::string::size_type end;
  end = full_path.rfind('/');
  if (end == std::string::npos) {
    return full_path;
  } else {
    return full_path.substr(0, end + 1);
  }
}

std::pair<bool, std::string> FileTools::Find(const std::string& file) noexcept {
  bool file_exists = false;
  std::string filename = "";
  for (auto it = kSearchedFolders.begin();
      !file_exists && it != kSearchedFolders.end();
      ++it) {
    try {
      filename = Resolve(*it, file);
      vrwm_log(Info, "Looking for file: " + filename);
      file_exists = FileExists(filename);
    } catch(std::exception& e) {
      vrwm_log(Error, "Unable to resolve file '" + filename + "' because "
        " of this exception: " + e.what());
    }
  }

  if (!file_exists) {
    filename = "";
  }
  return std::make_pair(file_exists, filename);
}

//TODO: check if extension is empty - currently files will be found twice
std::vector<std::string> FileTools::AllFilesIn(const std::string& subdir,
                                               const std::string& extension)
                                                 noexcept {
  // We later depend that the dir ends with a slash, so we enforce that.
  std::string subdir_slashed = subdir;
  if (subdir_slashed.size() > 0 && subdir_slashed.back() != '/') {
    subdir_slashed.push_back('/');
  }

  std::set<std::string> found_filenames; // Removes duplicate files
  std::vector<std::string> results;
  for (auto it = kSearchedFolders.begin(); it != kSearchedFolders.end(); ++it) {
    try {
      std::string directory_name = Resolve(*it, subdir_slashed);
      DIR* directory = opendir(directory_name.c_str());
      if (!directory) {
        vrwm_log(Warning, "Error opening directory (does it exist?): " +
          directory_name);
        throw std::runtime_error("");
      }
      dirent* entry;
      // Iterate over all files in that directory
      while ((entry = readdir(directory)) != nullptr) {
        std::string filename = entry->d_name;
        std::string complete_path = directory_name + filename;
        if (!IsDirectory(complete_path) &&
            MatchesExtension(filename, extension) &&
            found_filenames.insert(filename).second) {
          results.push_back(complete_path);
        }
      }
    } catch(std::exception& e) {
    }
  }
  return results;
}

void FileTools::Mkdir(const std::string& path) {
  errno = 0;

  if (FileExists(path) || IsDirectory(path)) {
    return;
  }

  if (-1 == mkdir(path.c_str(), S_IRWXU | S_IRUSR | S_IWUSR)) {
    throw std::runtime_error(strerror(errno));
  }
}

