#include "../include/Cursor.h"

#include "../include/Core.h"
#include "../include/OpenGLManager.h"
#include "../include/Renderable.h"
#include "../include/draw/VertexGroup.h"
#include "../include/XInputDeviceManager.h"

using namespace vrwm;

void VirtualCursor::CacheRaycast() {
  Ray ray = GetRay();
  // Update scene node grab. This is done before the raycast to prevent
  // raycasts not hitting the renderable attached to the scene node when the
  // user moves very fast.
  scene_node_grab_.Update(ray);

  // Cache the old values, we'll need them later.
  Renderable* old_renderable = GetRenderableUnderPointer();

  // Perform the raycast and store the result.
  cached_raycast_hit_ = RaycastHit();
  has_cached_raycast_hit_ = core_.openGL_manager().RaycastAll(ray,
    cached_raycast_hit_);

  // Call on hover exit based on the old data.
  if (old_renderable != nullptr &&
      old_renderable != GetRenderableUnderPointer() &&
      core_.openGL_manager().HasRenderable(old_renderable)) {
    old_renderable->on_hover_exit(cached_raycast_hit_,
      core_.event_handler().active_modifiers());
  }
}

void VirtualCursor::Hover() {
  if (has_cached_raycast_hit_) {
    cached_raycast_hit_.renderable->on_hover(cached_raycast_hit_,
      core_.event_handler().active_modifiers());
  } else {
    // Move Cursor out of screen (or at least try to)
    core_.xinput_manager().WarpPointer(core_.GetScreenSize());
  }
}

void VirtualCursor::Click(MouseButton button,
    const ActiveModifiers& modifiers) {
  if (has_cached_raycast_hit_) {
    cached_raycast_hit_.renderable->on_button(cached_raycast_hit_,
      button, true, modifiers);

    SceneNode* node = cached_raycast_hit_.renderable->grab_scene_node();
    if (node != nullptr && modifiers[core_.config().modkey] &&
        core_.config().direct_grab_enabled) {
      switch (button) {
        case MouseButton3:
          GrabSceneNode(*node, cached_raycast_hit_.GetPoint(),
              SceneNodeGrab::GrabType::Move);
          break;
        case MouseButton1: {
          SceneNodeGrab::GrabType type;
          glm::vec3 modelspace = cached_raycast_hit_.GetModelSpacePoint();
          modelspace.z = 0;
          modelspace = glm::normalize(modelspace);
          float cosine = std::fabs(std::cos(modelspace.x * 3.1415f * 0.5f));
          if (cosine > 0.4f && cosine < 0.6f) {
            type = SceneNodeGrab::GrabType::ScaleDiagonal;
          } else if (cosine <= 0.4f) {
            type = SceneNodeGrab::GrabType::ScaleHorizontal;
          } else {
            type = SceneNodeGrab::GrabType::ScaleVertical;
          }
          GrabSceneNode(*node, cached_raycast_hit_.GetPoint(), type);
          break;
        }
        case MouseButton4:
        case MouseButton5:
          GrabSceneNode(*node, cached_raycast_hit_.GetPoint(),
              SceneNodeGrab::GrabType::Move);
          scene_node_grab_.MoveGrabbedRenderableForward(
            button == MouseButton4 ? -0.1f : 0.1f);
          scene_node_grab_.Update(GetRay());
          scene_node_grab_.Abort();
          break;
        default: break;
      }
    }
  }
}
void VirtualCursor::Release(MouseButton button,
    const ActiveModifiers& modifiers) {
  if (has_cached_raycast_hit_) {
    cached_raycast_hit_.renderable->on_button(cached_raycast_hit_,
      button, false, modifiers);
  }

  scene_node_grab_.Abort();
}
