#include "../include/Renderable.h"

#include <math.h>

#include <glm/matrix.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "../include/Assets.h"
#include "../include/Client.h"
#include "../include/Core.h"
#include "../include/LogManager.h"
#include "../include/OpenGLManager.h"
#include "../include/PluginManager.h"
#include "../include/SceneNode.h"
#include "../include/draw/VertexAttribute.h"
#include "../include/draw/Material.h"

using namespace vrwm;


Renderable::Renderable(Core& c, SceneNode& n, draw::VertexGroupPtr mesh,
      draw::MaterialPtr m) :
    core_(c),
    client_(nullptr),
    scene_node_(&n),
    grab_scene_node_(nullptr),
    vertex_group_(mesh),
    index_in_sorted_(0),
    user_data_(nullptr),
    material_(m),
    on_scene_node_resize_(nullptr),
    on_client_map_(nullptr),
    on_client_iconic_(nullptr),
    on_client_close_(nullptr),
    on_client_resize_(nullptr) {
  n.AttachRenderable(*this);
  core_.openGL_manager().Add(this);
}

Renderable::Renderable(const Renderable& copy) : core_(copy.core_) {
  // In this special case, scene node is nullptr. But not for long.
  scene_node_ = nullptr;
  core_.openGL_manager().Add(this);
  *this = copy;
}

Renderable& Renderable::operator=(const Renderable& rhs) {
  assert(&core_ == &rhs.core_);
  if (this == &rhs) {
    return *this;
  }

  client_ = rhs.client_;
  grab_scene_node_ = rhs.grab_scene_node_;
  vertex_group_ = rhs.vertex_group_;
  // Don't copy index, will be update by OpenGL
  //index_in_sorted_ = rhs.index_in_sorted_;
  user_data_ = rhs.user_data_;
  material_ = rhs.material_;
  on_scene_node_resize_ = rhs.on_scene_node_resize_;
  on_button_ = rhs.on_button_;
  on_hover_ = rhs.on_hover_;
  on_hover_exit_ = rhs.on_hover_exit_;
  on_client_map_ = rhs.on_client_map_;
  on_client_iconic_ = rhs.on_client_iconic_;
  on_client_close_ = rhs.on_client_close_;
  on_client_resize_ = rhs.on_client_resize_;

  if (scene_node_) {
    scene_node_->RelocateRenderable(*this, *rhs.scene_node_);
  } else {
    rhs.scene_node_->AttachRenderable(*this);
  }
  scene_node_ = rhs.scene_node_;

  return *this;
}

Renderable::~Renderable() {
  core_.openGL_manager().Remove(this);
  if (client_) {
    client_->separate_from_renderable(this);
  }
}

void Renderable::set_client(Client* c) {
  if (client_) {
    client_->separate_from_renderable(this);
  }
  client_ = c;
  if (client_) {
    client_->associate_with_renderable(this);
  }
}

void Renderable::CreateDefaultClientRenderable(Core& core, Client* c) {
  assert(core.openGL_manager().root_scene_node());
  assert(core.openGL_manager().billboard_target());
  bool decorate = true;
  Client* relative_to = c->GetTransientFor();
  SceneNode* s;
  if (relative_to) {
    if (c->is_override_redirect()) {
      decorate = false;
    }
    s = core.openGL_manager().root_scene_node()->FindByName(
      std::to_string(relative_to->ID()));
    assert(s);
    auto tmp = s->FindByName("top-left");
    if (!tmp) {
      s = s->CreateChild("top-left");
      s->SetLocalPosition(glm::vec3(0.5f, 0.5f, 0.0f));
      s->SetInheritScale(false);
    } else {
      s = tmp;
    }
    assert(s);
    s = s->CreateChild(std::to_string(c->ID()));
    assert(s);
    s->SetLocalPosition(glm::vec3(
      -c->x11_position().pos.x / c->pixel_per_centimeter() * 0.01f -
        c->GetHalfWidth(),
      -c->x11_position().pos.y / c->pixel_per_centimeter() * 0.01f -
        c->GetHalfHeight(),
      -0.03f));
  } else {
    s = core.openGL_manager().root_scene_node()->CreateChild(
      std::to_string(c->ID()));
    s->SetBillboardTarget(core.openGL_manager().billboard_target());
    //TODO: make spawn distance configurable
    glm::vec3 pos = core.openGL_manager().GetLookAt(4.0f);
    s->SetLocalPosition(pos);
  }
  assert(s);

  s->SetVisible(false);

  glm::vec3 tmp_world_pos = s->world_position();
  glm::vec3 tmp_forward = s->GetForward();
  glm::vec3 tmp_up = s->GetUp();
  vrwm_clogdeb("New scene node at position: %.2f, %.2f, %.2f  Forward: %.2f, %.2f, %2.f Up: %.2f, %.2f, %2.f",
    tmp_world_pos.x, tmp_world_pos.y, tmp_world_pos.z, tmp_forward.x,
    tmp_forward.y, tmp_forward.z, tmp_up.x, tmp_up.y, tmp_up.z);

  //auto m = draw::Material::Create(Assets::program("window program"), false);
  // COPY material because we change the texture.
  draw::MaterialPtr tmp_m;
  if (c->is_transparent()) {
    tmp_m = Assets::material(Assets::MaterialType::TransparentDiffuseTextured);
  } else {
    tmp_m = Assets::material(Assets::MaterialType::Simple);
  }
  draw::MaterialPtr m = draw::MaterialPtr(new draw::Material(*tmp_m));
  // The texture pointer is never updated, so we don't need to store it
  // anywhere
  m->SetUniform<GLtexturePtr>("texture", c->texture());
  m->SetUniform<glm::vec4>("color", glm::vec4(1, 1, 1, 1));

  Renderable* r = new Renderable(core, *s,
    Assets::mesh("default client renderable"), m);
  if (relative_to) {
    r->set_grab_scene_node(core.openGL_manager().root_scene_node()->FindByName(
      std::to_string(relative_to->ID())));
  } else {
    r->set_grab_scene_node(s);
  }

  r->set_on_button([](Renderable& r, const RaycastHit& h, MouseButton button,
      bool pressed, const ActiveModifiers& modifiers) {
    assert(r.client());
    glm::vec3 modelspace = h.GetModelSpacePoint();
    glm::vec2 coordinates(-modelspace.x + 0.5f, -modelspace.y + 0.5f);
    if (button == MouseButton1 && pressed) {
      r.client()->Activate();
    }
    r.client()->InjectButtonEvent(pressed, coordinates, button, modifiers);
  });

  r->set_on_hover([](Renderable& r, const RaycastHit& h,
      const ActiveModifiers& modifiers) {
    assert(r.client());
    glm::vec3 modelspace = h.GetModelSpacePoint();
    glm::vec2 coordinates(-modelspace.x + 0.5f, -modelspace.y + 0.5f);
    r.core().set_cursor_client(*r.client(), coordinates);
    r.client()->InjectPointerMotion(coordinates, modifiers);
  });
  r->set_on_hover_exit([](Renderable& r, const RaycastHit& h,
      const ActiveModifiers& modifiers) {
    // We are not interested in the details of the ray cast that did not
    // hit the renderable. But you can use it for whatever you want.
    (void) h; (void) modifiers;
    r.core().unset_cursor_client();
  });

  // What should happen if the client is mapped?
  r->set_on_client_map([](Renderable& r, Client& c) {
    r.scene_node().SetVisible(true);
    r.scene_node().SetWorldScale(c.GetHalfWidth() * 2, c.GetHalfHeight() * 2,
      1.0f, true);
    //r.GetSceneNode().on_client_resize(c); // Set correct renderable size.
  });

  if (relative_to) {
    r->set_on_client_resize([](Renderable& r, Client& c) {
      r.scene_node().SetWorldScale(c.GetHalfWidth() * 2, c.GetHalfHeight() * 2,
        1.0f, true);
      r.scene_node().SetLocalPosition(glm::vec3(
        -c.x11_position().pos.x / c.pixel_per_centimeter() * 0.01f -
          c.GetHalfWidth(),
        -c.x11_position().pos.y / c.pixel_per_centimeter() * 0.01f -
          c.GetHalfHeight(),
        -0.03f));
    });
  } else {
    r->set_on_client_resize([](Renderable& r, Client& c) {
      r.scene_node().SetWorldScale(c.GetHalfWidth() * 2, c.GetHalfHeight() * 2,
        1.0f, true);
    });
  }

  // What should happen if the client is minimized?
  r->set_on_client_iconic([](Renderable& r, Client& c) {
    (void) c;
    r.scene_node().SetVisible(false);
  });

  // What should happen if the client is closed? In the default: simply destroy
  // the Renderable. May be used to display a fancy "shatter" animation or some
  // such (think Compiz).
  // Note that the client attribute must be set to nullptr because the client
  // usually first enters withdrawn state (which causes this function to be
  // called) before the client object is destroyed (which causes this function
  // to be called again if the renderable is still attached)
  r->set_on_client_close([](Renderable& r, Client& c) {
    (void) c;
    r.set_client(nullptr);
    r.scene_node().Destroy(); // Will also delete the renderable.
  });

  // This callback is the reason why you shouldn't attach more than one window
  // renderable to a scene node. Race conditions on the client resize may cause
  // issues if two or more renderables are on the same scene node.
  r->set_on_scene_node_resize(
    [](Renderable& r, const glm::vec3& future_world_scale) -> bool {
      assert(r.client());
      glm::vec2 size = glm::vec2(future_world_scale);
      size *= r.client()->pixel_per_centimeter() * 100.0f;
      vrwm_logdeb("new size: " + vrwm_to_string(size));
      if (r.client()->Resize(size.x, size.y)) {
        // The resize is now pending, on_client_resize will be called in the
        // future. (Once the client has resized).
        return true;
      } else {
        // The client does not support the size. We therefore prevent the scene
        // node from scaling.
        return false;
      }
  });

  r->set_client(c);

  if (decorate) {
    Renderable::CreateDefaultClientDecorators(core, *s, c);
  }
}

void Renderable::CreateDefaultClientDecorators(Core& core, SceneNode& n,
    Client* c) {
  SceneNode* border_node = n.CreateChild("border");
  assert(border_node);
  border_node->SetLocalPosition(glm::vec3(0.0f, 0.0f, 0.05f));
  auto border_mat = Assets::material("default client border");
  assert(border_mat);
  Renderable* border_renderable = new Renderable(core, *border_node,
    Assets::mesh("default client border"), border_mat);
  border_renderable->set_grab_scene_node(&n);

  border_renderable->set_on_button([](Renderable& r, const RaycastHit& h,
      MouseButton button, bool pressed, const ActiveModifiers& modifiers) {
    (void) modifiers;
    if (!pressed) {
      return;
    }
    if (button == MouseButton1) {
      SceneNodeGrab::GrabType type;
      glm::vec3 modelspace = h.GetModelSpacePoint();
      modelspace.z = 0;
      modelspace = glm::normalize(modelspace);
      float cosine = std::fabs(std::cos(modelspace.x * 3.1415f * 0.5f));
      if (cosine > 0.4f && cosine < 0.6f) {
        type = SceneNodeGrab::GrabType::ScaleDiagonal;
      } else if (cosine <= 0.4f) {
        type = SceneNodeGrab::GrabType::ScaleHorizontal;
      } else {
        type = SceneNodeGrab::GrabType::ScaleVertical;
      }
      r.core().cursor().GrabSceneNode(*r.grab_scene_node(), h.GetPoint(), type);
    } else if (button == MouseButton3) {
      r.core().cursor().GrabSceneNode(*r.grab_scene_node(), h.GetPoint(),
        SceneNodeGrab::GrabType::Move);
    }
  });


  auto titlebar_mat = Assets::material("default client titlebar");
  assert(titlebar_mat);

  SceneNode* buttons_node = n.CreateChild("buttons");
  assert(buttons_node);
  buttons_node->SetLocalPosition(glm::vec3(-0.5f, 0.5f, 0.00f));
  buttons_node->SetInheritScale(false);
  buttons_node->SetLocalScale(glm::vec3(0.3f, 0.3f, 0.3f));

  SceneNode* close_button_node = buttons_node->CreateChild("close button");
  assert(close_button_node);
  close_button_node->SetLocalPosition(glm::vec3(0.5f, 0.5f, 0.0f));

  Renderable* close_button_renderable = new Renderable(core,
    *close_button_node, Assets::mesh("default client button"),
    Assets::material("default client button1"));
  close_button_renderable->set_user_data(c);

  close_button_renderable->set_on_button(
    [](Renderable& r, const RaycastHit& h, MouseButton button,
        bool pressed, const ActiveModifiers& modifiers) {
      (void) h; (void) modifiers;
      if (button == MouseButton1 && !pressed) {
        static_cast<Client*>(r.user_data())->Close();
      }
    });

  SceneNode* maximize_button_node = buttons_node->CreateChild("close button");
  assert(maximize_button_node);
  maximize_button_node->SetLocalPosition(glm::vec3(1.5f, 0.5f, 0.0f));

  Renderable* maximize_button_renderable = new Renderable(core,
    *maximize_button_node, Assets::mesh("default client button"),
    Assets::material("default client button2"));
  maximize_button_renderable->set_user_data(c);

  maximize_button_renderable->set_on_button(
    [](Renderable& r, const RaycastHit& h, MouseButton button,
        bool pressed, const ActiveModifiers& modifiers) {
      (void) h; (void) modifiers;
      if (button == MouseButton1 && !pressed) {
        static_cast<Client*>(r.user_data())->SetMaximized(true);
      }
    });

}

void Renderable::ConstructDefaultClientRenderableMesh() {
  draw::VertexGroup* group = new draw::VertexGroup();
  float main_vertex_data[] = {
    // pos
     0.5f,  0.5f, 0.0f, // top left
    -0.5f,  0.5f, 0.0f, // top right
     0.5f, -0.5f, 0.0f, // bot left
    -0.5f, -0.5f, 0.0f, // bot right
    // tex
    0.0f, 0.0f,
    1.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 1.0f,
  };
  group->SetData(main_vertex_data, sizeof(main_vertex_data));
  std::vector<GLuint> rendering_order = { 0, 2, 1, 3 };
  group->SetElements(GL_TRIANGLE_STRIP, rendering_order);
  // position
  group->AddRaycastableVertexAttribute(draw::ResolvedVertexAttribute(0, 3));
  // texture
  group->AddVertexAttribute(draw::ResolvedVertexAttribute(1, 2,
    4*3*sizeof(float)));
  Assets::add("default client renderable", group);
}

void Renderable::ConstructDefaultClientDecoratorMeshes() {
  draw::VertexGroup* close_button = new draw::VertexGroup();
  float close_button_vertex_data[] = {
    // back
     0.5f,  0.5f, 0.0f, // top left
    -0.5f,  0.5f, 0.0f, // top right
     0.5f, -0.5f, 0.0f, // bot left
    -0.5f, -0.5f, 0.0f, // bot right
    // front (i.e. the cap of the button)
     0.3f,  0.3f, -0.2f, // top left
    -0.3f,  0.3f, -0.2f, // top right
     0.3f, -0.3f, -0.2f, // bot left
    -0.3f, -0.3f, -0.2f, // bot right
    // normals
     0.7071f,  0.7071f,  0.0f,
    -0.7071f,  0.7071f,  0.0f,
     0.7071f, -0.7071f,  0.0f,
    -0.7071f, -0.7071f,  0.0f,
     0.5774f,  0.5774f, -0.57734f,
    -0.5774f,  0.5774f, -0.57734f,
     0.5774f, -0.5774f, -0.57734f,
    -0.5774f, -0.5774f, -0.57734f,
  };
  close_button->SetData(close_button_vertex_data,
    sizeof(close_button_vertex_data));
  std::vector<GLuint> rendering_order_close_button = {
    0, 4, 1, 5, 3, 7, 2, 6, 0, 4, // border
    4, 6, 5, 7, // null-triangle followed by cap
  };
  close_button->SetElements(GL_TRIANGLE_STRIP, rendering_order_close_button);
  // position
  close_button->AddRaycastableVertexAttribute(
    draw::ResolvedVertexAttribute(0, 3));
  close_button->AddVertexAttribute(draw::ResolvedVertexAttribute(2, 3,
    8*3*sizeof(float)));
  Assets::add("default client button", close_button);

  draw::VertexGroup* titlebar = new draw::VertexGroup();
  float titlebar_vertex_data[] = {
     0.5f,  0.5f, 0.0f, // top left
    -0.5f,  0.5f, 0.0f, // top right
     0.5f, -0.5f, 0.0f, // bot left
    -0.5f, -0.5f, 0.0f, // bot right
    // Normals
     0.0f,  0.0f, -1.0f, // top left
     0.0f,  0.0f, -1.0f, // top right
     0.0f,  0.0f, -1.0f, // bot left
     0.0f,  0.0f, -1.0f, // bot right
  };
  titlebar->SetData(titlebar_vertex_data,
    sizeof(titlebar_vertex_data));
  std::vector<GLuint> rendering_order_titlebar = { 0, 1, 2, 3 };
  titlebar->SetElements(GL_TRIANGLE_STRIP, rendering_order_titlebar);
  // position
  titlebar->AddRaycastableVertexAttribute(draw::ResolvedVertexAttribute(0, 3));
  titlebar->AddVertexAttribute(draw::ResolvedVertexAttribute(2, 3,
    4*3*sizeof(float)));
  Assets::add("default client titlebar", titlebar);

  // The border: Numbers are the vertex IDs in the array. The middle piece
  // is empty. This is where the other mesh with the client window resides.
  //  4---9----------------------------11---5
  //  |   |                             |   |
  //  8---0-----------------------------1--10
  //  |   |                             |   |
  //  |   |    -this part is empty-     |   |
  //  |   |                             |   |
  //  13--2-----------------------------3--15
  //  |   |                             |   |
  //  6--12----------------------------14---7
  draw::VertexGroup* border = new draw::VertexGroup();
  float border_vertex_data[] = {
     //inner
     0.5f,  0.5f, 0.0f, // top left             0
    -0.5f,  0.5f, 0.0f, // top right            1
     0.5f, -0.5f, 0.0f, // bot left             2
    -0.5f, -0.5f, 0.0f, // bot right            3
     //outer
     0.55f,  0.55f, 0.0f, // top left           4
    -0.55f,  0.55f, 0.0f, // top right          5
     0.55f, -0.55f, 0.0f, // bot left           6
    -0.55f, -0.55f, 0.0f, // bot right          7
     //corners
     0.55f,  0.5f,  0.0f, // top left bottom    8
     0.5f,   0.55f, 0.0f, // top left upper     9
    -0.55f,  0.5f,  0.0f, // top right bottom  10
    -0.5f,   0.55f, 0.0f, // top right upper   11
     0.5f,  -0.55f, 0.0f, // bot left bottom   12
     0.55f, -0.5f,  0.0f, // bot left upper    13
    -0.5f,  -0.55f, 0.0f, // bot right bottom  14
    -0.55f, -0.5f,  0.0f, // bot right upper   15
    // Normals i.e. 16 times 0, 0, -1
     0.0f, 0.0f, -1.0f,
     0.0f, 0.0f, -1.0f,
     0.0f, 0.0f, -1.0f,
     0.0f, 0.0f, -1.0f,
     0.0f, 0.0f, -1.0f,
     0.0f, 0.0f, -1.0f,
     0.0f, 0.0f, -1.0f,
     0.0f, 0.0f, -1.0f,
     0.0f, 0.0f, -1.0f,
     0.0f, 0.0f, -1.0f,
     0.0f, 0.0f, -1.0f,
     0.0f, 0.0f, -1.0f,
     0.0f, 0.0f, -1.0f,
     0.0f, 0.0f, -1.0f,
     0.0f, 0.0f, -1.0f,
     0.0f, 0.0f, -1.0f,
  };
  border->SetData(border_vertex_data,
    sizeof(border_vertex_data));
  std::vector<GLuint> rendering_order_border = {
    9, 0, 11, 1, 5, 1, 10, 3, 15, 3, 7, 2, 14, 12, 6, 2, 13, 0, 8, 4, 9, 0
  };
  border->SetElements(GL_TRIANGLE_STRIP, rendering_order_border);
  // position
  border->AddRaycastableVertexAttribute(draw::ResolvedVertexAttribute(0, 3));
  border->AddVertexAttribute(draw::ResolvedVertexAttribute(2, 3,
    16 * 3 * sizeof(float)));
  Assets::add("default client border", border);

  auto titlebar_mat =
    new draw::Material(*Assets::material(Assets::MaterialType::Diffuse));
  titlebar_mat->SetUniform<glm::vec4>("color_diffuse",
    glm::vec4(0.5f, 0.8f, 0.5f, 1.0f));
  Assets::add("default client titlebar", titlebar_mat);

  auto border_mat =
    new draw::Material(*Assets::material(
      Assets::MaterialType::TransparentDiffuse));
  border_mat->SetUniform<glm::vec4>("color_diffuse",
    glm::vec4(0.0f, 0.0f, 1.0f, 0.4f));
  Assets::add("default client border", border_mat);

  auto button1_mat =
    new draw::Material(*Assets::material(Assets::MaterialType::Diffuse));
  button1_mat->SetUniform<glm::vec4>("color_diffuse",
    glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
  Assets::add("default client button1", button1_mat);
  auto button2_mat =
    new draw::Material(*Assets::material(Assets::MaterialType::Diffuse));
  button2_mat->SetUniform<glm::vec4>("color_diffuse",
    glm::vec4(0.0f, 0.8f, 0.5f, 1.0f));
  Assets::add("default client button2", button2_mat);
}

void Renderable::Render() {
  assert(material_);
  assert(vertex_group_);
  material_->Use();
  vertex_group_->Render();
}

bool Renderable::Transparent() const {
  return material_->transparent();
}

bool Renderable::Raycast(const Ray& r, RaycastHit& out_hit) {
  if (!scene_node_->is_visible()) {
    return false;
  }
  bool result = false;

  size_t triangle_ID = 0;
  for (auto triangle_iter = vertex_group_->begin_raycastable_triangles();
      triangle_iter != vertex_group_->end_raycastable_triangles();
      ++triangle_iter, ++triangle_ID) {
    const float epsilon = 1e-4f;

    // Uses the Moeller-Trumbore triangle intersection algorithm.
    // Basically copy pasted. Look it up if you're interested on how
    // it works, I'll not explain it here.
    // TODO: performance
    const glm::vec3 v1 = scene_node_->ToWorldSpace(triangle_iter->v1);
    const glm::vec3 v2 = scene_node_->ToWorldSpace(triangle_iter->v2);
    const glm::vec3 v3 = scene_node_->ToWorldSpace(triangle_iter->v3);
    const glm::vec3 e1 = v2 - v1;
    const glm::vec3 e2 = v3 - v1;

    const glm::vec3 pvec = glm::cross(r.direction, e2);
    float det = glm::dot(e1, pvec);
    if (glm::abs(det) < epsilon) {
      continue;
    }
    float inv_det = 1.0f / det;
    const glm::vec3 tvec = r.origin - v1;
    float u = glm::dot(tvec, pvec) * inv_det;
    if (u < 0.0f || u > 1.0f) {
      continue;
    }
    const glm::vec3 qvec = glm::cross(tvec, e1);
    float v = glm::dot(r.direction, qvec) * inv_det;

    if (v < 0.0f || u + v > 1.0f) {
      continue;
    }

    float t = glm::dot(e2, qvec) * inv_det;
    if (t > epsilon) {
      // Success! We hit the triangle.
      // But now we need to check if the hit occured before or behind a
      // previous hit.
      if (t < out_hit.t) {
        // Result is false if the hit has occurred in a previous renderable.
        if (!result) {
          out_hit.ray = r;
          out_hit.renderable = this;
        }
        result = true;
        out_hit.t = t;
        out_hit.triangle_ID = triangle_ID;
        out_hit.triangle = *triangle_iter;
        out_hit.v1_v2_factor = u;
        out_hit.v1_v3_factor = v;
      }
    }
  }
  return result;
}

void Renderable::SetSceneNode(SceneNode& n) {
  SceneNode* old = scene_node_;
  scene_node_ = &n;
  old->RelocateRenderable(*this, n);
}

