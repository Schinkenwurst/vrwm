
#include "../include/Assets.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/mesh.h>
#include <assimp/material.h>
#include <assimp/texture.h>
#include <assimp/vector3.h>
#include <assimp/quaternion.h>
#include <assimp/matrix4x4.h>

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/matrix.hpp>
#include <glm/gtc/epsilon.hpp>
#include <glm/gtc/quaternion.hpp>

#include "../include/Core.h"
#include "../include/FileTools.h"
#include "../include/GLprogram.h"
#include "../include/GLshader.h"
#include "../include/GLtexture.h"
#include "../include/draw/VertexGroup.h"
#include "../include/draw/Material.h"
#include "../include/SceneNode.h"

using namespace vrwm;

std::map<std::string, GLtexturePtr> Assets::g_textures;
std::map<std::string, draw::VertexGroupPtr> Assets::g_meshes;
std::array<draw::MaterialPtr, static_cast<size_t>(Assets::MaterialType::SIZE)>
  Assets::g_materials;
std::map<std::string, draw::MaterialPtr> Assets::g_custom_materials;
std::map<std::string, SceneNode*> Assets::g_asset_files;

const std::string Assets::kAssetFileSubPath = "assets/";
const std::string Assets::kAssetTextureFallbackSubPath = "assets/textures/";

GLtexturePtr Assets::texture(const std::string& name) {
  auto it = g_textures.find(name);
  if (it == g_textures.end())
    return nullptr;
  return it->second;
}

draw::VertexGroupPtr Assets::mesh(const std::string& name) {
  auto it = g_meshes.find(name);
  if (it == g_meshes.end())
    return nullptr;
  return it->second;
}

draw::MaterialPtr Assets::material(const std::string& name) {
  auto it = g_custom_materials.find(name);
  if (it == g_custom_materials.end())
    return nullptr;
  return it->second;
}

draw::MaterialPtr Assets::material(MaterialType type) {
  assert(type < MaterialType::SIZE);
  return g_materials[static_cast<size_t>(type)];
}

GLtexturePtr Assets::add(std::string&& name, GLtexture* texture) {
  auto r = g_textures.insert(
    std::make_pair(std::move(name), GLtexturePtr(texture)));
  return r.first->second;
}

GLtexturePtr Assets::add(const std::string& name, GLtexture* texture) {
  auto r = g_textures.insert(std::make_pair(name, GLtexturePtr(texture)));
  return r.first->second;
}

draw::VertexGroupPtr Assets::add(const std::string& name,
    draw::VertexGroup* mesh) {
  auto r = g_meshes.insert(std::make_pair(name, draw::VertexGroupPtr(mesh)));
  return r.first->second;
}

draw::MaterialPtr Assets::add(const std::string& name, draw::Material* mat) {
  auto r = g_custom_materials.insert(
    std::make_pair(name, draw::MaterialPtr(mat)));
  return r.first->second;
}

draw::MaterialPtr Assets::add(MaterialType type, draw::Material* mat) {
  assert(type < MaterialType::SIZE);
  auto ptr = draw::MaterialPtr(mat);
  g_materials[static_cast<size_t>(type)] = ptr;
  return ptr;
}

const SceneNode* Assets::LoadAssetFile(const std::string& pathless_filename) {
  auto it = g_asset_files.find(pathless_filename);
  // Already loaded
  if (it != g_asset_files.end()) {
    return it->second;
  }
  // Not loaded, make it so.

  // Try to search the file
  auto exists_file = FileTools::Find(kAssetFileSubPath + pathless_filename);
  if (!exists_file.first) {
    vrwm_log(Error, "File not found: " + pathless_filename);
    return nullptr;
  }

  std::string& file = exists_file.second;
  Assimp::Importer importer;
  const aiScene* scene = importer.ReadFile(file,
    aiProcessPreset_TargetRealtime_Fast);
  if (!scene) {
    vrwm_log(Error, "Unable to load asset file scene: " +
      std::string(importer.GetErrorString()));
    return nullptr;
  }

  SceneNode* root = nullptr;
  try {
    vrwm_logdeb("Assimp loaded the scene. Begin convert...");
    //ConvertAssimpTextures(scene->mTextures, scene->mNumTextures);
    std::string asset_dir = FileTools::Directory(file);
    auto materials = Import(scene->mMaterials, scene->mNumMaterials, asset_dir);
    auto meshes = Import(scene->mMeshes, scene->mNumMeshes);
    root = Import(scene, scene->mRootNode, meshes, materials, nullptr);
    g_asset_files[pathless_filename] = root;
    vrwm_logdeb("Done convert.");
  } catch(std::exception& e) {
    vrwm_clog(Error, "Exception on assimp scene parsing: %s", e.what());
  }

  return root;
}

std::deque<draw::VertexGroupPtr> Assets::Import(const aiMesh* const* meshes,
    unsigned int n) {
  std::deque<draw::VertexGroupPtr> to_return;
  for (unsigned i = 0; i < n; ++i) {
    const aiMesh* m = meshes[i];
    draw::VertexGroup* converted = Import(m);
    // Note: converted may be null.
    to_return.push_back(draw::VertexGroupPtr(converted));
  }
  return to_return;
}
std::deque<draw::MaterialPtr> Assets::Import(const aiMaterial* const* mats,
    unsigned int n, const std::string& asset_dir) {
  std::deque<draw::MaterialPtr> to_return;
  for (unsigned i = 0; i < n; ++i) {
    const aiMaterial* m = mats[i];
    draw::Material* converted = Import(m, asset_dir);
    to_return.push_back(draw::MaterialPtr(converted));
  }
  return to_return;
}

SceneNode* Assets::Import(const aiScene* scene, const aiNode* node,
    const std::deque<draw::VertexGroupPtr>& meshes,
    const std::deque<draw::MaterialPtr>& materials,
    SceneNode* parent) {
  SceneNode* vrwm_node;
  if (parent) {
    vrwm_node = parent->CreateChild(node->mName.C_Str());
  } else {
    vrwm_node = new SceneNode(Core::Instance(), node->mName.C_Str());
  }

  aiVector3D decomp_position, decomp_scale;
  aiQuaternion decomp_rotation;
  node->mTransformation.Decompose(decomp_scale, decomp_rotation,
    decomp_position);

  glm::vec3 local_position, local_scale;
  glm::quat local_rotation;

  for (int i = 0; i < 3; ++i) {
    local_position[i] = decomp_position[i];
    local_scale[i] = decomp_scale[i];
  }
  local_rotation.w = decomp_rotation.w;
  local_rotation.x = decomp_rotation.x;
  local_rotation.y = decomp_rotation.y;
  local_rotation.z = decomp_rotation.z;
  vrwm_node->SetLocalPosition(local_position);
  vrwm_node->SetLocalOrientation(local_rotation);
  vrwm_node->SetLocalScale(local_scale);

  for (size_t i = 0; i < node->mNumMeshes; ++i) {
    draw::VertexGroupPtr mesh = meshes[node->mMeshes[i]];
    assert(node->mMeshes[i] < scene->mNumMeshes);
    auto mat = materials[scene->mMeshes[node->mMeshes[i]]->mMaterialIndex];
    if (mesh && mat) {
      new Renderable(Core::Instance(), *vrwm_node, mesh, mat);
    }
  }

  for (size_t i = 0; i < node->mNumChildren; ++i) {
    Import(scene, node->mChildren[i], meshes, materials,
      vrwm_node);
  }
  return vrwm_node;
}

draw::VertexGroup* Assets::Import(const aiMesh* mesh) {
  if (!mesh->HasFaces()) {
    return nullptr;
  }

  LogMeshInfo(mesh);

  size_t buffer_size = sizeof(float) * mesh->mNumVertices *
    CalculateFloatsPerVertex(mesh);
  uint8_t* buffer = new uint8_t[buffer_size];
  FillVertexBuffer(mesh, buffer, buffer_size);
  auto vrwm_mesh = CreateMesh(mesh, buffer, buffer_size);
  delete buffer;
  // TODO: Add support in the shader for stuff like normals, tangents, etc.
  return vrwm_mesh;
}

size_t Assets::CalculateFloatsPerVertex(const aiMesh* mesh) {
  // 3 floats each per vertex position
  size_t floats_per_vert = 3;
  // 3 floats per vertex normal (if any)
  floats_per_vert += mesh->HasNormals() ? 3 : 0;
  // 3 floats per tangent + 3 floats per bitangent. In Assimp, one cannot exist
  // without the other. Much like the two sides of the force.
  floats_per_vert += mesh->HasTangentsAndBitangents() ? 6 : 0;
  // 2 floats per available UV channel. Assimp can do one or three dimensional
  // UV channels, but we do not allow it (to make things easier). Therefore, we
  // need to count them ourselves instead of using the function of aiMesh.
  // Note: currently only the first UV channel is supported.
  for (size_t i = 0; i < 1; ++i) {
    // Note: Currently, only 2D texture coords are supported.
    if (mesh->mNumUVComponents[i] != 2) {
      continue;
    }
    floats_per_vert += 2;
  }
  // RGBA colors per vertex per channel
  // Note: currently only the first UV channel is supported.
  floats_per_vert += (mesh->mColors[0]) ? 4 : 0;
  return floats_per_vert;
}

void Assets::FillVertexBuffer(const aiMesh* mesh, void* buffer,
    size_t buffer_size) {
  // Our data layout will be sequential (i.e. "blocks" of data) in the following order:
  // position        xyz  (float)
  // normal          xyz  (float)
  // tangent         xyz  (float)
  // bitangent       xyz  (float)
  // UV channels     uv   (float)
  // Color channels  rgba (float)

  // Buffer size only used for assertions. Fix warning "unused variable" in
  // release.

  void* current_buffer_pos = buffer;
  void* buffer_end = reinterpret_cast<uint8_t*> (buffer) + buffer_size;
  (void) buffer_end; // fix warning "unused variable" (asserts not in release)

  assert(current_buffer_pos < buffer_end);
  current_buffer_pos = WritePositions(mesh, current_buffer_pos);

  assert(!mesh->HasNormals() || current_buffer_pos < buffer_end);
  current_buffer_pos = WriteNormals(mesh, current_buffer_pos);

  assert(!mesh->HasTangentsAndBitangents() || current_buffer_pos < buffer_end);
  current_buffer_pos = WriteTangentsAndBitangents(mesh, current_buffer_pos);

  assert(mesh->GetNumUVChannels() == 0 || current_buffer_pos < buffer_end);
  current_buffer_pos = WriteTextureCoordinates(mesh, current_buffer_pos);

  assert(mesh->GetNumColorChannels() == 0 || current_buffer_pos < buffer_end);
  current_buffer_pos = WriteColorData(mesh, current_buffer_pos);

  assert(current_buffer_pos == buffer_end);
}

void* Assets::WritePositions(const aiMesh* mesh, void* buffer) {
  for (size_t i = 0; i < mesh->mNumVertices; ++i) {
    (reinterpret_cast<float*> (buffer))[i * 3 + 0] = mesh->mVertices[i].x;
    (reinterpret_cast<float*> (buffer))[i * 3 + 1] = mesh->mVertices[i].y;
    (reinterpret_cast<float*> (buffer))[i * 3 + 2] = mesh->mVertices[i].z;
  }
  return (reinterpret_cast<float*>(buffer)) + mesh->mNumVertices * 3;
}

void* Assets::WriteNormals(const aiMesh* mesh, void* buffer) {
  if (mesh->HasNormals()) {
    for (size_t i = 0; i < mesh->mNumVertices; ++i) {
      (reinterpret_cast<float*>(buffer))[i * 3 + 0] = mesh->mNormals[i].x;
      (reinterpret_cast<float*>(buffer))[i * 3 + 1] = mesh->mNormals[i].y;
      (reinterpret_cast<float*>(buffer))[i * 3 + 2] = mesh->mNormals[i].z;
    }
    buffer = (reinterpret_cast<float*>(buffer)) + mesh->mNumVertices * 3;
  }
  return buffer;
}

void* Assets::WriteTangentsAndBitangents(const aiMesh* mesh, void* buffer) {
  if (mesh->HasTangentsAndBitangents()) {
    float* bitangent_buffer = reinterpret_cast<float*>(buffer) +
      mesh->mNumVertices * 3;
    for (size_t i = 0; i < mesh->mNumVertices; ++i) {
      (reinterpret_cast<float*>(buffer))[i * 3 + 0] = mesh->mTangents[i].x;
      (reinterpret_cast<float*>(buffer))[i * 3 + 1] = mesh->mTangents[i].y;
      (reinterpret_cast<float*>(buffer))[i * 3 + 2] = mesh->mTangents[i].z;
      bitangent_buffer[i * 3 + 0] = mesh->mBitangents[i].x;
      bitangent_buffer[i * 3 + 1] = mesh->mBitangents[i].y;
      bitangent_buffer[i * 3 + 2] = mesh->mBitangents[i].z;
    }
    buffer = (reinterpret_cast<float*>(buffer)) + mesh->mNumVertices * 6;
  }
  return buffer;
}

void* Assets::WriteTextureCoordinates(const aiMesh* mesh, void* buffer) {
  // Note: Currently, only 2D texture coords are supported.
  // Note: Currently, only the first UV channel is used.
  for (size_t i = 0; i < 1; ++i) {
    size_t num_UVs = mesh->mNumUVComponents[i];
    if (num_UVs == 0 || num_UVs != 2) {
      continue;
    }
    for (size_t j = 0; j < mesh->mNumVertices; ++j) {
      for (size_t k = 0; k < num_UVs; ++k) {
        (reinterpret_cast<float*>(buffer))[j * num_UVs + k] =
          mesh->mTextureCoords[i][j][k];
      }
    }
    buffer = (reinterpret_cast<float*>(buffer)) + mesh->mNumVertices * num_UVs;
  }
  return buffer;
}

void* Assets::WriteColorData(const aiMesh* mesh, void* buffer) {
  // Note: Currently, only the first color channel is used.
  for (size_t i = 0; i < 1; ++i) {
    if (!mesh->mColors[i]) {
      continue;
    }
    for (size_t j = 0; j < mesh->mNumVertices; ++j) {
      (reinterpret_cast<float*>(buffer))[j * 4 + 0] = mesh->mColors[i][j].r;
      (reinterpret_cast<float*>(buffer))[j * 4 + 1] = mesh->mColors[i][j].g;
      (reinterpret_cast<float*>(buffer))[j * 4 + 2] = mesh->mColors[i][j].b;
      (reinterpret_cast<float*>(buffer))[j * 4 + 3] = mesh->mColors[i][j].a;
    }
    buffer = (reinterpret_cast<float*>(buffer)) + mesh->mNumVertices * 4;
  }
  return buffer;
}

draw::VertexGroup* Assets::CreateMesh(const aiMesh* assimp_mesh,
    const void* buffer, size_t buffer_size) {
  draw::VertexGroup* vrwm_mesh = new draw::VertexGroup();
  vrwm_mesh->SetData(buffer, buffer_size);
  SetRenderingOrder(vrwm_mesh, assimp_mesh);
  SetVertexAttributes(vrwm_mesh, assimp_mesh);
  return vrwm_mesh;
}

void Assets::SetRenderingOrder(draw::VertexGroup* vrwm_mesh,
    const aiMesh* assimp_mesh) {
  std::vector<GLuint> rendering_order;
  // We will not stripify, we will use GL_TRIANGLES.
  // TODO: implement stripification algorithm
  rendering_order.reserve(assimp_mesh->mNumFaces * 3);
  for (size_t i = 0; i < assimp_mesh->mNumFaces; ++i) {
    aiFace& face = assimp_mesh->mFaces[i];
    // > 3 is impossible due to assimp's triangulate postprocessing
    // < 3 may still happen, we ignore it
    if (face.mNumIndices != 3) {
      continue;
    }
    rendering_order.push_back(face.mIndices[0]);
    rendering_order.push_back(face.mIndices[1]);
    rendering_order.push_back(face.mIndices[2]);
  }
  vrwm_mesh->SetElements(GL_TRIANGLES, std::move(rendering_order));
}

void Assets::SetVertexAttributes(draw::VertexGroup* vrwm_mesh,
    const aiMesh* assimp_mesh) {
  size_t float_size = sizeof(float);
  size_t current_offset = 0;

  // Position
  vrwm_mesh->AddVertexAttribute(draw::ResolvedVertexAttribute(0, 3,
    current_offset * float_size));
  current_offset += assimp_mesh->mNumVertices * 3;

  // Normals
  if (assimp_mesh->HasNormals()) {
    vrwm_mesh->AddVertexAttribute(draw::ResolvedVertexAttribute(2, 3,
      current_offset * float_size));
    current_offset += assimp_mesh->mNumVertices * 3;
  }
  // Tangents and Bitangents
  if (assimp_mesh->HasTangentsAndBitangents()) {
    vrwm_mesh->AddVertexAttribute(draw::ResolvedVertexAttribute(3, 3,
      current_offset * float_size));
    current_offset += assimp_mesh->mNumVertices * 3;
    vrwm_mesh->AddVertexAttribute(draw::ResolvedVertexAttribute(4, 3,
      current_offset * float_size));
    current_offset += assimp_mesh->mNumVertices * 3;
  }

  // UVs
  // TODO: conflicts with color
  vrwm_mesh->AddVertexAttribute(draw::ResolvedVertexAttribute(1, 2,
    current_offset * float_size));
  current_offset += assimp_mesh->mNumVertices * 2;
}

draw::Material* Assets::Import(const aiMaterial* mat,
    const std::string& asset_dir) {
  aiString name;
  if (AI_SUCCESS == mat->Get(AI_MATKEY_NAME, name)) {
    vrwm_clog(Info, "Importing material: %s", name.C_Str());
  }

  MaterialInfo info = GetMaterialProperties(mat, asset_dir);
  MaterialType type = SelectAppropriateMaterialType(info);

  return CreateMaterial(type, info);
}

Assets::MaterialInfo Assets::GetMaterialProperties(const aiMaterial* mat,
    const std::string& asset_dir) {
  // As should be obvious from the MaterialInfo struct most stuff of assimp is
  // not supported.

  MaterialInfo to_return;
  aiColor3D diffuse, specular, ambient, emissive;
  diffuse = specular = ambient = emissive = aiColor3D(0, 0, 0);
  mat->Get(AI_MATKEY_COLOR_DIFFUSE, diffuse);
  mat->Get(AI_MATKEY_COLOR_SPECULAR, specular);
  mat->Get(AI_MATKEY_COLOR_AMBIENT, ambient);
  mat->Get(AI_MATKEY_COLOR_EMISSIVE, emissive);

  to_return.shininess = 0.0f;
  float opacity = 1.0f;
  mat->Get(AI_MATKEY_OPACITY, opacity);
  mat->Get(AI_MATKEY_SHININESS, to_return.shininess);

  to_return.color_diffuse = glm::vec4(diffuse.r, diffuse.g, diffuse.b,
    opacity);
  to_return.color_specular = glm::vec3(specular.r, specular.g, specular.b);
  to_return.color_ambient = glm::vec3(ambient.r, ambient.g, ambient.b);

  to_return.tex_diffuse =  LoadTexture(asset_dir, mat, aiTextureType_DIFFUSE);
  to_return.tex_specular = LoadTexture(asset_dir, mat, aiTextureType_SPECULAR);
  to_return.tex_ambient =  LoadTexture(asset_dir, mat, aiTextureType_AMBIENT);
  to_return.tex_normal =   LoadTexture(asset_dir, mat, aiTextureType_NORMALS);
  if (!to_return.tex_normal) {
    to_return.tex_normal = LoadTexture(asset_dir, mat, aiTextureType_HEIGHT,
      true);
  }
  to_return.tex_shiny =    LoadTexture(asset_dir, mat, aiTextureType_SHININESS);

  LogMaterialInfo(to_return);

  return to_return;
}

draw::Material* Assets::CreateMaterial(MaterialType t,
    const MaterialInfo& info) {
  // We're doing a shotgun-approach here: We throw EVERYTHING we have at the
  // shader and see what sticks. If a shader doesn't require one of our
  // variables, setting the uniform will simply not happen.
  // This is not performant (many OpenGL calls to see if a uniform exists).

  draw::MaterialPtr original = Assets::material(t);
  assert(original);
  draw::Material* mat = new draw::Material(*original);

  mat->SetUniform<glm::vec4>("color_diffuse", info.color_diffuse, false);
  mat->SetUniform<glm::vec3>("color_specular", info.color_specular,
    false);
  mat->SetUniform<glm::vec3>("color_ambient", info.color_ambient, false);
  mat->SetUniform<float>("specular_shininess", info.shininess, false);

#define VRWM_SET_MATERIAL_TEXTURE(type) \
  if (info.tex_##type) \
    mat->SetUniform<GLtexturePtr>("texture_" # type, info.tex_##type, false);

  VRWM_SET_MATERIAL_TEXTURE(diffuse);
  VRWM_SET_MATERIAL_TEXTURE(specular);
  VRWM_SET_MATERIAL_TEXTURE(ambient);
  VRWM_SET_MATERIAL_TEXTURE(normal);
  VRWM_SET_MATERIAL_TEXTURE(shiny);

#undef VRWM_SET_MATERIAL_TEXTURE
  return mat;
}

Assets::MaterialType Assets::SelectAppropriateMaterialType(
    const MaterialInfo& info) {
  bool trans = info.color_diffuse.a < 1.0;
  bool shiny = info.tex_shiny ? true : false;
  bool bump = info.tex_normal ? true : false;
  bool spec_tex = info.tex_specular ? true : false;
  bool spec_col = info.color_specular.r + info.color_specular.g +
    info.color_specular.b > 0.0;
  bool diffuse_tex = info.tex_diffuse ? true : false;

  if (!diffuse_tex) {
    return trans ? MaterialType::TransparentDiffuse : MaterialType::Diffuse;
  }
  if (shiny && bump) {
    return trans ?
      MaterialType::TransparentBumpedSpecularTexturedShinyTextured :
      MaterialType::BumpedSpecularTexturedShinyTextured;
  } else if (bump && spec_tex) {
    return trans ?
      MaterialType::TransparentBumpedSpecularTextured :
      MaterialType::BumpedSpecularTextured;
  } else if (bump && spec_col) {
    return trans ? MaterialType::TransparentBumpedSpecular :
                   MaterialType::BumpedSpecular;
  } else if (bump) {
    return trans ? MaterialType::TransparentBumped : MaterialType::Bumped;
  } else if (shiny) {
    return trans ?
      MaterialType::TransparentSpecularTexturedShinyTextured :
      MaterialType::SpecularTexturedShinyTextured;
  } else if (spec_tex) {
    return trans ? MaterialType::TransparentSpecularTextured :
                   MaterialType::SpecularTextured;
  } else if (spec_col) {
    return trans ? MaterialType::TransparentSpecular : MaterialType::Specular;
  } else {
    return trans ? MaterialType::TransparentDiffuseTextured :
      MaterialType::DiffuseTextured;
  }
}

GLtexturePtr Assets::LoadTexture(const std::string& asset_directory,
    const aiMaterial* mat, aiTextureType type, bool as_heightmap) {
  unsigned int texture_count = mat->GetTextureCount(type);
  if (texture_count == 0) {
    return nullptr;
  }
  if (texture_count > 1) {
    vrwm_clog(Warning, "More than one texture of type %i. "
      "Only the first one will be used", type);
  }

  // Note: Assimp has a aiMaterial::GetTexture() function to query all texture
  // informations at the same time. However, many of those properties don't
  // have a default value defined and Assimp has no capabilities to check
  // whether they are defined. We are therefore forced to query each property
  // individually.
  // Each undefined property assumes a default value.
  aiString rel_path("");
  aiTextureMapMode map_mode_u, map_mode_v;
  map_mode_u = map_mode_v = aiTextureMapMode_Wrap;
  if (AI_SUCCESS != mat->Get(AI_MATKEY_TEXTURE(type, 0), rel_path)) {
    vrwm_clog(Error, "Unable to read texture of type %i.", type);
    return nullptr;
  }
  assert(std::strcmp(rel_path.C_Str(), ""));

  mat->Get(AI_MATKEY_MAPPINGMODE_U(type, 0), map_mode_u);
  mat->Get(AI_MATKEY_MAPPINGMODE_V(type, 0), map_mode_v);

  LogTextureInfo(rel_path, map_mode_u, map_mode_v);

  std::string texture_path = FindTexture(asset_directory, rel_path.C_Str());
  if (texture_path.empty()) {
    vrwm_clog(Warning, "Could not find texture named '%s'. Some exporters and "
      "file formats (e.g. Blender's .obj exporter) use backslashes (\\) "
      "instead of forward slahes (/) for sub directories when exported using "
      "Windows. This may parse incorrectly.",
      rel_path.C_Str());
    return nullptr;
  }

  GLtexturePtr to_return = as_heightmap ?
    GLtexture::LoadHeightmap(texture_path) :
    GLtexture::Load(texture_path);
  to_return->SetParameter(GL_TEXTURE_WRAP_S, ToGLWrapParameter(map_mode_u));
  to_return->SetParameter(GL_TEXTURE_WRAP_T, ToGLWrapParameter(map_mode_v));

  return to_return;
}

std::string Assets::FindTexture(const std::string& asset_directory,
    const std::string& texture_name) {
  assert(!asset_directory.empty() && asset_directory.back() == '/');
  assert(!asset_directory.empty());

  // Search relative to asset file
  std::string path = FileTools::Resolve(asset_directory, texture_name);
  if (!FileTools::FileExists(path)) {
    // Fallback - Search for   asset/textures/subpath/file.ext
    auto found = FileTools::Find(kAssetTextureFallbackSubPath + texture_name);
    if (found.first) {
      path = found.second;
    } else {
      // Fallback stage 2 - Search for  asset/textures/file.ext
      std::string name_only = FileTools::FilenameExt(texture_name);
      if (name_only == texture_name) {
        // Names are equal, no need to search again.
        path = "";
      } else {
        found = FileTools::Find(kAssetTextureFallbackSubPath + name_only);
        // !found.first => found.second.empty()
        path = found.second;
      }
    }
  }
  // Returns full path or "" if not found.
  return path;
}

void Assets::LogMeshInfo(const aiMesh* mesh) {
  // This code is only used for debug output, so we don't compile it into
  // release.
#ifdef VRWM_DEBUG
  unsigned int num_col_chan = mesh->GetNumColorChannels();
  unsigned int num_uv_chan = mesh->GetNumUVChannels();
  bool normals = mesh->HasNormals();
  bool tang = mesh->HasTangentsAndBitangents();
  bool coords = mesh->HasTextureCoords(0);
  bool colors = mesh->HasVertexColors(0);
  unsigned int material = mesh->mMaterialIndex;
  vrwm_clogdeb("Mesh: %s\n"
    "\tVertices:         %u\n"
    "\tFaces:            %u\n"
    "\tBones:            %u\n"
    "\tColor Channels:   %u\n"
    "\tTexture Channels: %u\n"
    "\tHas color chan 0? %s\n"
    "\tHas texture chan0?%s\n"
    "\tHas normals?      %s\n"
    "\tHas (bi-)tangents?%s\n"
    "\tMaterial index:   %u\n",
    mesh->mName.C_Str(), mesh->mNumVertices, mesh->mNumFaces, mesh->mNumBones,
    num_col_chan, num_uv_chan, (colors  ? "[X]" : "[ ]"),
    (coords  ? "[X]" : "[ ]"), (normals ? "[X]" : "[ ]"),
    (tang    ? "[X]" : "[ ]"), material);
#else
  (void) mesh;
#endif
}

void Assets::LogTextureInfo(const aiString& path,
    aiTextureMapMode map_mode_u, aiTextureMapMode map_mode_v) {
  // This code is only used for debug output, so we don't compile it into
  // release.
#ifdef VRWM_DEBUG
  std::ostringstream str;
  str << "Texture: " << path.C_Str();
  for (int i = 0; i < 2; ++i) {
    str << std::endl
      << "\tMap Mode " << (i == 0 ? "U" : "V") << ":   ";
    switch (i == 0 ? map_mode_u : map_mode_v) {
      case  aiTextureMapMode_Wrap:   str << "Wrap";           break;
      case  aiTextureMapMode_Clamp:  str << "Clamp";          break;
      case  aiTextureMapMode_Decal:  str << "Decal";          break;
      case  aiTextureMapMode_Mirror: str << "Mirror";         break;
      default:                       str << "<Invalid Enum>"; break;
    }
  }
  vrwm_logdeb(str.str());
#else
  (void) path;
  (void) map_mode_u;
  (void) map_mode_v;
#endif
}

void Assets::LogMaterialInfo(const MaterialInfo& info) {
  // This code is only used for debug output, so we don't compile it into
  // release.
#ifdef VRWM_DEBUG
  vrwm_clogdeb(
    "\tDiffuse:        %s\n"
    "\tSpecular:       %s\n"
    "\tAmbient:        %s\n"
    "\tShininess:      %f\n"
    "\tTex Diffuse:    %s\n"
    "\tTex Specular:   %s\n"
    "\tTex Ambient:    %s\n"
    "\tTex Normal:     %s\n"
    "\tTex Shiny:      %s\n",
    vrwm_to_string(info.color_diffuse).c_str(),
    vrwm_to_string(info.color_specular).c_str(),
    vrwm_to_string(info.color_ambient).c_str(),
    info.shininess,
    (info.tex_diffuse  ? "[X]" : "[ ]"), (info.tex_specular ? "[X]" : "[ ]"),
    (info.tex_ambient  ? "[X]" : "[ ]"), (info.tex_normal   ? "[X]" : "[ ]"),
    (info.tex_shiny    ? "[X]" : "[ ]"));
#else
  (void) info;
#endif
}

int Assets::ToGLWrapParameter(aiTextureMapMode m) {
  switch (m) {
    case aiTextureMapMode_Wrap:       return GL_REPEAT;
    case aiTextureMapMode_Clamp:      return GL_CLAMP_TO_EDGE;
    case aiTextureMapMode_Decal:      return GL_CLAMP_TO_BORDER;
    case aiTextureMapMode_Mirror:     return GL_MIRRORED_REPEAT;
    default:                          return GL_REPEAT;
  }
}

void Assets::TearDownTextures() {
  g_textures.clear();
}
void Assets::TearDownMeshes() {
  g_meshes.clear();
}
void Assets::TearDownMaterials() {
  g_custom_materials.clear();
  for (size_t i = 0; i < static_cast<size_t>(MaterialType::SIZE); ++i) {
    g_materials[i] = nullptr;
  }
}

void Assets::TearDownAssetFiles() {
  g_asset_files.clear();
}

void Assets::TearDown() {
  TearDownTextures();
  TearDownMeshes();
  TearDownMaterials();
  TearDownAssetFiles();
}

