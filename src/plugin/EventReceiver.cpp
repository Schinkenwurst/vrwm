#include "../../include/plugin/EventReceiver.h"
#include "../../include/Core.h"
#include "../../include/PluginManager.h"

using namespace vrwm::plugin;

// This macro is used to remove duplicate code of the same kind.
// It makes every receiver register itself with the plugin manager on ctor
// and unregister itself on dtor.
// It also implements sets the static const ReceiverID kID of that class to
// the enum-value of with the same name as the class.
//
// In some cases, Use of that macro is not possible (i.e. when additional
// code must be executed in the ctor), in that case it is not used (e.g.
// XlibEventReceiver)
#define IMPLEMENT_PLUGIN(name) \
  const ReceiverID name::kID = ReceiverID::name; \
  name::name(int priority) : priority(priority) { \
    Core::Instance().plugin_manager().Add(this); \
  } \
  name::~name() { \
    Core::Instance().plugin_manager().Remove(this); \
  }

IMPLEMENT_PLUGIN(RenderEventReceiver)
// IMPLEMENT_PLUGIN(XlibEventReceiver) // Manual implementation is below.
IMPLEMENT_PLUGIN(MouseCursorMovedReceiver)
IMPLEMENT_PLUGIN(ProgrammStatusReceiver)
IMPLEMENT_PLUGIN(MouseClickReceiver)
IMPLEMENT_PLUGIN(KeyReceiver)
IMPLEMENT_PLUGIN(ClientEventReceiver)


////////////////////////////////////////////////
// Manual implementation of XlibEventReceiver //
////////////////////////////////////////////////

const ReceiverID XlibEventReceiver::kID = ReceiverID::XlibEventReceiver;

XlibEventReceiver::XlibEventReceiver(int priority) : priority(priority) {
  Core::Instance().plugin_manager().Add(this);
  memset(event_handlers_, 0, sizeof(EventHandlerFunc) * LASTEvent);
}
XlibEventReceiver::~XlibEventReceiver() {
  Core::Instance().plugin_manager().Remove(this);
}
void XlibEventReceiver::SetFunc(int event_type, EventHandlerFunc f) {
  event_handlers_[event_type] = f;
}
bool XlibEventReceiver::Handle(const XEvent& e) {
  if (event_handlers_[e.type]) {
    return event_handlers_[e.type](e);
  }
  return true;
}

