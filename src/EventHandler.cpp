#include "../include/EventHandler.h"

#include <cstring>

#include <X11/keysym.h>
#include <X11/extensions/XInput2.h>

#include "../include/Core.h"
#include "../include/Cursor.h"
#include "../include/OpenGLManager.h"
#include "../include/LogManager.h"
#include "../include/PluginManager.h"
#include "../include/plugin/EventReceiver.h"
#include "../include/XInputDeviceManager.h"

using namespace vrwm;

EventHandler::EventHandler() {
  memset(event_handlers_,    0, sizeof(EventHandlerFunc)        * LASTEvent);
  memset(xi_event_handlers_, 0, sizeof(GenericEventHandlerFunc) * XI_LASTEVENT);

  event_handlers_[KeyPress] =       &EventHandler::EventKeyPressOrReleased;
  event_handlers_[KeyRelease] =     &EventHandler::EventKeyPressOrReleased;
  event_handlers_[DestroyNotify] =  &EventHandler::EventDestroyNotify;
  event_handlers_[MapNotify] =      &EventHandler::EventMapNotify;
  event_handlers_[MapRequest] =     &EventHandler::EventMapRequest;
  event_handlers_[ConfigureRequest]=&EventHandler::EventConfigureRequest;
  event_handlers_[ConfigureNotify] =&EventHandler::EventConfigureNotify;
  event_handlers_[Expose] =         &EventHandler::EventExpose;
  event_handlers_[UnmapNotify] =    &EventHandler::EventUnmapNotify;
  event_handlers_[FocusIn] =        &EventHandler::EventFocusChange;
  event_handlers_[FocusOut] =       &EventHandler::EventFocusChange;
  event_handlers_[ClientMessage] =  &EventHandler::EventClientMessage;
  event_handlers_[PropertyNotify] = &EventHandler::EventPropertyChange;
  event_handlers_[EnterNotify] =    &EventHandler::EventWindowCrossing;
  event_handlers_[LeaveNotify] =    &EventHandler::EventWindowCrossing;
  event_handlers_[ColormapNotify] = &EventHandler::EventColormap;

  xi_event_handlers_[XI_RawMotion] =        &EventHandler::XIEventRawMotion;
  xi_event_handlers_[XI_RawButtonPress] =   &EventHandler::XIEventButton;
  xi_event_handlers_[XI_RawButtonRelease] = &EventHandler::XIEventButton;

  #define VRWM_CMSG(key, val) \
    client_message_handlers_.insert(std::make_pair(key, val));

  VRWM_CMSG(Atoms::WM_CHANGE_STATE,            &EventHandler::CMsgWM_CHANGE_STATE);
  VRWM_CMSG(Atoms::_NET_ACTIVE_WINDOW,         &EventHandler::CMsg_NET_ACTIVE_WINDOW);
  VRWM_CMSG(Atoms::_NET_CLOSE_WINDOW,          &EventHandler::CMsg_NET_CLOSE_WINDOW);
  VRWM_CMSG(Atoms::_NET_MOVERESIZE_WINDOW,     &EventHandler::CMsg_NET_MOVERESIZE_WINDOW);
  VRWM_CMSG(Atoms::_NET_REQUEST_FRAME_EXTENTS, &EventHandler::CMsg_NET_REQUEST_FRAME_EXTENTS);
  VRWM_CMSG(Atoms::_NET_WM_DESKTOP,            &EventHandler::CMsg_NET_WM_DESKTOP);
  VRWM_CMSG(Atoms::_NET_WM_STATE,              &EventHandler::CMsg_NET_WM_STATE);
  VRWM_CMSG(Atoms::_NET_WM_PING,               &EventHandler::CMsg_NET_WM_PING);

  #undef VRWM_CMSG

  // Assume that the mouse is not grabbed when VRWM is started as this is the
  // most common case.
  mouse_grabbed_ = false;
}

EventHandler::~EventHandler() {
}

void EventHandler::HandleQueuedEvents() {
  Core& c = Core::Instance();
  XEvent e;

  // TODO: time limitation
  // For as long as there are still events in the event queue
  while(XPending(c.display())) {
    XNextEvent(c.display(), &e);

    // Handle the event.
    assert(e.type < LASTEvent);

    // Send the event to all plugins that are interested in it.
    // They may return false to make us not handle the event.
    if (!c.plugin_manager().ForEach(&plugin::XlibEventReceiver::Handle, e)) {
      // Any plugin wishes to supress this event. Handle the next one!
      continue;
    }

    // Generic events (sent by extensions such as XInput) usually have
    // cookies containing their data.
    if (e.type == GenericEvent) {
      XGenericEventCookie& cookie = *reinterpret_cast<XGenericEventCookie*>(
        &e.xcookie);
      if (XGetEventData(c.display(), &cookie)) {
        if (cookie.extension == xi2_opcode_) {
          //vrwm_clogdeb("Got XInput event %i (%s)", cookie.evtype,
          //  GetReadableXIEventType(cookie.evtype));
          if (xi_event_handlers_[cookie.evtype]) {
            // Call the XInput event handler function.
            (this->*(xi_event_handlers_[cookie.evtype]))(cookie);
          }
        } else {
          vrwm_clogdeb("Got generic event from extension %i", cookie.extension);
        }
        XFreeEventData(c.display(), &cookie);
      } else {
        vrwm_logdeb("Got generic event but cookie could not be read.");
      }
    // Regular events (i.e. not GenericEvents caused by extensions.
    } else {
      if (e.type != MotionNotify) {
        vrwm_clogdeb("Got event of type %i (%s)", e.type,
          GetReadableEventType(e.type));
      }
      if (event_handlers_[e.type]) {
        // Call appropriate event handler
        (this->*(event_handlers_[e.type]))(e);
      }
    }
  }

}

void EventHandler::EventMapRequest(const XEvent& evt) {
  assert(evt.type == MapRequest);
  const XMapRequestEvent& e = evt.xmaprequest;
  Client* c = Core::Instance().Manage(e.window);
  if (!c) {
    return;
  }
  c->Activate();
}

void EventHandler::EventMapNotify(const XEvent& evt) {
  assert(evt.type == MapNotify);
  const XMapEvent& e = evt.xmap;
  vrwm_logdeb("MapNotify of window " + std::to_string(e.window) + ": event: " +
    std::to_string(e.event));
  // Manage the window if that did not happen already.
  Client* c = Core::Instance().Manage(e.window);
  if (!c) {
    return;
  }
  c->PrintDetails();
  c->GotMapped();

  //TODO
  //Inform all plugins of the MapNotify.
  //Core::Instance().plugin_manager().ForEach(
  //&plugin::ClientEventReceiver::ClientMapped, *c);
}

void EventHandler::EventKeyPressOrReleased(const XEvent& evt) {
  assert(evt.type == KeyPress || evt.type == KeyRelease);

  // We'll copy the XKeyEvent here, because Xlib's XLookupKeysym requires a
  // non-const object but our parameter is const.
  XKeyEvent e = evt.xkey;

  // Store the current server time.
  current_time_ = e.time;

  // Why the fuck is the keycode not a KeyCode but an unsigned int?
  KeyCode code = static_cast<KeyCode>(e.keycode);
  KeySym sym = XLookupKeysym(&e, 0);

  // Assign the currently active modifiers
  active_modifiers_ &= 0;
  active_modifiers_ |= e.state;

  bool input_is_mod_key = false;
  ModifierIndex modkey = Core::Instance().config().modkey;
  if (e.keycode == ModifierNames::GetKeycodeOfModifier(modkey)) {
    input_is_mod_key = true;
  }
  bool active_modkey = active_modifiers_[modkey];

  vrwm_clogdeb("%s %u (%s) Modifiers: %x",
      (input_is_mod_key ? "Modkey: " : "Pressed: "), e.keycode,
      XKeysymToString(sym), active_modifiers_);

  // TODO: refactor, too long
  if (evt.type == KeyPress) {
    if (input_is_mod_key) {
      Core::Instance().plugin_manager().ForEach(
          &plugin::KeyReceiver::ModKeyPressed, code, active_modifiers_, sym);
    } else {
      bool plugins_accept_input = Core::Instance().plugin_manager().ForEach(
          &plugin::KeyReceiver::KeyPressed, code, active_modifiers_, sym);
      vrwm_clogdeb("Accepted by plugins? %i  Modkey active? %i",
          plugins_accept_input, active_modkey);
      if (plugins_accept_input) {
        if (active_modkey) {
          // TODO: This is Debug stuff
          if (sym == XK_0) {
            Core::Instance().Shutdown();
          } else if (sym == XK_9) {
            Core::RestartVRWM();
          } else if (sym == XK_a) {
            Core::Launch(Core::Instance().config().terminal);
          } else if (sym == XK_1) {
            Core::Instance().openGL_manager().RecompileAllPrograms();
          }
        } else {
          Client* focus =
            Core::Instance().focus_client();
          if (focus) {
            focus->InjectKeyEvent(e);
          }
        }
      }
    }
  } else {
    if (input_is_mod_key) {
      Core::Instance().plugin_manager().ForEach(
          &plugin::KeyReceiver::ModKeyReleased, code, active_modifiers_, sym);
    } else {
      Core::Instance().plugin_manager().ForEach(
          &plugin::KeyReceiver::KeyReleased, code, active_modifiers_, sym);
      if (!active_modkey) {
        Client* focus =
          Core::Instance().focus_client();
        if (focus) {
          focus->InjectKeyEvent(e);
        }
      }
    }
  }

  // If the key that has been pressed or released just now is a modifier then
  // we need to make the state reflect this.
  ModifierIndex modifier = ModifierNames::GetModifier(code);
  if (modifier != ModifierInvalid) {
    active_modifiers_[modifier] = evt.type == KeyPress;
  }
}

void EventHandler::EventConfigureRequest(const XEvent& evt) {
  assert(evt.type == ConfigureRequest);
  const XConfigureRequestEvent& e = evt.xconfigurerequest;

  Client* c = Core::Instance().Manage(e.window);
  if (!c) {
    return;
  }
  glm::uvec2 size = c->CalculateNearestSize(e.width, e.height);
  // VRWM does not allow moving the window or changing the border size as well
  // as changing the stacking order, but always performs (or tries to perform)
  // the resize.
  //
  // The reason VRWM does not allow changing the stacking order via a configure
  // request is that it does not use the tradional stacking model.
  // Position changes for top-level windows are not valid because VRWM assumes
  // top-level positions to be at 0, 0 (or negative when the window is larger
  // than the screen and the user clicks a position that would be out of
  // screen.)
  if (size.x == c->x11_position().size.x &&
      size.y == c->x11_position().size.y) {
    // No resize happened. And since the configure request in VRWM can only
    // resize, we need to send a synthetic ConfigureNotify as specified in the
    // ICCCM.
    XConfigureEvent synthetic;
    synthetic.type = ConfigureNotify;
    synthetic.event = e.parent;
    synthetic.window = e.window;
    synthetic.x = 0;
    synthetic.y = 0;
    synthetic.width = size.x;
    synthetic.height = size.y;
    synthetic.border_width = 0;
    synthetic.above = None;
    synthetic.override_redirect = c->is_override_redirect();
    XSendEvent(Core::Instance().display(), c->ID(), false, StructureNotifyMask,
      reinterpret_cast<XEvent*>(&synthetic));
  } else {
    int w = static_cast<int> (size.x);
    int h = static_cast<int> (size.y);
    XWindowChanges winchanges = {0, 0, w, h, 0, e.above, None};
    unsigned long value_mask = CWWidth | CWHeight;
    XConfigureWindow(e.display, e.window, value_mask, &winchanges);
  }
}

void EventHandler::EventConfigureNotify(const XEvent& evt) {
  assert(evt.type == ConfigureNotify);
  const XConfigureEvent& e = evt.xconfigure;
  Client* c = Core::Instance().Manage(e.window);
  if (!c) {
    return;
  }

  c->SizeChanged(e.width, e.height);

  Core::Instance().Update_NET_CLIENT_LIST_STACKING(e.window, e.above);
}

void EventHandler::EventFocusChange(const XEvent& evt) {
  assert(evt.type == FocusIn || evt.type == FocusOut);
  const XFocusChangeEvent& e = evt.xfocus;
  //TODO: replace all Core::Instance() with a attribute reference
  Client* c = Core::Instance().GetClient(e.window);
  if (!c) {
      vrwm_log(Warning, "Window " + std::to_string(e.window) + " not managed "
        "by VRWM.");
      return;
  }
  Core::Instance().set_focus_client(*c);
}

void EventHandler::EventClientMessage(const XEvent& evt) {
  assert(evt.type == ClientMessage);
  const XClientMessageEvent& e = evt.xclient;
  auto it = client_message_handlers_.find(e.message_type);
  if (it != client_message_handlers_.end()) {
    (this->*(it->second))(e);
  }
}

void EventHandler::CMsgWM_CHANGE_STATE(const XClientMessageEvent& e) {
  if (e.data.l[0] == IconicState) {
    Client* c = Core::Instance().GetClient(e.window);
    if (!c) return;
    c->Hide();
  }
}

void EventHandler::CMsg_NET_ACTIVE_WINDOW(const XClientMessageEvent& e) {
  Client* c = Core::Instance().GetClient(e.window);
  if (!c) return;
  c->Activate();
}
void EventHandler::CMsg_NET_CLOSE_WINDOW(const XClientMessageEvent& e) {
  Client* c = Core::Instance().GetClient(e.window);
  if (!c) return;
  c->Close();
}
void EventHandler::CMsg_NET_MOVERESIZE_WINDOW(const XClientMessageEvent& e) {
  Client* c = Core::Instance().GetClient(e.window);
  if (!c) return;
  // TODO: respect gravity. EWMH says that we SHOULD, not that we MUST.
  int gravity = e.data.l[0] & 0xFF;
  (void) gravity;
  bool resize_x = e.data.l[0] & 0x400;
  bool resize_y = e.data.l[0] & 0x800;
  glm::uvec2 size = c->CalculateNearestSize(
    resize_x ? e.data.l[3] : c->x11_position().size.x,
    resize_y ? e.data.l[4] : c->x11_position().size.y);
  if (size.x == c->x11_position().size.x &&
      size.y == c->x11_position().size.y) {
    // No resize happened. And since the configure request in VRWM can only
    // resize, we need to send a synthetic ConfigureNotify as specified in the
    // ICCCM.
    XConfigureEvent synthetic;
    synthetic.type = ConfigureNotify;
    synthetic.event = None;
    synthetic.window = e.window;
    synthetic.x = 0;
    synthetic.y = 0;
    synthetic.width = size.x;
    synthetic.height = size.y;
    synthetic.border_width = 0;
    synthetic.above = None;
    synthetic.override_redirect = c->is_override_redirect();
    XSendEvent(Core::Instance().display(), c->ID(), false, StructureNotifyMask,
      reinterpret_cast<XEvent*>(&synthetic));
  } else {
    c->Resize(size.x, size.y);
  }
}
void EventHandler::CMsg_NET_REQUEST_FRAME_EXTENTS(
    const XClientMessageEvent& e) {
  Client* c = Core::Instance().GetClient(e.window);
  if (!c) return;
  c->UpdateFrameExtents();
}
void EventHandler::CMsg_NET_WM_DESKTOP(const XClientMessageEvent& e) {
  Client* c = Core::Instance().GetClient(e.window);
  if (!c) return;
  c->ChangeDesktop(e.data.l[0]);
}
void EventHandler::CMsg_NET_WM_STATE(const XClientMessageEvent& e) {
  if (e.data.l[1] == None) {
    return;
  }

  Client* c = Core::Instance().GetClient(e.window);
  if (!c) return;

  // These may be invalid if the state is not recognized.
  EWMHWindowState first = WindowProperties::ToWindowState(e.data.l[1]);
  EWMHWindowState second = WindowProperties::ToWindowState(e.data.l[2]);
  bool new_value[2];
  if (static_cast<Atom>(e.data.l[0]) == Atoms::_NET_WM_STATE_REMOVE) {
    new_value[0] = new_value[1] = false;
  } else if (static_cast<Atom>(e.data.l[1]) == Atoms::_NET_WM_STATE_ADD) {
    new_value[0] = new_value[1] = true;
  } else { //TOGGLE
    WindowProperties::EWMHWindowStates states = c->GetEWMHStates();
    if (first != EWMHWindowState::Invalid) {
      new_value[0] = !states[static_cast<int>(first)];
    }
    if (second != EWMHWindowState::Invalid) {
      new_value[1] = !states[static_cast<int>(second)];
    }
  }
  if (first != EWMHWindowState::Invalid) {
    if (second != EWMHWindowState::Invalid) {
      c->SetEWMHState(first, second, new_value[0], new_value[1]);
    } else {
      c->SetEWMHState(first, new_value[0]);
    }
  } else if (second != EWMHWindowState::Invalid) {
    c->SetEWMHState(second, new_value[1]);
  }
}
void EventHandler::CMsg_NET_WM_PING(const XClientMessageEvent& e) {
  Client* c = Core::Instance().GetClient(e.data.l[2]);
  if (!c) return;
  c->ChangeDesktop(e.data.l[0]);
}

void EventHandler::EventPropertyChange(const XEvent& evt) {
  assert(evt.type == PropertyNotify);
  const XPropertyEvent& e = evt.xproperty;
  vrwm_logdeb(std::string("Property ") +
    (e.state == PropertyNewValue ? "changed: " : "deleted: ") +
    Atoms::GetName(e.atom));
  Client* c = Core::Instance().GetClient(e.window);
  if (!c) {
      vrwm_log(Warning, "Window " + std::to_string(e.window) + " not managed "
        "by VRWM.");
      return;
  }
  c->UpdateInternalData(e.atom, e.state == PropertyNewValue);
}

void EventHandler::XIEventRawMotion(const XGenericEventCookie& evt) {
  assert(evt.evtype == XI_RawMotion);

  const XIRawEvent& e = *reinterpret_cast<XIRawEvent*>(evt.data);
  current_time_ = e.time;

  int num_valuators = e.valuators.mask_len;
  const unsigned char* mask = e.valuators.mask;
  int valuator_x = Core::Instance().xinput_manager().get_x_axis_valuator_id();
  int valuator_y = Core::Instance().xinput_manager().get_y_axis_valuator_id();

  float x = 0, y = 0;
  if (valuator_y < num_valuators && XIMaskIsSet(mask, valuator_x)) {
    x = static_cast<float>(*(e.valuators.values + valuator_x));
  }
  if (valuator_x < num_valuators && XIMaskIsSet(mask, valuator_y)) {
    y = static_cast<float>(*(e.valuators.values + valuator_y));
  }

  float delta_time = Core::Instance().time_since_last_event();
  x *= delta_time;
  y *= delta_time;

  Core::Instance().plugin_manager().ForEach(
    &plugin::MouseCursorMovedReceiver::MouseMoved, x, y);
}

void EventHandler::EventDestroyNotify(const XEvent& evt) {
  assert(evt.type == DestroyNotify);
  const XDestroyWindowEvent& e = evt.xdestroywindow;
  vrwm_logdeb("DestroyNotify of window " + std::to_string(e.window));
  Core::Instance().Unmanage(e.window);
}

void EventHandler::XIEventButton(const XGenericEventCookie& evt) {
  assert(evt.evtype == XI_RawButtonPress || evt.evtype == XI_RawButtonRelease);
  const XIRawEvent& e = *reinterpret_cast<const XIRawEvent*>(evt.data);

  MouseButton button = static_cast<MouseButton>(e.detail);
  // Menubars from GTK are a pain in the ass:
  // GTK applications usually handle mouse clicks and mouse motion events by
  // querying the cursor position (regardless of whether or not the window is
  // obstructed by the composite overlay window). Which is the reason why VRWM
  // always warps the X pointer to the position on that window. This is strange
  // in itself, but still acceptable.
  // When the user clicks on the menu bar, the application grabs the cursor and
  // spawns an override redirect window with the menu bar drop down menu. In
  // "normal" VRWM circumstances the button release would be redirected to the
  // GTK application window which would recognize it as a command to close the
  // menu bar drop down. This is done because VRWM does not care about grabs
  // (because it needs to redirect the events). We therefore programmed VRWM to
  // respect grabs and not redirect mouse buttons for as long as the grab
  // persist. This in turn caused the menu bar not to receive any input
  // whatsoever. Constricting events to the grabbing client doesn't work either
  // as in GTK menubars the application window requests the grab instead of the
  // drop down window.
  //
  // Note: The above also holds true for right click menus in GTK.
  //
  // These facts left us with a very strange and ugly solution: We ignore the
  // first mouse release event after the grab.
  // Let's hope that this does not interfere with specific applications...
  if (mouse_grabbed_) {
    if (evt.evtype == XI_RawButtonPress) {
      // If the user presses the mouse after grab, we don't ignore the release.
      // We only want to ignore the release when the grab has started AFTER
      // the mouse has been pressed.
      releases_since_mouse_grab_ = 1;
    }
    else if (evt.evtype == XI_RawButtonRelease &&
        !(releases_since_mouse_grab_++)) {
      return;
    }
  }

  if (evt.evtype == XI_RawButtonPress) {
    if (Core::Instance().plugin_manager().ForEach(
        &plugin::MouseClickReceiver::MousePressed, button,
        active_modifiers_)) {
      Core::Instance().cursor().Click(button, active_modifiers_);
    }
  } else {
    Core::Instance().plugin_manager().ForEach(
        &plugin::MouseClickReceiver::MouseReleased, button,
        active_modifiers_);
    Core::Instance().cursor().Release(button, active_modifiers_);
  }
}

void EventHandler::EventExpose(const XEvent& evt) {
  assert(evt.type == Expose);
  const XExposeEvent& e = evt.xexpose;
  (void) e;
  try {
    //Client& c = Core::Instance().GetClient(e.window);
    //c.UpdatePixmap();
  } catch(std::exception& e) {
    vrwm_log(Error, std::string(e.what()));
  }
}

void EventHandler::EventUnmapNotify(const XEvent& evt) {
  assert(evt.type == UnmapNotify);
  // Unmap events are described insufficient in the ICCCM. It states that
  // when a window is put into iconic state it gets an UnmapNotify. It also
  // states that when a window is put into withdrawn state it gets a real
  // UnmapNotify and a synthetic UnmapNotify but must not depend on the
  // synthetic UnmapNotify to be received (because some obsolete clients won't
  // send it).  Additionally, changes into withdrawn state may only be
  // triggered by the client. This essentially means that the window manager
  // has no definitive way of knowing when an obsolete client is withdrawn from
  // iconic state.
  //
  // Therefore, we assume the following if an UnmapNotify is received:
  //
  // If the client is in normal state, it will be put into withdrawn state.
  // Regardless of wether the event was real or synthetic.
  //
  // When the client is in iconic state, real UnmapNotifies will trigger
  // GotMinimized() in the client because the window manager has unmapped the
  // window shortly after setting its state to iconic. Since the window is
  // already unmapped, no other real UnmapNotifies can be received because the
  // Xlib manual states that unmapping an unmapped window has no effect (see
  // XUnmapWindow()). Synthetic unmap events on an iconic window will put it in
  // the withdrawn state (as per ICCCM). If an obsolete client in iconic state
  // does not send a synthetic UnmapNotify it will remain iconic.
  //
  // Clients in withdrawn state ignore all unmap events.
  //
  // Additionally, there is a minor problem with override redirect windows:
  // Normally, the window manager is not supposed to handle them but since VRWM
  // is a composite window manager, it needs to. Such windows don't have a
  // state even if they are top level because of the override redirect. VRWM
  // assumes: If an override redirect window is unmapped, it is withdrawn to
  // delete the respective Renderable.
  const XUnmapEvent& e = evt.xunmap;
  vrwm_logdeb(std::string(e.send_event ? "Synthetic" : "Regular") +
    " UnmapNotify of win " + std::to_string(e.window));
  Client* c = Core::Instance().GetClient(e.window);
  if (!c) {
    return;
  }
  std::pair<bool, WindowProperties::WmState> exists_state = c->GetState();
  auto& state = exists_state.second;
  if (c->is_override_redirect() && !e.send_event) {
      vrwm_logdeb("Override redirect window " + c->ToString() +
        " got withdrawn.");
      c->GotWithdrawn();
  } else if (exists_state.first) {
    if (state.state == WindowState::WS_NormalState ||
      (state.state == WindowState::WS_IconicState && e.send_event)) {
      vrwm_logdeb("Client " + c->ToString() + " got withdrawn.");
      c->GotWithdrawn();
    } else if (state.state == WindowState::WS_IconicState) {
      vrwm_logdeb("Client " + c->ToString() + " got minimized.");
      c->GotMinimized();
    }
  }
}

void EventHandler::EventWindowCrossing(const XEvent& evt) {
  assert(evt.type == EnterNotify || evt.type == LeaveNotify);
  const XCrossingEvent& e = evt.xcrossing;
  // Crossing events are triggered when a client grabs or ungrabs the mouse
  // button. VRWM reacts to such a grab and temporarily suspends handling of
  // button events. This is done to not confuse the grabbing client with the
  // synthetic button events sent by VRWM.
  //
  // Handling of mouse motion is not suspended during a grab because it is
  // required to warp the X cursor to the position on the window that the user
  // points at in 3D space.
  if (e.mode != NotifyNormal) {
    mouse_grabbed_ = (e.mode == NotifyGrab);
    releases_since_mouse_grab_ = 0;
  }
}

void EventHandler::EventColormap(const XEvent& evt) {
  assert(evt.type == ColormapNotify);
  const XColormapEvent& e = evt.xcolormap;
  if (e.c_new) {
    Client* c = Core::Instance().GetClient(e.window);
    if (!c) {
      return;
    }
    c->InstallColormaps();
  }
}

const char* EventHandler::GetReadableEventType(int type) {
  switch (type) {
    case  2: return "KeyPress";
    case  3: return "KeyRelease";
    case  4: return "ButtonPress";
    case  5: return "ButtonRelease";
    case  6: return "MotionNotify";
    case  7: return "EnterNotify";
    case  8: return "LeaveNotify";
    case  9: return "FocusIn";
    case 10: return "FocusOut";
    case 11: return "KeymapNotify";
    case 12: return "Expose";
    case 13: return "GraphicsExpose";
    case 14: return "NoExpose";
    case 15: return "VisibilityNotify";
    case 16: return "CreateNotify";
    case 17: return "DestroyNotify";
    case 18: return "UnmapNotify";
    case 19: return "MapNotify";
    case 20: return "MapRequest";
    case 21: return "ReparentNotify";
    case 22: return "ConfigureNotify";
    case 23: return "ConfigureRequest";
    case 24: return "GravityNotify";
    case 25: return "ResizeRequest";
    case 26: return "CirculateNotify";
    case 27: return "CirculateRequest";
    case 28: return "PropertyNotify";
    case 29: return "SelectionClear";
    case 30: return "SelectionRequest";
    case 31: return "SelectionNotify";
    case 32: return "ColormapNotify";
    case 33: return "ClientMessage";
    case 34: return "MappingNotify";
    case 35: return "GenericEvent";
    default: return "Unknown x11 event type.";
  }
}

const char* EventHandler::GetReadableXIEventType(int xi2_type) {
  switch (xi2_type) {
    case XI_DeviceChanged:    return "XI_DeviceChanged";
    case XI_KeyPress:         return "XI_KeyPress";
    case XI_KeyRelease:       return "XI_KeyRelease";
    case XI_ButtonPress:      return "XI_ButtonPress";
    case XI_ButtonRelease:    return "XI_ButtonRelease";
    case XI_Motion:           return "XI_Motion";
    case XI_Enter:            return "XI_Enter";
    case XI_Leave:            return "XI_Leave";
    case XI_FocusIn:          return "XI_FocusIn";
    case XI_FocusOut:         return "XI_FocusOut";
    case XI_HierarchyChanged: return "XI_HierarchyChanged";
    case XI_PropertyEvent:    return "XI_PropertyEvent";
    case XI_RawKeyPress:      return "XI_RawKeyPress";
    case XI_RawKeyRelease:    return "XI_RawKeyRelease";
    case XI_RawButtonPress:   return "XI_RawButtonPress";
    case XI_RawButtonRelease: return "XI_RawButtonRelease";
    case XI_RawMotion:        return "XI_RawMotion";
    case XI_TouchBegin:       return "XI_TouchBegin";
    case XI_TouchUpdate:      return "XI_TouchUpdate";
    case XI_TouchEnd:         return "XI_TouchEnd";
    case XI_TouchOwnership:   return "XI_TouchOwnership";
    case XI_RawTouchBegin:    return "XI_RawTouchBegin";
    case XI_RawTouchUpdate:   return "XI_RawTouchUpdate";
    case XI_RawTouchEnd:      return "XI_RawTouchEnd";
    case XI_BarrierHit:       return "XI_BarrierHit";
    case XI_BarrierLeave:     return "XI_BarrierLeave";
    default:                  return "Unkown XI2 event.";
  }
}

void EventHandler::SetXInputExtensionData(int opcode, int eventbase,
                                          int errorbase) {
  xi2_opcode_ = opcode;
  xi2_event_ = eventbase;
  xi2_error_ = errorbase;
}
