#include "../include/OculusManager.h"

#include <cstdlib>

#include <exception>

#include <glm/gtc/matrix_transform.hpp>

#include "../include/Core.h"
#include "../include/GLglobal.h"
#include "../include/GLtextureSwapper.h"
#include "../include/LogManager.h"
#include "../include/OpenGLManager.h"
#include "../include/PluginManager.h"
#include "../include/plugin/EventReceiver.h"

using namespace vrwm;

OculusManager::OculusManager(Core& core, UsageMode mode)
    : core_(core)
    , mode_(mode)
    , head_mounted_display_(0)
    , color_texture_(0)
    , depth_renderbuffer_(0)
    , framebuffer_(0) {
  Init();
  InitRendering();
}

OculusManager::~OculusManager() {
  ovrHmd_Destroy(head_mounted_display_);
  ovr_Shutdown();
  CALL_GL(glDeleteTextures(1, &color_texture_));
  CALL_GL(glDeleteRenderbuffers(1, &depth_renderbuffer_));
  CALL_GL(glBindFramebuffer(GL_FRAMEBUFFER, 0));
  CALL_GL(glDeleteFramebuffers(1, &framebuffer_));
}

void OculusManager::Init() {
  if (mode_ == UsageMode::NoOculus) {
    return;
  }

  GLtextureSwapper::Instance().FreezeUnitForever(0);

  rendering_only_ = mode_ == UsageMode::Simulated;

  ovr_Initialize();

  if (mode_ == UsageMode::Full) {
    head_mounted_display_ = ovrHmd_Create(0);
  }
  if (!head_mounted_display_) {
    vrwm_log(Warning,
      "No Oculus Rift device detected. Falling back to a simulated device "
      "without input.");
  }
  if (mode_ == UsageMode::Simulated || !head_mounted_display_) {
    head_mounted_display_ = ovrHmd_CreateDebug(ovrHmd_DK1);
  }

  // Because we create a debug device if no real device was found, we must
  // definitly have a head mounted display at this point.
  assert(head_mounted_display_);
  ovrHmd_GetDesc(head_mounted_display_, &description_);

  vrwm_clog(Info, "Oculus Rift Info\n"
      "\tType:         %i\n"
      "\tProduct name: %s\n"
      "\tManufacturer: %s\n"
      "\tResolution:   %ix%i\n",
      description_.Type, description_.ProductName, description_.Manufacturer,
      description_.Resolution.w, description_.Resolution.h);

  // Try to start the oculus rift's sensors
  bool result = ovrHmd_StartSensor(head_mounted_display_,
      ovrSensorCap_Position       |
        ovrSensorCap_Orientation  |
        ovrSensorCap_YawCorrection,
      0);
  if (!result) {
    throw std::runtime_error("Unable to start Oculus Rift sensors.");
  }
}

void OculusManager::InitRendering() {
  if (mode_ == UsageMode::NoOculus) {
    return;
  }
  // rcmmnd means recommended. It's too long to write imho.
  ovrSizei rcmmnd_left_eye_texture_size = ovrHmd_GetFovTextureSize(
      head_mounted_display_,
      ovrEye_Left,
      description_.DefaultEyeFov[0],
      1.0f);
  ovrSizei rcmmnd_right_eye_texture_size = ovrHmd_GetFovTextureSize(
      head_mounted_display_,
      ovrEye_Right,
      description_.DefaultEyeFov[1],
      1.0f);
  ovrSizei render_target_size;
  render_target_size.w =
    rcmmnd_left_eye_texture_size.w + rcmmnd_right_eye_texture_size.w;
  render_target_size.h = std::max(
      rcmmnd_left_eye_texture_size.h, rcmmnd_right_eye_texture_size.h);
  vrwm_clog(Info, "Render Target size: %ix%i", render_target_size.w,
      render_target_size.h);

  eye_render_size_ = render_target_size;
  eye_render_size_.w /= 2;

  constexpr int eye_render_multisample = 0;

  GLtextureSwapper::Instance().SetActiveUnit(0);

  // Create the color texture to render to.
  CALL_GL(glGenTextures(1, &color_texture_));
  CALL_GL(glBindTexture(GL_TEXTURE_2D, color_texture_));
  CALL_GL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT));
  CALL_GL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT));
  CALL_GL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
  CALL_GL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
  CALL_GL(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8,
                       render_target_size.w, render_target_size.h, 0,
                       GL_RGBA, GL_UNSIGNED_BYTE, NULL));

  // Create renderbuffer for depth
  CALL_GL(glGenRenderbuffers(1, &depth_renderbuffer_));
  CALL_GL(glBindRenderbuffer(GL_RENDERBUFFER, depth_renderbuffer_));
  CALL_GL(glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24,
                                render_target_size.w, render_target_size.h));

  // Create the framebuffer object
  CALL_GL(glGenFramebuffers(1, &framebuffer_));
  CALL_GL(glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_));
  CALL_GL(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                                 GL_TEXTURE_2D, color_texture_, 0));
  CALL_GL(glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                                    GL_RENDERBUFFER, depth_renderbuffer_));

  GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  if (status != GL_FRAMEBUFFER_COMPLETE) {
    vrwm_log(Warning, "Frambuffer not available for oculus rift rendering. "
        "It's possible your GPU does not support it.");
  }

  //TODO: mip maps
  eye_texture_[0].OGL.Header.API =             ovrRenderAPI_OpenGL;
  eye_texture_[0].OGL.Header.TextureSize =     render_target_size;
  eye_texture_[0].OGL.Header.RenderViewport =  {{0, 0}, eye_render_size_};
  eye_texture_[0].OGL.TexId =                  color_texture_;

  eye_texture_[1] =                            eye_texture_[0];
  eye_texture_[1].OGL.Header.RenderViewport =  {{eye_render_size_.w+1, 0},
                                                eye_render_size_};

  ovrGLConfig config;
  config.OGL.Header.API = ovrRenderAPI_OpenGL;
  config.OGL.Header.RTSize =
    {description_.Resolution.w, description_.Resolution.h};
  config.OGL.Header.Multisample = eye_render_multisample;
  config.OGL.Disp = core_.display();
  config.OGL.Win = core_.GetOpenGLWindowID();

  if (!ovrHmd_ConfigureRendering(head_mounted_display_, &config.Config,
                                 description_.DistortionCaps,
                                 description_.DefaultEyeFov,
                                 &eye_render_desc_[0])) {
    throw std::runtime_error("Unable to configure libOVR for rendering.");
  }
}

void OculusManager::Render() {
  const auto render_stage = plugin::RenderEventReceiver::RenderStage::Oculus;
  core_.plugin_manager().ForEach(&plugin::RenderEventReceiver::PreRenderStage,
      render_stage);

  GLtextureSwapper::Instance().SetActiveUnit(0);
  ovrFrameTiming timing = ovrHmd_BeginFrame(head_mounted_display_, 0);
  (void) timing; //currently unused, I'll keep it for now

  // Render both eyes to the specified frame buffer
  CALL_GL(glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_));
  GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  if (status != GL_FRAMEBUFFER_COMPLETE) {
    vrwm_log(Warning, "Framebuffer not available for Oculus Rift rendering.");
  }

  OpenGLManager& gl_manager = core_.openGL_manager();
  gl_manager.Clear();

  // Render once for each eye
  for (int eye_index = 0; eye_index < ovrEye_Count; ++eye_index) {
    ovrEyeType eye =         description_.EyeRenderOrder[eye_index];
    ovrPosef   eye_pose =     ovrHmd_BeginEyeRender(head_mounted_display_, eye);
    const ovrRecti& viewport =
      eye_texture_[eye_index].Texture.Header.RenderViewport;
    CALL_GL(glViewport(viewport.Pos.x, viewport.Pos.y,
                       viewport.Size.w, viewport.Size.h));

    gl_manager.SetProjectionMatrix(glm::transpose(OvrToGlm(
        ovrMatrix4f_Projection(eye_render_desc_[eye].Fov, 0.01f, 10000.0f,
                               true))));

    if (mode_ == UsageMode::Full) {
      core_.openGL_manager().set_orientation(OvrToGlm(eye_pose.Orientation));
    }
    core_.openGL_manager().SetupViewMatrix();
    // Only sort renderables on first eye because the second eye won't be
    // significantly different (in terms of distance from the user)
    if (eye_index == 0) {
      core_.openGL_manager().SortRenderables();
    }

    gl_manager.Render();

    GLtextureSwapper::Instance().SetActiveUnit(0);
    ovrHmd_EndEyeRender(head_mounted_display_, eye, eye_pose,
                        &eye_texture_[eye].Texture);
    CHECK_GL_ERRORS();
  }

  ovrHmd_EndFrame(head_mounted_display_);
  CHECK_GL_ERRORS();
  core_.plugin_manager().ForEach(&plugin::RenderEventReceiver::PostRenderStage,
      render_stage);

  CALL_GL(glBindTexture(GL_TEXTURE_2D, 0));
  CALL_GL(glBindFramebuffer(GL_FRAMEBUFFER, 0));
  CHECK_GL_ERRORS();
}

