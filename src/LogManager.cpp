#include "../include/LogManager.h"

#include <cassert>

#include <assimp/DefaultLogger.hpp>

#include "../include/FileTools.h"

using namespace vrwm;

LogManager* LogManager::g_singleton_ = nullptr;
void (*LogManager::error_callback_)(const std::string & ) = nullptr;

LogManager::LogManager() : log_to_file_(true) {
  std::string full_path;
  try {
    FileTools::MkdirFirstSearchPath();
    const std::string& dir = FileTools::kSearchedFolders[0];
    full_path = FileTools::Resolve(dir, LogManager::kLogFileName);
    file_.open(full_path.c_str(), std::ios::out | std::ios::trunc);
    if (!file_) {
      throw std::runtime_error("Unable to open file for writing.");
    }
  } catch (std::exception& e) {
      std::string err = "Error initializing LogManager: ";
      err.append(e.what());
      std::cerr << err << std::endl;
      if(error_callback_) {
        error_callback_(err);
      }
      log_to_file_ = false;
  }

  InitialLog();

  // Attach to Assimp
  Assimp::DefaultLogger::set(this);
}

LogManager::~LogManager() {
  Log("\t\tFin.", LogLevel::Info);
  if (log_to_file_)
    file_.close();
}

LogManager & LogManager::Instance() {
  if(!g_singleton_)
    g_singleton_ = new LogManager();

  return* g_singleton_;
}
void LogManager::DestroySingleton() {
  if(g_singleton_)
    delete g_singleton_;
  g_singleton_ = nullptr;
}

void LogManager::SetErrorCallback(void (*f)(const std::string & )) {
  error_callback_ = f;
}

void LogManager::Log(const std::string & msg, LogLevel level) {
  //omit debug messages, if not compiled as debug.
#ifndef VRWM_DEBUG
  if(level == Debug)
    return;
#endif

  std::string level_str = Stringify(level);

  if (log_to_file_) {
    using namespace std::chrono;
    time_t now = system_clock::to_time_t(system_clock::now());
    std::tm* t = std::localtime(&now);
    //I'll diregard the 80 character limit here.
    std::string hour = t->tm_hour >= 10 ? std::to_string(t->tm_hour) : ("0" + std::to_string(t->tm_hour));
    std::string min =  t->tm_min  >= 10 ? std::to_string(t->tm_min)  : ("0" + std::to_string(t->tm_min));
    std::string sec =  t->tm_sec  >= 10 ? std::to_string(t->tm_sec)  : ("0" + std::to_string(t->tm_sec));
    file_ << hour << ":" << min << ":" << sec << " | " << level_str << " | "
      << msg << std::endl;
    //TODO: use this if the compilers support all std::put_time.
    //Currently, neither clang nor gcc supports this C++11 feature... in 2014.
    //   -.-
    //file_ << std::put_time(std::localtime(&now), "%X") << " | " << levelStr << " | "
    //  << msg << std::endl;
  }

  std::cout << level_str << ": " << msg << std::endl;

  if(error_callback_ && level == Error)
    error_callback_(msg);
}
void LogManager::Log(const char* msg, LogLevel level, ...) {
  //omit debug messages, if not compiled as debug.
#ifndef VRWM_DEBUG
  if(level == Debug)
    return;
#endif

  va_list args;
  va_start(args, level);
  vLog(msg, level, args);
  va_end(args);
}
void LogManager::vLog(const char* msg, LogLevel level, va_list args) {
  //omit debug messages, if not compiled as debug.
#ifndef VRWM_DEBUG
  if(level == Debug)
    return;
#endif

  char buf[512];
  int need = 1 + vsnprintf(buf, sizeof(buf), msg, args);
  if (static_cast<size_t>(need) > sizeof(buf)) {
    char* large_buffer = new char[need];
    vsprintf(large_buffer, msg, args);
    large_buffer[need-1] = '\0';
    Log(std::string(large_buffer), level);
    delete[] large_buffer;
  } else {
    Log(std::string(buf), level);
  }
}

std::string LogManager::Stringify(LogLevel level) {
  std::string toReturn;
  switch(level)
  {
    case LogLevel::Debug:
      toReturn = " Debug ";
      break;
    case LogLevel::Info:
      toReturn = " Info  ";
      break;
    case LogLevel::Warning:
      toReturn = "Warning";
      break;
    case LogLevel::Error:
      toReturn = " Error ";
      break;
    default:
      toReturn = "Unkown ";
      break;
  }
  return toReturn;
}

void LogManager::InitialLog() {
  using namespace std::chrono;
  std::time_t now = system_clock::to_time_t(system_clock::now());
  std::ostringstream converter;
  converter << "Logging started on "
    << asctime(localtime(&now))
  //TODO: use below if clang and gcc support std::put_time
  //  << std::put_time(std::localtime(&now_c), "%x %X")
    << " to file " << kLogFileName;
  Log(converter.str(), LogLevel::Info);
}

