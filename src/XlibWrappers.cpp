#include "../include/XlibWrappers.h"

#include <X11/keysymdef.h>

#include "../include/Core.h"

using namespace vrwm;

const std::map<std::string, int> ModifierNames::g_names {
  { "Shift",       KeyboardShift },
  { "Lock",        KeyboardLock },
  { "Ctrl",        KeyboardControl },
  { "Control",     KeyboardControl },
  { "Mod1",        KeyboardMod1 },
  { "Mod2",        KeyboardMod2 },
  { "Mod3",        KeyboardMod3 },
  { "Mod4",        KeyboardMod4 },
  { "Mod5",        KeyboardMod5 },

  //{ "Alt",         Mod1Mask },
  //{ "AltMask",     Mod1Mask },
  //{ "Win",         Mod2Mask },
  //{ "WinMask",     Mod2Mask },
};

ModifierIndex ModifierNames::GetModifier(const std::string& name) {
  auto it = ModifierNames::g_names.find(name);
  if (it == ModifierNames::g_names.end()) return ModifierInvalid;
  return static_cast<ModifierIndex>(it->second);
}

std::string ModifierNames::GetName(ModifierIndex m) {
  switch (m) {
    case KeyboardShift:   return "Shift";
    case KeyboardLock:    return "Lock";
    case KeyboardControl: return "Control";
    case KeyboardMod1:    return "Mod1";
    case KeyboardMod2:    return "Mod2";
    case KeyboardMod3:    return "Mod3";
    case KeyboardMod4:    return "Mod4";
    case KeyboardMod5:    return "Mod5";
    default: return "Invalid";
  }
}

KeyCode ModifierNames::GetKeycodeOfModifier(ModifierIndex m) {
  if (m == ModifierInvalid)
    return 0;

  XModifierKeymap *map = XGetModifierMapping(Core::Instance().display());
  KeyCode k = 0;
  int index = static_cast<int> (m);
  KeyCode* kp = map->modifiermap + index * map->max_keypermod * sizeof(KeyCode);
  if (kp) {
    k = *kp;
  }
  XFreeModifiermap(map);
  return k;
}

ModifierIndex ModifierNames::GetModifier(KeyCode c) {
  // TODO: This function could most likely be cached.
  if (c == 0) {
    return ModifierInvalid;
  }

  ModifierIndex index = ModifierInvalid;
  XModifierKeymap *map = XGetModifierMapping(Core::Instance().display());
  // 8 because the xlib documentation states that the modifiermap is "An 8 by
  // max_keypermod array of the modifiers"
  for (int i = 0; i < 8; ++i) {
    if (map->modifiermap[i * map->max_keypermod] == c) {
      index = static_cast<ModifierIndex>(i);
      break;
    }
  }
  XFreeModifiermap(map);
  return index;
}

