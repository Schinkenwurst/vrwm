#include "../include/GLshader.h"

#include <stdexcept>
#include <fstream>
#include <vector>
#include <sstream>

#include "../include/Assets.h"
#include "../include/GLglobal.h"
#include "../include/LogManager.h"

using namespace vrwm;

const char* GLshader::kGLSLVersionStr = "#version 330";

GLshader::GLshader(const char* code, GLenum type)
    : handle_(glCreateShader(type))
    , type_(type) {
  // Check for errors during shader creation
  if (!handle_) {
    throw std::runtime_error("glCreateShader failed");
  }

  Compile(code);
}

void GLshader::Compile(const char* code) {
  // Pass code to OpenGL
  glShaderSource(handle_, 1, &code, nullptr);
  // Compile!
  glCompileShader(handle_);
}

void GLshader::Recompile() {
  if (filename_.empty()) {
    // Was not loaded from file, cannot recompile.
    return;
  }
  std::string code = LoadFile(filename_, defines_);
  Compile(code.c_str());
}

void GLshader::PrintCompileLog() {
  // Check for errors and print log
  GLint success;
  glGetShaderiv(handle_, GL_COMPILE_STATUS, &success);

  // TODO: larger buffer
  char logMessage[2048];
  GLsizei messageLength;
  glGetShaderInfoLog(handle_, 2048, &messageLength, logMessage);
  if (success == GL_FALSE) {
    if (filename_.empty()) {
      vrwm_clog(Error, "Shader compiling failed!\n%s\n", logMessage);
    } else {
      vrwm_clog(Error, "Shader compiling (file: '%s') failed!\n%s",
        filename_.c_str(), logMessage);
    }
  } else {
    if (filename_.empty()) {
      vrwm_clog(Info, "Shader successfully compiled! %s", logMessage);
    } else {
      vrwm_clog(Info, "Shader (file: '%s') successfully compiled! %s",
        filename_.c_str(), logMessage);
    }
  }

}

GLshader::GLshader(GLshader&& move) :
    handle_(move.handle_) ,
    type_(move.type_),
    filename_(std::move(move.filename_)),
    defines_(std::move(move.defines_)) {
  move.handle_ = 0; // Prevent deletion
}

GLshader GLshader::FromFile(const std::string& filename, GLenum type,
    const std::set<std::string>& defines) {
  std::string code = LoadFile(filename, defines);
  GLshader s(code.c_str(), type);
  s.filename_ = filename;
  s.defines_ = defines;
  return s;
}

std::string GLshader::LoadFile(const std::string& filename,
    const std::set<std::string>& defines) {
  std::string definesStr;
  for (auto& variable : defines) {
    definesStr.append(variable + " ");
  }

  vrwm_log(Info, "Compiling shader from file: '" + filename + "' "
    "with #defines: " + definesStr);
  // Open file
  std::ifstream inputFile(filename, std::ios::in | std::ios::binary);
  if (!inputFile.good()) {
    throw std::runtime_error(std::string("Unable to open file: ") + filename);
  }

  std::stringstream buffer;
  buffer << GLshader::kGLSLVersionStr << std::endl;

  // define the variables.
  for (auto& variable : defines) {
    buffer << "#define " << variable << std::endl;
  }
  buffer << "#line 0" << std::endl;

  // Read file
  buffer << inputFile.rdbuf();

  return buffer.str();
}

GLshader GLshader::FromCode(const std::string& code, GLenum type) {
  vrwm_log(Info, "Creating shader from code: Code dump:\n" + code);
  return GLshader(code.c_str(), type);
}

GLshader::~GLshader() {
  if (handle_ != 0) {
    glDeleteShader(handle_);
  }
}

