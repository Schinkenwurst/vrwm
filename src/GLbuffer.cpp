#include "../include/GLbuffer.h"

#include <utility>
#include <cassert>

using namespace vrwm;

GLmappedBuffer::GLmappedBuffer(GLenum target, void* mem, GLintptr offset,
    GLsizeiptr length) {
  data = mem;
  target_ = target;
  offset_ = offset;
  length_= length;
}

GLmappedBuffer::GLmappedBuffer(GLmappedBuffer&& rvalue) {
  data = nullptr;
  std::swap(data, rvalue.data);
  target_ = rvalue.target_;
  length_ = rvalue.length_;
}

GLmappedBuffer::~GLmappedBuffer() {
  if (data) {
    GLboolean success = CALL_GL(glUnmapBuffer(target_));
    if (success == GL_FALSE) {
      // TODO:
      // Unmap buffer failed, the data store is corrupt. We need to re-init.
      // We cannot throw an exception because we are in a dtor.
    }
  }
}

