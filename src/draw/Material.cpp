#include "../../include/draw/Material.h"

#include "../../include/GLtextureSwapper.h"

using namespace vrwm::draw;

uint16_t Material::g_next_regular_ID_ = 0;
std::vector<uint16_t> Material::g_unused_IDs_;

Material::Material() : transparent_(false), program_(nullptr) {
  id_ = Material::GetNextID();
}

Material::Material(GLprogramPtr program, bool transparent) {
  id_ = Material::GetNextID();
  transparent_ = transparent;
  program_ = program;
}

Material::Material(const Material& copy) {
  id_ = Material::GetNextID();
  *this = copy;
}

Material& Material::operator=(const Material& other) {
  if (this == &other) {
    return *this;
  }

  DestroyAllUniforms();

  for (auto& pair : other.uniforms_) {
    uniforms_[pair.first] = pair.second->Clone();
  }
  transparent_ = other.transparent_;
  program_ = other.program_;
  return *this;
}

//static
uint16_t Material::GetNextID() {
  uint16_t id;
  if (Material::g_unused_IDs_.empty()) {
    id = ++Material::g_next_regular_ID_;
  } else {
    id = Material::g_unused_IDs_.back();
    Material::g_unused_IDs_.pop_back();
  }
  return id;
}

Material::~Material() {
  Material::g_unused_IDs_.push_back(id_);
  DestroyAllUniforms();
}

void Material::Use() {
  // TODO: We can improve performance by not uploading all Uniforms.
  // This would mean having a separate program for each material. OpenGL states
  // that attributes and uniforms are bound to a program.
  assert(program_);
  program_->Use();
  GLtextureSwapper::Instance().Freeze();
  for (auto pair : uniforms_) pair.second->Use();
  GLtextureSwapper::Instance().Thaw();
}

void Material::DestroyUniform(GLint location) {
  auto uniform = uniforms_[location];
  if (!uniform) {
    return;
  }
  delete uniform;
}

void Material::DestroyAllUniforms() {
  for (auto pair : uniforms_) {
    delete pair.second;
  }
}

void Material::RecompileProgram() {
#ifndef VRWM_DISCARD_INACTIVE_UNIFORMS
  program_->Recompile();
  std::map<GLint, AbstractUniform*> relocated_uniforms;
  std::map<std::string, AbstractUniform*> inactives;
  // Find new location of all previously active uniforms
  for (auto& location_uniform : uniforms_) {
    auto uniform = location_uniform.second;
    GLint location = program_->Uniform(uniform->name(), true);
    if (location == -1) {
      inactives.insert(std::make_pair(uniform->name(), uniform));
    } else {
      relocated_uniforms.insert(std::make_pair(location, uniform));
    }
    uniform->set_location(location);
  }
  // Find new location of all previously inactive uniforms
  for (auto& name_uniform : inactive_uniforms_) {
    auto uniform = name_uniform.second;
    GLint location = program_->Uniform(uniform->name(), true);
    if (location == -1) {
      inactives.insert(std::make_pair(uniform->name(), uniform));
    } else {
      relocated_uniforms.insert(std::make_pair(location, uniform));
    }
    uniform->set_location(location);
  }
  uniforms_ = std::move(relocated_uniforms);
  inactive_uniforms_ = std::move(inactives);
#else
  vrwm_log(Warning, "Unable to recompile program: VRWM was compiled with "
    " VRWM_DISCARD_INACTIVE_UNIFORMS which disables storing inactive "
    " uniforms. Recompiling a shader would lead to wrong results. Please "
    " compile VRWM without the flag and try again.");
#endif
}



