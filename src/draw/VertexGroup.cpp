#include "../../include/draw/VertexGroup.h"

#include <algorithm>
#include <cstring>

#include "../../include/draw/VertexAttribute.h"

using namespace vrwm::draw;

VertexGroup::RaycastableTriangleIterator::RaycastableTriangleIterator(
    const VertexGroup* v) :
      cached_value_({glm::vec3(0.0f), glm::vec3(0.0f), glm::vec3(0.0f)}),
      vertex_group_(v),
      position_(0) {
  if (vertex_group_->is_raycastable_) {
    Cache();
  }
}

VertexGroup::RaycastableTriangleIterator::RaycastableTriangleIterator(
    const RaycastableTriangleIterator& cc) :
      cached_value_(cc.cached_value_),
      vertex_group_(cc.vertex_group_),
      position_(cc.position_) {
  if (vertex_group_->is_raycastable_) {
    Cache();
  }
}

bool VertexGroup::RaycastableTriangleIterator::operator==(
    const RaycastableTriangleIterator& other) const {
  return vertex_group_ == other.vertex_group_ && position_ == other.position_;
}

bool VertexGroup::RaycastableTriangleIterator::operator!=(
    const RaycastableTriangleIterator& other) const {
  return !(*this == other);
}

VertexGroup::RaycastableTriangleIterator::const_reference
VertexGroup::RaycastableTriangleIterator::operator*() const {
  assert(position_ < vertex_group_->num_elements_);
  return cached_value_;
}

VertexGroup::RaycastableTriangleIterator::const_pointer
VertexGroup::RaycastableTriangleIterator::operator->() const {
  assert(position_ < vertex_group_->num_elements_);
  return &cached_value_;
}

VertexGroup::RaycastableTriangleIterator&
VertexGroup::RaycastableTriangleIterator::operator++() {
  assert(position_ < vertex_group_->num_elements_);

  if (vertex_group_->rendering_mode_ == GL_TRIANGLES || position_ == 0) {
    position_ += 3;
  } else {
    ++position_;
  }
  if (position_ < vertex_group_->num_elements_) {
    Cache();
  }

  return *this;
}

void VertexGroup::RaycastableTriangleIterator::Cache() {
  if (position_ >= vertex_group_->num_elements_) {
    return;
  }

  size_t i = position_;
  const float* vert = CalculateVertexPointer(i);
  // Update the cached values
  // Because triangle fan and strip both require 3 new vertices for the
  // very first triangle, this case is handled in the GL_TRIANGLES block
  if (vertex_group_->rendering_mode_ == GL_TRIANGLES || i == 0) {
    // GL_TRIANGLES:
    //  Every triangle is specified by exactly 3 new vertices. No vertices
    //  are reused for each new draw.
    //      1 *        4 *
    //       /|          |\                                                    .
    //    2 *-* 3      5 *-* 6
    cached_value_.v1 = glm::vec3(vert[0], vert[1], vert[2]);
    ++i;
    vert = CalculateVertexPointer(i);
    cached_value_.v2 = glm::vec3(vert[0], vert[1], vert[2]);
    ++i;
    vert = CalculateVertexPointer(i);
    cached_value_.v3 = glm::vec3(vert[0], vert[1], vert[2]);
  } else if (vertex_group_->rendering_mode_ == GL_TRIANGLE_STRIP) {
    // GL_TRIANGLE_STRIP:
    //  The first Triangle is specified by 3 new vertices (see above). After
    //  that, only one vertex is needed to form the next triangle.
    //   0  2  4  6  8          Draw order: 0 1 2
    //   *--*--*--*--*                      2 1 3
    //   | /| /| /| /|                      2 3 4
    //   |/ |/ |/ |/ |                      4 3 5
    //   *--*--*--*--*                      4 5 6
    //   1  3  5  7  9
    // Please note that the direction of the triangles normal is inverted on
    // every second triangle.
    *(&cached_value_.v1 + (i % 3)) = glm::vec3(vert[0], vert[1], vert[2]);
  } else if (vertex_group_->rendering_mode_ == GL_TRIANGLE_FAN) {
    // GL_TRIANGLE_STRIP:
    //  The first triangle is specified by 3 new vertices. After that, only
    //  one vertex is needed to form the next triangle.
    //         1 *              Draw order: 0 1 2
    //          /|                          0 2 3
    //       2 *-* 0                        0 3 4
    //          \|                          0 4 5
    //         3 *
    //  As you can see, the first vertex always stays the same. The last one
    //  must take the place of the middle one to form a fan with consistent
    //  normals.
    cached_value_.v2 = cached_value_.v3;
    cached_value_.v3 = glm::vec3(vert[0], vert[1], vert[2]);
  } else {
    throw std::runtime_error("Invalid rendering mode for raycast.");
  }
}

const VertexGroup::RaycastableTriangleIterator
VertexGroup::RaycastableTriangleIterator::operator++(int) {
  RaycastableTriangleIterator tmp(*this);
  ++(*this);
  return tmp;
}

const float*
VertexGroup::RaycastableTriangleIterator::CalculateVertexPointer(size_t i)
    const {
  // Cast to int8_t* to ensure pointer arithmetic jumps 1 byte (stride is
  // in byte as per OpenGL specification
  const int8_t* const base = static_cast<const int8_t*>(
      vertex_group_->vertex_data_);
  const size_t offset = reinterpret_cast<const size_t>(
      vertex_group_->raycastable_offset_);
  const GLsizei stride = vertex_group_->raycastable_stride_;
  const size_t vertex_size = sizeof(float) * 3;
  return reinterpret_cast<const float*> (
      base + offset + i * (vertex_size + stride));
}


VertexGroup::VertexGroup() :
    vertex_data_(nullptr),
    is_raycastable_(false),
    vertex_buffer_(0),
    vertex_array_(0),
    index_buffer_(0),
    rendering_mode_(GL_TRIANGLE_STRIP),
    elements_type_(-1),
    num_elements_(0) {
  CALL_GL(glGenVertexArrays(1, &vertex_array_));
  CALL_GL(glBindVertexArray(vertex_array_));
  CALL_GL(glGenBuffers(1, &vertex_buffer_));
  CALL_GL(glGenBuffers(1, &index_buffer_));
}

VertexGroup::VertexGroup(const VertexGroup& cc) {
  CALL_GL(glGenVertexArrays(1, &vertex_array_));
  CALL_GL(glBindVertexArray(vertex_array_));
  CALL_GL(glGenBuffers(1, &vertex_buffer_));
  CALL_GL(glGenBuffers(1, &index_buffer_));
  *this = cc;
}

VertexGroup::~VertexGroup() {
  glDeleteBuffers(1, &vertex_buffer_);
  glDeleteBuffers(1, &index_buffer_);
  glDeleteVertexArrays(1, &vertex_array_);
  if (vertex_data_) {
    delete[] static_cast<int8_t*>(vertex_data_);
  }
}

VertexGroup::VertexGroup(VertexGroup&& c) noexcept :
    vertex_data_(nullptr),
    is_raycastable_(false),
    vertex_buffer_(0),
    vertex_array_(0),
    index_buffer_(0),
    rendering_mode_(GL_TRIANGLE_STRIP),
    elements_type_(-1),
    num_elements_(0) {
  *this = std::move(c);
}

VertexGroup& VertexGroup::operator=(VertexGroup&& move) noexcept {
  if (&move == this) {
    return *this;
  }
  vertex_data_ = move.vertex_data_;
  vertex_data_size_ = move.vertex_data_size_;
  raycastable_stride_ = move.raycastable_stride_;
  raycastable_offset_ = move.raycastable_offset_;
  is_raycastable_ = move.is_raycastable_;
  vertex_buffer_ = move.vertex_buffer_;
  vertex_array_ = move.vertex_array_;
  index_buffer_ = move.index_buffer_;
  rendering_mode_ = move.rendering_mode_;
  elements_type_ = move.elements_type_;
  num_elements_ = move.num_elements_;

  num_elements_ = 0;
  return *this;
}

VertexGroup& VertexGroup::operator=(const VertexGroup& copy) {
  if (&copy == this) {
    return *this;
  }

  SetData(copy.vertex_data_, copy.vertex_data_size_);

  vertex_data_ = new int8_t[vertex_data_size_];
  std::memcpy(vertex_data_, copy.vertex_data_, vertex_data_size_);
  raycastable_stride_ = copy.raycastable_stride_;
  raycastable_offset_ = copy.raycastable_offset_;
  is_raycastable_ = copy.is_raycastable_;

  elements_type_ = copy.elements_type_;
  num_elements_ = copy.num_elements_;

  GLsizeiptr size;
  if (elements_type_ == GL_UNSIGNED_BYTE) {
    size = num_elements_ * sizeof(GLubyte);
  } else if (elements_type_ == GL_UNSIGNED_SHORT) {
    size = num_elements_ * sizeof(GLushort);
  } else {
    size = num_elements_ * sizeof(GLuint);
  }
  uint8_t* copy_elements_data = new uint8_t[size];
  CALL_GL(glBindVertexArray(vertex_array_));
  CALL_GL(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, copy.index_buffer_));
  CALL_GL(glGetBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, size,
    &copy_elements_data));
  CALL_GL(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer_));
  CALL_GL(glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, copy_elements_data,
    GL_STATIC_DRAW));
  delete[] copy_elements_data;

  return *this;
}

void VertexGroup::SetData(const void* new_data, GLsizeiptr data_size) {
  if (vertex_data_) {
    delete[] static_cast<int8_t*>(vertex_data_);
  }
  vertex_data_ = new int8_t[data_size];
  vertex_data_size_ = data_size;
  std::memcpy(vertex_data_, new_data, data_size);
  CALL_GL(glBindVertexArray(vertex_array_));
  CALL_GL(glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_));
  CALL_GL(glBufferData(GL_ARRAY_BUFFER, data_size, new_data, GL_STATIC_DRAW));
}

void VertexGroup::ModifyData(void* replacement_data, GLintptr offset,
    GLsizeiptr data_size) {
  if (offset + data_size > vertex_data_size_) {
    throw std::out_of_range("ModifyData parameters are too large.");
  }
  std::memcpy(static_cast<char*>(vertex_data_) + offset, replacement_data,
      data_size);
  CALL_GL(glBindVertexArray(vertex_array_));
  CALL_GL(glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_));
  CALL_GL(glBufferSubData(GL_ARRAY_BUFFER, offset, data_size,
        replacement_data));
}

void VertexGroup::SetElements(GLenum mode,
    const std::vector<GLuint>& elements) {
  rendering_mode_ = mode;
  CALL_GL(glBindVertexArray(vertex_array_));
  CALL_GL(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer_));
  CALL_GL(glBufferData(GL_ELEMENT_ARRAY_BUFFER,
    elements.size() * sizeof(GLuint), elements.data(), GL_STATIC_DRAW));
  elements_type_ = GL_UNSIGNED_INT;
  num_elements_ = elements.size();
}

VertexGroup::RaycastableTriangleIterator
VertexGroup::begin_raycastable_triangles() const {
  if (!is_raycastable_) {
    return end_raycastable_triangles();
  }
  return RaycastableTriangleIterator(this);
}

VertexGroup::RaycastableTriangleIterator
VertexGroup::end_raycastable_triangles() const {
  RaycastableTriangleIterator t(this);
  t.position_ = num_elements_;
  return t;
}

void VertexGroup::AddRaycastableVertexAttribute(
    const ResolvedVertexAttribute& a) {
  if (a.type() != GL_FLOAT || a.num_components() != 3) {
    throw std::runtime_error("Invalid format for raycastable vertex "
        "attribute.");
  }
  raycastable_stride_ = a.stride();
  raycastable_offset_ = a.offset();
  is_raycastable_ = true;
  AddVertexAttribute(a);
}

void VertexGroup::AddVertexAttribute(const ResolvedVertexAttribute& a) {
  CALL_GL(glBindVertexArray(vertex_array_));
  CALL_GL(glEnableVertexAttribArray(a.index()));
  CALL_GL(glVertexAttribPointer(a.index(), a.num_components(), a.type(),
    a.normalized(), a.stride(), a.offset()));
}

void VertexGroup::Render() {
  assert(num_elements_ > 0);
  assert(elements_type_ == GL_UNSIGNED_BYTE ||
    elements_type_ == GL_UNSIGNED_SHORT ||
    elements_type_ == GL_UNSIGNED_INT);
  CALL_GL(glBindVertexArray(vertex_array_));
  CALL_GL(glDrawElements(rendering_mode_, num_elements_, elements_type_, 0));
}


