#include "../../include/draw/VertexAttribute.h"

using namespace vrwm::draw;

VertexAttribute::VertexAttribute(GLint num_components, GLenum type,
    GLboolean normalized, GLsizei stride, const GLvoid* offset) :
      num_components_(num_components),
      type_(type),
      normalized_(normalized),
      stride_(stride),
      offset_(offset) {
}

VertexAttribute::VertexAttribute(const VertexAttribute& cc) :
    num_components_(cc.num_components_),
    type_(cc.type_),
    normalized_(cc.normalized_),
    stride_(cc.stride_),
    offset_(cc.offset_) {
}

VertexAttribute::~VertexAttribute() {
}

ResolvedVertexAttribute::ResolvedVertexAttribute(GLuint index,
    GLint num_components, GLenum type, GLboolean normalized, GLsizei stride,
    const GLvoid* offset) :
      VertexAttribute(num_components, type, normalized, stride, offset),
      index_(index) {
}
ResolvedVertexAttribute::ResolvedVertexAttribute(GLuint index,
    const NamedVertexAttribute& a) :
      VertexAttribute(a.num_components(), a.type(), a.normalized(), a.stride(),
        a.offset()),
      index_(index) {
}
ResolvedVertexAttribute::ResolvedVertexAttribute(GLuint index,
    GLint num_components, const GLvoid* offset) :
      VertexAttribute(num_components, GL_FLOAT, GL_FALSE, 0, offset),
      index_(index) {
}
ResolvedVertexAttribute::ResolvedVertexAttribute(GLuint index,
    GLint num_components, size_t offset) :
      ResolvedVertexAttribute(index, num_components,
        reinterpret_cast<const GLvoid*>(offset)) {
}

ResolvedVertexAttribute::ResolvedVertexAttribute(GLuint index,
    GLint num_components) :
      VertexAttribute(num_components, GL_FLOAT, GL_FALSE, 0, nullptr),
      index_(index) {
}

ResolvedVertexAttribute::ResolvedVertexAttribute(
  const ResolvedVertexAttribute& cc) :
    ResolvedVertexAttribute(cc.index_, cc.num_components_, cc.type_,
      cc.normalized_, cc.stride_, cc.offset_) {
}


NamedVertexAttribute::NamedVertexAttribute(const std::string& name,
    GLint num_components, GLenum type, GLboolean normalized, GLsizei stride,
    const GLvoid* offset) :
      VertexAttribute(num_components, type, normalized, stride, offset),
      name_(name) {
}
NamedVertexAttribute::NamedVertexAttribute(const std::string& name,
    GLint num_components, const GLvoid* offset) :
      VertexAttribute(num_components, GL_FLOAT, GL_FALSE, 0, offset),
      name_(name) {
}
NamedVertexAttribute::NamedVertexAttribute(const std::string& name,
    GLint num_components, size_t offset) :
      NamedVertexAttribute(name, num_components,
        reinterpret_cast<const GLvoid*>(offset)) {
}

NamedVertexAttribute::NamedVertexAttribute(const std::string& name,
    GLint num_components) :
      VertexAttribute(num_components, GL_FLOAT, GL_FALSE, 0, nullptr),
      name_(name) {
}

NamedVertexAttribute::NamedVertexAttribute(
  const NamedVertexAttribute& cc) :
    NamedVertexAttribute(cc.name_, cc.num_components_, cc.type_,
      cc.normalized_, cc.stride_, cc.offset_) {
}

