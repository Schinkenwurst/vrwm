#include "../include/Core.h"

#include <cassert>
#include <cstring>

#include <chrono>
#include <exception>
#include <string>

#include <glm/glm.hpp>
#include <glm/matrix.hpp>

#include <unistd.h>

#include <X11/Xlib.h>
#include <X11/extensions/Xcomposite.h>
#include <X11/extensions/Xge.h>
#include <X11/extensions/ge.h>

#include "../include/Atoms.h"
#include "../include/EventHandler.h"
#include "../include/GLglobal.h"
#include "../include/LogManager.h"
#include "../include/OculusManager.h"
#include "../include/OpenGLManager.h"
#include "../include/PluginManager.h"
#include "../include/plugin/EventReceiver.h"
#include "../include/Renderable.h"
#include "../include/XInputDeviceManager.h"

using namespace vrwm;

Core* Core::g_core_instance = nullptr;
XErrorHandler Core::g_default_xlib_error_handler = nullptr;

int Core::InitializeInstance() {
  assert(!g_core_instance);
  g_core_instance = new Core();
  return g_core_instance->Init();
}

Core& Core::Instance() {
  assert(g_core_instance);
  return *g_core_instance;
}

void Core::DestroyInstance() {
  if (g_core_instance) {
    delete g_core_instance;
  }
  g_core_instance = nullptr;
}

Core::Core()
    : event_handler_(nullptr)
    , oculus_manager_(nullptr)
    , openGL_manager_(nullptr)
    , cursor_(*this)
    , display_(nullptr)
    , screen_(0)
    , composite_overlay_window_(nullptr)
    , openGL_window_(nullptr)
    , focus_client_(nullptr)
    , cursor_client_(nullptr)
    // shutdown true during startup, will be set to false when running
    , shutdown_(true) {
}

Core::~Core() {
  plugin_manager_->ForEach(&plugin::ProgrammStatusReceiver::Teardown);

  // Release all keys. Otherwise causes bugs on vrwm restart.
  XUngrabKey(display_, AnyKey, AnyModifier, RootWindow(display_, screen_));

  vrwm_log(Info, "Destruction of XInputDeviceManager");
  if (xinput_manager_)
    delete xinput_manager_;
  vrwm_log(Info, "Destruction of EventHandler");
  if (event_handler_)
    delete event_handler_;
  vrwm_log(Info, "Destruction of OculusManager");
  if (oculus_manager_)
    delete oculus_manager_;
  vrwm_log(Info, "Destruction of OpenGLManager");
  if (openGL_manager_)
    delete openGL_manager_;
  vrwm_log(Info, "Destruction of OpenGL window");
  if (openGL_window_)
    XDestroyWindow(display_, openGL_window_->ID());
  vrwm_log(Info, "Destruction of composite overlay window");
  if (composite_overlay_window_)
    XCompositeReleaseOverlayWindow(display_, composite_overlay_window_->ID());
  vrwm_log(Info, "Destruction of all managed windows");
  for (auto managed : managed_windows_) {
    delete managed.second;
  }
  vrwm_log(Info, "Destruction of PluginManager");
  if (plugin_manager_)
    delete plugin_manager_;
}

int Core::Init() {
  try {
    ParseConfig();
    InitPlugins();

    InitConnection();
    Atoms::Initialize(display_);
    InitInput();
    InitComposite();
    InitOpenGLWindow();
    InitWindowManager();

    PostStartup();
  } catch(std::exception& e) {
    vrwm_clog(Error, "Fatal error on initialization: %s", e.what());
    return -1;
  }
  return 0;
}

void Core::ParseConfig() {
  config_.ParseConfig();
  config_.InitCoreConfiguration();
}

void Core::InitPlugins() {
  plugin_manager_ = new plugin::PluginManager();
  plugin_manager_->LoadAllPlugins();
  plugin_manager_->ForEach(&plugin::ProgrammStatusReceiver::PreInit);
}

#include "../include/Assets.h"
#include "../include/SceneNode.h"
void Core::PostStartup() {
  plugin_manager_->ForEach(&plugin::ProgrammStatusReceiver::PostInit);
}

void Core::InitConnection() {
  display_ = XOpenDisplay(0);
  if (!display_) {
    throw std::runtime_error("Unable to connect to the X server");
  }
  screen_ = DefaultScreen(display_);
  g_default_xlib_error_handler = XSetErrorHandler(Core::ErrorHandler);
  vrwm_log(Info, "Connection to the X server established.\nRoot window has ID "+
      std::to_string(GetRootWindow()));
  LogManager::SetErrorCallback(&Core::LogErrorCallback);

  vrwm_log(Info, "Setting possible icon sizes...");
  XIconSize *s = XAllocIconSize();
  s->min_width = s->min_height = 16;
  s->max_width = s->max_height = 512;
  s->width_inc = s->height_inc = 16;
  XSetIconSizes(display_, GetRootWindow(), s, 1);
  vrwm_log(Info, "Done setting possible icon sizes!");
}

void Core::InitComposite() {
  int event_base, error_base;
  if (!XCompositeQueryExtension(display_, &event_base, &error_base)) {
    throw std::runtime_error("X server does not support composite extension");
  }
  int major_version = 0, minor_version = 4;
  XCompositeQueryVersion(display_, &major_version, &minor_version);

  if (minor_version < 4 && major_version == 0) {
    throw std::runtime_error("Composite version too low. Expected 0.4 or"
      " higher.");
  }
  vrwm_clog(Info, "Composite Version: %i.%i", major_version, minor_version);

  Window overlay_window = XCompositeGetOverlayWindow(
          display_,
          RootWindow(display_, screen_));

  vrwm_clog(Info, "Overlay window has ID: %u", overlay_window);

  XWindowAttributes attrs;
  XGetWindowAttributes(display_, overlay_window, &attrs);
  composite_overlay_window_ = new Client(*this, overlay_window, attrs, nullptr);
  composite_overlay_window_->SetName("Composite Overlay Window");
  composite_overlay_window_->SetIconName("Composite Overlay Window");
}

void Core::InitInput() {
  event_handler_ = new EventHandler();

  vrwm_log(Info, "Initializing XInputExtension...");
  int xi_opcode, xi_event_base, xi_error_base;
  if (!XQueryExtension(display_, "XInputExtension", &xi_opcode, &xi_event_base,
      &xi_error_base)) {
    vrwm_log(Error, "XInput2 extension not available.");
    throw std::runtime_error("XInput2 not available.");
  }
  event_handler_->SetXInputExtensionData(xi_opcode, xi_event_base,
    xi_error_base);
  vrwm_clog(Info, "XInputExtension has \n"
    "\tOpCode:     %i\n"
    "\tEvent base: %i\n"
    "\tError base: %i", xi_opcode, xi_event_base, xi_error_base);
  xinput_manager_ = new XInputDeviceManager(*this);
  xinput_manager_->Init();
  vrwm_log(Info, "XInputExtension initialized!");
}

void Core::InitOpenGLWindow() {
  GLint window_requirements[] = {
    GLX_RGBA,
    GLX_DEPTH_SIZE, 24,
    GLX_DOUBLEBUFFER,
    None
  };

  XVisualInfo* visual;
  visual = glXChooseVisual(display_, screen_, window_requirements);
  if (visual) {
    vrwm_clog(Info, "GLX visuals:\n"
        "\tBits per RGB: %i\n"
        "\tDepth:        %i", visual->bits_per_rgb, visual->depth);
  } else {
    throw std::runtime_error("glXChooseVisual failed to find a fitting "
        "visual.");
  }

  Colormap color_map = XCreateColormap(display_,
                                       RootWindow(display_, screen_),
                                       visual->visual,
                                       AllocNone);

  XSetWindowAttributes opengl_window_attributes;
  opengl_window_attributes.colormap = color_map;

  glm::ivec2 screen_size = GetScreenSize();
  Window openGL_window_id = XCreateWindow(
      display_,
      composite_overlay_window_->ID(),
      0,
      0,
      screen_size.x,
      screen_size.y,
      0,
      visual->depth,
      InputOutput,
      visual->visual,
      CWColormap,
      &opengl_window_attributes);

  XWindowAttributes attrs;
  XGetWindowAttributes(display_, openGL_window_id, &attrs);
  openGL_window_ = new Client(*this, openGL_window_id, attrs, nullptr);
  openGL_window_->SetName("VRWM OpenGL window");
  openGL_window_->SetIconName("VRWM OpenGL window");
  XMapWindow(display_, openGL_window_->ID());

  int glxmajor, glxminor;
  if (glXQueryVersion(display_, &glxmajor, &glxminor)) {
    vrwm_clog(Info, "GLX client version: %i.%i", glxmajor, glxminor);
  }
  else {
    throw std::runtime_error("Unable to query GLX version.");
  }

  vrwm_log(Info, "Choosing framebuffer config... ");
  GLint framebuffer_requirements[] = {
    GLX_RENDER_TYPE, GLX_RGBA_BIT,
    GLX_DEPTH_SIZE, 24,
    GLX_DOUBLEBUFFER, true,
    None
  };
  int num_configs = 0;
  GLXFBConfig* configs = glXChooseFBConfig(
      display_,
      DefaultScreen(display_),
      framebuffer_requirements,
      &num_configs);
  if (num_configs == 0) {
    vrwm_log(Error, "Number of availabe framebuffer configurations is 0");
    throw std::runtime_error("Unable to choose GLX framebuffer configuration.");
  }
  vrwm_log(Info, "Creating context...");
  GLXFBConfig config = configs[0];
  GLXContext glxcontext = glXCreateNewContext(display_, config, GLX_RGBA_TYPE,
                                              NULL, GL_TRUE);
  vrwm_log(Info, "Making context current...");
  if (!glXMakeContextCurrent(
        display_,
        openGL_window_->ID(),
        openGL_window_->ID(),
        glxcontext)) {
    throw std::runtime_error("Error assigning the window designated for "
      "OpenGL drawing to the GLX context.");
  }

  vrwm_log(Info, "Initializing glew...");
  GLenum glError;
  if((glError = glewInit()) != GLEW_OK) {
    throw std::runtime_error(std::string("Unable to initialize GLEW:\n") +
        reinterpret_cast<const char*>(glewGetErrorString(glError)));
  }

  vrwm_clog(Info, "OpenGL version: %s\n"
      "GLSL version:   %s\n"
      "Vendor:         %s\n"
      "Renderer:       %s", glGetString(GL_VERSION),
      glGetString(GL_SHADING_LANGUAGE_VERSION), glGetString(GL_VENDOR),
      glGetString(GL_RENDERER));

  if (!GLEW_VERSION_3_3) {
    throw std::runtime_error("OpenGL API 3.3 is not available.");
  }
  vrwm_log(Info, "[X] OpenGL version at least 3.3");

  std::string ext = glXQueryExtensionsString(display_, DefaultScreen(display_));
  for(size_t pos = ext.find(' ');
      pos != std::string::npos;
      pos = ext.find(' ')) {
    ext.replace(pos, 1, "\n\t");
  }
  vrwm_log(Info, "Available OpenGL extensions: \n\t" + ext);
  if (ext.find("GLX_EXT_texture_from_pixmap") == std::string::npos) {
    throw std::runtime_error("GLX extension 'texture from pixmap' is not "
      "supported.");
  }

  glXBindTexImageEXT_func =
    reinterpret_cast<void(*)(Display*, GLXDrawable, int, const int*)>(
      glXGetProcAddress(
        reinterpret_cast<const GLubyte*>("glXBindTexImageEXT")));
  glXReleaseTexImageEXT_func =
    reinterpret_cast<void(*)(Display*, GLXDrawable, int)>(
      glXGetProcAddress(
        reinterpret_cast<const GLubyte*>("glXReleaseTexImageEXT")));
  if (!glXBindTexImageEXT_func || !glXReleaseTexImageEXT_func) {
    throw std::runtime_error("GLX extension 'texture from pixmap' is not "
      "supported.");
  } else {
    vrwm_clogdeb("GLX extension texture from pixmap found!:\n"
      "\tglXBindTexImageEXT:    %p\n"
      "\tglXReleaseTexImageEXT: %p\n",
      glXBindTexImageEXT_func, glXReleaseTexImageEXT_func);
  }

#ifdef VRWM_DEBUG
  vrwm_logdeb("Creating debug context...");
  if (GLX_ARB_create_context) {
    PFNGLXCREATECONTEXTATTRIBSARBPROC glXCreateContextAttribsARB;
    glXCreateContextAttribsARB = (PFNGLXCREATECONTEXTATTRIBSARBPROC)
      glXGetProcAddress(reinterpret_cast<const GLubyte*>(
            "glXCreateContextAttribsARB"));
    if (glXCreateContextAttribsARB != nullptr) {
      glXMakeCurrent(display_, None, nullptr);
      glXDestroyContext(display_, glxcontext);
      const int glx_context_attributes[] = {
        GLX_CONTEXT_FLAGS_ARB, GLX_CONTEXT_DEBUG_BIT_ARB,
        None
      };
      glxcontext = glXCreateContextAttribsARB(display_, config, NULL,
                                              GL_TRUE, glx_context_attributes);
      if (!glxcontext) {
        throw std::runtime_error("Unable to create debug context.");
      }
      vrwm_logdeb("DEBUG: Debug context created.");
      glXMakeContextCurrent(display_, openGL_window_->ID(),
                            openGL_window_->ID(), glxcontext);
      if (GLEW_ARB_debug_output) {
        glDebugMessageCallbackARB(&OpenGLManager::DebugCallbackARB, nullptr);
        //glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
        vrwm_logdeb("DEBUG: registered debug callback.");
      } else {
        vrwm_log(Warning, "GLEW_ARB_debug_output not available.");
      }
    } else {
      vrwm_log(Warning, "Warning: glXCreateContextAttribsARB not available.");
    }
  } else {
    vrwm_log(Warning, "Warning: GLXEW_ARB_create_context not available.");
  }
#endif

  openGL_manager_ = new OpenGLManager(*this);
  vrwm_clog(Info, "Oculus Rift Mode: %i", (int)config_.oculus_mode);
  oculus_manager_ = new OculusManager(*this, config_.oculus_mode);
}

void Core::InitWindowManager() {
  vrwm_log(Info, "Checking for any other active window managers...");

  // Opt-in for information on all map events, etc. Will fail if another WM is
  // running.
  // TODO: register error callback function to print a nicer error message in
  // case of failure.
  XSelectInput(display_, RootWindow(display_, screen_),
      SubstructureRedirectMask | SubstructureNotifyMask);
  // Begin redirection of all windows and future windows to offscreen storage.
  // This will later allow us to access each window's contents as pixmap.
  XCompositeRedirectSubwindows(display_, GetRootWindow(),
    CompositeRedirectAutomatic);
  XSync(display_, false);

  // Grab all keys with the modkey modifier as well as all possible combinations
  // of modkey and control, alt, numlock, mouse1, etc. This is done to still
  // receive input when modkey is held, regardless of currently focused window
  // as well as other active modifiers.
  // If we would not grab those keys, we would be unable to control vrwm
  // when any window has focus.
  // Also, active numlock would prevent controlling vrwm.
  vrwm_log(Info, "Grabbing keys...");
  int modkey = 1 << config_.modkey;
  KeyCode modkey_code = ModifierNames::GetKeycodeOfModifier(config_.modkey);

  // Note: The ICCCM actually forbids grabbing keys like that. It states that
  // the window manager should have some sort of mechanism to allow a client
  // window to receive input that is also used for window manager capabilities.
  // I think this is not worth the effort and I will therefore not implement
  // it.
  // TODO: Once the key bindings can be configured in the config file: Only
  // grab those. The procedure below fails easily when some combinations are
  // already grabbed (e.g. in xbindkeys)
  // TODO: On nicer error message: If this fails, print: Check if the keycode
  // is already used in xbindkeys
  for (int i = 0; i < (1 << KeyboardMod5); ++i) {
    if ((i & modkey) == modkey) continue;
    XGrabKey(display_, AnyKey, i | modkey, GetRootWindow(), false,
      GrabModeAsync, GrabModeAsync);
    XSync(display_, false);
  }
  // We also grab the modkey in combination with no modifier so that we always
  // receive events when the modkey is pressed or released.
  XGrabKey(display_, modkey_code, 0, RootWindow(display_, screen_),
    false, GrabModeAsync, GrabModeAsync);

  vrwm_log(Info, "Grabs successful!");

  // NOTE: The ICCCM section 4.1.8 (Colormaps) states that the window manager
  // should suspend colormap installation when the client specifies it.
  // I have decided to not implement this behavior as it is simply not worth
  // the effort because color maps are not widely used anymore.

  // TODO: EWMH Window Manager Selection (_NET_WM_CM_S0)

  vrwm_log(Info, "Setting supported EWMH protocols...");

  // Clear potentially existing _NET_CLIENT_LIST from a previous window
  // manager. This property will refresh when VRWN searches currently active
  // clients.
  XDeleteProperty(display_, GetRootWindow(), Atoms::_NET_CLIENT_LIST);
  XDeleteProperty(display_, GetRootWindow(), Atoms::_NET_CLIENT_LIST_STACKING);

  int tmp_int = 1;
  XChangeProperty(display_, GetRootWindow(), Atoms::_NET_NUMBER_OF_DESKTOPS,
    XA_CARDINAL, 32, PropModeReplace,
    reinterpret_cast<unsigned char*>(&tmp_int), 1);
  // _NET_NUMBER_OF_DESKTOPS request from Pager is NOT honored.

  glm::uvec2 tmp_uvec2 = GetScreenSize();
  XChangeProperty(display_, GetRootWindow(), Atoms::_NET_DESKTOP_GEOMETRY,
    XA_CARDINAL, 32, PropModeReplace,
    reinterpret_cast<unsigned char*>(&tmp_uvec2), 2);
  // _NET_DESKTOP_GEOMETRY command from Pager is NOT supported.

  tmp_uvec2.x = tmp_uvec2.y = 0;
  XChangeProperty(display_, GetRootWindow(), Atoms::_NET_DESKTOP_VIEWPORT,
    XA_CARDINAL, 32, PropModeReplace,
    reinterpret_cast<unsigned char*>(&tmp_uvec2), 2);
  // _NET_DESKTOP_VIEWPORT command from client is NOT supported.

  tmp_int = 0;
  XChangeProperty(display_, GetRootWindow(), Atoms::_NET_CURRENT_DESKTOP,
    XA_CARDINAL, 32, PropModeReplace,
    reinterpret_cast<unsigned char*>(&tmp_int), 1);
  // As _NET_NUMBER_OF_DESKTOPS cannot be changed by a Pager and VRWM itself
  // does not change it, _NET_CURRENT_DESKTOP will always be 0. We therefore
  // ignore the _NET_CURRENT_DESKTOP pager command as it cannot switch to any
  // other desktop.

  Update_NET_ACTIVE_WINDOW(nullptr);
  // The respective client request is honored.
  // See EventHandler::CMsg_NET_ACTIVE_WINDOW()

  tmp_uvec2 = GetScreenSize();
  uint workarea[] = { 0, 0, tmp_uvec2.x, tmp_uvec2.y };
  XChangeProperty(display_, GetRootWindow(), Atoms::_NET_WORKAREA,
    XA_CARDINAL, 32, PropModeReplace,
    reinterpret_cast<unsigned char*>(workarea), 4);

  Window w = openGL_window_->ID();
  XChangeProperty(display_, GetRootWindow(), Atoms::_NET_SUPPORTING_WM_CHECK,
    XA_WINDOW, 32, PropModeReplace,
    reinterpret_cast<unsigned char*>(&w), 1);
  XChangeProperty(display_, openGL_window_->ID(),
    Atoms::_NET_SUPPORTING_WM_CHECK, XA_WINDOW, 32, PropModeReplace,
    reinterpret_cast<unsigned char*>(&w), 1);

  //_NET_SHOWING_DESKTOP is NOT supported. (for now)
  //_NET_WM_MOVERESIZE is NOT supported.
  //_NET_WM_RESTACK_WINDOW is NOT supported.

  //_NET_WM_DESKTOP not supported as only one desktop exists
  //_NET_WM_STRUT and _PARTIAL are not supported: No use in 3D.
  //_NET_WM_ICON_GEOMETRY not supported as it is 2D.
  //_NET_WM_OPAQUE_REGION not supported as currently not useful.
  //_NET_WM_BYPASS_COMPOSITOR not supported as VRWM MUST composite everything.
  //  (except non-VR fullscreen windows which will automatically go
  //  uncomposited)

  // TODO: visible name (NAME_VISIBLE)
  Atom supported_EWMH_protocols[] = {
    Atoms::_NET_SUPPORTED, Atoms::_NET_CLIENT_LIST,
    Atoms::_NET_CLIENT_LIST_STACKING, Atoms::_NET_NUMBER_OF_DESKTOPS,
    Atoms::_NET_DESKTOP_GEOMETRY, Atoms::_NET_DESKTOP_VIEWPORT,
    Atoms::_NET_CURRENT_DESKTOP, Atoms::_NET_ACTIVE_WINDOW,
    Atoms::_NET_ACTIVE_WINDOW, Atoms::_NET_SUPPORTING_WM_CHECK,
    Atoms::_NET_CLOSE_WINDOW, Atoms::_NET_MOVERESIZE_WINDOW,
    Atoms::_NET_REQUEST_FRAME_EXTENTS, Atoms::_NET_WM_NAME,
    Atoms::_NET_WM_ICON_NAME,

    Atoms::_NET_WM_WINDOW_TYPE,
    Atoms::_NET_WM_WINDOW_TYPE_DESKTOP, Atoms::_NET_WM_WINDOW_TYPE_DOCK,
    Atoms::_NET_WM_WINDOW_TYPE_TOOLBAR, Atoms::_NET_WM_WINDOW_TYPE_MENU,
    Atoms::_NET_WM_WINDOW_TYPE_UTILITY, Atoms::_NET_WM_WINDOW_TYPE_SPLASH,
    Atoms::_NET_WM_WINDOW_TYPE_DIALOG,
    Atoms::_NET_WM_WINDOW_TYPE_DROPDOWN_MENU,
    Atoms::_NET_WM_WINDOW_TYPE_POPUP_MENU, Atoms::_NET_WM_WINDOW_TYPE_TOOLTIP,
    Atoms::_NET_WM_WINDOW_TYPE_NOTIFICATION, Atoms::_NET_WM_WINDOW_TYPE_COMBO,
    Atoms::_NET_WM_WINDOW_TYPE_DND, Atoms::_NET_WM_WINDOW_TYPE_NORMAL,

    Atoms::_NET_WM_STATE,
    Atoms::_NET_WM_STATE_MODAL, Atoms::_NET_WM_STATE_STICKY,
    Atoms::_NET_WM_STATE_MAXIMIZED_VERT, Atoms::_NET_WM_STATE_MAXIMIZED_HORZ,
    Atoms::_NET_WM_STATE_SHADED, Atoms::_NET_WM_STATE_SKIP_TASKBAR,
    Atoms::_NET_WM_STATE_SKIP_PAGER, Atoms::_NET_WM_STATE_HIDDEN,
    Atoms::_NET_WM_STATE_FULLSCREEN, Atoms::_NET_WM_STATE_ABOVE,
    Atoms::_NET_WM_STATE_BELOW, Atoms::_NET_WM_STATE_DEMANDS_ATTENTION,
    Atoms::_NET_WM_STATE_FOCUSED,

    Atoms::_NET_WM_ALLOWED_ACTIONS,
    Atoms::_NET_WM_ACTION_MOVE, Atoms::_NET_WM_ACTION_RESIZE,
    Atoms::_NET_WM_ACTION_MINIMIZE, Atoms::_NET_WM_ACTION_SHADE,
    Atoms::_NET_WM_ACTION_STICK, Atoms::_NET_WM_ACTION_MAXIMIZE_HORZ,
    Atoms::_NET_WM_ACTION_MAXIMIZE_VERT, Atoms::_NET_WM_ACTION_FULLSCREEN,
    Atoms::_NET_WM_ACTION_CHANGE_DESKTOP, Atoms::_NET_WM_ACTION_CLOSE,
    Atoms::_NET_WM_ACTION_ABOVE, Atoms::_NET_WM_ACTION_BELOW,

    Atoms::_NET_WM_ICON, Atoms::_NET_WM_PID, Atoms::_NET_WM_HANDLED_ICONS,
    Atoms::_NET_WM_USER_TIME, Atoms::_NET_WM_USER_TIME_WINDOW,
    Atoms::_NET_FRAME_EXTENTS,

    Atoms::_NET_WM_PING,
    Atoms::_NET_WM_FULL_PLACEMENT,
  };
  XChangeProperty(display_, GetRootWindow(), Atoms::_NET_SUPPORTED, XA_ATOM,
    32, PropModeReplace,
    reinterpret_cast<unsigned char*>(supported_EWMH_protocols),
    sizeof(supported_EWMH_protocols) / sizeof(Atom));


  vrwm_log(Info, "Searching for all previous windows...");
  plugin_manager_->ForEach(&plugin::ProgrammStatusReceiver::PreWindowSearch);

  unsigned int windowCount;
  Window root, parent;
  Window* windows;
  XQueryTree(display_, GetRootWindow(), &root, &parent, &windows,
    &windowCount);

  for (unsigned i = 0; i < windowCount; ++i) {
    Manage(windows[i]);
  }
  XFree(windows);

  vrwm_log(Info, "All windows are now managed by VRWM");
}

int Core::MainLoop() {
  typedef std::chrono::duration<float> FloatSeconds;

  XSync(display_, false);
  shutdown_ = false; // startup complete! Shutdown now means shutdown
  vrwm_log(Info, "Entering main loop!");
  //Time keeping
  FloatSeconds timeSinceLastLog(0);
  int renderingsSinceLastLog = 0;
  while(!shutdown_) {
    auto frameStart = std::chrono::high_resolution_clock::now();

    try {
      event_handler_->IncrementTime(static_cast<Time>(
          time_since_last_event_ * 1000));

      event_handler_->HandleQueuedEvents();
      // Cache the current pointer raycast. It will not change during the frame.
      cursor_.CacheRaycast();
      // Send MouseMotions to windows. The reason that is here is the following:
      // When a window is moved despite the pointer now moving (e.g. when a
      // plugin gives windows a velocity) the window would not receive correct
      // XMotionEvents.
      cursor_.Hover();
    } catch(std::exception& e) {
      vrwm_clog(Warning, "Unhandled exception: %s", e.what());
    } catch(...) {
      vrwm_log(Warning, "Unhandled exception of unkown type.");
    }

    auto inputEnd = std::chrono::high_resolution_clock::now();
    FloatSeconds inputDuration = inputEnd - frameStart;
    time_since_last_event_ = inputDuration.count();

    //TODO: check if target FPS

    try {
      plugin_manager_->ForEach(&plugin::RenderEventReceiver::PreRender);
      if (oculus_manager_->mode() == OculusManager::UsageMode::NoOculus) {
        openGL_manager_->SetupMatrices();
        openGL_manager_->SortRenderables();
        openGL_manager_->Clear();
        openGL_manager_->Render();
        glXSwapBuffers(display_, openGL_window_->ID());
      } else {
        oculus_manager_->Render();
      }
      plugin_manager_->ForEach(&plugin::RenderEventReceiver::PostRender);
    } catch(std::exception& e) {
      vrwm_clog(Warning, "Unhandled exception: %s", e.what());
    } catch(...) {
      vrwm_log(Warning, "Unhandled exception of unkown type.");
    }

    ++renderingsSinceLastLog;
    auto frameEnd = std::chrono::high_resolution_clock::now();
    FloatSeconds frameDuration = frameEnd - frameStart;

    time_since_last_event_ = frameDuration.count();
    time_since_last_frame_ = frameDuration.count();

    timeSinceLastLog += frameDuration;
    if (timeSinceLastLog > FloatSeconds(0.5f)) {
      vrwm_clogdeb("FPS: %i", renderingsSinceLastLog * 2);
      timeSinceLastLog -= FloatSeconds(0.5f);
      renderingsSinceLastLog = 0;
    }
    time_since_start_ += frameDuration.count();
    //usleep(1000 * 1000);
    //XEvent event;
    //while(
  }

  return 0;
}

Client* Core::GetClient(Window id) {
  if (id == composite_overlay_window_->ID()) {
    return composite_overlay_window_;
  }
  if (id == openGL_window_->ID()) {
    return openGL_window_;
  }

  auto it = managed_windows_.find(id);
  if (it == managed_windows_.end()) {
    return nullptr;
  }
  return it->second;
}

const Client& Core::GetClient(Window id) const {
  if (id == composite_overlay_window_->ID()) {
    return *composite_overlay_window_;
  }
  if (id == openGL_window_->ID()) {
    return *openGL_window_;
  }

  auto it = managed_windows_.find(id);
  if (it == managed_windows_.end()) {
    throw std::invalid_argument(std::string("Window id ") +
        std::to_string(id) + " not managed by vrwm!");
  }
  return *(it->second);
}

void Core::unset_focus_client() {
  if (focus_client_) {
    focus_client_->set_has_focus(false);
  }
  focus_client_ = nullptr;
  Update_NET_ACTIVE_WINDOW(nullptr);
}
void Core::set_focus_client(Client& c) {
  unset_focus_client();
  focus_client_ = &c;
  focus_client_->set_has_focus(true);
  Update_NET_ACTIVE_WINDOW(&c);
  //currently_active_client_->InjectEnterWindow(normalized_pointer_pos);
}

void Core::unset_cursor_client() {
  if (cursor_client_ && cursor_client_->IsInNormalState()) {
    cursor_client_->InjectCrossingEvent(false);
  }
  cursor_client_ = nullptr;
}
void Core::set_cursor_client(Client& c,
    const glm::vec2& normalized_coordinates) {
  if (&c == cursor_client_) {
    return;
  }
  unset_cursor_client();
  cursor_client_ = &c;
  cursor_client_->InjectCrossingEvent(true, normalized_coordinates);
}

Client* Core::Manage(Window id) {
  // Always map composite overlay or opengl window without further processing.
  // They must not render inside vrwm for obvious reasons.
  if (id == openGL_window_->ID() ||
   id == composite_overlay_window_->ID()) {
    XMapWindow(display_, id);
    return (id == openGL_window_->ID()) ? openGL_window_ :
      composite_overlay_window_;
  }

  Client* c;
  auto it = managed_windows_.find(id);
  if (it == managed_windows_.end()) {
    vrwm_clogdeb("Managing window %u", id);
    XWindowAttributes attrs;
    int success = XGetWindowAttributes(display_, id, &attrs);
    if (!success) {
      // TODO: override redirect stuff
      vrwm_log(Warning, "Getting attrs failed! The window was most likely "
        "destroyed before VRWM got a chance to manage it.");
      return nullptr;
    }

    c = new Client(*this, id, attrs, true);
    vrwm_logdeb("Manage start of window " + c->ToString());
    // Be notified of various events.
    XSelectInput(display_, id, ExposureMask | StructureNotifyMask |
      PropertyChangeMask | FocusChangeMask | EnterWindowMask |
      LeaveWindowMask | ColormapChangeMask);
    c->set_dpi(config_.virtual_dpi);
    managed_windows_[id] = c;
    c->Initialize();

    AddTo_NET_CLIENT_LISTs(id);

  } else {
    vrwm_log(Debug, "Window " + std::to_string(id) + " already managed!");
    c = it->second;
  }
  return c;
}

void Core::Unmanage(Window id) {
  auto it = managed_windows_.find(id);
  if (it != managed_windows_.end()) {
    Client* c = it->second;

    if (c == focus_client_) {
      focus_client_ = nullptr;
      Update_NET_ACTIVE_WINDOW(nullptr);
    }
    if (c == cursor_client_) {
      cursor_client_ = nullptr;
    }

    RemoveFrom_NET_CLIENT_LISTs(id);

    // Client will automatically notify all attached Renderables as well as
    // all Plugins of its destruction.
    // This callback may also cause a Renderable to be destroyed which is
    // completely acceptable and is even done in the default case.
    delete c;
    managed_windows_.erase(it);
    XSync(display_, false);
  }
}

void Core::RestartVRWM() {
  DestroyInstance();
  execlp("vrwm", "vrwm", nullptr);
}

void Core::Launch(const std::string& command) {
  if (command.empty()) return;
  pid_t pID = fork();
  if (pID == -1) {
    vrwm_log(Error, "Unable to fork!");
  } else if (pID == 0) {
    if (setsid() == -1) {
      std::cerr << "Error setting session ID" << std::endl;
      exit(1);
    }
    char *argv[2];
    argv[0] = new char[command.size()];
    strcpy(argv[0], command.c_str());
    argv[1] = NULL;
    if (execvp(command.c_str(), argv) == -1) {
      std::cerr << "Error encountered running " << command << std::endl;
    }
    delete argv[0];
    exit(EXIT_SUCCESS);
  }
}

int Core::ErrorHandler(Display* dpy, XErrorEvent* err) {
  char errStr[1024];
  XGetErrorText(dpy, err->error_code, errStr, 1024);
  errStr[1023] = '\0';
  vrwm_clog(Error, "X Error: %s", errStr);
  // During start up and shut down shutdown_ is true. In those cases, we want
  // any X error to fail (as the default error handler does)
  // However, during normal run, X errors do inevitably occur and working around
  // each and every one of them is next to impossible.
  // Therefore, we simply log these errors and go on our merry way.
  if (Core::Instance().shutdown_ && g_default_xlib_error_handler) {
    return g_default_xlib_error_handler(dpy, err);
  }
  return 0;
}
void Core::LogErrorCallback(const std::string& err) {
  (void) err;
#ifdef VRWM_DEBUG
  Core::Instance().openGL_manager().ErrorOccured();
#endif
}

float Core::AspectRatio() const {
  auto w = composite_overlay_window_->x11_position().size.x;
  auto h = composite_overlay_window_->x11_position().size.y;
  return static_cast<float> (w) / static_cast<float> (h);
}

void Core::Update_NET_ACTIVE_WINDOW(Client* c) {
  Window tmp_window = c ? c->ID() : None;
  XChangeProperty(display_, GetRootWindow(), Atoms::_NET_ACTIVE_WINDOW,
    XA_WINDOW, 32, PropModeReplace,
    reinterpret_cast<unsigned char*>(&tmp_window), 1);
}

void Core::AddTo_NET_CLIENT_LISTs(Window window) {
  // Bottom to top stacking order. the window is topmost when first managed.
  net_client_list_stacking_.push_back(window);
  // It's slower than appending, but EWMH states that it should have initial
  // mapping order.
  net_client_list_.insert(net_client_list_.begin(), window);
  Set_NET_CLIENT_LIST();
  Set_NET_CLIENT_LIST_STACKING();
}

void Core::RemoveFrom_NET_CLIENT_LISTs(Window window) {
  for (auto it = net_client_list_stacking_.begin();
      it != net_client_list_stacking_.end();
      ++it) {
    if (*it == window) {
      net_client_list_stacking_.erase(it);
      break;
    }
  }

  for (auto it = net_client_list_.begin(); it != net_client_list_.end(); ++it) {
    if (*it == window) {
      net_client_list_.erase(it);
      break;
    }
  }

  Set_NET_CLIENT_LIST_STACKING();
  Set_NET_CLIENT_LIST();
}

void Core::Update_NET_CLIENT_LIST_STACKING(Window window, Window above) {
  // prevent later iterator invalidation due to insertion
  net_client_list_stacking_.reserve(net_client_list_stacking_.size() + 1);

  auto configured_window_stack_position = net_client_list_stacking_.end();
  auto above_stack_position = net_client_list_stacking_.end();
  for (auto it = net_client_list_stacking_.begin();
      it != net_client_list_stacking_.end();
      ++it) {
    if (*it == window) {
      configured_window_stack_position = it;
    }
    if (*it == above) {
      above_stack_position = it;
    }
  }

  assert(configured_window_stack_position != net_client_list_stacking_.end());
  assert(configured_window_stack_position != above_stack_position);
  if (above_stack_position == configured_window_stack_position - 1) {
    // Already sorted, the configure event apparently did not restack.
    return;
  }

  // Prevent iterator invalidation by doing the backmost operation first
  if (above_stack_position < configured_window_stack_position) {
    net_client_list_stacking_.erase(configured_window_stack_position);
    net_client_list_stacking_.insert(above_stack_position, window);
  } else {
    net_client_list_stacking_.insert(above_stack_position, window);
    net_client_list_stacking_.erase(configured_window_stack_position);
  }

  Set_NET_CLIENT_LIST_STACKING();
}

void Core::Set_NET_CLIENT_LIST_STACKING() {
  XChangeProperty(display_, GetRootWindow(),
    Atoms::_NET_CLIENT_LIST_STACKING, XA_WINDOW, 32, PropModeReplace,
    reinterpret_cast<unsigned char*>(net_client_list_stacking_.data()),
    net_client_list_stacking_.size());
}

void Core::Set_NET_CLIENT_LIST() {
  XChangeProperty(display_, GetRootWindow(),
    Atoms::_NET_CLIENT_LIST, XA_WINDOW, 32, PropModeReplace,
    reinterpret_cast<unsigned char*>(net_client_list_.data()),
    net_client_list_.size());
}

