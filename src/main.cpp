#include "../include/GLglobal.h"
#include "../include/Core.h"
#include "../include/LogManager.h"

#include <cstring>


int main(int argc, char* argv[]) {
  (void) argc;
  (void) argv;
  vrwm::LogManager::Instance(); // Ensure initialization of LogManager.

  int retval = -1;
  try {
    retval = vrwm::Core::InitializeInstance();
  }
  catch(std::exception& e) {
    vrwm_log(vrwm::Error, std::string(e.what()));
    return -1;
  }
  if (retval == 1) {
    return 0;
  }
  else if (retval != 0) {
    return retval;
  }

  vrwm::Core& c = vrwm::Core::Instance();
  try {
    retval = c.MainLoop();
    vrwm_log(vrwm::Info, "main loop exited with code " +
      std::to_string(retval));
  }
  catch(std::exception& e) {
    vrwm_log(vrwm::Error, "Unrecoverable exception: " + std::string(e.what()));
  }
  vrwm::Core::DestroyInstance();
  vrwm::LogManager::DestroySingleton();
  return retval;
}
