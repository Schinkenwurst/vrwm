#include "../include/SceneNode.h"

#include "../include/SceneNodeGrab.h"
#include "../include/Ray.h"

using namespace vrwm;

SceneNodeGrab::SceneNodeGrab(SceneNode& n, const glm::vec3& world_point,
    GrabType type) {
  node_ = &n;
  start_pos_ = n.world_position();
  start_orientation_ = n.world_orientation();
  start_scale_ = n.world_scale();
  start_point_relative_ = n.ToLocalSpace(world_point);
  start_point_world_ = world_point;
  type_ = type;
  forward_ = 0.0f;
}

SceneNodeGrab::SceneNodeGrab() : node_(nullptr) {
}

SceneNodeGrab::SceneNodeGrab(const SceneNodeGrab& copy) :
    node_(copy.node_),
    start_pos_(copy.start_pos_),
    start_orientation_(copy.start_orientation_),
    start_scale_(copy.start_scale_),
    start_point_relative_(copy.start_point_relative_),
    start_point_world_(copy.start_point_world_),
    type_(copy.type_),
    forward_(copy.forward_) {
}

SceneNodeGrab::~SceneNodeGrab() {
}

void SceneNodeGrab::Update(const Ray& r) {
  if (!node_) {
    return;
  }

  switch(type_) {
    case GrabType::Move:
      MoveOnPlane(r);
      MoveAlongForward();
      break;
    case GrabType::ScaleHorizontal:
      ScaleHorizontal(r);
      break;
    case GrabType::ScaleVertical:
      ScaleVertical(r);
      break;
    case GrabType::ScaleDiagonal:
      ScaleDiagonal(r);
      break;
  }

//    // Plugins accept the scale. Tell the client to resize!
//    // This will call the client_resized callback function sometime in the
//    // future.
//    if (client_ && scale_affects_client_) {
//      // Scale affects the client. Therefore we do not scale.
//      scale_ = old_scale;
//      // ... but tell the client that it should rescale.
//      unsigned int width, height;
//      width = scale_ratio_horizontal * g_grab_start_client_size_.x;
//      height = scale_ratio_vertical * g_grab_start_client_size_.y;
//      client_->Resize(width, height);
//    }

}

void SceneNodeGrab::MoveOnPlane(const Ray& end_grab) {
  if (node_->IsBillboard()) {
    float depth = glm::length(
      start_point_world_ - node_->billboard_target()->world_position());
    glm::vec3 offset =
      node_->world_position() - node_->ToWorldSpace(start_point_relative_);
    node_->SetWorldPosition(end_grab.GetPoint(depth) + offset);
  } else {
    std::pair<glm::vec3, float> plane = node_->GetPlane();
    float t;
    if (vrwm::math::RaycastPlane(end_grab, plane.first, plane.second, t)) {
      node_->SetWorldPosition(end_grab.GetPoint(t) - start_point_relative_);
    }
  }
}

void SceneNodeGrab::MoveAlongForward() {
  glm::vec3 move = node_->GetForward() * forward_;
  node_->SetWorldPosition(node_->world_position() + move);
}

void SceneNodeGrab::ScaleHorizontal(const Ray& end_grab) {
  glm::vec3 right = node_->GetRight();
  if (node_->IsBillboard()) {
    // The approach:
    // We first calculate the new center of the Renderable. This is
    // accomplished by measuring the angle difference between the grab start
    // point and the grab end point. This difference is then halfed beause we
    // want the opposing edge to stay at roughly the same point.
    // Due to setting the new position the billboard's orientation changes.
    // This new orientation is then used during the actual scaling: We cast the
    // ray onto the plane and measure the ratio of grab start in local space
    // and the point on plane in local space.
    //
    // Because we are only interested in horizontal scaling and billboards are
    // upright (although a tilted, but we can ignore that tilt if we are only
    // interested in the horizontal direction) we can disregard the y axis on
    // all local vectors.

    ///////////////////////
    // Moving:
    ///////////////////////
    // The depth stays the same for billboards to allow them to move around
    // the user.
    float depth = glm::length(
      start_point_world_ - node_->billboard_target()->world_position());
    glm::vec3 start = start_point_world_;
    glm::vec3 end = end_grab.GetPoint(depth);
    // Only scale horizontal, no change in y.
    start.y = end.y = start_point_world_.y;

    start = glm::normalize(start);
    end = glm::normalize(end);

    glm::quat half_diff = glm::slerp(glm_enh::quat::Identity,
        glm_enh::FromToRotation(start, end), 0.5f);
    node_->SetWorldPosition(half_diff * start_pos_);

    ///////////////////////
    // Scaling:
    ///////////////////////
    // Relative starting point.
    glm::vec3 delta_start = start_point_relative_;
    delta_start.y = 0.0f;

    // raycast onto the plane. the plane has rotated due to the position set.
    float new_depth = 1.0f;
    if (!vrwm::math::RaycastPlane(end_grab, node_->GetPlane(), new_depth)) {
      new_depth = 1.0f;
    }
    // delta_end is the local space vector of that plane intersection point.
    glm::vec3 delta_new = node_->ToLocalSpace(end_grab.GetPoint(new_depth));
    delta_new.y = 0.0f;

    // ...and the ratio of those vectors are how much scaling has occured.
    float scaled_length_new = (glm::length(delta_new) * node_->world_scale().x);
    float scaled_length_start = (glm::length(delta_start) * start_scale_.x);
    float ratio = scaled_length_new / scaled_length_start;
    glm::vec3 scale = start_scale_;
    scale.x *= ratio;
    node_->SetWorldScale(scale);
  } else {
    std::pair<glm::vec3, float> plane = node_->GetPlane();
    float t;
    if (vrwm::math::RaycastPlane(end_grab, plane.first, plane.second, t)) {
      glm::vec3 projected_start =
        glm::dot(start_point_relative_, right) * right;
      glm::vec3 projected_end =
        glm::dot(end_grab.GetPoint(t) - start_pos_, right) * right;
      node_->SetWorldPosition(node_->world_position() +
        (projected_end - projected_start) * 0.5f);
      float ratio = glm::length(projected_end) / glm::length(projected_start);
      node_->SetWorldScale(glm::vec3(
        start_scale_.x * ratio, start_scale_.y, start_scale_.z));
    }
  }
}

void SceneNodeGrab::ScaleVertical(const Ray& end_grab) {
  if (node_->IsBillboard()) {
    // The approach is the same as in ScaleHorizontal(), i.e. move first then
    // scale by taking into consideration the new orientation.
    float depth = glm::length(
      start_point_world_ - node_->billboard_target()->world_position());

    glm::vec3 start = start_point_world_;
    glm::vec3 end = end_grab.GetPoint(depth);
    end.x = start.x;
    end.z = start.z;
    start = glm::normalize(start);
    end = glm::normalize(end);

    glm::quat half_diff = glm::slerp(glm_enh::quat::Identity,
        glm_enh::FromToRotation(start, end), 0.5f);
    node_->SetWorldPosition(half_diff * start_pos_);

    float new_depth = 1.0f;
    if (!vrwm::math::RaycastPlane(end_grab, node_->GetPlane(), new_depth)) {
      new_depth = 1.0f;
    }

    glm::vec3 delta_new = node_->ToLocalSpace(end_grab.GetPoint(new_depth));
    float scaled_length_new = (delta_new.y * node_->world_scale().y);
    float scaled_length_start = (start_point_relative_.y * start_scale_.y);
    float ratio = scaled_length_new / scaled_length_start;
    glm::vec3 scale = start_scale_;
    scale.y *= ratio;
    node_->SetWorldScale(scale);
  } else {
    throw std::runtime_error("Not implemneted yet :(");
  }
}

void SceneNodeGrab::ScaleDiagonal(const Ray& end_grab) {
  //glm::vec3 right = GetRight();
  if (node_->IsBillboard()) {
    // Exactly what it says in the comment in the header: Does both horizontal
    // and vertical scale. See the other both for explanation.
    float depth = glm::length(
      start_point_world_ - node_->billboard_target()->world_position());

    glm::vec3 start = glm::normalize(start_point_world_);
    glm::vec3 end = glm::normalize(end_grab.GetPoint(depth));

    glm::quat half_diff = glm::slerp(glm_enh::quat::Identity,
        glm_enh::FromToRotation(start, end), 0.5f);
    node_->SetWorldPosition(half_diff * start_pos_);

    float new_depth = 1.0f;
    if (!vrwm::math::RaycastPlane(end_grab, node_->GetPlane(), new_depth)) {
      new_depth = 1.0f;
    }

    glm::vec3 delta_new = node_->ToLocalSpace(end_grab.GetPoint(new_depth));
    // Horizontal ratio
    glm::vec3 delta_start_hor = start_point_relative_;
    glm::vec3 delta_new_hor = delta_new;
    delta_start_hor.y = delta_new_hor.y = 0.0f;
    float scaled_length_hor_new = (glm::length(delta_new_hor) * node_->world_scale().x);
    float scaled_length_hor_start = (glm::length(delta_start_hor) * start_scale_.x);
    glm::vec3 scale = start_scale_;
    scale.x *= scaled_length_hor_new / scaled_length_hor_start;
    // Vertical ratio
    float scaled_length_ver_new = (delta_new.y * node_->world_scale().y);
    float scaled_length_ver_start = (start_point_relative_.y * start_scale_.y);
    scale.y *= scaled_length_ver_new / scaled_length_ver_start;
    node_->SetWorldScale(scale);
  } else {
    throw std::runtime_error("Not implemneted yet :(");
  }
}

