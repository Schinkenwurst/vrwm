#include "../include/GLtexture.h"

#include <cassert>
#include <vector>
#include <SOIL/SOIL.h>

#include "../include/Assets.h"
#include "../include/GLglobal.h"
#include "../include/LogManager.h"
#include "../include/GLtextureSwapper.h"

using namespace vrwm;

GLtexture::GLtexture() : texture_unit_(-1) {
  glGenTextures(1, &handle_);
}
GLtexture::GLtexture(GLtexture&& move) : handle_(move.handle_),
    texture_unit_(move.texture_unit_) {
  move.handle_ = 0;
  move.texture_unit_ = -1;
}

GLtexture::~GLtexture() {
  GLtextureSwapper::Instance().Unbind(*this);
  glDeleteTextures(1, &handle_);
}

GLtexturePtr GLtexture::Load(const std::string& filename) {
  auto ptr = Assets::texture(filename);
  if (!ptr) {
    GLtexture* created = new GLtexture();
    created->Use();
    created->LoadImage(filename);
    ptr = Assets::add(filename, created);
  }
  return ptr;
}

GLtexturePtr GLtexture::LoadHeightmap(const std::string& filename) {
  auto ptr = Assets::texture(filename);
  if (!ptr) {
    GLtexture* created = new GLtexture();
    created->Use();
    created->LoadHeightmapImage(filename);
    ptr = Assets::add(filename, created);
  }
  return ptr;
}

void GLtexture::LoadImage(const std::string& filename) {
  handle_ = SOIL_load_OGL_texture(filename.c_str(), SOIL_LOAD_AUTO,
      handle_, SOIL_FLAG_MIPMAPS | SOIL_FLAG_POWER_OF_TWO);
  CHECK_GL_ERRORS();
  if (!handle_) {
    vrwm_log(Error, std::string("Error loading image texture from file: ") +
      SOIL_last_result());
    throw std::runtime_error("Error loading image texture from file.");
  }
}

void GLtexture::LoadHeightmapImage(const std::string& filename) {
  int w, h, channels;
  unsigned char* data = SOIL_load_image(filename.c_str(), &w, &h, &channels,
    0);
  if (!data) {
    vrwm_log(Error, std::string("Error loading image texture from file: ") +
      SOIL_last_result());
    throw std::runtime_error("Error loading image texture from file.");
  }
  assert(w > 0);
  assert(h > 0);
  assert(channels > 0);

  // This function is mainly for assimp. Assimp's material importer (at least
  // for .obj) has the strange behaviour that it also imports normal maps as
  // height map. Therefore, we need to check whether the loaded height map is
  // actually a height map (grayscale or all pixel-values are equal) or it is
  // already a normal map.
  // In the event that the values of the first pixel are equal but the image is
  // actually a normal map, we are screwed (but this should not happen anyway).

  bool is_heightmap = std::all_of(data + 1, data + channels,
    [&](unsigned char& sp) {
      return data[0] == sp;
    });

  if (is_heightmap) {
    vrwm_logdeb("Texture is a height map, converting...");
    auto normal_map = ConvertToNormalMap(data, w, h, channels);
    handle_ = SOIL_create_OGL_texture(normal_map.data(), w, h, 3, handle_,
      SOIL_FLAG_MIPMAPS | SOIL_FLAG_POWER_OF_TWO);
  } else {
    vrwm_logdeb("Height map is already a normal map. No need to convert.");
    handle_ = SOIL_create_OGL_texture(data, w, h, channels, handle_,
      SOIL_FLAG_MIPMAPS | SOIL_FLAG_POWER_OF_TWO);
  }
  CHECK_GL_ERRORS();

  SOIL_free_image_data(data);
}


uint8_t GetHeight(const unsigned char* heightmap, int channels, int row,
    int col) {
  return heightmap[(row * col + col) * channels];
}

std::vector<uint8_t> GLtexture::ConvertToNormalMap(
    const unsigned char* heightmap, int w, int h, int channels) {
  const float normal_strength = 0.2f;
  // output format: RGB with each channel 8 bit uint (0, 255)
  std::vector<uint8_t> normal_map(w * h * 3, 0);
  for (int row = 0; row < h; ++row) {
    for (int col = 0; col < w; ++col) {
      // The code for converting a height map to a normal map uses
      // the sobel operator.
      // t = top, m = middle, b = bottom, l = left, r = right
      // Even though the height values are uint8, we need higher numbers
      // later so we'll use float.
      float tl, tm, tr, ml, mr, bl, bm, br;
      // "Average" value on border pixels
      int prev_row = std::max(row - 1, 0);
      int prev_col = std::max(col - 1, 0);
      int next_row = std::min(row + 1, h - 1);
      int next_col = std::min(col + 1, w - 1);
      tl = GetHeight(heightmap, channels, prev_row, prev_col);
      tm = GetHeight(heightmap, channels, prev_row, col);
      tr = GetHeight(heightmap, channels, prev_row, next_col);
      ml = GetHeight(heightmap, channels, row,      prev_col);
      mr = GetHeight(heightmap, channels, row,      next_col);
      bl = GetHeight(heightmap, channels, next_row, prev_col);
      bm = GetHeight(heightmap, channels, next_row, col);
      br = GetHeight(heightmap, channels, next_row, next_col);

      float gx = tr + 2*mr + br - tl - 2*ml - bl;
      float gy = bl + 2*bm + br - tl - 2*tm - tl;
      glm::vec3 normal = glm::normalize(glm::vec3(gx, normal_strength, gy));
      uint8_t max = std::numeric_limits<uint8_t>().max();
      normal_map[(row * col + col) * 3 + 0] = normal.x * max;
      normal_map[(row * col + col) * 3 + 1] = normal.y * max;
      normal_map[(row * col + col) * 3 + 2] = normal.z * max;
    }
  }
  return normal_map;
}

void GLtexture::SetParameter(GLenum pname, GLfloat param) {
  Use();
  CALL_GL(glTexParameterf(GL_TEXTURE_2D, pname, param));
}
void GLtexture::SetParameter(GLenum pname, GLint param) {
  Use();
  CALL_GL(glTexParameteri(GL_TEXTURE_2D, pname, param));
}

GLint GLtexture::Use() {
  GLtextureSwapper::Instance().Bind(*this);
  return texture_unit_;
}

void GLtexture::SetData(GLint level, GLsizei w, GLsizei h, void* data,
    GLenum type, GLenum internal_format, GLenum format) {
  Use();
  CALL_GL(glTexImage2D(GL_TEXTURE_2D, level, internal_format, w, h, 0,
      format, type, data));
}

void GLtexture::GenerateMipmap() {
  Use();
  glGenerateMipmap(GL_TEXTURE_2D);
}

