#include "../include/SceneNode.h"

#include "../include/Core.h"
#include "../include/OpenGLManager.h"
#include "../include/PluginManager.h"
#include "../include/plugin/EventReceiver.h"

using namespace vrwm;

SceneNodeObserver::SceneNodeObserver() : observing_(nullptr) {
}
SceneNodeObserver::SceneNodeObserver(const SceneNodeObserver& copy) :
    observing_(nullptr) {
  *this = copy;
}
SceneNodeObserver::SceneNodeObserver(SceneNodeObserver&& move) :
    observing_(nullptr) {
  *this = std::move(move);
}

SceneNodeObserver& SceneNodeObserver::operator=(const SceneNodeObserver& copy) {
  if (this == &copy) {
    return *this;
  }
  Observe(copy.observing_);
  return *this;
}
SceneNodeObserver& SceneNodeObserver::operator=(SceneNodeObserver&& move) {
  if (this == &move) {
    return *this;
  }
  Observe(move.observing_);
  move.Observe(nullptr);
  return *this;
}

SceneNodeObserver::~SceneNodeObserver() {
  if (observing_) {
    observing_->RemoveObserver(*this);
  }
}

void SceneNodeObserver::Observe(SceneNode* n) {
  if (observing_) {
    observing_->RemoveObserver(*this);
  }
  observing_ = n;
  if (observing_) {
    observing_->AddObserver(*this);
  }
}
void SceneNodeObserver::PositionChanged(const glm::vec3& n,
    const glm::vec3& o) {
  (void) n; (void) o;
}
void SceneNodeObserver::OrientationChanged(const glm::quat& n,
    const glm::quat& o) {
  (void) n; (void) o;
}
void SceneNodeObserver::ScaleChanged(const glm::vec3& n,
    const glm::vec3& o) {
  (void) n; (void) o;
}
bool SceneNodeObserver::ScaleChangeRequest(const glm::vec3& n,
    const glm::vec3& o) {
  (void) n; (void) o;
  return true;
}
void SceneNodeObserver::SceneNodeDestroyed() {
}
void SceneNodeObserver::ChildAdded(SceneNode& child) {
  (void) child;
}
void SceneNodeObserver::ChildRemoved(SceneNode& child) {
  (void) child;
}

SceneNode::SceneNode(Core& core, const std::string& name, SceneNode* parent) :
    core_(core),
    local_position_(0.0f, 0.0f, 0.0f),
    local_orientation_(glm_enh::quat::Identity),
    local_scale_(1.0f, 1.0f, 1.0f),
    world_orientation_(glm_enh::quat::Identity),
    world_scale_(1.0f, 1.0f, 1.0f),
    inherit_position_(true),
    inherit_orientation_(true),
    inherit_scale_(true),
    model_matrix_(1.0f),
    inverse_model_matrix_(1.0f),
    billboard_target_(nullptr),
    is_visible_(true),
    is_visible_in_hierarchy_(false),
    name_(name),
    parent_(parent) {
  if (parent_) {
    model_matrix_ = parent_->model_matrix_;
    inverse_model_matrix_ = parent_->inverse_model_matrix_;
    world_orientation_ = parent->world_orientation_;
    world_scale_ = parent->world_scale_;
    parent_->children_.insert(this);
    is_visible_in_hierarchy_ = parent->is_visible_in_hierarchy_;
  }
}

SceneNode::SceneNode(const SceneNode& copy) : core_(copy.core_) {
  parent_ = nullptr;
  *this = copy;
}

SceneNode& SceneNode::operator=(const SceneNode& copy) {
  assert(&core_ == &copy.core_);
  if (this == &copy) {
    return *this;
  }

  for (auto renderable : renderables_) {
    delete renderable;
  }
  for (auto child : children_) {
    delete child;
  }

  if (parent_) {
    parent_->children_.erase(this);
  }

  local_position_ = copy.local_position_;
  local_orientation_ = copy.local_orientation_;
  local_scale_ = copy.local_scale_;
  world_orientation_ = copy.world_orientation_;
  world_scale_ = copy.world_scale_;
  inherit_position_ = copy.inherit_position_;
  inherit_orientation_ = copy.inherit_orientation_;
  inherit_scale_ = copy.inherit_scale_;
  model_matrix_ = copy.model_matrix_;
  inverse_model_matrix_ = copy.inverse_model_matrix_;
  billboard_target_ = copy.billboard_target_;
  is_visible_ = copy.is_visible_;
  is_visible_in_hierarchy_ = copy.is_visible_in_hierarchy_;
  name_ = copy.name_;
  parent_ = copy.parent_;

  if(parent_) {
    parent_->children_.insert(this);
  }

  for (auto child : copy.children_) {
    assert(child != nullptr);
    CopyAndAttach(child);
  }
  for (auto renderable : copy.renderables_) {
    assert(renderable != nullptr);
    auto copied_renderable = new Renderable(*renderable);
    AttachRenderable(*copied_renderable);
    copied_renderable->ForceSetSceneNode(*this);
  }

  return *this;
}

SceneNode* SceneNode::CreateChild(const std::string& name) {
  return new SceneNode(core_, name, this);
}

SceneNode* SceneNode::CopyAndAttach(const SceneNode* to_clone) {
  if (to_clone) {
    SceneNode* copy = new SceneNode(*to_clone);
    copy->Reparent(this);
    return copy;
  }
  return nullptr;
}

SceneNode::~SceneNode() {
  for (auto renderable : renderables_) {
    delete renderable;
  }
  for (auto child : children_) {
    delete child;
  }
  // TODO: This is horrible!
  auto& grab = core_.cursor().scene_node_grab();
  if (grab.node() == this) {
    grab.Abort();
  }
  for (auto observer : observers_) {
    observer->observing_ = nullptr;
  }
}

void SceneNode::Destroy() {
  RecursiveRemoveAllFromDraw();
  for (auto it = observers_.begin(); it != observers_.end();) {
    (*it++)->SceneNodeDestroyed();
  }
  if (parent_) {
    parent_->children_.erase(this);
  }
  delete this;
}

void SceneNode::SetLocalPosition(const glm::vec3& p) {
  glm::vec3 old_pos = world_position();
  local_position_ = p;
  //if (!core_.plugin_manager().ForEach(
  //      &plugin::RenderableMovedReceiver::RenderableMoved, *this, old_pos)) {
  //  // plugin denied move - reset.
  //  local_position_ = old_pos;
  //  return;
  //}
  UpdateMatrices();
  for (auto it = observers_.begin(); it != observers_.end();) {
    (*it++)->PositionChanged(world_position(), old_pos);
  }
  CalculateBillboardOrientation();
}

void SceneNode::SetWorldPosition(const glm::vec3& p) {
  SetLocalPosition(parent_ && inherit_position_ ?
    glm::vec3(parent_->inverse_model_matrix_ * glm::vec4(p, 1.0f)) :
    p);
}

void SceneNode::SetLocalOrientation(const glm::quat& o) {
  //glm::quat old_orientation = local_orientation_;
  glm::quat old_orientation = world_orientation_;
  local_orientation_ = o;
  if (inherit_orientation_ && parent_) {
    world_orientation_ = parent_->world_orientation_ * o;
  } else {
    world_orientation_ = o;
  }
  //if (!core_.plugin_manager().ForEach(
  //      &plugin::RenderableMovedReceiver::RenderableRotated, *this,
  //      old_orientation)) {
  //  // plugin denied rotation - reset.
  //  local_orientation_ = old_orientation;
  //}
  UpdateMatrices();
  for (auto it = observers_.begin(); it != observers_.end();) {
    (*it++)->OrientationChanged(world_orientation_, old_orientation);
  }
  for (auto child : children_) {
    child->SetLocalPosition(child->local_position_);
    child->SetLocalOrientation(child->local_orientation_);
  }
}

void SceneNode::SetWorldOrientation(const glm::quat& o) {
  SetLocalOrientation(parent_ && inherit_orientation_ ?
      glm_enh::FromToRotation(parent_->world_orientation_, o) :
      o);
}

void SceneNode::SetLocalScale(const glm::vec3& s, bool force) {
  glm::vec3 old_scale = world_scale_;
  glm::vec3 future_world_scale;
  if (parent_ && inherit_scale_) {
    future_world_scale = parent_->world_scale_ * s;
  } else {
    future_world_scale = s;
  }

  for (auto renderable : renderables_) {
    if (!renderable->on_scene_node_resize(future_world_scale) && !force) {
      return;
    }
  }
  for (auto it = observers_.begin(); it != observers_.end();) {
    if (!(*it++)->ScaleChangeRequest(future_world_scale, old_scale) &&
        !force) {
      return;
    }
  }

  //glm::vec3 old_scale = local_scale_;
  local_scale_ = s;
  world_scale_ = future_world_scale;
  //if (!core_.plugin_manager().ForEach(
  //      &plugin::RenderableMovedReceiver::RenderableScaled, *this,
  //      old_scale)) {
  //  // plugin denied scale - reset.
  //  local_scale_ = old_scale;
  //  return;
  //}
  UpdateMatrices();
  for (auto it = observers_.begin(); it != observers_.end();) {
    (*it++)->ScaleChanged(world_scale_, old_scale);
  }
  for (auto child : children_) {
    child->SetLocalPosition(child->local_position_);
    child->SetLocalScale(child->local_scale_, force);
  }
}

void SceneNode::SetWorldScale(const glm::vec3& s, bool force) {
  SetLocalScale(parent_ && inherit_scale_ ?  s / parent_->world_scale_ : s,
    force);
}

void SceneNode::UpdateMatrices() {
  // Model matrix

  glm::mat4 pos(1.0f);
  if (parent_ && inherit_position_) {
    pos[3] = parent_->model_matrix_ * glm::vec4(local_position_, 1.0f);
  } else {
    pos[3] = glm::vec4(local_position_, 1.0f);
  }
  glm::mat4 scale(1.0f);
  scale[0][0] = world_scale_.x;
  scale[1][1] = world_scale_.y;
  scale[2][2] = world_scale_.z;
  glm::mat4 rot = glm::mat4_cast(world_orientation_);

  model_matrix_ = pos * rot * scale;

  // Inverse model matrix

  glm::mat4 inv_pos(1.0f);
  if (parent_ && inherit_position_) {
    inv_pos[3] = parent_->inverse_model_matrix_ * glm::vec4(-local_position_, 1.0f);
  } else {
    inv_pos[3] = glm::vec4(-local_position_, 1.0f);
  }
  glm::mat4 inv_rot(glm::transpose(glm::mat3_cast(world_orientation_)));
  inv_rot[3][3] = 1.0f;
  glm::mat4 inv_scale(1.0f);
  inv_scale[0][0] = 1.0f / world_scale_.x;
  inv_scale[1][1] = 1.0f / world_scale_.y;
  inv_scale[2][2] = 1.0f / world_scale_.z;

  inverse_model_matrix_ = inv_scale * inv_rot * inv_pos;
}

void SceneNode::LookAt(const glm::vec3& dir, const glm::vec3& desiredUp) {
  SetWorldOrientation(glm_enh::LookAt(dir, desiredUp));
}
void SceneNode::CalculateBillboardOrientation() {
  if (!billboard_target_) {
    return;
  }

  glm::vec3 dir = billboard_target_->world_position() - world_position();
  // Prevent division by 0
  if (abs(glm_enh::lengthSq(dir)) < 1e-6f) {
    return;
  }
  LookAt(glm::normalize(dir));
}

void SceneNode::SetInheritPosition(bool inherit) {
  inherit_position_ = inherit;
  SetLocalPosition(local_position_);
}
void SceneNode::SetInheritOrientation(bool inherit) {
  inherit_orientation_ = inherit;
  SetLocalOrientation(local_orientation_);
}
void SceneNode::SetInheritScale(bool inherit) {
  inherit_scale_ = inherit;
  SetLocalScale(local_scale_);
}

std::pair<glm::vec3, float> SceneNode::GetPlane() const {
  glm::vec3 normal = GetForward();
  float d = glm::dot(world_position(), normal);
  return std::make_pair(normal, d);
}

void SceneNode::Rotate(const glm::vec3& rads, bool local_space) {
  if (rads.x != 0)
    local_orientation_ = glm_enh::rotate(local_orientation_, rads.x,
        local_space ? GetRight() : glm::vec3(1.0f, 0.0f, 0.0f));
  if (rads.y != 0)
    local_orientation_ = glm_enh::rotate(local_orientation_, rads.y,
        local_space ? GetUp() : glm::vec3(0.0f, 1.0f, 0.0f));
  if (rads.z != 0)
    local_orientation_ = glm_enh::rotate(local_orientation_, rads.z,
        local_space ? -GetForward() : glm::vec3(0.0f, 0.0f, 1.0f));
  UpdateMatrices();
}
void SceneNode::Rotate(float rads, const glm::vec3& axis) {
  local_orientation_ = glm_enh::rotate(local_orientation_, rads, axis);
  UpdateMatrices();
}
void SceneNode::Rotate(const glm::quat& q) {
  local_orientation_ *= q;
  UpdateMatrices();
}

void SceneNode::SetVisible(bool v) {
  if (is_visible_ == v && parent_) {
    return;
  }

  is_visible_ = v;
  if (!parent_ || (parent_ && parent_->is_visible_in_hierarchy_)) {
    RecursiveSetVisibleInHierarchy(is_visible_);
    if (is_visible_) {
      RecursiveAddAllToDraw();
    } else {
      RecursiveRemoveAllFromDraw();
    }
  }
}

SceneNode* SceneNode::FindByName(const std::string& name) {
  SceneNode* to_return = nullptr;
  for (auto child : children_) {
    if (child->name_ == name) {
      to_return = child;
    } else {
      to_return = child->FindByName(name);
    }
    if (to_return) {
      break;
    }
  }
  return to_return;
}
const SceneNode* SceneNode::FindByName(const std::string& name) const {
  SceneNode* to_return = nullptr;
  for (auto child : children_) {
    if (child->name_ == name) {
      to_return = child;
    } else {
      to_return = child->FindByName(name);
    }
    if (to_return) {
      break;
    }
  }
  return to_return;
}
void SceneNode::FindAllByName(const std::string& name,
    std::set<SceneNode*>& out) {
  for (auto child : children_) {
    if (child->name_ == name) {
      out.insert(child);
    }
    child->FindAllByName(name, out);
  }
}
void SceneNode::FindAllByName(const std::string& name,
    std::set<const SceneNode*>& out) const {
  for (auto child : children_) {
    if (child->name_ == name) {
      out.insert(child);
    }
    child->FindAllByName(name, out);
  }
}

void SceneNode::Reparent(SceneNode* new_parent) {
  if (this == core_.openGL_manager().root_scene_node()) {
    throw std::runtime_error("Must not reparent root scene node.");
  }
  if (parent_ == new_parent) {
    return;
  }

  if (parent_) {
    parent_->children_.erase(this);
  }

  if (!new_parent) {
    if (is_visible_in_hierarchy_) {
      RecursiveRemoveAllFromDraw();
    }
    is_visible_in_hierarchy_ = false;
  } else if (is_visible_in_hierarchy_ &&
      !new_parent->is_visible_in_hierarchy_) {
    RecursiveRemoveAllFromDraw();
  } else if (!is_visible_in_hierarchy_ &&
      new_parent->is_visible_in_hierarchy_ &&
      is_visible_) {
    RecursiveAddAllToDraw();
  }

  parent_ = new_parent;
  if (parent_) {
    parent_->children_.insert(this);
  }
  RecursiveSetVisibleInHierarchy(
    new_parent->is_visible_in_hierarchy_ && is_visible_);
}

void SceneNode::AttachRenderable(Renderable& r) {
  auto pair = renderables_.insert(&r);
  if (!pair.second) {
    return;
  }
  if (is_visible_in_hierarchy_) {
    AddToDraw(r);
  }
}

void SceneNode::RelocateRenderable(Renderable& r, SceneNode& target) {
  size_t count = renderables_.erase(&r);
  if (!count) {
    return;
  }

  target.renderables_.insert(&r);
  if (is_visible_in_hierarchy_ && !target.is_visible_in_hierarchy_) {
    RemoveFromDraw(r);
  } else if (!is_visible_in_hierarchy_ && target.is_visible_in_hierarchy_) {
    AddToDraw(r);
  }
}

void SceneNode::DetachRenderable(Renderable& r) {
  size_t count = renderables_.erase(&r);
  if (!count) {
    return;
  }

  if (is_visible_in_hierarchy_) {
    RemoveFromDraw(r);
  }
}

void SceneNode::RecursiveSetVisibleInHierarchy(bool v) {
  is_visible_in_hierarchy_ = v;
  for (auto child : children_) {
    child->RecursiveSetVisibleInHierarchy(v);
  }
}
void SceneNode::RecursiveRemoveAllFromDraw() {
  std::set<Renderable*> r;
  RecursiveGatherRenderables(r);
  core_.openGL_manager().RemoveFromRendering(r);
}

void SceneNode::RemoveAllFromDraw() {
  core_.openGL_manager().RemoveFromRendering(renderables_);
}

void SceneNode::RemoveFromDraw(Renderable& r) {
  core_.openGL_manager().RemoveFromRendering(r);
}

void SceneNode::RecursiveAddAllToDraw() {
  std::set<Renderable*> r;
  RecursiveGatherRenderables(r);
  core_.openGL_manager().AddToRendering(r);
}

void SceneNode::AddAllToDraw() {
  core_.openGL_manager().AddToRendering(renderables_);
}

void SceneNode::AddToDraw(Renderable& r) {
  core_.openGL_manager().AddToRendering(r);
}

void SceneNode::RecursiveGatherRenderables(std::set<Renderable*>& out) {
  out.insert(renderables_.begin(), renderables_.end());
  for (auto child : children_) {
    child->RecursiveGatherRenderables(out);
  }
}

void SceneNode::SetBillboardTarget(SceneNode* target) {
  // Sanity checks - target is self or target is one of children? => throw!
  if (this == target) {
    throw std::runtime_error("Cannot set a SceneNode's billboard target to "
      "itself.");
  }
  RecursiveBillboardTargetCheck(target);

  billboard_target_ = target;
  Observe(billboard_target_);
  if (billboard_target_) {
    CalculateBillboardOrientation();
  }
}

void SceneNode::RecursiveBillboardTargetCheck(SceneNode* target) {
  for (auto child : children_) {
    if (child == target) {
      throw std::runtime_error("Cannot set a SceneNode's billboard target to "
        "one of its children (circular dependency)");
    }
    child->RecursiveBillboardTargetCheck(target);
  }
}

void SceneNode::AddObserver(SceneNodeObserver& o) {
  observers_.insert(&o);
}
void SceneNode::RemoveObserver(SceneNodeObserver& o) {
  observers_.erase(&o);
}

void SceneNode::PositionChanged(const glm::vec3& n, const glm::vec3& o) {
  (void) n; (void) o;
  CalculateBillboardOrientation();
}
void SceneNode::OrientationChanged(const glm::quat& n, const glm::quat& o) {
  (void) n; (void) o;
  CalculateBillboardOrientation();
}
void SceneNode::ScaleChanged(const glm::vec3& n, const glm::vec3& o) {
  (void) n; (void) o;
  CalculateBillboardOrientation();
}
void SceneNode::SceneNodeDestroyed() {
  billboard_target_ = nullptr;
}



