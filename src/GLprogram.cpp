#include "../include/GLprogram.h"

#include <stdexcept>
#include <assert.h>
#include <utility>

#include "../include/LogManager.h"
#include "../include/GLglobal.h"

using namespace vrwm;

GLprogram::GLprogram(ShaderList&& shaders) {
  // Create program
  if (!(program_ = glCreateProgram())) {
    throw std::runtime_error("glCreateProgram failed");
  }

  shaders_ = std::move(shaders);
  Link();

#ifdef VRWM_DEBUG
  // Immediatly print compile and link log when in debug. This is not done in
  // release because it stalls the pipeline. Printing is in release is done
  // when the program is first used.
  PrintCompileLog();
#endif
}

void GLprogram::Link() {
  printed_linklog_ = false;
  for(auto& shader : shaders_) {
    glAttachShader(program_, shader.handle_);
  }
  glLinkProgram(program_);
  for(auto& shader : shaders_) {
    glDetachShader(program_, shader.handle_);
  }
}

void GLprogram::Recompile() {
  for(auto& shader : shaders_) {
    shader.Recompile();
  }
  Link();
  for (auto block_binding : active_block_bindings_) {
    Bind(block_binding.first, block_binding.second);
  }
}

void GLprogram::PrintCompileLog() {
  printed_linklog_ = true;
  vrwm_log(Info, "==== Shader Program Link Log ====");

  for(auto& shader : shaders_) {
    shader.PrintCompileLog();
  }

  GLint linkSuccess;
  glGetProgramiv(program_, GL_LINK_STATUS, &linkSuccess);
  char logMessage[2048];
  GLsizei messageLength;
  glGetProgramInfoLog(program_, 2048, &messageLength, logMessage);
  if (linkSuccess == GL_FALSE) {
    vrwm_clog(Error, "Shader program linking failed!\n%s\n", logMessage);
  } else {
    vrwm_clog(Info, "Program successfully linked! %s", logMessage);
  }

  vrwm_log(Info, "=================================");
}

GLint GLprogram::Attrib(const char* name) const {
  assert(name != nullptr);

  GLint attrib = glGetAttribLocation(program_, name);
  if (attrib == -1) {
    vrwm_clog(Warning, "Shader attribute '%s' not found. It might have been "
        "discarded due to optimization or your code is old.", name);
  }

  return attrib;
}

GLint GLprogram::Uniform(const char* name, bool suppressWarning) const {
  assert(name != nullptr);

  GLint attrib = glGetUniformLocation(program_, name);
  if (attrib == -1 && !suppressWarning) {
    vrwm_clog(Warning, "Shader uniform '%s' not found. It might have been "
        "dicarded due to optimization or your code is old.", name);
  }

  return attrib;
}

GLuint GLprogram::UniformBlockIndex(const char* name) {
  GLuint block = CALL_GL(glGetUniformBlockIndex(program_, name));
  if (block == GL_INVALID_INDEX) {
    vrwm_clog(Warning, "Uniform block '%s' not active.", name);
  }
  return block;
}

void GLprogram::Bind(GLuint block_index, GLuint binding_point) {
  if (block_index != GL_INVALID_INDEX) {
    active_block_bindings_[block_index] = binding_point;
    CALL_GL(glUniformBlockBinding(program_, block_index, binding_point));
  }
}

GLprogram::~GLprogram(void) {
  if (program_ != 0) {
    glDeleteProgram(program_);
  }
}

