#include "../include/Client.h"

#include <algorithm>

#include <X11/extensions/Xcomposite.h>
#include <X11/extensions/Xdamage.h>
#include <X11/extensions/XTest.h>
#include <glm/gtc/matrix_transform.hpp>

#include "../include/Core.h"
#include "../include/OpenGLManager.h"
#include "../include/LogManager.h"
#include "../include/Ray.h"
#include "../include/PluginManager.h"
#include "../include/XInputDeviceManager.h"

using namespace vrwm;

Client::Client(Core& core, Window id, const XWindowAttributes& attr, bool
      rendering_enabled) :
    core_(core),
    is_override_redirect_(attr.override_redirect),
    id_(id),
    x11_position_(attr.x, attr.y, attr.width, attr.height),
    x11_position_before_fullscreen_(attr.x, attr.y, attr.width, attr.height),
    last_normalized_pointer_position_(0.0f, 0.0f),
    pixel_per_centimeter_(72.0f),
    has_focus_(false),
    rendering_enabled_(rendering_enabled),
    pixmap_(0),
    glxpixmap_(0),
    pending_resize_(glm::uvec2(0, 0)),
    ping_start_time_(0),
    texture_(rendering_enabled ? new GLtexture() : nullptr),
    framebuffer_config_(nullptr),
    icon_(rendering_enabled_ ? new GLtexture() : nullptr) {

  // Move the window to 0, 0 if it is a top-level window.
  //
  // Child windows will not be moved because they are often drop-down menus,
  // combo boxes, etc. A renderable should check whether a window is a child
  // window and display it relative to its appropriate top-level window (which
  // is available in the top_level_ attribute of the Client at the offset
  // specified as X11_position_. This position would be the windows position
  // relative to its top-level window in traditional window managers.
  if (!is_override_redirect_) {
    XMoveWindow(core_.display(), id_, 0, 0);
    x11_position_.pos.x = x11_position_.pos.y = 0;
  }
  //XSetWindowAttributes attributes;
  //attributes.save_under = true;
  //XChangeWindowAttributes(core_.display(), id_, CWSaveUnder, &attributes);
}

Client::~Client() {
  FreePixmap();
  // Notify all Renderables that observe this client of destruction.
  // Because they might detach themselves from us, we use postincrement to
  // ensure the iterator stays defined.
  for (auto it = begin_renderables(); it != end_renderables();) {
    (*(it++))->on_client_close(*this);
  }
  core_.plugin_manager().ForEach(&plugin::ClientEventReceiver::ClientClosed,
      *this);
}

void Client::Activate() {
  std::pair<bool, WindowProperties::WmState> exists_state = GetState();
  auto& state = exists_state.second;
  if (!exists_state.first || state.state == WindowState::WS_WithdrawnState) {
    ChangeDesktop(0); // Only one desktop currently.
    // This is an initial map.
    state.icon_window = nullptr;
    std::shared_ptr<XWMHints> hints = GetWindowManagerHints();
    if ((hints->flags & IconWindowHint) && hints->icon_window != None) {
      Client* icon_window = core_.GetClient(hints->icon_window);
      if (!icon_window) {
        vrwm_log(Warning, "Icon window not managed!");
        icon_window = core_.Manage(hints->icon_window);
      }
      state.icon_window = icon_window;
    }
    // I'll not use the IconPixmap in the wm hints as there is a more modern
    // _NET_WM_ICON specified in the EWMH. If clients don't supply a
    // _NET_WM_ICON, no icon will be displayed.
    if ((hints->flags & StateHint) && hints->initial_state == IconicState) {
      state.state = WindowState::WS_IconicState;
    } else {
      state.state = WindowState::WS_NormalState;
    }
  } else {
    // Not initial map.
    state.state = WindowState::WS_NormalState;
  }

  Map();
  SetEWMHState(EWMHWindowState::DemandsAttention, false);
  SetEWMHState(EWMHWindowState::Hidden, false);
  SetAllowedAction(AllowedAction::Minimize, true);
  SetState(state);
  InstallColormaps();
  core_.set_focus_client(*this);

  if (state.state == WindowState::WS_NormalState) {
    // As per ICCCM:
    // Try to give the client's top level window input focus. If the input
    // model is "passive or "local active", the input focus will be given. If
    // the model is "no input" or "globally active" it will not be given.
    // In all cases, if the WM_TAKE_FOCUS protocol is supported by the client
    // (i.e. its model is either locally or globally active) the client will be
    // offered input focus which it then can accept or deny.
    Focus();
    OfferFocus();
  }
  // Withdrawn -> IconicState
  else if (state.state == WindowState::WS_IconicState) {
    Hide();
  }
}

void Client::Hide() {
  std::pair<bool, WindowProperties::WmState> exists_state = GetState();
  auto& state = exists_state.second;
  if (!exists_state.first) {
    vrwm_log(Warning, "Window that did not have WM_STATE got put into iconic "
      " state. Window: " + ToString());
    // Initialize the state so that we have one. This should not happen anyway
    // which is why we don't do any other work here.
    state.icon_window = nullptr;
  }

  if (state.state == WindowState::WS_IconicState) {
    // Already iconic. Nothing to do here!
    return;
  }

  Unmap();
  SetEWMHState(EWMHWindowState::Hidden, true);
  SetAllowedAction(AllowedAction::Minimize, false);
  state.state = WindowState::WS_IconicState;
  SetState(state);

  // As per ICCCM: When the client is in iconic state, the icon window will be
  // mapped (if one exists).
  // The window specified in the icon_window of WM_STATE is set to be the same
  // as the one in WM_HINTS (See Activate()) if it exists.
  if (state.icon_window) {
    state.icon_window->Map();
  } else {
    // TODO: Display pixmap.
  }
}

void Client::GotMapped() {
  // Note: ICCCM states that the window manager regards the top-level window to
  // be in Normal, Iconic or Withdrawn state. Child windows do not have a
  // state. Setting the state is handled in the Activate() function which is
  // called as part of a MapRequest or called directly by VRWM. MapRequests can
  // only be received for a top-level window. Therefore, GotMapped() does not
  // need to update the state to Normal.
  // (Just in case you wonder why the state is not updated in this function)
  UpdatePixmap();

  if (renderables_.empty()) {
    // If there are no renderables, this means that the client was not visible
    // beforehand. We therefore create new renderables.
    bool created_by_plugin = false;
    core_.plugin_manager().ForEach(&plugin::ClientEventReceiver::ClientCreated,
        *this, created_by_plugin);
    if (!created_by_plugin) {
      Renderable::CreateDefaultClientRenderable(core_, this);
    }
  } else {
    // There are renderable(s) observing this client. We tell them to make
    // themselves visible again.
    for (auto renderable : renderables_) {
      renderable->on_client_map(*this);
    }
    core_.plugin_manager().ForEach(&plugin::ClientEventReceiver::ClientMapped,
        *this);
  }
}

void Client::GotMinimized() {
  for (auto it = begin_renderables(); it != end_renderables();) {
    // Because they might detach themselves from us, we use postincrement to
    // ensure the iterator stays defined.
    (*(it++))->on_client_iconic(*this);
  }
  core_.plugin_manager().ForEach(&plugin::ClientEventReceiver::ClientMinimized,
      *this);
}

void Client::GotWithdrawn() {
  for (auto it = begin_renderables(); it != end_renderables();) {
    // Because they might detach themselves from us, we use postincrement to
    // ensure the iterator stays defined.
    (*(it++))->on_client_close(*this);
  }
  core_.plugin_manager().ForEach(&plugin::ClientEventReceiver::ClientClosed,
      *this);

  std::pair<bool, WindowProperties::WmState> exists_state = GetState();
  if (!exists_state.first ||
      exists_state.second.state == WindowState::WS_WithdrawnState) {
    return;
  }
  if (exists_state.second.icon_window) {
    exists_state.second.icon_window->Unmap();
  }
  DeleteProperty(Atoms::WM_STATE);
  DeleteProperty(Atoms::_NET_WM_DESKTOP);
  DeleteProperty(Atoms::_NET_WM_STATE);

  //core_.plugin_manager().ForEach(&plugin::ClientEventReceiver::PreClientClose,
      //*this);
}

void Client::Map() {
  XMapWindow(core_.display(), id_);
}
void Client::MapRaised() {
  XMapRaised(core_.display(), id_);
}
void Client::Unmap() {
  XUnmapWindow(core_.display(), id_);
}

void Client::Close() {
  // Already withdrawn?
  std::pair<bool, WindowProperties::WmState> exists_state = GetState();
  auto& state = exists_state.second;
  if (!exists_state.first || state.state == WindowState::WS_IconicState) {
    return;
  }

  if (GetProtocols().count(Atoms::WM_DELETE_WINDOW)) {
    SendClientMessage(Atoms::WM_DELETE_WINDOW);
  } else {
    XKillClient(core_.display(), id_);
  }
}

void Client::Initialize() {
  if (!rendering_enabled_) {
    return;
  }

  UpdateIcon();

  WindowProperties::AllowedActions allowed_actions;
  auto size_hints = GetSizeHints();
  //TODO: Keep updated
  allowed_actions[(int)AllowedAction::Resize] =
    size_hints->min_width == size_hints->max_width &&
    size_hints->min_height == size_hints->max_height;
  allowed_actions[(int)AllowedAction::MaximizeHorz] = true;
  allowed_actions[(int)AllowedAction::MaximizeVert] = true;
  allowed_actions[(int)AllowedAction::Close] = true;
  SetAllowedActions(allowed_actions);

  UpdateFrameExtents();

  // Window contents are already redirected to pixmap due to a
  // XCompositeRedirectSubwindows() call on the root window.

  CALL_GL(glEnable(GL_TEXTURE_2D));
  texture_->SetParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  texture_->SetParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  //texture_->SetEnvironment(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

  XWindowAttributes attribs;
  XGetWindowAttributes(core_.display(), id_, &attribs);

  //Taken from the usage of
  //https://www.opengl.org/registry/specs/EXT/texture_from_pixmap.txt
  Display* dpy = core_.display(); // local access for easier readiblity
  VisualID visualid = XVisualIDFromVisual(attribs.visual);
  int num_configs;
  GLXFBConfig* fbconfigs = glXGetFBConfigs(dpy,
      DefaultScreen(dpy), &num_configs);
  GLXFBConfig* fb = nullptr;
  for (int i = 0; i < num_configs; ++i) {
    GLXFBConfig* tmp_fb = &(fbconfigs[i]);
    XVisualInfo* visinfo = glXGetVisualFromFBConfig(dpy, *tmp_fb);
    if (!visinfo || visinfo->visualid != visualid) {
      continue;
    }
    int value;
    glXGetFBConfigAttrib(dpy, *tmp_fb, GLX_DRAWABLE_TYPE, &value);
    if (!(value & GLX_PIXMAP_BIT) || !(value & GLX_WINDOW_BIT)) {
      continue;
    }
    glXGetFBConfigAttrib(dpy, *tmp_fb, GLX_BIND_TO_TEXTURE_RGBA_EXT, &value);
    if (!value) {
      glXGetFBConfigAttrib(dpy, *tmp_fb, GLX_BIND_TO_TEXTURE_RGB_EXT, &value);
      if (!value) {
        continue;
      }
      is_transparent_ = false;
      fb = tmp_fb;
      break;
    } else {
      is_transparent_ = true;
      fb = tmp_fb;
      break;
    }
  }
  if (!fb) {
    throw std::runtime_error("Unable to select appropriate frame buffer "
        "configuration for glx pixmap.");
  }
  framebuffer_config_ = fb;

  // Check if the window is already mapped (because this instance of VRWM
  // replaced the previous window manager)
  std::pair<bool, WindowProperties::WmState> exists_state = GetState();
  auto& state = exists_state.second;
  if (exists_state.first && state.state == WindowState::WS_NormalState) {
    GotMapped();
  }
}

void Client::UpdatePixmap() {
  if (!rendering_enabled_ || !framebuffer_config_) {
    return;
  }

  XFlush(core_.display());
  XWindowAttributes attribs;
  XGetWindowAttributes(core_.display(), id_, &attribs);
  if (attribs.map_state != IsViewable) {
    vrwm_logdeb("Not viewable, returning");
    return;
  }

  FreePixmap();
  pixmap_ = XCompositeNameWindowPixmap(core_.display(), id_);

  if (!pixmap_) {
    vrwm_log(Error, "Unable to name window pixmap of " + ToString());
    return;
  }

  texture_->Use();

  int pixmap_attribs[] =
  { GLX_TEXTURE_TARGET_EXT, GLX_TEXTURE_2D_EXT,
    GLX_TEXTURE_FORMAT_EXT, is_transparent_ ? GLX_TEXTURE_FORMAT_RGBA_EXT :
                            GLX_TEXTURE_FORMAT_RGB_EXT,
    //GLX_MIPMAP_TEXTURE_EXT, GL_TRUE,
    None };
  glxpixmap_ = glXCreatePixmap(core_.display(), *framebuffer_config_, pixmap_,
    pixmap_attribs);
  if (!glxpixmap_) {
    throw std::runtime_error("Unable to generate GLX pixmap.");
  }
  glXBindTexImageEXT_func(core_.display(), glxpixmap_, GLX_FRONT_LEFT_EXT,
    NULL);
}

void Client::FreePixmap() {
  if (pixmap_) {
    texture_->Use();
    glXReleaseTexImageEXT_func(core_.display(), glxpixmap_, GLX_FRONT_LEFT_EXT);
    glXDestroyPixmap(core_.display(), glxpixmap_);
    glxpixmap_ = 0;
    XFreePixmap(core_.display(), pixmap_);
    pixmap_ = 0;
  }
}

void Client::Reparent(const Client& newparent) {
  vrwm_log(Info, "Reparenting " + ToString() + " to " + newparent.ToString());
  int newX = newparent.x11_position_.pos.x - x11_position_.pos.x;
  int newY = newparent.x11_position_.pos.y - x11_position_.pos.y;
  XReparentWindow(
    core_.display(),
    id_,
    newparent.id_,
    newX, newY);
}

void Client::UpdateFrameExtents() {
  uint32_t frame_extents[] = {0, 0, 0, 0};
  XChangeProperty(core_.display(), id_, Atoms::_NET_FRAME_EXTENTS,
    XA_CARDINAL, 32, PropModeReplace,
    reinterpret_cast<unsigned char*>(frame_extents), 4);
}

void Client::associate_with_renderable(Renderable* r) {
  if (rendering_enabled_) {
    renderables_.insert(r);
    if (IsInNormalState() || is_override_redirect_) {
      r->on_client_map(*this);
    } else {
      r->on_client_iconic(*this);
    }
  }
}

void Client::InjectKeyEvent(const XKeyEvent& e) {
  XKeyEvent toSend = e;
  toSend.send_event = false;
  toSend.window = id_;
  toSend.subwindow = None;
  toSend.x = last_normalized_pointer_position_.x * x11_position_.size.x;
  toSend.y = last_normalized_pointer_position_.y * x11_position_.size.y;
  toSend.x_root = x11_position_.pos.x + toSend.x;
  toSend.y_root = x11_position_.pos.x + toSend.y;

  XSendEvent(core_.display(), id_, true,
      e.type == KeyPress ? KeyPressMask : KeyReleaseMask,
      reinterpret_cast<XEvent*> (&toSend));
}

void Client::InjectButtonEvent(bool pressed,
    const glm::vec2& normalized_coordinates,
    MouseButton button, const ActiveModifiers& modifiers) {

  last_normalized_pointer_position_ = normalized_coordinates;
  XButtonEvent e;
  e.type = pressed ? ButtonPress : ButtonRelease;
  e.send_event = true;
  e.display = core_.display();
  e.window = id_;
  e.root = XDefaultRootWindow(core_.display());
  e.subwindow = None;
  e.time = core_.event_handler().current_time();
  e.x = normalized_coordinates.x * x11_position_.size.x;
  e.y = normalized_coordinates.y * x11_position_.size.y;
  e.x_root = x11_position_.pos.x + e.x;
  e.y_root = x11_position_.pos.y + e.y;
  e.state = static_cast<unsigned int> (modifiers.to_ulong());
  e.button = static_cast<unsigned int>(button);
  e.same_screen = true;

  // Warp the pointer to the position to fix several issues with GTK
  // applications and because this is also the case in traditional window
  // managers.
  core_.xinput_manager().WarpPointer(e.x_root, e.y_root);
  XSendEvent(core_.display(), id_, false,
      pressed ? ButtonPressMask : ButtonReleaseMask,
      reinterpret_cast<XEvent*> (&e));
}

void Client::InjectCrossingEvent(bool enter,
    const glm::vec2& normalized_pointer_pos) {
  if (enter) {
    last_normalized_pointer_position_ = normalized_pointer_pos;
  }

  XCrossingEvent e;
  e.type = enter ? EnterNotify : LeaveNotify;
  e.send_event = false;
  e.display = core_.display();
  e.window = id_;
  e.root = XDefaultRootWindow(core_.display());
  e.subwindow = None;
  e.time = core_.event_handler().current_time();
  e.x = last_normalized_pointer_position_.x * x11_position_.size.x;
  e.y = last_normalized_pointer_position_.y * x11_position_.size.y;
  e.x_root = x11_position_.pos.x + e.x;
  e.y_root = x11_position_.pos.x + e.y;
  e.mode = NotifyNormal;
  e.detail = NotifyNonlinearVirtual;
  e.same_screen = true;
  e.focus = false;
  e.state = static_cast<unsigned int>(
      core_.event_handler().active_modifiers().to_ulong());

  // Warp the pointer to the position to fix several issues with GTK
  // applications and because this is also the case in traditional window
  // managers.
  core_.xinput_manager().WarpPointer(e.x_root, e.y_root);
  XSendEvent(core_.display(), id_, true,
      enter ? EnterWindowMask : LeaveWindowMask,
      reinterpret_cast<XEvent*> (&e));
}

void Client::InjectPointerMotion(const glm::vec2& normalized_coordinates,
    const ActiveModifiers& modifiers) {
  int old_x, old_y;
  old_x = last_normalized_pointer_position_.x * x11_position_.size.x;
  old_y = last_normalized_pointer_position_.y * x11_position_.size.y;
  int new_x, new_y;
  new_x = normalized_coordinates.x * x11_position_.size.x;
  new_y = normalized_coordinates.y * x11_position_.size.y;

  // Don't send an event when the pixel coordinates of the cursor did not
  // change.
  if (old_x == new_x && old_y == new_y) return;

  XMotionEvent e;
  e.type = MotionNotify;
  e.display = core_.display();
  e.window = id_;
  e.root = XDefaultRootWindow(core_.display());
  e.subwindow = None;
  e.time = core_.event_handler().current_time();
  e.x = new_x;
  e.y = new_y;
  e.x_root = x11_position_.pos.x + new_x;
  e.y_root = x11_position_.pos.y + new_y;
  e.state = static_cast<unsigned int>(modifiers.to_ulong());
  e.is_hint = false;
  e.same_screen = true;

  // Warp the pointer to the position to fix several issues with GTK
  // applications and because this is also the case in traditional window
  // managers.
  core_.xinput_manager().WarpPointer(e.x_root, e.y_root);
  last_normalized_pointer_position_ = normalized_coordinates;

  long mask = 0l;
  // Button1MotionMask is 1 << 8. So we start at mask_shift = 8 search for
  // active modifiers and bitwise or them to the mask.
  for (int mod = ModMouseButton1, mask_shift = 8;
      mod <= ModMouseButton5;
      ++mod, ++mask_shift) {
    if (modifiers[mod]) {
      mask |= 1 << mask_shift;
    }
  }
  // Any modifier set? Then the mask is also ButtonMotionMask
  if (mask != 0l) {
    mask |= ButtonMotionMask;
  }
  // In any case, the mask also contains PointerMotionMask.
  mask |= PointerMotionMask;
  // Send the event!
  XSendEvent(core_.display(), id_, true, mask, reinterpret_cast<XEvent*> (&e));
}

void Client::Focus() {
  core_.set_focus_client(*this);
  auto hints = GetWindowManagerHints();
  if ((hints->flags & InputHint) && !hints->input) {
    return;
  }
  //Raise();
  XSetInputFocus(core_.display(), id_, RevertToParent, CurrentTime);
}

void Client::OfferFocus() {
  std::set<Atom> protocols = GetProtocols();
  if (!protocols.count(Atoms::WM_TAKE_FOCUS)) {
    return;
  }
  SendClientMessage(Atoms::WM_TAKE_FOCUS);
}

void Client::SendClientMessage(Atom message_type, long additional_data[3]) {
  XClientMessageEvent msg;
  msg.type = ClientMessage;
  // most data is set by X server
  msg.send_event = true;
  msg.display = core_.display();
  msg.window = id_;
  msg.message_type = Atoms::WM_PROTOCOLS;
  msg.format = 32;
  msg.data.l[0] = message_type;
  msg.data.l[1] = core_.event_handler().current_time();
  msg.data.l[2] = additional_data[0];
  msg.data.l[3] = additional_data[1];
  msg.data.l[4] = additional_data[2];
  XSendEvent(core_.display(), id_, false, 0, reinterpret_cast<XEvent*>(&msg));
}

bool Client::Resize(unsigned int width, unsigned int height) {
  glm::uvec2 size = CalculateNearestSize(width, height);
  width = size.x;
  height = size.y;

  // We already have a pending resize. Update that one and execute later!
  if (pending_resize_.x != 0 && pending_resize_.y != 0) {
    pending_resize_.x = width;
    pending_resize_.y = height;
    return false;
  }
  // Don't resize if already at that size.
  if (width ==  x11_position_.size.x &&
      height == x11_position_.size.y) {
    return false;
  }
  pending_resize_.x = width;
  pending_resize_.y = height;
  XResizeWindow(core_.display(), id_, width, height);
  return true;
}

glm::uvec2 Client::CalculateNearestSize(unsigned int width,
                                        unsigned int height) const {
  // Clamp the size on screen size to prevent bad stuff.
  std::shared_ptr<XSizeHints> hints = GetSizeHints();
  width = std::min(width, core_.GetScreenSize().x);
  height = std::min(height, core_.GetScreenSize().y);
  if (hints->flags & PMaxSize) {
    width = std::min(width, static_cast<unsigned int>(hints->max_width));
    height = std::min(height, static_cast<unsigned int>(hints->max_height));
  }

  // Respect aspect ratio hints
  if (hints->flags & PAspect) {
    float ar_min = static_cast<float>(hints->min_aspect.x) /
      static_cast<float>(hints->min_aspect.y);
    float ar_max = static_cast<float>(hints->max_aspect.x) /
      static_cast<float>(hints->max_aspect.y);
    float ar;
    // As per ICCCM: Subtract base width and height if supplied before
    // determining the aspect ratio.
    if (hints->flags & PBaseSize) {
      ar = static_cast<float>(width - hints->base_width) /
        static_cast<float>(height - hints->base_height);
    } else {
      ar = static_cast<float>(width) / static_cast<float>(height);
    }
    if (ar < ar_min) {
      width = height * ar_min;
    } else if (ar > ar_max) {
      width = height * ar_max;
    }
  }

  // Respect increment hints
  if ((hints->flags & PResizeInc) && (hints->flags & PBaseSize)) {
    int i = std::abs((width  - hints->base_width)  / hints->width_inc);
    int j = std::abs((height - hints->base_height) / hints->height_inc);
    width =  hints->base_width  + i * hints->width_inc;
    height = hints->base_height + j * hints->height_inc;
  }
  // If aspect ratio is not correct after respecting the increment hints, we
  // will not correct that. Such a client is way to picky about its size.

  // Respect minimum size hints
  if (hints->flags & PMinSize) {
    if (width < static_cast<unsigned int>(hints->min_width)) {
      width = static_cast<unsigned int>(hints->min_width);
    }
    if (height < static_cast<unsigned int>(hints->min_height)) {
      height = static_cast<unsigned int>(hints->min_height);
    }
  }
  // Default minimum size check to ensure the window is always at least 1x1
  if (width <= 0) width = 1;
  if (height <= 0) height = 1;
  return glm::uvec2(width, height);
}

void Client::SizeChanged(unsigned int new_width, unsigned int new_height) {
  UpdatePixmap();
  x11_position_.size.x = new_width;
  x11_position_.size.y = new_height;

  // Because they might detach themselves from us, we use postincrement to
  // ensure the iterator stays defined.
  for (auto it = begin_renderables(); it != end_renderables();) {
    (*(it++))->on_client_resize(*this);
  }
  core_.plugin_manager().ForEach(&plugin::ClientEventReceiver::ClientResized,
      *this, new_width, new_height);

  // If we have a pending resize, execute it!
  if (pending_resize_.x == 0 && pending_resize_.y == 0) {
    return;
  }
  unsigned int pending_x = pending_resize_.x, pending_y = pending_resize_.y;
  pending_resize_ = glm::vec2(0, 0);
  if (pending_x != new_width || pending_y != new_height) {
    Resize(pending_x, pending_y);
  }
  // TODO: update ewmh states to signal if maximize or full screen is possible.
}

std::string Client::ToString() const {
  return "'" + GetName() + "' (" + std::to_string(id_) + ")";
}

void Client::Raise() {
  XRaiseWindow(core_.display(), id_);
}

void Client::UpdateInternalData(Atom property, bool update) {
  try {
    if (property == Atoms::WM_COLORMAP_WINDOWS) {
      InstallColormaps();
    } else if (property == Atoms::_NET_WM_ICON) {
      UpdateIcon();
    }
  } catch(std::exception& e) {
    vrwm_clog(Warning, "Failure updating client data: %s", e.what());
  }
}

std::string Client::QueryStringProperty(Atom property, bool utf8) const {
  Atom type;
  int format;
  unsigned long num_items;
  unsigned long bytes_remaining;
  unsigned char* data;
  XGetWindowProperty(core_.display(), id_, property, 0,
    std::numeric_limits<long>::max(), false,
    utf8 ? Atoms::UTF8_STRING : XA_STRING, &type, &format, &num_items,
    &bytes_remaining, &data);
  if (type == None && format == 0) {
    throw std::runtime_error("Not set!");
  }
  if (type != (utf8 ? Atoms::UTF8_STRING : XA_STRING)) {
    XFree(data);
    throw std::runtime_error("Property type is not string.");
  }
  if (num_items == 0) {
    XFree(data);
    return "";
  }
  // We null terminate the string, just to be on the safe side. If we get a
  // string that is not null terminated, the last character will be missing.
  // The data returned by XGetWindowProperty is not const so we are allowed to
  // change it.
  assert(format == 8);
  data[num_items - 1] = '\0';
  std::string to_return(reinterpret_cast<char*>(data));
  XFree(data);
  return to_return;
}
std::vector<Client*> Client::QueryWindowProperty(Atom property) const {
  Atom type;
  int format;
  unsigned long num_items;
  unsigned long bytes_remaining;
  unsigned char* data_raw;
  XGetWindowProperty(core_.display(), id_, property, 0,
    std::numeric_limits<long>::max(), false, XA_WINDOW, &type, &format,
    &num_items, &bytes_remaining, &data_raw);
  std::vector<Client*> to_return;
  if (type == None && format == 0) {
    return to_return;;
  }
  if (type != XA_WINDOW) {
    XFree(data_raw);
    vrwm_log(Warning, "Type of property '" + Atoms::GetName(property) +
      "' is not 'XA_WINDOW'. It is of type '" + Atoms::GetName(type) + "'.");
    return to_return;
  }
  assert(format == 32);
  if (num_items == 0) {
    XFree(data_raw);
    return to_return;
  }
  uint32_t* data = reinterpret_cast<uint32_t*>(data_raw);
  to_return.resize(num_items);
  for (unsigned long i = 0; i < num_items; ++i) {
    to_return.push_back(core_.GetClient(static_cast<Window>(data[i])));
  }
  XFree(data_raw);
  return to_return;
}
std::vector<long> Client::QueryCardinalProperty(Atom property,
    bool is_signed) const {
  Atom type;
  int format;
  unsigned long num_items;
  unsigned long bytes_remaining;
  unsigned char* data_raw;
  XGetWindowProperty(core_.display(), id_, property, 0,
    std::numeric_limits<long>::max(), false, XA_CARDINAL, &type, &format,
    &num_items, &bytes_remaining, &data_raw);
  std::vector<long> to_return;
  if (type == None && format == 0) {
    return to_return;
  }
  if (type != XA_CARDINAL) {
    XFree(data_raw);
    vrwm_log(Warning, "Type of property '" + Atoms::GetName(property) +
      "' is not 'XA_CARDINAL'. It is of type '" + Atoms::GetName(type) + "'.");
    return to_return;
  }
  if (num_items == 0) {
    XFree(data_raw);
    return to_return;
  }
  to_return.resize(num_items);
  if (format == 8) {
    if (is_signed) {
      int8_t* data = reinterpret_cast<int8_t*>(data_raw);
      to_return.insert(to_return.end(), data, data + num_items);
    } else {
      uint8_t* data = reinterpret_cast<uint8_t*>(data_raw);
      to_return.insert(to_return.end(), data, data + num_items);
    }
  }
  else if (format == 16) {
    if (is_signed) {
      int16_t* data = reinterpret_cast<int16_t*>(data_raw);
      to_return.insert(to_return.end(), data, data + num_items);
    } else {
      uint16_t* data = reinterpret_cast<uint16_t*>(data_raw);
      to_return.insert(to_return.end(), data, data + num_items);
    }
  }
  else if (format == 32) {
    if (is_signed) {
      int32_t* data = reinterpret_cast<int32_t*>(data_raw);
      to_return.insert(to_return.end(), data, data + num_items);
    } else {
      uint32_t* data = reinterpret_cast<uint32_t*>(data_raw);
      to_return.insert(to_return.end(), data, data + num_items);
    }
  }
  XFree(data_raw);
  return to_return;
}
std::vector<Atom> Client::QueryAtomProperty(Atom property) const {
  Atom type;
  int format;
  unsigned long num_items;
  unsigned long bytes_remaining;
  unsigned char* data_raw;
  XGetWindowProperty(core_.display(), id_, property, 0,
    std::numeric_limits<long>::max(), false, XA_ATOM, &type, &format,
    &num_items, &bytes_remaining, &data_raw);
  std::vector<Atom> to_return;
  if (type == None && format == 0) {
    return to_return;;
  }
  if (type != XA_ATOM) {
    XFree(data_raw);
    vrwm_log(Warning, "Type of property '" + Atoms::GetName(property) +
      "' is not 'XA_ATOM'. It is of type '" + Atoms::GetName(type) + "'.");
    return to_return;
  }
  if (format != 32) {
    throw std::runtime_error("Format != 32");
  }
  if (num_items == 0) {
    XFree(data_raw);
    return to_return;
  }
  uint32_t* data = reinterpret_cast<uint32_t*>(data_raw);
  to_return.resize(num_items);
  for (unsigned long i = 0; i < num_items; ++i) {
    to_return.push_back(static_cast<Atom>(data[i]));
  }
  XFree(data_raw);
  return to_return;
}

std::pair<bool, WindowProperties::WmState> Client::GetState() const {
  Atom type;
  int format;
  unsigned long num_items, bytes_remaining;
  unsigned char* raw_data;
  XGetWindowProperty(core_.display(), id_, Atoms::WM_STATE, 0, 2,
      false, Atoms::WM_STATE, &type, &format, &num_items, &bytes_remaining,
      &raw_data);
  WindowProperties::WmState state;
  if (type == None) {
    return std::make_pair(false, state);
  }
  if (type != Atoms::WM_STATE) {
    vrwm_log(Warning, "WM_STATE Property type mismatch: Expected 'WM_STATE', "
      "got " + Atoms::GetName(type));
    return std::make_pair(false, state);
  }
  assert(format == 32 && num_items == 2 && bytes_remaining == 0);
  uint32_t* data = reinterpret_cast<uint32_t*>(raw_data);
  state.state = static_cast<WindowState>(data[0]);
  state.icon_window = core_.GetClient(static_cast<Window>(data[1]));
  return std::make_pair(true, state);
}

void Client::SetState(const WindowProperties::WmState& s) {
  uint32_t data[2] = {
    static_cast<uint32_t>(s.state), s.icon_window ? s.icon_window->ID() : None
  };
  XChangeProperty(core_.display(), id_, Atoms::WM_STATE, Atoms::WM_STATE,
    32, PropModeReplace, reinterpret_cast<unsigned char*>(data), 2);
}

WindowProperties::EWMHWindowStates Client::GetEWMHStates() const {
  return WindowProperties::ToWindowStates(
    QueryAtomProperty(Atoms::_NET_WM_STATE));
}

void Client::SetEWMHState(EWMHWindowState s, bool value) {
  if (s == EWMHWindowState::Size || s == EWMHWindowState::Invalid) {
    return;
  }
  WindowProperties::EWMHWindowStates states = GetEWMHStates();
  WindowProperties::EWMHWindowStates old_states = states;
  int index = static_cast<int>(s);
  if (states[index] == value) {
    return; // Nothing to do here.
  }
  states[index] = value;
  UpdateState(states, old_states);
}
void Client::SetEWMHState(EWMHWindowState s1, EWMHWindowState s2, bool v1,
                          bool v2) {
  WindowProperties::EWMHWindowStates states = GetEWMHStates();
  WindowProperties::EWMHWindowStates old_states = states;
  bool change = false;
  if (s1 != EWMHWindowState::Size && s1 != EWMHWindowState::Invalid) {
    int index1 = static_cast<int>(s1);
    if (states[index1] != v1) {
      change = true;
      states[index1] = v1;
    }
  }
  if (s2 != EWMHWindowState::Size && s2 != EWMHWindowState::Invalid) {
    int index2 = static_cast<int>(s2);
    if (states[index2] != v2) {
      change = true;
      states[index2] = v2;
    }
  }
  if (!change) {
    return;
  }
  UpdateState(states, old_states);
}

void Client::SetEWMHState(const WindowProperties::EWMHWindowStates& states) {
  UpdateState(states, GetEWMHStates());
}
void Client::UpdateState(const WindowProperties::EWMHWindowStates& new_states,
                         const WindowProperties::EWMHWindowStates& old_states) {
  assert(new_states.size() == static_cast<size_t>(EWMHWindowState::Size));
  for (size_t i = 0; i < new_states.size(); ++i) {
    EWMHWindowState s = static_cast<EWMHWindowState>(i);
    if (new_states[i] == old_states[i]) {
      continue;
    }
    bool v = new_states[i];
    // TODO: Actual implementation of stuff here. Some are still missing
    switch(s) {
      case EWMHWindowState::Modal:
        break;
      case EWMHWindowState::MaximizedVert:
        if (v && new_states[(int) EWMHWindowState::MaximizedHorz]) {
          MaximizeHV(old_states);
        } else if (v) {
          MaximizeV(old_states);
        } else {
          Restore(false, true);
        }
        break;
      case EWMHWindowState::MaximizedHorz:
        if (v && new_states[(int) EWMHWindowState::MaximizedVert]) {
          // The above case will always execute first if both are true.
          // We depend on it to prevent race conditions.
          static_assert(EWMHWindowState::MaximizedVert <
            EWMHWindowState::MaximizedHorz, "order mismatch.");
        } else if (v) {
          MaximizeH(old_states);
        } else {
          Restore(true, false);
        }
        break;
      case EWMHWindowState::Shaded:
        break;
      case EWMHWindowState::SkipTaskbar:
        break;
      case EWMHWindowState::SkipPager:
        break;
      case EWMHWindowState::Hidden:
        break;
      case EWMHWindowState::Fullscreen:
        if (v) {
          MakeFullscreen(old_states);
        } else {
          Restore(true, true);
        }
        break;
      case EWMHWindowState::WS_Above:
        break;
      case EWMHWindowState::WS_Below:
        break;
      case EWMHWindowState::DemandsAttention:
        break;
      case EWMHWindowState::Focused:
        break;
      default: break;
    }
  }

  std::vector<Atom> atoms = WindowProperties::ToAtoms(new_states);
  XChangeProperty(core_.display(), id_, Atoms::_NET_WM_STATE, XA_ATOM, 32,
    PropModeReplace, reinterpret_cast<unsigned char*>(atoms.data()),
    atoms.size());
}

void Client::MaximizeV(const WindowProperties::EWMHWindowStates& old_states) {
  if (old_states[static_cast<int>(EWMHWindowState::Fullscreen)]) {
    SetFullscreen(false);
  } else {
    // Note: VRWM does not support moving => No need to restore pos.
    x11_position_before_fullscreen_.size.y = x11_position_.size.y;
  }
  SetAllowedAction(AllowedAction::MaximizeVert, false);
  SetAllowedAction(AllowedAction::Fullscreen, true);
  // Resize will clamp the value.
  Resize(x11_position_.size.x, std::numeric_limits<unsigned int>().max());
}
void Client::MaximizeH(const WindowProperties::EWMHWindowStates& old_states) {
  if (old_states[static_cast<int>(EWMHWindowState::Fullscreen)]) {
    SetFullscreen(false);
  } else {
    // Note: VRWM does not support moving => No need to restore pos.
    x11_position_before_fullscreen_.size.x = x11_position_.size.x;
  }
  SetAllowedAction(AllowedAction::MaximizeHorz, false);
  SetAllowedAction(AllowedAction::Fullscreen, true);
  Resize(std::numeric_limits<unsigned int>().max(), x11_position_.size.y);
}
void Client::MaximizeHV(const WindowProperties::EWMHWindowStates& old_states) {
  if (old_states[static_cast<int>(EWMHWindowState::Fullscreen)]) {
    SetFullscreen(false);
  } else {
    // Note: VRWM does not support moving => No need to restore pos.
    x11_position_before_fullscreen_.size.x = x11_position_.size.x;
    x11_position_before_fullscreen_.size.y = x11_position_.size.y;
  }
  SetAllowedAction(AllowedAction::MaximizeVert, false);
  SetAllowedAction(AllowedAction::MaximizeHorz, false);
  SetAllowedAction(AllowedAction::Fullscreen, true);
  // Resize will clamp the value.
  Resize(std::numeric_limits<unsigned int>().max(),
    std::numeric_limits<unsigned int>().max());
}

void Client::MakeFullscreen(
    const WindowProperties::EWMHWindowStates& old_states) {
  if (old_states[static_cast<int>(EWMHWindowState::MaximizedHorz)]) {
    SetMaximizedH(false);
  } else {
    x11_position_before_fullscreen_.size.x = x11_position_.size.x;
  }
  if (old_states[static_cast<int>(EWMHWindowState::MaximizedVert)]) {
    SetMaximizedV(false);
  } else {
    x11_position_before_fullscreen_.size.y = x11_position_.size.y;
  }
  SetAllowedAction(AllowedAction::MaximizeHorz, true);
  SetAllowedAction(AllowedAction::MaximizeVert, true);
  glm::uvec2 screen = core_.GetScreenSize();
  Resize(screen.x, screen.y);
}
void Client::Restore(bool w, bool h) {
  if (w) {
    SetAllowedAction(AllowedAction::MaximizeHorz, true);
    SetAllowedAction(AllowedAction::Fullscreen, true);
  }
  if (h) {
    SetAllowedAction(AllowedAction::MaximizeVert, true);
    SetAllowedAction(AllowedAction::Fullscreen, true);
  }
  Resize(
    w ? x11_position_before_fullscreen_.size.x : x11_position_.size.x,
    h ? x11_position_before_fullscreen_.size.y : x11_position_.size.y);
}

WindowProperties::AllowedActions Client::GetAllowedActions() const {
  return WindowProperties::ToAllowedActions(
    QueryAtomProperty(Atoms::_NET_WM_ALLOWED_ACTIONS));
}

void Client::SetAllowedAction(AllowedAction a, bool value) {
  if (a == AllowedAction::Size || a == AllowedAction::Invalid) {
    return;
  }
  WindowProperties::AllowedActions actions = GetAllowedActions();
  int index = static_cast<int>(a);
  if (actions[index] == value) {
    return; // Nothing to do here.
  }
  actions[index] = value;
  SetAllowedActions(actions);
}
void Client::SetAllowedActions(
    const WindowProperties::AllowedActions& actions) {
  assert(actions.size() == static_cast<size_t>(AllowedAction::Size));

  std::vector<Atom> atoms = WindowProperties::ToAtoms(actions);
  XChangeProperty(core_.display(), id_, Atoms::_NET_WM_ALLOWED_ACTIONS,
    XA_ATOM, 32, PropModeReplace,
    reinterpret_cast<unsigned char*>(atoms.data()), atoms.size());
}

void Client::DeleteProperty(Atom property) {
  XDeleteProperty(core_.display(), id_, property);
}

std::shared_ptr<XWMHints> Client::GetWindowManagerHints() const {
  XWMHints* hints = XGetWMHints(core_.display(), id_);
  if (!hints) {
    hints = XAllocWMHints();
  }
  return std::shared_ptr<XWMHints>(hints, XFree);
}

std::shared_ptr<XSizeHints> Client::GetSizeHints() const {
  XSizeHints* size_hints = XAllocSizeHints();
  long supplied;
  XGetWMNormalHints(core_.display(), id_, size_hints, &supplied);
  return std::shared_ptr<XSizeHints>(size_hints, XFree);
}

std::string Client::GetName() const {
  try {
    return QueryStringProperty(Atoms::_NET_WM_NAME);
  } catch(...) {
  }

  XTextProperty text_property;
  if (!XGetWMName(core_.display(), id_, &text_property)) {
    return "";
  }
  int num_strings;
  char** strings;
  if (XmbTextPropertyToTextList(core_.display(), &text_property, &strings,
        &num_strings) != Success ||
      num_strings <= 0) {
    throw std::runtime_error("Unable to convert window name text property to "
      " string.");
  }
  std::string name(strings[0]);
  XFreeStringList(strings);
  return name;
}

std::string Client::GetIconName() const {
  try {
    return QueryStringProperty(Atoms::_NET_WM_ICON_NAME);
  } catch(...) {
  }

  XTextProperty text_property;
  if (!XGetWMIconName(core_.display(), id_, &text_property)) {
    return "";
  }
  int num_strings;
  char** strings;
  if (XmbTextPropertyToTextList(core_.display(), &text_property, &strings,
        &num_strings) != Success ||
      num_strings <= 0) {
    throw std::runtime_error("Unable to convert window icon name text property"
      " to string.");
  }
  std::string name(strings[0]);
  XFreeStringList(strings);
  return name;
}

std::string Client::GetApplicationName() const {
  XClassHint class_hint;
  if (!XGetClassHint(core_.display(), id_, &class_hint)) {
    return "";
  }
  std::string to_return = class_hint.res_name;
  XFree(class_hint.res_name);
  XFree(class_hint.res_class);
  return to_return;
}

std::string Client::GetClassName() const {
  XClassHint class_hint;
  if (!XGetClassHint(core_.display(), id_, &class_hint)) {
    return "";
  }
  std::string to_return = class_hint.res_class;
  XFree(class_hint.res_name);
  XFree(class_hint.res_class);
  return to_return;
}

Client* Client::GetTransientFor() const {
  Window win;
  if (!XGetTransientForHint(core_.display(), id_, &win) || !win) {
    return nullptr;
  }
  try {
    return core_.GetClient(win);
  } catch(...) {
  }
  return nullptr;
}

std::set<Atom> Client::GetProtocols() const {
  Atom* atoms;
  int num_atoms;
  if (!XGetWMProtocols(core_.display(), id_, &atoms, &num_atoms)) {
    return std::set<Atom>();
  }
  std::set<Atom> to_return(atoms, atoms + num_atoms);
  XFree(atoms);
  return to_return;
}

std::vector<Window> Client::GetColormapWindows() const {
  Window* windows;
  int num_windows;
  if (!XGetWMColormapWindows(core_.display(), id_, &windows, &num_windows)) {
    return std::vector<Window>();
  }
  std::vector<Window> to_return(windows, windows + num_windows);
  XFree(windows);
  return to_return;
}

WindowType Client::GetWindowType() const {
  Atom type;
  int format;
  unsigned long num_items, bytes_remaining;
  unsigned char* raw_data;
  XGetWindowProperty(core_.display(), id_, Atoms::_NET_WM_WINDOW_TYPE, 0,
    std::numeric_limits<long>().max(), false, XA_ATOM, &type, &format,
    &num_items, &bytes_remaining, &raw_data);
  if (type != None) {
    if (type != XA_ATOM) {
      vrwm_log(Warning, "_NET_WM_WINDOW_TYPE property type mismatch: Expected "
        "'XA_ATOM', got " + Atoms::GetName(type));
    }
    if (is_override_redirect_) {
      return WindowType::Normal;
    } else {
      return GetTransientFor() ? WindowType::Dialog : WindowType::Normal;
    }
  }
  if (format != 32) {
    throw std::runtime_error("Format != 32");
  }
  Atom* data = reinterpret_cast<Atom*>(raw_data);
  WindowType to_return = WindowType::Invalid;
  for (unsigned long i = 0; i < num_items; ++i) {
    to_return = WindowProperties::ToWindowType(data[i]);
    if (to_return != WindowType::Invalid) {
      break;
    }
  }
  if (to_return == WindowType::Invalid) {
    if (is_override_redirect_) {
      to_return = WindowType::Normal;
    } else {
      to_return = GetTransientFor() ? WindowType::Dialog : WindowType::Normal;
    }
  }
  return to_return;
}

void Client::SetName(const std::string& name) {
  XTextProperty title;
  // X requries char* instead of const char*
  char* tmp = new char[name.size() + 1];
  std::strncpy(tmp, name.c_str(), name.size());
  tmp[name.size()] = '\0';

  // _NET_WM_NAME
  XChangeProperty(core_.display(), id_, Atoms::_NET_WM_NAME,
    Atoms::UTF8_STRING, 8, PropModeReplace,
    reinterpret_cast<unsigned char*>(tmp), name.size());

  // WM_NAME
  char* list[1] = { tmp };
  XmbTextListToTextProperty(core_.display(), list, 1, XStringStyle, &title);
  XSetWMName(core_.display(), id_, &title);

  delete[] tmp;
}
void Client::SetIconName(const std::string& name) {
  XTextProperty title;
  // X requries char* instead of const char*
  char* tmp = new char[name.size() + 1];
  std::strncpy(tmp, name.c_str(), name.size());
  tmp[name.size()] = '\0';

  // _NET_WM_ICON_NAME
  XChangeProperty(core_.display(), id_, Atoms::_NET_WM_NAME,
    Atoms::UTF8_STRING, 8, PropModeReplace,
    reinterpret_cast<unsigned char*>(tmp), name.size());

  // WM_ICON_NAME
  char* list[1] = { tmp };
  XmbTextListToTextProperty(core_.display(), list, 1, XStringStyle, &title);
  XSetWMIconName(core_.display(), id_, &title);
  delete[] tmp;
}

XWindowAttributes Client::GetAttributes() {
  XWindowAttributes to_return;
  std::memset(&to_return, 0, sizeof(to_return));
  XGetWindowAttributes(core_.display(), id_, &to_return);
  return to_return;
}

void Client::InstallColormaps() {
  bool this_appeared_in_list = false;
  auto colormap_windows = QueryWindowProperty(Atoms::WM_COLORMAP_WINDOWS);
  for (auto it = colormap_windows.rbegin(); it != colormap_windows.rend();
      ++it) {
    Client* c = *it;
    assert(c);
    if (c == this) {
      this_appeared_in_list = true;
    }
    auto attributes = c->GetAttributes();
    if (attributes.colormap != None) {
      core_.InstallColormap(attributes.colormap);
    }
  }

  if (!this_appeared_in_list) {
    auto attributes = GetAttributes();
    if (attributes.colormap != None) {
      core_.InstallColormap(attributes.colormap);
    }
  }
}

void Client::ChangeDesktop(int new_desktop) {
  XChangeProperty(core_.display(), id_, Atoms::_NET_WM_DESKTOP, XA_CARDINAL,
    32, PropModeReplace, reinterpret_cast<unsigned char*>(&new_desktop), 1);
}

void Client::PrintDetails() const {
  std::shared_ptr<XWMHints> hints = GetWindowManagerHints();
  std::shared_ptr<XSizeHints> size = GetSizeHints();
  Client* transient = GetTransientFor();

  std::set<Atom> protocols = GetProtocols();
  std::ostringstream protocols_str;
  if (protocols.empty()) {
    protocols_str << "No supported protocols.";
  } else {
    for (Atom i : protocols) {
      protocols_str << Atoms::GetName(i) << ", ";
    }
  }
  const glm::ivec2& xpos = x11_position_.pos;
  const glm::uvec2& xsiz = x11_position_.size;

  std::string initial_state = "not set";
  if (hints->flags & StateHint) {
    switch(hints->initial_state) {
      case WithdrawnState: initial_state = "Withdrawn (not valid!!)"; break;
      case NormalState: initial_state = "Normal"; break;
      case IconicState: initial_state = "Iconic"; break;
      default: initial_state = "Unrecognized (not valid!!)"; break;
    }
  }

#define CONVERT_SIZE(flag, x, y) ((size->flags & flag) ? \
    (std::to_string(size->x) + "x" + std::to_string(size->y)) : "not set")
#define CONVERT_ASPECT(flag, x, y) ((size->flags & flag) ? \
    (std::to_string(size->x) + "/" + std::to_string(size->y)) : "not set")
  std::ostringstream msg;
  msg << "Window \t" << id_ << ": " << GetName()
    << "\n\tPos, Size:    {" << xpos.x << "," << xpos.y << "}, " << xsiz.x << "x" << xsiz.y
    << "\n\tTransientFor: " << (transient ? transient->ToString() : "-")
    << "\n\tOverride-redirect? [" << (is_override_redirect_ ? "X" : " ") << "]"
    << "\n\tTransparent?       [" << (is_transparent_ ? "X" : " ") << "]"
    << "\n\tIcon Name:    " << GetIconName()
    << "\n\tApplication:  " << GetApplicationName()
    << "\n\tClass:        " << GetClassName()
    << "\n\tSize Hints:   "
       << "\n\t\tmin:        " << CONVERT_SIZE(PMinSize,    min_width, min_height)
       << "\n\t\tmax:        " << CONVERT_SIZE(PMaxSize,    max_width, max_height)
       << "\n\t\tinc:        " << CONVERT_SIZE(PResizeInc,  width_inc, height_inc)
       << "\n\t\tmin aspect: " << CONVERT_ASPECT(PAspect,   min_aspect.x, min_aspect.y)
       << "\n\t\tmax aspect: " << CONVERT_ASPECT(PAspect,   max_aspect.x, max_aspect.y)
       << "\n\t\tbase:       " << CONVERT_SIZE(PBaseSize,   base_width, base_height)
       << "\n\t\tgravity:    " << ((size->flags & PWinGravity) ? std::to_string(size->win_gravity) : ("not set"))
    << "\n\tWM Hints: (only relevant)"
      << "\n\t\tInput:      " << ((hints->flags & InputHint) ? (hints->input ? "[X]" : "[ ]") : "not set")
      << "\n\t\tInitial:    " << initial_state
      << "\n\t\tIcon Pixmap:" << ((hints->flags & IconPixmapHint) ? std::to_string(hints->icon_pixmap) : "not set")
      << "\n\t\tIcon Window:" << ((hints->flags & IconWindowHint) ? std::to_string(hints->icon_window) : "not set")
      << "\n\t\tGroup:      " << ((hints->flags & WindowGroupHint) ? std::to_string(hints->window_group) : "not set")
    << "\n\tProtocols:    " + protocols_str.str();
#undef CONVERT_SIZE
#undef CONVERT_ASPECT

  vrwm_log(Info, msg.str());
}

void Client::UpdateIcon() {
  Atom type;
  int format;
  unsigned long num_items;
  unsigned long bytes_remaining;
  unsigned char* data_raw;
  XGetWindowProperty(core_.display(), id_, Atoms::_NET_WM_ICON, 0,
    std::numeric_limits<long>::max(), false, XA_CARDINAL, &type, &format,
    &num_items, &bytes_remaining, &data_raw);
  if (type == None || format == 0 || type != XA_CARDINAL || num_items == 0) {
    if (type != None && (type != XA_CARDINAL || num_items == 0)) {
      XFree(data_raw);
    }
    if (type != None && type != XA_CARDINAL) {
      vrwm_log(Warning, "Type of property '_NET_WM_ICON'"
          "' is not 'XA_CARDINAL'. It is of type '" + Atoms::GetName(type) + "'.");
    }
    float texel[] = { 0.0f, 0.0f, 0.0f, 1.0f };
    icon_->SetData(0, 1, 1, texel);
    return;
  }
  uint32_t* data = reinterpret_cast<uint32_t*>(data_raw);
  std::vector<WindowProperties::Icon> icons;
  bool invalid = false;

  // Parse the data to find out how many icons there are. Each icon has:
  // width, height, data[]   where each of them is uint32.
  // This allows us to easily find the next texture.
  unsigned long item_counter = 0; // To ensure we don't surpass num_items
  uint32_t* current = data;
  while (item_counter < num_items) {
    WindowProperties::Icon current_icon;
    current_icon.width = current[0];
    current_icon.height = current[1];
    current_icon.data_ptr =
      reinterpret_cast<WindowProperties::Icon::Color*>(current + 2);
    uint32_t size = current_icon.width * current_icon.height;
    item_counter += 2L + size; // 2 because width and height are also items.
    if (item_counter > num_items) {
      invalid = true;
      break;
    }
    icons.push_back(current_icon);
    current = reinterpret_cast<uint32_t*>(current) + 2 + size;
  }

  if (invalid) {
    vrwm_log(Warning, "_NET_WM_ICON data of " + ToString() + " is invalid!")
    float texel[] = { 0.0f, 0.0f, 0.0f, 1.0f };
    icon_->SetData(0, 1, 1, texel);
    return;
  }

  assert(!icons.empty());

  // Sort decending based on width and height if width is equal
  std::sort(icons.begin(), icons.end(),
    [](const WindowProperties::Icon& lhs, const WindowProperties::Icon& rhs) {
      return (lhs.width == rhs.width) ?
        (lhs.height > rhs.height) :
        (lhs.width  > rhs.width);
  });

  // Only square icons with a power of two are accepted.
  // For mimap levels that don't exist as icon, a mimap will be created by
  // OpenGL.

  std::vector<std::vector<WindowProperties::Icon>::iterator> appropriate_images;

  auto it = icons.begin();
  uint32_t mip = math::LowerPowerOfTwo(icons[0].width);

  while (mip > 1 && it != icons.end()) {
    auto next = it + 1;
    while(next != icons.end() && next->width >= mip) {
      it = next++;
    }
    if (it->width == mip && it->height == mip) {
      appropriate_images.push_back(it);
    }
    it = next;
    mip /= 2;
  }

  if (appropriate_images.empty()) {
    vrwm_log(Warning, "_NET_WM_ICON data of " + ToString() + " does not "
      "contain any square images with power of two.");
    float texel[] = { 0.0f, 0.0f, 0.0f, 1.0f };
    icon_->SetData(0, 1, 1, texel);
    return;
  }

  //icon_->SetParameter(GL_TEXTURE_MAX_LEVEL,
    //static_cast<int>(appropriate_images.size()));
  mip = appropriate_images[0]->width;

  for (size_t i = 0; i < appropriate_images.size(); ++i) {
    WindowProperties::Icon& current = *(appropriate_images[i]);
    uint32_t size = current.width;
    if (size != mip) {
      //TODO: downsample from higher resolution for nicer result.
      continue;
    }
    icon_->SetData(i, size, size, current.data_ptr, GL_UNSIGNED_BYTE, GL_RGBA,
      GL_BGRA);

    // First run? Generate mipmaps from the largest icon to have an appropriate
    // approximation for non-existing icons.
    if (i == 0) {
      icon_->GenerateMipmap();
    }
  }

  icon_->SetParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  icon_->SetParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  XFree(data_raw);
}

void Client::Ping() {
  // Ping not supported?
  if (GetProtocols().count(Atoms::_NET_WM_PING) == 0) {
    return;
  }

  long data[3] = {static_cast<long>(id_), 0L, 0L};
  ping_start_time_ = core_.event_handler().current_time();
  SendClientMessage(Atoms::_NET_WM_PING, data);
}
void Client::PingAnswered() {
  ping_start_time_ = 0;
}
Time Client::GetTimeSincePing() const {
  return ping_start_time_ == 0 ? 0 :
    core_.event_handler().current_time() - ping_start_time_;
}

