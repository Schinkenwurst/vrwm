#include "../include/GLglobal.h"

#include <GL/gl.h>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/matrix.hpp>
#include <glm/gtc/epsilon.hpp>
#include <glm/gtc/quaternion.hpp>

#include "../include/Ray.h"

void (*glXBindTexImageEXT_func)(Display*, GLXDrawable, int, const int*) = nullptr;
void (*glXReleaseTexImageEXT_func)(Display*, GLXDrawable, int) = nullptr;

using namespace vrwm;

std::string GLErrToStr(int err) {
  switch (err) {
    case GL_INVALID_ENUM:
      return "GL_INVALID_ENUM";
    case GL_INVALID_VALUE:
      return "GL_INVALID_VALUE";
    case GL_INVALID_OPERATION:
      return "GL_INVALID_OPERATION";
    case GL_STACK_OVERFLOW:
      return "GL_STACK_OVERFLOW";
    case GL_STACK_UNDERFLOW:
      return "GL_STACK_UNDERFLOW";
    case GL_OUT_OF_MEMORY:
      return "GL_OUT_OF_MEMORY";
    case GL_INVALID_FRAMEBUFFER_OPERATION:
      return "GL_INVALID_FRAMEBUFFER_OPERATION";
    case GL_TABLE_TOO_LARGE:
      return "GL_TABLE_TOO_LARGE";
    default:
      return "unrecognized error";
  }
}

glm::quat glm_enh::rotate(const glm::quat& q, float radians,
    const glm::vec3& axis) {
  glm::vec3 normalized_axis = glm::epsilonEqual(glm::dot(axis, axis),
    1.0f, 0.0001f) ? axis : glm::normalize(axis);
  return glm::angleAxis(radians, normalized_axis) * q;
}
glm::quat glm_enh::rotate(const glm::quat& q, const glm::vec3& axis) {
  const float length = glm::length(axis);
  return glm_enh::rotate(q, length, axis / length);
}

glm::quat glm_enh::FromToRotation(const glm::vec3& from, const glm::vec3& to) {
  // Based on the source code of OGRE, which in turn states that it is based
  // on Stan Melax's article in Game Programming Gems
  constexpr float epsilon = 1e-6;
  glm::quat q;
  float dot = glm::dot(from, to);
  if (glm::abs(dot - 1.0f) < epsilon) {
    q = glm_enh::quat::Identity;
  }
  else if (dot < epsilon - 1.0f) {
    // Pick "random" axis
    glm::vec3 axis = glm::cross(from, glm_enh::vec3::Up);
    // Choose another one, because they are colinear
    if (glm::abs(glm::dot(axis, axis)) < epsilon) {
      axis = glm::cross(from, glm_enh::vec3::Right);
    }
    q = glm::angleAxis(glm::radians(180.0f), axis);
  }
  else {
    float s = glm::sqrt((1 + dot) * 2);
    float inv_s = 1.0f / s;
    glm::vec3 c = glm::cross(from, to);
    q.x = c.x * inv_s;
    q.y = c.y * inv_s;
    q.z = c.z * inv_s;
    q.w = 0.5f * s;
    q = glm::normalize(q);
  }
  return q;
}
glm::quat glm_enh::FromToRotation(const glm::quat& from, const glm::quat& to) {
  return to * glm::inverse(from);
  //return FromToRotation(
      //from * glm_enh::vec3::Forward, to * glm_enh::vec3::Forward);
}

glm::quat glm_enh::LookAt(const glm::vec3& dir, const glm::vec3& desiredUp) {
  // The internet did report some other possible solutions to this problem,
  // however all of them somehow did not work for me.
  // This is why I decided to settle for the matrix LookAt approach.
  float dot = glm::dot(dir, desiredUp);
  const float epsilon = 1e-6f;
  if (glm::abs(dot - 1.0f) < epsilon || dot < epsilon - 1.0f) {
    // direction and up were the exact same or the exact opposite. Prevent
    // division by zero by picking another axis.
    const glm::vec3 Z = -dir;
    // Pick "random" axis. Most likely: desireUp was Up, so we pick right.
    glm::vec3 axis = glm_enh::vec3::Right;
    dot = glm::dot(dir, axis);
    if (glm::abs(dot - 1.0f) < epsilon || dot < epsilon - 1.0f) {
      axis = glm_enh::vec3::Up;
      assert(glm::abs(glm::dot(dir, axis)) < epsilon);
    }
    const glm::vec3 X = glm::normalize(glm::cross(axis, Z));
    const glm::vec3 Y = glm::normalize(glm::cross(Z, X));
    return glm::quat_cast(glm::mat3(X, Y, Z));
  } else {
    const glm::vec3 Z = -dir;
    const glm::vec3 X = glm::normalize(glm::cross(desiredUp, Z));
    const glm::vec3 Y = glm::normalize(glm::cross(Z, X));
    return glm::quat_cast(glm::mat3(X, Y, Z));
  }
}

std::ostream& operator<<(std::ostream& o, const glm::vec2& v) {
  return o << "{" << v[0] << ", " << v[1] << "}";
}
std::ostream& operator<<(std::ostream& o, const glm::vec3& v) {
  return o << "{" << v[0] << ", " << v[1] << ", " << v[2] << "}";
}
std::ostream& operator<<(std::ostream& o, const glm::vec4& v) {
  return o << "{" << v[0] << ", " << v[1] << ", " << v[2] << ", " << v[3] << "}";
}
std::ostream& operator<<(std::ostream& o, const glm::mat4& m) {
  for (size_t row = 0; row < 4; ++row) {
    for (size_t col = 0; col < 4; ++col) {
      o << std::setw(20) << m[col][row];
    }
    o << std::endl;
  }
  return o;
}
std::ostream& operator<<(std::ostream& o, const glm::quat& q) {
  return o << glm::eulerAngles(q);
}

bool math::RaycastPlane(const Ray& r, const glm::vec3& plane_normal,
    float plane_offset, float& out_t) {
  const float denominator = glm::dot(r.direction, plane_normal);
  if (glm::abs(denominator) < 0.0001f) {
    return false;
  }
  out_t = glm::dot(plane_normal * plane_offset - r.origin, plane_normal) /
    denominator;
  if (out_t < 0.0f) {
    return false;
  }
  return true;
}

