#include "../include/GLtextureSwapper.h"

#include <cstring>

#include "../include/GLglobal.h"

#define VRWM_TEXTURE_SWAPPER_VERBOSE 1

using namespace vrwm;

GLtextureSwapper* GLtextureSwapper::g_instance_ = nullptr;

GLtextureSwapper::GLtextureSwapper() {
  CALL_GL(glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS,
    &num_texture_units_));

  // OpenGL guarantees at least 48 texture units.
  assert(num_texture_units_ >= 48);

  vrwm_clog(Info, "Texture unit manager: Available texture units: %i",
    num_texture_units_);

  usage_count_[0].resize(num_texture_units_, 0);
  usage_count_[1].resize(num_texture_units_, 0);

  current_index_ = 0;

  bound_textures_.resize(num_texture_units_, nullptr);
  unused_units_.reserve(num_texture_units_);

  // At the beginning, all units are unused
  for (int i = 0; i < num_texture_units_; ++i) {
    usage_count_[0][i] = usage_count_[1][i] = 0;
    unused_units_.push_back(num_texture_units_ - 1 - i);
  }

  active_unit_ = -1;

  frozen_ = false;
}

GLtextureSwapper::~GLtextureSwapper() {
}

void GLtextureSwapper::Bind(GLtexture& t) {
  int u = t.texture_unit();
  if (u == -1) {
    Fault(t);
  } else {
    assert(u < num_texture_units_);
    ++usage_count_[current_index_][u];
    SetActiveUnit(u);
    TryFreezeUnit(u);
  }
}

void GLtextureSwapper::Unbind(GLtexture& t) {
  int u = t.texture_unit();
  if (u == -1) {
    return;
  }
  assert(unused_units_.size() < (size_t) num_texture_units_);
  unused_units_.push_back(u);
  t.set_texture_unit(-1);
  CALL_GL(glBindTexture(GL_TEXTURE_2D, 0));
}

void GLtextureSwapper::Fault(GLtexture& t) {
  assert(t.texture_unit() == -1);
  int u = -1;
  if (!unused_units_.empty()) {
    u = unused_units_.back();
    unused_units_.pop_back();
#ifdef VRWM_TEXTURE_SWAPPER_VERBOSE
    vrwm_clogdeb("Texture fault of texture %u. Assigning to unused unit: %i",
      t.handle(), u);
#endif
  } else {
    size_t lru_count = std::numeric_limits<size_t>().max();
    for (int i = 0; i < num_texture_units_; ++i) {
      size_t usage = usage_count_[(current_index_ + 1) % 2][i];
      // Frozen?
      if (usage == kFrozen) {
        continue;
      }
      usage += usage_count_[current_index_][i];
      if (usage < lru_count) {
        lru_count = usage;
        u = i;
      }
    }
    // If all textures are frozen, we simply use the first.
    if (u == -1) {
      vrwm_log(Warning, "Unable to determine LRU: All textures are frozen!");
      u = 0;
    }
#ifdef VRWM_TEXTURE_SWAPPER_VERBOSE
    vrwm_clogdeb("Texture fault of texture %u. "
      "Assigning to unit %i with LRU count %u", t.handle(), u, lru_count);
#endif
    assert(u != -1);
    assert(u < num_texture_units_);
    bound_textures_[u]->set_texture_unit(-1);
  }

  assert(u != -1);
  assert(u < num_texture_units_);

  SetActiveUnit(u);
  usage_count_[(current_index_ + 1) % 2][u] = 0;
  usage_count_[current_index_][u] = 1;
  CALL_GL(glBindTexture(GL_TEXTURE_2D, t.handle()));
  t.set_texture_unit(u);
  bound_textures_[u] = &t;
  TryFreezeUnit(u);
}

void GLtextureSwapper::BeginFrame() {
  Thaw();
  current_index_ = (current_index_ + 1) % 2;
  for (auto it = usage_count_[current_index_].begin();
      it != usage_count_[current_index_].end();
      ++it) {
    *it = 0;
  }
}

void GLtextureSwapper::FreezeUnitForever(int unit) {
  GLtexture* to_relocate = bound_textures_[unit];
  if (to_relocate) {
    Unbind(*to_relocate);
  }

  // Remove it from unused units.
  for (auto it = unused_units_.begin(); it != unused_units_.end(); ++it) {
    if (*it == unit) {
      unused_units_.erase(it);
      break;
    }
  }
  // also freeze it to prevent it from being found when checking LRU.
  usage_count_[0][unit] = usage_count_[1][unit] = kFrozen;

  if (to_relocate) {
    Bind(*to_relocate);
    assert(to_relocate->texture_unit() != unit);
  }
}

void GLtextureSwapper::Freeze() {
  Thaw();
  frozen_ = true;
}

void GLtextureSwapper::Thaw() {
  if (!frozen_) {
    return;
  }
  int next_index = (current_index_ + 1) % 2;
  while (!frozen_units_.empty()) {
    auto& old = frozen_units_.back();
    usage_count_[next_index][old.first] = old.second;
    frozen_units_.pop_back();
  }
}

void GLtextureSwapper::TryFreezeUnit(int u) {
  if (!frozen_) {
    return;
  }
  int next_index = (current_index_ + 1) % 2;
  if (usage_count_[next_index][u] == kFrozen) {
    return;
  }
  frozen_units_.push_back(std::make_pair(u, usage_count_[next_index][u]));
  usage_count_[next_index][u] = kFrozen;
}

void GLtextureSwapper::SetActiveUnit(int u) {
  if (active_unit_ != u) {
    active_unit_ = u;
    CALL_GL(glActiveTexture(GL_TEXTURE0 + u));
  }
}

