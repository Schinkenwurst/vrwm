#include "../include/WindowProperties.h"

#include <cassert>

using namespace vrwm;

WindowType WindowProperties::ToWindowType(Atom a) {
  if (a == Atoms::_NET_WM_WINDOW_TYPE_DESKTOP) {
    return WindowType::Desktop;
  } else if (a == Atoms::_NET_WM_WINDOW_TYPE_DOCK) {
    return WindowType::Dock;
  } else if (a == Atoms::_NET_WM_WINDOW_TYPE_TOOLBAR) {
    return WindowType::Toolbar;
  } else if (a == Atoms::_NET_WM_WINDOW_TYPE_MENU) {
    return WindowType::Menu;
  } else if (a == Atoms::_NET_WM_WINDOW_TYPE_UTILITY) {
    return WindowType::Utility;
  } else if (a == Atoms::_NET_WM_WINDOW_TYPE_SPLASH) {
    return WindowType::Splash;
  } else if (a == Atoms::_NET_WM_WINDOW_TYPE_DIALOG) {
    return WindowType::Dialog;
  } else if (a == Atoms::_NET_WM_WINDOW_TYPE_DROPDOWN_MENU) {
    return WindowType::Dropdown;
  } else if (a == Atoms::_NET_WM_WINDOW_TYPE_POPUP_MENU) {
    return WindowType::Popup;
  } else if (a == Atoms::_NET_WM_WINDOW_TYPE_TOOLTIP) {
    return WindowType::Tooltip;
  } else if (a == Atoms::_NET_WM_WINDOW_TYPE_NOTIFICATION) {
    return WindowType::Notification;
  } else if (a == Atoms::_NET_WM_WINDOW_TYPE_COMBO) {
    return WindowType::Combo;
  } else if (a == Atoms::_NET_WM_WINDOW_TYPE_DND) {
    return WindowType::Dnd;
  } else if (a == Atoms::_NET_WM_WINDOW_TYPE_NORMAL) {
    return WindowType::Normal;
  } else {
    return WindowType::Invalid;
  }
}

Atom WindowProperties::ToAtom(WindowType t) {
  switch(t) {
    case WindowType::Desktop:      return Atoms::_NET_WM_WINDOW_TYPE_DESKTOP;
    case WindowType::Dock:         return Atoms::_NET_WM_WINDOW_TYPE_DOCK;
    case WindowType::Toolbar:      return Atoms::_NET_WM_WINDOW_TYPE_TOOLBAR;
    case WindowType::Menu:         return Atoms::_NET_WM_WINDOW_TYPE_MENU;
    case WindowType::Utility:      return Atoms::_NET_WM_WINDOW_TYPE_UTILITY;
    case WindowType::Splash:       return Atoms::_NET_WM_WINDOW_TYPE_SPLASH;
    case WindowType::Dialog:       return Atoms::_NET_WM_WINDOW_TYPE_DIALOG;
    case WindowType::Dropdown:     return Atoms::_NET_WM_WINDOW_TYPE_DROPDOWN_MENU;
    case WindowType::Popup:        return Atoms::_NET_WM_WINDOW_TYPE_POPUP_MENU;
    case WindowType::Tooltip:      return Atoms::_NET_WM_WINDOW_TYPE_TOOLTIP;
    case WindowType::Notification: return Atoms::_NET_WM_WINDOW_TYPE_NOTIFICATION;
    case WindowType::Combo:        return Atoms::_NET_WM_WINDOW_TYPE_COMBO;
    case WindowType::Dnd:          return Atoms::_NET_WM_WINDOW_TYPE_DND;
    case WindowType::Normal:       return Atoms::_NET_WM_WINDOW_TYPE_NORMAL;
    default:                       return None;
  }
}

EWMHWindowState WindowProperties::ToWindowState(Atom a) {
  if (a == Atoms::_NET_WM_STATE_MODAL) {
    return EWMHWindowState::Modal;
  } else if (a == Atoms::_NET_WM_STATE_STICKY) {
    return EWMHWindowState::Sticky;
  } else if (a == Atoms::_NET_WM_STATE_MAXIMIZED_VERT) {
    return EWMHWindowState::MaximizedVert;
  } else if (a == Atoms::_NET_WM_STATE_MAXIMIZED_HORZ) {
    return EWMHWindowState::MaximizedHorz;
  } else if (a == Atoms::_NET_WM_STATE_SHADED) {
    return EWMHWindowState::Shaded;
  } else if (a == Atoms::_NET_WM_STATE_SKIP_TASKBAR) {
    return EWMHWindowState::SkipTaskbar;
  } else if (a == Atoms::_NET_WM_STATE_SKIP_PAGER) {
    return EWMHWindowState::SkipPager;
  } else if (a == Atoms::_NET_WM_STATE_HIDDEN) {
    return EWMHWindowState::Hidden;
  } else if (a == Atoms::_NET_WM_STATE_FULLSCREEN) {
    return EWMHWindowState::Fullscreen;
  } else if (a == Atoms::_NET_WM_STATE_ABOVE) {
    return EWMHWindowState::WS_Above;
  } else if (a == Atoms::_NET_WM_STATE_BELOW) {
    return EWMHWindowState::WS_Below;
  } else if (a == Atoms::_NET_WM_STATE_DEMANDS_ATTENTION) {
    return EWMHWindowState::DemandsAttention;
  } else if (a == Atoms::_NET_WM_STATE_FOCUSED) {
    return EWMHWindowState::Focused;
  } else {
    return EWMHWindowState::Invalid;
  }
}

Atom WindowProperties::ToAtom(EWMHWindowState t) {
  switch(t) {
    case EWMHWindowState::Modal:            return Atoms::_NET_WM_STATE_MODAL;
    case EWMHWindowState::Sticky:           return Atoms::_NET_WM_STATE_STICKY;
    case EWMHWindowState::MaximizedVert:    return Atoms::_NET_WM_STATE_MAXIMIZED_VERT;
    case EWMHWindowState::MaximizedHorz:    return Atoms::_NET_WM_STATE_MAXIMIZED_HORZ;
    case EWMHWindowState::Shaded:           return Atoms::_NET_WM_STATE_SHADED;
    case EWMHWindowState::SkipTaskbar:      return Atoms::_NET_WM_STATE_SKIP_TASKBAR;
    case EWMHWindowState::SkipPager:        return Atoms::_NET_WM_STATE_SKIP_PAGER;
    case EWMHWindowState::Hidden:           return Atoms::_NET_WM_STATE_HIDDEN;
    case EWMHWindowState::Fullscreen:       return Atoms::_NET_WM_STATE_FULLSCREEN;
    case EWMHWindowState::WS_Above:         return Atoms::_NET_WM_STATE_ABOVE;
    case EWMHWindowState::WS_Below:         return Atoms::_NET_WM_STATE_BELOW;
    case EWMHWindowState::DemandsAttention: return Atoms::_NET_WM_STATE_DEMANDS_ATTENTION;
    case EWMHWindowState::Focused:          return Atoms::_NET_WM_STATE_FOCUSED;
    default:                                return None;
  }
}
std::vector<Atom> WindowProperties::ToAtoms(const EWMHWindowStates& states) {
  assert(states.size() == static_cast<size_t>(EWMHWindowState::Size));
  std::vector<Atom> to_return;
  for (size_t i = 0; i < states.size(); ++i) {
    if (states[i]) {
      // ToAtom() will never return None because we only iterate from 0 to
      // EWMHWindowState::Size.
      to_return.push_back(ToAtom(static_cast<EWMHWindowState>(i)));
    }
  }
  return to_return;
}
WindowProperties::EWMHWindowStates WindowProperties::ToWindowStates(
    const std::vector<Atom>& atoms) {
  EWMHWindowStates to_return; //default ctor: all zeroes.
  for (Atom a : atoms) {
    EWMHWindowState state = ToWindowState(a);
    if (state != EWMHWindowState::Invalid) {
      to_return.set(static_cast<int>(state));
    }
  }
  return to_return;
}

AllowedAction WindowProperties::ToAllowedAction(Atom a) {
  if(a == Atoms::_NET_WM_ACTION_MOVE) {
    return AllowedAction::Move;
  } else if(a == Atoms::_NET_WM_ACTION_RESIZE) {
    return AllowedAction::Resize;
  } else if(a == Atoms::_NET_WM_ACTION_MINIMIZE) {
    return AllowedAction::Minimize;
  } else if(a == Atoms::_NET_WM_ACTION_SHADE) {
    return AllowedAction::Shade;
  } else if(a == Atoms::_NET_WM_ACTION_STICK) {
    return AllowedAction::Stick;
  } else if(a == Atoms::_NET_WM_ACTION_MAXIMIZE_HORZ) {
    return AllowedAction::MaximizeHorz;
  } else if(a == Atoms::_NET_WM_ACTION_MAXIMIZE_VERT) {
    return AllowedAction::MaximizeVert;
  } else if(a == Atoms::_NET_WM_ACTION_FULLSCREEN) {
    return AllowedAction::Fullscreen;
  } else if(a == Atoms::_NET_WM_ACTION_CHANGE_DESKTOP) {
    return AllowedAction::ChangeDesktop;
  } else if(a == Atoms::_NET_WM_ACTION_CLOSE) {
    return AllowedAction::Close;
  } else if(a == Atoms::_NET_WM_ACTION_ABOVE) {
    return AllowedAction::AA_Above;
  } else if(a == Atoms::_NET_WM_ACTION_BELOW) {
    return AllowedAction::AA_Below;
  } else {
    return AllowedAction::Invalid;
  }
}

Atom WindowProperties::ToAtom(AllowedAction t) {
  switch(t) {
    case AllowedAction::Move:          return Atoms::_NET_WM_ACTION_MOVE;
    case AllowedAction::Resize:        return Atoms::_NET_WM_ACTION_RESIZE;
    case AllowedAction::Minimize:      return Atoms::_NET_WM_ACTION_MINIMIZE;
    case AllowedAction::Shade:         return Atoms::_NET_WM_ACTION_SHADE;
    case AllowedAction::Stick:         return Atoms::_NET_WM_ACTION_STICK;
    case AllowedAction::MaximizeHorz:  return Atoms::_NET_WM_ACTION_MAXIMIZE_HORZ;
    case AllowedAction::MaximizeVert:  return Atoms::_NET_WM_ACTION_MAXIMIZE_VERT;
    case AllowedAction::Fullscreen:    return Atoms::_NET_WM_ACTION_FULLSCREEN;
    case AllowedAction::ChangeDesktop: return Atoms::_NET_WM_ACTION_CHANGE_DESKTOP;
    case AllowedAction::Close:         return Atoms::_NET_WM_ACTION_CLOSE;
    case AllowedAction::AA_Above:      return Atoms::_NET_WM_ACTION_ABOVE;
    case AllowedAction::AA_Below:      return Atoms::_NET_WM_ACTION_BELOW;
    default:                           return None;
  }
}

std::vector<Atom> WindowProperties::ToAtoms(const AllowedActions& states) {
  assert(states.size() == static_cast<size_t>(AllowedAction::Size));
  std::vector<Atom> to_return;
  for (size_t i = 0; i < states.size(); ++i) {
    if (states[i]) {
      // ToAtom() will never return None because we only iterate from 0 to
      // AllowedAction::Size.
      to_return.push_back(ToAtom(static_cast<AllowedAction>(i)));
    }
  }
  return to_return;
}
WindowProperties::AllowedActions WindowProperties::ToAllowedActions(
    const std::vector<Atom>& atoms) {
  AllowedActions to_return; //default ctor: all zeroes.
  for (Atom a : atoms) {
    AllowedAction state = ToAllowedAction(a);
    if (state != AllowedAction::Invalid) {
      to_return.set(static_cast<int>(state));
    }
  }
  return to_return;
}



