#include "../include/PluginManager.h"

#include <cstring>

#include "../include/Version.h"

using namespace vrwm::plugin;

PluginManager::PluginManager() {
}
PluginManager::~PluginManager() {
  for(auto i : loaded_libraries_) {
    vrwm_log(Info, "Unloading plugin: " + i.first);
    i.second.teardown_function();
    if (dlclose(i.second.handle)) {
      vrwm_log(Warning, "Error closing library: " + std::string(dlerror()));
    }
  }
  loaded_libraries_.clear();
}

const std::string PluginManager::kPluginSubPath = "plugin/";
const std::array<std::string, 2> PluginManager::kPluginExtensions { {
  "", ".so"
} };

void PluginManager::LoadAllPlugins() noexcept {
  for (const std::string& extension : kPluginExtensions) {
    auto plugs = FileTools::AllFilesIn(kPluginSubPath, extension);
    for (const std::string& file : plugs) {
      try {
        LoadPluginByPath(file);
      } catch(...) {
      }
    }
  }
}

void PluginManager::LoadPluginsByPath(const std::string* paths, size_t count)
    noexcept{
  for (size_t i = 0; i < count; ++i) {
    try {
      LoadPluginByPath(paths[i]);
    } catch(...) {
    }
  }
}

void PluginManager::LoadPluginsByFileName(const std::string* file_names,
    size_t count) noexcept {
  for (size_t i = 0; i < count; ++i) {
    try {
      LoadPluginByFileName(file_names[i]);
    } catch(...) {
    }
  }
}

const std::string& PluginManager::LoadPluginByName(const std::string& name) {
  vrwm_log(Info, "Searching for plugin named " + name);
  for (const std::string& extension : kPluginExtensions) {
    auto plugs = FileTools::AllFilesIn(kPluginSubPath, extension);
    for (const std::string& file : plugs) {
      try {
        void* handle = dlopen(file.c_str(), RTLD_LAZY | RTLD_GLOBAL);
        if (handle == nullptr) {
          vrwm_log(Warning, "Error probing plugin '" + file + "': " +
              dlerror());
          continue;
        }
        if (const char* probe_name = *static_cast<const char**>(dlsym(handle,
                "_vrwm_plugin_name"))) {
          if (std::strcmp(probe_name, name.c_str()) == 0) {
            const std::string& loaded_name = LoadPluginByPath(file);
            dlclose(handle);
            return loaded_name;
          }
        } else {
          vrwm_log(Warning, "Unable to find name in shared library: " + file);
        }
        dlclose(handle);
      } catch(...) {
      }
    }
  }
  vrwm_log(Error, "Unable to find plugin named " + name);
  throw std::runtime_error("No such plugin");
}

const std::string& PluginManager::LoadPluginByFileName(
    const std::string& file_name) {
  std::pair<bool, std::string> file;
  for (const std::string& extension : kPluginExtensions) {
    file = FileTools::Find(kPluginSubPath + file_name + extension);
    if (file.first) {
      break;
    }
  }
  if (file.first) {
    return LoadPluginByPath(file.second);
  } else {
    std::ostringstream dirs, files;
    for (const std::string& path : FileTools::kSearchedFolders) {
      dirs << "\t\t" << path + kPluginSubPath << std::endl;
    }
    for (const std::string& extension : kPluginExtensions) {
      files << "\t\t" << file_name + extension << std::endl;
    }
    vrwm_log(Error, "Plugin '" + file_name + "' not found.\n"
        "\tLooked in the following paths: \n" +
        dirs.str() +
        "\tLooked for the following files: \n" +
        files.str());
    throw std::runtime_error("Plugin not found.");
  }
}

const std::string& PluginManager::LoadPluginByPath(const std::string& path) {
  if (!FileTools::FileExists(path)) {
    vrwm_log(Error, "Plugin file '" + path + "' is not a file.");
    throw std::runtime_error("Plugin file is not a file.");
  }
  if (FileTools::IsDirectory(path)) {
    vrwm_log(Error, "Plugin file '" + path + "' is a directory.");
    throw std::runtime_error("Plugin file is a directory.");
  }

  vrwm_log(Info, "Loading plugin: " + path);
  void* handle = dlopen(path.c_str(), RTLD_LAZY | RTLD_GLOBAL);
  if (handle == nullptr) {
    vrwm_log(Error, "Error loading plugin! Error: " + std::string(dlerror()));
    throw std::runtime_error("Error loading plugin.");
  }
  if (char* error = dlerror()) {
    vrwm_log(Warning, "dlopen succeeded, but reported the following error: " +
      std::string(error));
  }

  const char* symbol_names[] = {
    "_vrwm_plugin_name",
    "_vrwm_plugin_description",
    "_vrwm_required_version_major",
    "_vrwm_required_version_minor",
    "_vrwm_plugin_version_major",
    "_vrwm_plugin_version_minor",
    "_vrwm_plugin_setup_function",
    "_vrwm_plugin_teardown_function",
  };
  size_t n_symbols = sizeof(symbol_names) / sizeof(const char*);
  void* symbols[n_symbols];
  for (size_t i = 0; i < n_symbols; ++i) {
    symbols[i] = dlsym(handle, symbol_names[i]);
    if (symbols[i] == nullptr) {
      vrwm_clog(Warning, "Required symbol '%s' not defined. "
        "Have you used the VRWM_PLUGIN macro correctly?  Error: %s",
        symbol_names, dlerror());
      throw std::runtime_error("Required symbol not defined.");
    }
    if (char* error = dlerror()) {
      vrwm_log(Warning, "dlsym succeeded, but reported the following error: " +
          std::string(error));
    }
  }

  // dlysum returns pointer to the symbol. Our symbol is of type const char*
  // therefore, the pointer to the symbol is of type const char**
  std::string name = *static_cast<const char**>(symbols[0]);
  auto insert_result =
    loaded_libraries_.insert(std::make_pair(name, PluginInformation()));
  if (!insert_result.second) {
    vrwm_log(Warning, "Plugin '" + name + "' already loaded.");
    throw std::runtime_error("Plugin already loaded.");
  }

  PluginInformation& info = insert_result.first->second;
  info.handle = handle;
  info.name = std::move(name);
  info.description = *static_cast<const char**>(symbols[1]);
  info.required_version_major = *static_cast<int*>(symbols[2]);
  info.required_version_minor = *static_cast<int*>(symbols[3]);
  info.plugin_version_major = *static_cast<int*>(symbols[4]);
  info.plugin_version_minor = *static_cast<int*>(symbols[5]);
  info.setup_function = *static_cast<void(**)()>(symbols[6]);
  info.teardown_function = *static_cast<void(**)()>(symbols[7]);
  info.file_path = path;

  if (!PluginManager::IsCompatible(info.required_version_major,
        info.required_version_minor)) {
    loaded_libraries_.erase(insert_result.first);
    vrwm_clog(Warning,
        "Plugin '%s' is not compatible with this version of vrwm. "
        "vrwm version: %i.%i. Plugin compiled for version: %i.%i.",
        name.c_str(), VRWM_VERSION_MAJOR, VRWM_VERSION_MINOR,
        info.required_version_major, info.required_version_minor);
    throw std::runtime_error("Plugin version not compatible "
        "with vrwm version.");
  }

  vrwm_log(Info, "Loaded plugin: " + path +
      "\n\tName: " + info.name +
      "\n\tDesc: " + info.description +
      "\n\tVer:  " + std::to_string(info.plugin_version_major) + "." +
        std::to_string(info.plugin_version_minor));
  info.setup_function();
  return info.name;
}

void PluginManager::UnloadPlugin(const std::string& name) noexcept {
  auto plugin = loaded_libraries_.find(name);
  if (plugin != loaded_libraries_.end()) {
    vrwm_log(Info, "Unloading plugin: " + name);
    plugin->second.teardown_function();
    if (dlclose(plugin->second.handle)) {
      vrwm_log(Warning, "Error closing library! Error: " +
        std::string(dlerror()));
    }
    loaded_libraries_.erase(plugin);
  } else {
    vrwm_log(Warning, "Plugin '" + name + "' not loaded.");
  }
}

bool PluginManager::IsCompatible(int required_major, int required_minor) {
  if (required_major > VRWM_PLUGIN_VERSION_MAJOR ||
      (required_major == VRWM_PLUGIN_VERSION_MAJOR &&
        required_minor > VRWM_PLUGIN_VERSION_MINOR)) {
    return false;
  }
  return true;
}

