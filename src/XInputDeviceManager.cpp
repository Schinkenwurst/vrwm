#include "../include/XInputDeviceManager.h"

#include "../include/Atoms.h"
#include "../include/Core.h"

using namespace vrwm;

XInputDeviceManager::XInputDeviceManager(Core& c)
  : core_(c)
  , mouse_device_id_(-1)
  , x_axis_valuator_id_(-1)
  , y_axis_valuator_id_(-1)
{
  // Version query
  int major_version = XI_2_Major, minor_version = XI_2_Minor;
  if(!XIQueryVersion(c.display(), &major_version, &minor_version)) {
    // The server will not return a version higher than the requested version
    if (major_version != XI_2_Major || minor_version != XI_2_Minor) {
      throw std::runtime_error("XInput 2 Version 2.3 not supported by server.");
    }
  }
}

void XInputDeviceManager::Init() {
  //TODO: the main part of this function is actually pretty useless
  vrwm_log(Info, "Searching for the mouse device...");

  // Query all master devices
  // One of those should be the mouse.
  int num_devices;
  XIDeviceInfo* devices = XIQueryDevice(core_.display(), XIAllDevices,
    &num_devices);
  if (!devices || !num_devices) {
    throw std::runtime_error("No input devices found.");
  }

  XIDeviceInfo* mouse_device = nullptr;
  // Look at the details of each master device until we have found the mouse.
  vrwm_logdeb("I've found the following devices:");
  for (int i = 0; i < num_devices; ++i) {
    XIDeviceInfo& crnt = devices[i];
    if (crnt.use == XIMasterPointer) {
      if (!mouse_device) {
        mouse_device = &crnt;
        mouse_device_id_ = crnt.deviceid;
#ifndef VRWM_DEBUG
        // Continue if in debug to print details on other devices.
        return;
#endif
      }
    }
#ifdef VRWM_DEBUG
    // Sometimes it could be useful to know exactly what devices are connected
    // to the machine. Print all of them (until master mouse if not in debug).
    XInputDeviceManager::PrintDeviceInfo(&crnt, Debug);
#endif
  }

  if (!mouse_device) {
    throw std::runtime_error("No master mouse device found.");
  }
  vrwm_log(Info, "Found master pointer called '" +
    std::string(mouse_device->name) + "' with device ID " +
    std::to_string(mouse_device->deviceid) + ". Details: ");
  XInputDeviceManager::PrintDeviceInfo(mouse_device, Info);
  // Retrieve the IDs of the valuators for X and Y axis.
  for (int i = 0; i < mouse_device->num_classes; ++i) {
    if (mouse_device->classes[i]->type == XIValuatorClass) {
      XIValuatorClassInfo* valuator = reinterpret_cast<XIValuatorClassInfo*>(
        mouse_device->classes[i]);
      if (valuator->label == Atoms::REL_X) {
        x_axis_valuator_id_ = valuator->number;
      } else if (valuator->label == Atoms::REL_Y) {
        y_axis_valuator_id_ = valuator->number;
      }
    }
  }
  if (x_axis_valuator_id_ == -1 || y_axis_valuator_id_ == -1) {
    vrwm_log(Error, "Unable to find X and Y axis of the mouse.");
    throw std::runtime_error("Unable to find X and Y axis of the mouse.");
  }

  // We have our master pointer. We'll now select the raw input events.
  uint32_t mask = 0;
  XISetMask(&mask, XI_RawButtonPress);
  XISetMask(&mask, XI_RawButtonRelease);
  XISetMask(&mask, XI_RawMotion);
  vrwm_clogdeb("Expected: 0x%x", mask);
  XIEventMask event_mask = {
    XIAllMasterDevices, sizeof(mask), reinterpret_cast<unsigned char*>(&mask)
  };
  // The XInput documentation specifies that raw events are exclusive to the
  // root window.
  XISelectEvents(core_.display(), core_.GetRootWindow(), &event_mask, 1);
  XFlush(core_.display());
  XFlush(core_.display());

  //Checks for debug
  int num_masks = 0;
  XIEventMask* set_masks = XIGetSelectedEvents(core_.display(),
      core_.GetRootWindow(), &num_masks);
  for (int i = 0; i < num_masks; ++i) {
    vrwm_clogdeb("\tMask set: Device id: %i  mask len: %i",
      set_masks[i].deviceid, set_masks[i].mask_len);
    for (int j = 0; j < set_masks[i].mask_len; j += 4) {
      vrwm_clogdeb("\t\t%x", reinterpret_cast<uint32_t*>(set_masks[i].mask)[j]);
    }
  }

  // Cleanup
  XIFreeDeviceInfo(devices);
}

void XInputDeviceManager::WarpPointer(double dest_x, double dest_y) {
  XIWarpPointer(core_.display(), mouse_device_id_, None, core_.GetRootWindow(),
    0.0, 0.0, 0, 0, dest_x, dest_y);
}

void XInputDeviceManager::PrintDeviceInfo(const XIDeviceInfo* device,
    LogLevel level) {
  const char* device_use = "Unknown";
  switch (device->use) {
    case XIMasterPointer:  device_use = "Master Pointer";    break;
    case XIMasterKeyboard: device_use = "Master Keyboard";   break;
    case XISlavePointer:   device_use = "Slave Pointer";     break;
    case XISlaveKeyboard:  device_use = "Slave Keyboard";    break;
    case XIFloatingSlave:  device_use = "Slave (Floating)";  break;
  }
  vrwm_clogdeb("%i: '%s' \t[%c] Enabled? \tUse: %s", device->deviceid,
    device->name, device->enabled ? 'X' : ' ', device_use);
  if (device->num_classes == 0) {
    vrwm_logdeb("\t---This input device does not have any classes.---");
  }
  for (int i = 0; i < device->num_classes; ++i) {
    switch(device->classes[i]->type) {
      case XIKeyClass: {
          XIKeyClassInfo* crnt_class =
            reinterpret_cast<XIKeyClassInfo*>(device->classes[i]);
          vrwm_clog(level, "\tKeyClass with %i key codes.",
            crnt_class->num_keycodes);
        }
        break;
      case XIButtonClass: {
          XIButtonClassInfo* crnt_class =
            reinterpret_cast<XIButtonClassInfo*>(device->classes[i]);
          vrwm_clog(level, "\tButtonClass with %i buttons:",
            crnt_class->num_buttons);
          for (int j = 0; j < crnt_class->num_buttons; ++j) {
            vrwm_log(level, "\t\t" + std::to_string(j) + ": " +
              Atoms::GetName(crnt_class->labels[j]) + " (" +
              std::to_string(crnt_class->labels[j]) + ")");
          }
        }
        break;
      case XIValuatorClass: {
          XIValuatorClassInfo* crnt_class =
            reinterpret_cast<XIValuatorClassInfo*>(device->classes[i]);
          std::string axis_name = Atoms::GetName(crnt_class->label);
          vrwm_clog(level, "\tValuatorClass\n"
            "\t\tAxis:       '%s' (%i)\n"
            "\t\tRange:      [%f, %f]\n"
            "\t\tResolution: %i counts/meter\n"
            "\t\tMode:       %s\n"
            "\t\tLastVal:    %f   (only in absolute mode)",
            axis_name.c_str(), crnt_class->number,
            crnt_class->min, crnt_class->max, crnt_class->resolution,
            (crnt_class->mode == XIModeRelative) ? "Relative" : "Absolute",
            crnt_class->value);
        }
        break;
      case XIScrollClass: {
          XIScrollClassInfo* crnt_class =
            reinterpret_cast<XIScrollClassInfo*>(device->classes[i]);
          bool no_emulation_flag = crnt_class->flags & XIScrollFlagNoEmulation;
          bool preferred_flag = crnt_class->flags & XIScrollFlagPreferred;
          vrwm_clog(level, "\tScrollClass:\n"
            "\t\tAxis:      %i (see ValuatorClass for name)\n"
            "\t\tDirection: %s\n"
            "\t\tIncrement: %f"
            "\t\tFlags:     [%c] NoEmulation, [%c] Preferred\n",
            crnt_class->number,
            (crnt_class->scroll_type == XIScrollTypeVertical) ? "Vertical" :
              "Horizontal",
            crnt_class->increment, no_emulation_flag ? 'X' : ' ',
            preferred_flag ? 'X' : ' ');
        }
        break;
      case XITouchClass: {
          XITouchClassInfo* crnt_class =
            reinterpret_cast<XITouchClassInfo*>(device->classes[i]);
          vrwm_clog(level, "\tTouchClass with mode '%s' and max. %i simultaneous "
            "touch points.",
            (crnt_class->mode == XIDirectTouch) ? "Direct" : "Dependent",
            crnt_class->num_touches);
        }
        break;
      default:
        vrwm_log(level, "\tUnknown class");
        break;
    }
  }
}

